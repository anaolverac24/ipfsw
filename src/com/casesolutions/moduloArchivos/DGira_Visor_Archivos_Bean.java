/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.casesolutions.moduloArchivos;

import java.io.Serializable;
import java.sql.SQLException;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;

/**
 * Controlador de la vista de carga de archivos.
 * 
 * @author Mauricio A. Solis Beltran
 */
@ManagedBean(name = "dgiraVisorArchivos")
@ViewScoped
public class DGira_Visor_Archivos_Bean implements Serializable {

    private static final long serialVersionUID = 1000000000140099999L;
    private static final Logger LOG = Logger.getLogger(DGira_Visor_Archivos_Bean.class.getName());
    private List<DGira_Archivo_Adjunto_Model> archivosAnexos;

    /**
     * Inicializa el controlador
     * 
     * @param seccionId
     * @param capituloId
     * @param subCapituloId
     * @param folio
     * @param serial
     */
    public void init(int seccionId, int capituloId, int subCapituloId, String folio, String serial) {
        LOG.log(Level.INFO, "Parametros: {0}", seccionId);
        try {
            archivosAnexos = DGira_Modulo_Archivos_DAO.consultaArchivosAdjuntos(seccionId, capituloId, subCapituloId, folio, serial);
        } catch (SQLException sqe) {
            LOG.severe(sqe.getMessage());
        }
    }

    /**
     * @return the archivosAnexos
     */
    public List<DGira_Archivo_Adjunto_Model> getArchivosAnexos() {
        return archivosAnexos;
    }

    /**
     * @param archivosAnexos the archivosAnexos to set
     */
    public void setArchivosAnexos(List<DGira_Archivo_Adjunto_Model> archivosAnexos) {
        this.archivosAnexos = archivosAnexos;
    }

}
