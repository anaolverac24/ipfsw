/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.casesolutions.moduloArchivos;

import java.awt.Graphics2D;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.Serializable;
import java.nio.file.Files;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.UUID;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.validator.ValidatorException;
import javax.imageio.ImageIO;
import javax.servlet.http.Part;

/**
 * Bean para la carga de archivos
 *
 * @author Mauricio Solis Beltran.
 * @version 1
 */
@ManagedBean(name = "dgiraCargaArchivos")
@ViewScoped
public class DGira_Carga_Archivos_Bean implements Serializable {

    private static final Logger LOG = Logger.getLogger(DGira_Carga_Archivos_Bean.class.getName());
    private static final long serialVersionUID = 1000000000120099999L;
    private static final long MB = 1048576;
    private static final long KB = 1024;

    private List<DGira_Archivo_Adjunto_Model> archivosAnexos;

    /**
     * Crea una nueva instancia
     */
    public DGira_Carga_Archivos_Bean() {
        archivosAnexos = new LinkedList<DGira_Archivo_Adjunto_Model>();
    }

    /**
     * Manda a guardar cada uno de los elementos de la lista
     */
    public void guardarArchivosAnexos(int seccionId, int capituloId, int subCapituloId, String folio, String serial) {

        try {
            for (DGira_Archivo_Adjunto_Model archivo : archivosAnexos) {
                LOG.log(Level.INFO, "Guardando {0}", archivo.getId());
                archivo.setCapituloId(capituloId);
                archivo.setSeccionId(seccionId);
                archivo.setSubCapituloId(subCapituloId);
                archivo.setFolioProyecto(folio);
                archivo.setSerialProyecto(serial);
                guardaArchivo(archivo);
            }
        } catch (IOException ioe) {
            LOG.log(Level.SEVERE, ioe.getMessage());
        } catch (SQLException sqe) {
            LOG.log(Level.SEVERE, sqe.getMessage());
        }
    }

    /**
     * Guarda un archivo en la base de datos y en el disco duro
     *
     * @param archivo
     * @throws IOException
     */
    private void guardaArchivo(DGira_Archivo_Adjunto_Model archivo) throws IOException, SQLException {

        //Almacenar archivo en hd
        String path = DGira_Modulo_Archivos_DAO.getConfigProperties().getProperty("dgira_path");
        String pathCache = DGira_Modulo_Archivos_DAO.getConfigProperties().getProperty("dgira_path_cache");
        String uname = UUID.randomUUID().toString();
        InputStream in = archivo.getFile().getInputStream();
        File tmpFile = new File(path + uname);
        Files.copy(in, tmpFile.toPath());
        archivo.setUrlArchivo(tmpFile.toPath().toString());
        String[] nomArch = extractFileName(archivo.getFile());
        archivo.setNombreArchivo(nomArch[1]);
        archivo.setExtensionArchivo(nomArch[0]);

        // Crear cache
        if (tmpFile.length() > (512 * KB)) {
            String pathCacheImg = pathCache + UUID.randomUUID().toString();
            String ext = archivo.getExtensionArchivo().toLowerCase();
            if (ext.equals("jpg") || ext.equals("png") || ext.equals("gif") || ext.equals("jpeg")) {
                BufferedImage imgTmp = ImageIO.read(new File(archivo.getUrlArchivo()));
                if (imgTmp.getHeight() > 640) {
                    float factor = 640 / (float)imgTmp.getHeight();
                    float imgWidth = (float) (imgTmp.getWidth() * factor);
                    float imgHeigth = (float) (imgTmp.getHeight() * factor);
                 
                    System.out.println("factor " + factor);
                    System.out.println("width " + imgTmp.getWidth());
                    System.out.println("heigth " + imgTmp.getHeight());
                    
                    BufferedImage imgTmp2 = new BufferedImage((int)imgWidth, (int)imgHeigth, BufferedImage.TYPE_INT_ARGB);
                    Graphics2D g = imgTmp2.createGraphics();
                    g.drawImage(imgTmp, 0, 0, (int)imgWidth, (int)imgHeigth, null);
                    g.dispose();
                    ImageIO.write(imgTmp2, "png", new File(pathCacheImg));
                    archivo.setCacheArchivo(pathCacheImg);
                }
            }

            if (ext.equals("dwg")) {
                // TODO
            }

            archivo.setCacheArchivo(pathCacheImg);
        } else {
            // Solo se crea cache para archivos grandes
            archivo.setCacheArchivo("");
        }

        //Almacenar el registro en la bd
        DGira_Modulo_Archivos_DAO.registraArchivoAdjunto(archivo);

        //Actualizar estado
        archivo.setEstado(true);
    }

    /**
     * Extrea el nombre y extension del mensaje http
     *
     * @param part
     * @return
     */
    private String[] extractFileName(Part part) {
        String fname[] = new String[2];
        String contentDisp = part.getHeader("content-disposition");
        String[] items = contentDisp.split(";");
        String out = "";
        for (String s : items) {
            if (s.trim().startsWith("filename")) {
                out = s.substring(s.indexOf("=") + 2, s.length() - 1);
            }
        }

        String[] tmp = out.split("\\.");
        if (tmp.length > 0) {
            fname[0] = tmp[tmp.length - 1];
            String file = "";
            for (int i = 0; i < tmp.length - 1; i++) {
                file += tmp[i] + ".";
            }
            file = file.substring(0, file.length() - 1);
            fname[1] = file;
        }

        return fname;
    }

    /**
     * Agregar archivo a la lista
     */
    public void agregarArchivo() {
        DGira_Archivo_Adjunto_Model archivo = new DGira_Archivo_Adjunto_Model(UUID.randomUUID().toString());
        archivosAnexos.add(archivo);
    }

    /**
     * Quitar el archivo de la lista
     *
     * @param id, id del archivo en la lista
     */
    public void quitarArchivo(String id) {
        DGira_Archivo_Adjunto_Model archivo = new DGira_Archivo_Adjunto_Model(id);
        archivosAnexos.remove(archivo);
    }

    /**
     * Valida que el archivo cumpla con el tamaño y extensiones establecidas
     *
     * @param ctx
     * @param comp
     * @param value
     */
    public void validataArchivo(FacesContext ctx, UIComponent comp, Object value) {
        List<FacesMessage> msgs = new ArrayList<FacesMessage>();
        Part file = (Part) value;

        if (file.getSize() > Integer.parseInt(DGira_Modulo_Archivos_DAO.getConfigProperties().getProperty("dgira_tam_max"))) {
            msgs.add(new FacesMessage("El archivo excede el tamaño permitido."));
        }

        if (!msgs.isEmpty()) {
            throw new ValidatorException(msgs);
        }
    }

    /**
     * @return the archivosAnexos
     */
    public List<DGira_Archivo_Adjunto_Model> getArchivosAnexos() {
        return archivosAnexos;
    }

    /**
     * @param archivosAnexos the archivosAnexos to set
     */
    public void setArchivosAnexos(List<DGira_Archivo_Adjunto_Model> archivosAnexos) {
        this.archivosAnexos = archivosAnexos;
    }

}
