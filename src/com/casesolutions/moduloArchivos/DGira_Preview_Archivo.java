/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.casesolutions.moduloArchivos;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.sql.SQLException;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author mauricio
 */
public class DGira_Preview_Archivo extends HttpServlet {

    private static final Logger LOG = Logger.getLogger(DGira_Preview_Archivo.class.getName());

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        int id = 0;
        DGira_Archivo_Adjunto_Model arcInfo = null;

        try {
            id = Integer.parseInt(request.getParameter("id"));
            arcInfo = DGira_Modulo_Archivos_DAO.consultaArchivosAdjuntos(id);
        } catch (NumberFormatException nfe) {

        } catch (SQLException sqe) {

        }

        String filePath;
        if (arcInfo.getCacheArchivo() == null || arcInfo.getCacheArchivo().isEmpty()) {
            LOG.info("Imagen desde ubicacion original");
            filePath = arcInfo.getUrlArchivo();
        } else {
            LOG.info("Imagen desde cache");
            filePath = arcInfo.getCacheArchivo();
        }

        File downloadFile = new File(filePath);
        FileInputStream inStream = new FileInputStream(downloadFile);

        String ext = arcInfo.getExtensionArchivo().toLowerCase();
        String mimeType = "application/octet";

        if (ext.equals("jpg") || ext.equals("png") || ext.equals("gif")) {
            mimeType = "image/" + ext;
        }
        if (ext.equals("pdf")) {
            mimeType = "application/pdf";
        }

        System.out.println("MIME type: " + mimeType);

        // modifies response
        response.setContentType(mimeType);
        response.setContentLength((int) downloadFile.length());

        OutputStream outStream = response.getOutputStream();

        byte[] buffer = new byte[4096];
        int bytesRead = -1;

        while ((bytesRead = inStream.read(buffer)) != -1) {
            outStream.write(buffer, 0, bytesRead);
        }

        inStream.close();
        outStream.close();
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
