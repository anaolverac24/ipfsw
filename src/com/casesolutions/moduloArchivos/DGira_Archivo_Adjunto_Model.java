/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.casesolutions.moduloArchivos;

import java.io.Serializable;
import java.util.Objects;
import javax.servlet.http.Part;

/**
 *
 * @author mauricio
 */
public class DGira_Archivo_Adjunto_Model implements Serializable {

    private static final long serialVersionUID = 1000000000110099999L;
    private String id;
    private Part file;
    private String msg;
    private String descripcion;
    private int seccionId;
    private int capituloId;
    private int subCapituloId;
    private String folioProyecto;
    private String serialProyecto;
    private boolean estado;
    private String nombreArchivo;
    private String extensionArchivo;
    private String urlArchivo;
    private String cacheArchivo;

    
    @Override
    public int hashCode() {
        int hash = 7;
        hash = 59 * hash + Objects.hashCode(this.id);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final DGira_Archivo_Adjunto_Model other = (DGira_Archivo_Adjunto_Model) obj;
        if (!Objects.equals(this.id, other.id)) {
            return false;
        }
        return true;
    }

    /**
     * Constructor vacio
     */
    public DGira_Archivo_Adjunto_Model() {

    }

    /**
     * Constructor con id
     *
     * @param id, id unico del objeto (utilizado para la eliminacion en la
     * lista)
     */
    public DGira_Archivo_Adjunto_Model(String id) {
        this.id = id;
    }

    /**
     * @return the id
     */
    public String getId() {
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(String id) {
        this.id = id;
    }

    /**
     * @return the file
     */
    public Part getFile() {
        return file;
    }

    /**
     * @param file the file to set
     */
    public void setFile(Part file) {
        this.file = file;
    }

    /**
     * @return the msg
     */
    public String getMsg() {
        return msg;
    }

    /**
     * @param msg the msg to set
     */
    public void setMsg(String msg) {
        this.msg = msg;
    }

    /**
     * @return the descripcion
     */
    public String getDescripcion() {
        return descripcion;
    }

    /**
     * @param descripcion the descripcion to set
     */
    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    /**
     * @return the seccionId
     */
    public int getSeccionId() {
        return seccionId;
    }

    /**
     * @param seccionId the seccionId to set
     */
    public void setSeccionId(int seccionId) {
        this.seccionId = seccionId;
    }

    /**
     * @return the capituloId
     */
    public int getCapituloId() {
        return capituloId;
    }

    /**
     * @param capituloId the capituloId to set
     */
    public void setCapituloId(int capituloId) {
        this.capituloId = capituloId;
    }

    /**
     * @return the subCapituloId
     */
    public int getSubCapituloId() {
        return subCapituloId;
    }

    /**
     * @param subCapituloId the subCapituloId to set
     */
    public void setSubCapituloId(int subCapituloId) {
        this.subCapituloId = subCapituloId;
    }

    /**
     * @return the folioProyecto
     */
    public String getFolioProyecto() {
        return folioProyecto;
    }

    /**
     * @param folioProyecto the folioProyecto to set
     */
    public void setFolioProyecto(String folioProyecto) {
        this.folioProyecto = folioProyecto;
    }

    /**
     * @return the serialProyecto
     */
    public String getSerialProyecto() {
        return serialProyecto;
    }

    /**
     * @param serialProyecto the serialProyecto to set
     */
    public void setSerialProyecto(String serialProyecto) {
        this.serialProyecto = serialProyecto;
    }

    /**
     * @return the seleccionado
     */
    public boolean isEstado() {
        return estado;
    }

    /**
     * @param seleccionado the seleccionado to set
     */
    public void setEstado(boolean estado) {
        this.estado = estado;
    }

    /**
     * @return the nombreArchivo
     */
    public String getNombreArchivo() {
        return nombreArchivo;
    }

    /**
     * @param nombreArchivo the nombreArchivo to set
     */
    public void setNombreArchivo(String nombreArchivo) {
        this.nombreArchivo = nombreArchivo;
    }

    /**
     * @return the extensionArchivo
     */
    public String getExtensionArchivo() {
        return extensionArchivo;
    }

    /**
     * @param extensionArchivo the extensionArchivo to set
     */
    public void setExtensionArchivo(String extensionArchivo) {
        this.extensionArchivo = extensionArchivo;
    }

    /**
     * @return the urlArchivo
     */
    public String getUrlArchivo() {
        return urlArchivo;
    }

    /**
     * @param urlArchivo the urlArchivo to set
     */
    public void setUrlArchivo(String urlArchivo) {
        this.urlArchivo = urlArchivo;
    }

    /**
     * @return the cacheArchivo
     */
    public String getCacheArchivo() {
        return cacheArchivo;
    }

    /**
     * @param cacheArchivo the cacheArchivo to set
     */
    public void setCacheArchivo(String cacheArchivo) {
        this.cacheArchivo = cacheArchivo;
    }

}
