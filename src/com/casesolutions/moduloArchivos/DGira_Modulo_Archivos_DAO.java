/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.casesolutions.moduloArchivos;

import java.io.IOException;
import java.io.Serializable;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.LinkedList;
import java.util.List;
import java.util.Properties;
import java.util.logging.Logger;
import oracle.jdbc.pool.OracleDataSource;

/**
 *
 * @author mauricio
 */
public class DGira_Modulo_Archivos_DAO implements Serializable {

    private static final Logger LOG = Logger.getLogger(DGira_Modulo_Archivos_DAO.class.getName());
    private static final long serialVersionUID = 1000000000130099999L;
    private static OracleDataSource dataSource = null;
    private static Properties configProperties;

    static {
        try {
            configProperties = new Properties();
            configProperties.loadFromXML(DGira_Modulo_Archivos_DAO.class.getResourceAsStream("/com/casesolutions/moduloArchivos/Config.xml"));
            dataSource = new OracleDataSource();
            dataSource.setURL("jdbc:oracle:thin:@//"
                    + configProperties.getProperty("dgira_bd_host") + ":"
                    + configProperties.getProperty("dgira_bd_port") + "/"
                    + configProperties.getProperty("dgira_bd_sid"));
            dataSource.setUser(configProperties.getProperty("dgira_bd_usu"));
            dataSource.setPassword(configProperties.getProperty("dgira_bd_pas"));
            dataSource.setPortNumber(Integer.parseInt(configProperties.getProperty("dgira_bd_port")));
        } catch (Exception exception) {
            LOG.severe(exception.getLocalizedMessage());
            exception.printStackTrace();
        }
    }

    public static Properties getConfigProperties() {
        return configProperties;
    }

    public static void registraArchivoAdjunto(DGira_Archivo_Adjunto_Model a) throws SQLException {
        Connection con = dataSource.getConnection();
        PreparedStatement pst = con.prepareCall(configProperties.getProperty("dgira_sql_insarchivo"));
        pst.setInt(1, a.getSeccionId());
        pst.setInt(2, a.getCapituloId());
        pst.setInt(3, a.getSubCapituloId());
        pst.setString(4, a.getFolioProyecto());
        pst.setString(5, a.getSerialProyecto());
        pst.setString(6, a.getDescripcion());
        pst.setString(7, a.getNombreArchivo());
        pst.setString(8, a.getUrlArchivo());
        pst.setString(9, a.getExtensionArchivo());
        pst.setString(10, a.getCacheArchivo());
        pst.executeUpdate();
        pst.close();
        con.close();
    }

    public static DGira_Archivo_Adjunto_Model consultaArchivosAdjuntos(int id) throws SQLException{
        DGira_Archivo_Adjunto_Model tmp  =null;
 
        Connection con = dataSource.getConnection();
        PreparedStatement pst = con.prepareCall(configProperties.getProperty("dgira_sql_selarchivo"));
        pst.setInt(1, id);
        ResultSet rst = pst.executeQuery();
        if (rst.next()) {
            tmp = new DGira_Archivo_Adjunto_Model();
            tmp.setId("" + rst.getInt("ANEXO_ID"));
            tmp.setSeccionId(rst.getInt("SECCION_ID"));
            tmp.setCapituloId(rst.getInt("CAPITULO_ID"));
            tmp.setSubCapituloId(rst.getInt("SUBCAPITULO_ID"));
            tmp.setFolioProyecto(rst.getString("FOLIO_PROYECTO"));
            tmp.setSerialProyecto(rst.getString("SERIAL_PROYECTO"));
            tmp.setDescripcion(rst.getString("ANEXO_DESCRIPCION"));
            tmp.setNombreArchivo(rst.getString("ANEXO_NOMBRE"));
            tmp.setUrlArchivo(rst.getString("ANEXO_URL"));
            tmp.setExtensionArchivo(rst.getString("ANEXO_EXTENSION"));
            tmp.setCacheArchivo(rst.getString("ANEXO_CACHE"));
        }
        rst.close();
        pst.close();
        con.close();

        return tmp;
    }

    public static List<DGira_Archivo_Adjunto_Model> consultaArchivosAdjuntos(
            int seccionId, int capituloId, int subCapituloId, String folio, String serial) throws SQLException {
        List<DGira_Archivo_Adjunto_Model> archivos = new LinkedList<DGira_Archivo_Adjunto_Model>();
        Connection con = dataSource.getConnection();
        PreparedStatement pst = con.prepareCall(configProperties.getProperty("dgira_sql_selarchivos"));
        pst.setInt(1, seccionId);
        pst.setInt(2, capituloId);
        pst.setInt(3, subCapituloId);
        pst.setString(4, folio);
        pst.setString(5, serial);
        ResultSet rst = pst.executeQuery();
        while (rst.next()) {
            DGira_Archivo_Adjunto_Model tmp = new DGira_Archivo_Adjunto_Model();
            tmp.setId("" + rst.getInt("ANEXO_ID"));
            tmp.setSeccionId(rst.getInt("SECCION_ID"));
            tmp.setCapituloId(rst.getInt("CAPITULO_ID"));
            tmp.setSubCapituloId(rst.getInt("SUBCAPITULO_ID"));
            tmp.setFolioProyecto(rst.getString("FOLIO_PROYECTO"));
            tmp.setSerialProyecto(rst.getString("SERIAL_PROYECTO"));
            tmp.setDescripcion(rst.getString("ANEXO_DESCRIPCION"));
            tmp.setNombreArchivo(rst.getString("ANEXO_NOMBRE"));
            tmp.setUrlArchivo(rst.getString("ANEXO_URL"));
            tmp.setExtensionArchivo(rst.getString("ANEXO_EXTENSION"));
            archivos.add(tmp);
        }
        rst.close();
        pst.close();
        con.close();

        return archivos;
    }
}
