/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package InformePreventivo.SINATEC;

import java.io.Serializable;

/**
 *
 * @author Case Solutions 10
 */
public class Dgira_Datos_SinatecRL_Model implements Serializable{
    private int tamiteID;
    private String RFC;
    private String nombreRL;
    private String apPatRL;
    private String apMatRL;
    private String CURP;

    
    public Dgira_Datos_SinatecRL_Model()
    {
    }
    
    public Dgira_Datos_SinatecRL_Model(int tamiteID, String RFC, String nombreRL, String apPatRL,String apMatRL, String CURP)
    {
        this.tamiteID = tamiteID;
        this.RFC = RFC;
        this.nombreRL = nombreRL;
        this.apPatRL = apPatRL;
        this.apPatRL = apMatRL;
        this.CURP = CURP;
    }
    
    /**
     * @return the tamiteID
     */
    public int getTamiteID() {
        return tamiteID;
    }

    /**
     * @param tamiteID the tamiteID to set
     */
    public void setTamiteID(int tamiteID) {
        this.tamiteID = tamiteID;
    }
    
    /**
     * @return the RFC
     */
    public String getRFC() {
        return RFC;
    }

    /**
     * @param RFC the RFC to set
     */
    public void setRFC(String RFC) {
        this.RFC = RFC;
    }

    /**
     * @return the nombreRL
     */
    public String getNombreRL() {
        return nombreRL;
    }

    /**
     * @param nombreRL the nombreRL to set
     */
    public void setNombreRL(String nombreRL) {
        this.nombreRL = nombreRL;
    }

    /**
     * @return the apPatRL
     */
    public String getApPatRL() {
        return apPatRL;
    }

    /**
     * @param apPatRL the apPatRL to set
     */
    public void setApPatRL(String apPatRL) {
        this.apPatRL = apPatRL;
    }

    /**
     * @return the apMatRL
     */
    public String getApMatRL() {
        return apMatRL;
    }

    /**
     * @param apMatRL the apMatRL to set
     */
    public void setApMatRL(String apMatRL) {
        this.apMatRL = apMatRL;
    }

    /**
     * @return the CURP
     */
    public String getCURP() {
        return CURP;
    }

    /**
     * @param CURP the CURP to set
     */
    public void setCURP(String CURP) {
        this.CURP = CURP;
    }    
}
