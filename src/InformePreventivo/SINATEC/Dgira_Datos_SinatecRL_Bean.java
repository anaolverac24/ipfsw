/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package InformePreventivo.SINATEC;

import java.io.Serializable;
import java.sql.SQLException;
import java.util.List;
import java.util.logging.Logger;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;

/**
 *
 * @author Case Solutions 10
 */
@ManagedBean(name = "dgiraDatosSINATECRepLeg")
@ViewScoped
public class Dgira_Datos_SinatecRL_Bean implements Serializable{
    private static final Logger LOG = Logger.getLogger(DGira_Datos_Sinatec_Bean.class.getName());
    private List<Dgira_Datos_SinatecRL_Model> datosGralRepLeg;

    /**
     *
     * @param tramiteID
     */
    public void init(int tramiteID) {

        LOG.info("Parametros: " + tramiteID);
        try {
            datosGralRepLeg = DGira_Sinatec_DAO.consultaRepLegal(tramiteID);
        } catch (SQLException sqe) {
            LOG.severe(sqe.getMessage());
        }
    }
    
    /**
     * @return the archivosAnexos
     */
    public List<Dgira_Datos_SinatecRL_Model> getTodosRepLeg() {
        return datosGralRepLeg;
    }
    
    
    
}
