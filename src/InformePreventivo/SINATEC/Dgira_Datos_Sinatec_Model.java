/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package InformePreventivo.SINATEC;

import java.io.Serializable;
import javax.servlet.http.Part;

/**
 *
 * @author Case Solutions 10
 */
public class Dgira_Datos_Sinatec_Model implements Serializable {
    
    private int tamiteID;
    private String nombProm;
    private String ApPatprom;
    private String ApMatProm;
    private String RFC;
    private String tipoVialidad;
    private String nombVialidad;
    private String numInt;
    private String numExt;
    private String tipoAsent;
    private String nombAsent;
    private String CP;
    private String localidad;
    
    
    /**
     * Constructor vacio
     */
    public Dgira_Datos_Sinatec_Model() {

    }
    
    public Dgira_Datos_Sinatec_Model(int tamiteID, String nombProm, String ApPatprom, String ApMatProm, String RFC, String tipoVialidad, String nombVialidad, String numInt, 
String numExt, String tipoAsent1, String nombAsent, String CP, String localidad ) {
        this.tamiteID = tamiteID;
        this.nombProm = nombProm ;
        this.ApPatprom = ApPatprom;
        this.ApMatProm = ApMatProm;
        this.RFC = RFC;
        this.tipoVialidad = tipoVialidad;
        this.nombVialidad = nombVialidad;
        this.numExt = numExt;
        this.numInt = numInt;
        this.tipoAsent = tipoAsent1;
        this.nombAsent = nombAsent;
        this.CP = CP;
        this.localidad = localidad;        
    }

    /**
     * @return the tamiteID
     */
    public int getTamiteID() {
        return tamiteID;
    }

    /**
     * @param tamiteID the tamiteID to set
     */
    public void setTamiteID(int tamiteID) {
        this.tamiteID = tamiteID;
    }

    /**
     * @return the nombProm
     */
    public String getNombProm() {
        return nombProm;
    }

    /**
     * @param nombProm the nombProm to set
     */
    public void setNombProm(String nombProm) {
        this.nombProm = nombProm;
    }

    /**
     * @return the ApPatprom
     */
    public String getApPatprom() {
        return ApPatprom;
    }

    /**
     * @param ApPatprom the ApPatprom to set
     */
    public void setApPatprom(String ApPatprom) {
        this.ApPatprom = ApPatprom;
    }

    /**
     * @return the ApMatProm
     */
    public String getApMatProm() {
        return ApMatProm;
    }

    /**
     * @param ApMatProm the ApMatProm to set
     */
    public void setApMatProm(String ApMatProm) {
        this.ApMatProm = ApMatProm;
    }

    /**
     * @return the RFC
     */
    public String getRFC() {
        return RFC;
    }

    /**
     * @param RFC the RFC to set
     */
    public void setRFC(String RFC) {
        this.RFC = RFC;
    }

    /**
     * @return the tipoVialidad
     */
    public String getTipoVialidad() {
        return tipoVialidad;
    }

    /**
     * @param tipoVialidad the tipoVialidad to set
     */
    public void setTipoVialidad(String tipoVialidad) {
        this.tipoVialidad = tipoVialidad;
    }

    /**
     * @return the nombVialidad
     */
    public String getNombVialidad() {
        return nombVialidad;
    }

    /**
     * @param nombVialidad the nombVialidad to set
     */
    public void setNombVialidad(String nombVialidad) {
        this.nombVialidad = nombVialidad;
    }

    /**
     * @return the numInt
     */
    public String getNumInt() {
        return numInt;
    }

    /**
     * @param numInt the numInt to set
     */
    public void setNumInt(String numInt) {
        this.numInt = numInt;
    }

    /**
     * @return the numExt
     */
    public String getNumExt() {
        return numExt;
    }

    /**
     * @param numExt the numExt to set
     */
    public void setNumExt(String numExt) {
        this.numExt = numExt;
    }

    /**
     * @return the tipoAsent
     */
    public String getTipoAsent() {
        return tipoAsent;
    }

    /**
     * @param tipoAsent the tipoAsent to set
     */
    public void setTipoAsent(String tipoAsent) {
        this.tipoAsent = tipoAsent;
    }

    /**
     * @return the nombAsent
     */
    public String getNombAsent() {
        return nombAsent;
    }

    /**
     * @param nombAsent the nombAsent to set
     */
    public void setNombAsent(String nombAsent) {
        this.nombAsent = nombAsent;
    }

    /**
     * @return the CP
     */
    public String getCP() {
        return CP;
    }

    /**
     * @param CP the CP to set
     */
    public void setCP(String CP) {
        this.CP = CP;
    }

    /**
     * @return the localidad
     */
    public String getLocalidad() {
        return localidad;
    }

    /**
     * @param localidad the localidad to set
     */
    public void setLocalidad(String localidad) {
        this.localidad = localidad;
    }

}
