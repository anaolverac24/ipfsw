/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package InformePreventivo.SINATEC;


import com.casesolutions.moduloArchivos.DGira_Modulo_Archivos_DAO;
import java.io.Serializable;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.LinkedList;
import java.util.List;
import java.util.Properties;
import java.util.logging.Logger;
import oracle.jdbc.pool.OracleDataSource;

/**
 *
 * @author Case Solutions 10
 */
public class DGira_Sinatec_DAO  implements Serializable  {
    private static final Logger LOG = Logger.getLogger(DGira_Sinatec_DAO.class.getName());
    private static OracleDataSource dataSource = null;
    private static Properties configProperties;
    
    static {
        try {
            configProperties = new Properties();
            configProperties.loadFromXML(DGira_Modulo_Archivos_DAO.class.getResourceAsStream("/com/casesolutions/moduloArchivos/Config.xml"));
            dataSource = new OracleDataSource();
            dataSource.setURL("jdbc:oracle:thin:@//"
                    + configProperties.getProperty("dgiraSINATEC_bd_host") + ":"
                    + configProperties.getProperty("dgiraSINATEC_bd_port") + "/"
                    + configProperties.getProperty("dgiraSINATEC_bd_sid"));
            dataSource.setUser(configProperties.getProperty("dgiraSINATEC_bd_usu"));
            dataSource.setPassword(configProperties.getProperty("dgiraSINATEC_bd_pas"));
            dataSource.setPortNumber(Integer.parseInt(configProperties.getProperty("dgiraSINATEC_bd_port")));
        } catch (Exception exception) {
            LOG.severe(exception.getLocalizedMessage());
            exception.printStackTrace();
        }
    }

    public static Properties getConfigProperties() {
        return configProperties;
    }   

    public static List<Dgira_Datos_Sinatec_Model> consultaPromovente(int tramiteId) throws SQLException {
        
        List<Dgira_Datos_Sinatec_Model> promovente = new LinkedList<Dgira_Datos_Sinatec_Model>();
        Connection con = dataSource.getConnection();
        PreparedStatement pst = con.prepareCall(configProperties.getProperty("dgiraSINATEC_sql_selProm"));
        pst.setInt(1, tramiteId);
        ResultSet rst = pst.executeQuery();
        while (rst.next()) {
            Dgira_Datos_Sinatec_Model tmp = new Dgira_Datos_Sinatec_Model();
            tmp.setNombProm(rst.getString("VCNOMBRE"));
            tmp.setApPatprom(rst.getString("VCAPELLIDOPATERNO"));
            tmp.setApMatProm(rst.getString("VCAPELLIDOMATERNO"));
            tmp.setRFC(rst.getString("VCRFC")); //
            tmp.setTipoVialidad(rst.getString("DESCRIPCION"));
            tmp.setNombVialidad(rst.getString("VCNOMBREVIALIDAD"));
            tmp.setNumExt(rst.getString("VCNUMEXTERIOR"));
            tmp.setNumInt(rst.getString("VCNUMINTERIOR"));
            tmp.setTipoAsent(rst.getString("NOMBRE"));
            tmp.setNombAsent(rst.getString("VCTIPOASENTAMIENTO"));
            tmp.setCP(rst.getString("VCD_CODIGO_LK"));
            tmp.setLocalidad(rst.getString("VCLOCALIDAD"));            
            promovente.add(tmp);
        }
        rst.close();
        pst.close();
        con.close();

        return promovente;
    }
    
    public static List<Dgira_Datos_SinatecRL_Model> consultaRepLegal(int tramiteId) throws SQLException {
        
        List<Dgira_Datos_SinatecRL_Model> repLegal = new LinkedList<Dgira_Datos_SinatecRL_Model>();
        Connection con = dataSource.getConnection();
        PreparedStatement pst = con.prepareCall(configProperties.getProperty("dgiraSINATEC_sql_selRepLeg"));
        pst.setInt(1, tramiteId);
        ResultSet rst = pst.executeQuery();
        while (rst.next()) {
            Dgira_Datos_SinatecRL_Model tmp = new Dgira_Datos_SinatecRL_Model();
            //tmp.setRFC(rst.getString("VCRFC"));
            tmp.setNombreRL(rst.getString("VCNOMBRE"));
            tmp.setApPatRL(rst.getString("VCAPELLIDO1"));
            tmp.setApMatRL(rst.getString("VCAPELLIDO2")); //
            tmp.setCURP(rst.getString("VCCURP"));
            tmp.setTamiteID(rst.getInt("BGTRAMITEID"));          
            repLegal.add(tmp);
        }
        rst.close();
        pst.close();
        con.close();

        return repLegal;
    }
}
