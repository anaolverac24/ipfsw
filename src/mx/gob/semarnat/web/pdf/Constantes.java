
package mx.gob.semarnat.web.pdf;

//<editor-fold defaultstate="collapsed" desc="Imports">
import com.itextpdf.text.BaseColor;
import com.itextpdf.text.Chunk;
import com.itextpdf.text.Element;
import com.itextpdf.text.Font;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.html.simpleparser.HTMLWorker;
import java.io.IOException;
import java.io.StringReader;
import java.text.DecimalFormat;
import java.util.HashMap;
import java.util.List;
import mx.gob.semarnat.dao.VisorDao;
import mx.gob.semarnat.model.AnexosProyecto;
import mx.gob.semarnat.model.EspecificacionAnexo;
import mx.gob.semarnat.model.SustanciaAnexo;//</editor-fold>

/**
 * Constantes para la generación de PDF's
 *
 * @author Rodrigo
 */
public class Constantes {

    //<editor-fold defaultstate="collapsed" desc="Formato de decimales">
    public static final DecimalFormat DOS_DECIMALES_COMAS = new DecimalFormat("###,###.##");
    public static final DecimalFormat MONEDA = new DecimalFormat("$ ###,###.##");//</editor-fold>

    //<editor-fold defaultstate="collapsed" desc="Fuentes Constantes para el PDF">
    public static class FUENTE {

        public static final Font TITULO1 = new Font(Font.FontFamily.HELVETICA, 16, Font.BOLD, BaseColor.BLACK);
        public static final Font TITULO2 = new Font(Font.FontFamily.HELVETICA, 14, Font.BOLD, BaseColor.BLACK);
        public static final Font TITULO3 = new Font(Font.FontFamily.HELVETICA, 12, Font.BOLD, BaseColor.BLACK);
        public static final Font TITULO4 = new Font(Font.FontFamily.HELVETICA, 10, Font.BOLD, BaseColor.BLACK);
        public static final Font TITULO_CARATULA = new Font(Font.FontFamily.HELVETICA, 20, Font.BOLD);
        public static final Font TITULO_CARATULA2 = new Font(Font.FontFamily.COURIER, 14, Font.NORMAL);

        public static final Font PARRAFO = new Font(Font.FontFamily.HELVETICA, 8);
        public static final Font PARRAFO_TITULO = new Font(Font.FontFamily.HELVETICA, 12);
        public static final Font PARRAFO_NEGRITA = new Font(Font.FontFamily.HELVETICA, 8, Font.BOLD, BaseColor.BLACK);
        public static final Font PARRAFO_SUB = new Font(Font.FontFamily.HELVETICA, 8, Font.UNDERLINE, BaseColor.BLACK);
    }//</editor-fold>

    //<editor-fold defaultstate="collapsed" desc="Poner en mayusculas el String">
    /**
     * Método para hacer mayúscula cada palabra de una cadena, excepto las
     * conjunciones, artículos, preposiciones, etc
     *
     * @param ses
     * @return
     */
    public static String tipoFrase(String[] ses) {
        int i = 0, j = 0;
        String s = "";
        for (i = 0; i < ses.length; i++) {
            String[] pre;
            pre = ses[i].split(" ");
            for (j = 0; j < pre.length; j++) {
                String toUp;
                if (pre[j].length() > 0) {
                    toUp = pre[j].substring(0, 1);
                    toUp = toUp.equals("y") || toUp.equals("de") || toUp.equals("del") ? toUp.toLowerCase() : toUp.toUpperCase();
                    s = s.concat(toUp.concat(pre[j].
                            substring(1, pre[j].length()).
                            toLowerCase().concat(" ")));
                }
            }
        }
        if (s.length() > 0) {
            return s.substring(0, s.length() - 1);
        } else {
            return s;
        }
    }//</editor-fold>

    //<editor-fold defaultstate="collapsed" desc="Convierte un texto en HTML a objetos de iText">
    /**
     * Convierte un texto capturado desde un editor de texto enriquecido(HTML) a
     * elementos nativos de iText (tablas, spans, divs, etc)
     *
     * @param mapa
     * @param html
     * @return
     * @throws IOException
     */
    public static Paragraph htmlConverter(HashMap<String, Object> mapa, String html) throws IOException {
        StringReader strReader;
        List<Element> txt;
        Paragraph p = new Paragraph();
        if (html != null) {
            strReader = new StringReader(html);
            txt = HTMLWorker.parseToList(strReader, null, mapa);
            for (Element ele : txt) {
                p.add(ele);
            }
            p.setAlignment(Element.ALIGN_JUSTIFIED);
        }
        return p;
    }//</editor-fold>

    //<editor-fold defaultstate="collapsed" desc="Anexos generales">
    /**
     * Método para obtener la lista de anexos generales y añadirles una liga al
     * visor de archivos.
     *
     * @param url
     * @param capitulo
     * @param sub
     * @param apartado
     * @param subApart
     * @param folio
     * @param serial
     * @return
     */
    public static Paragraph anexo(String url, Short capitulo, Short sub, Short apartado, Short subApart, String folio, Short serial) {
        Paragraph parrafo = new Paragraph();
        Chunk chk;
        List<AnexosProyecto> anexosProyecto = VisorDao.getAnexos(capitulo, sub, apartado, subApart, folio, serial);
        if (!anexosProyecto.isEmpty()) {
            for (AnexosProyecto ap : anexosProyecto) {
                chk = new Chunk(ap.getAnexoNombre(), Constantes.FUENTE.PARRAFO_SUB);
                chk.setAnchor(url.concat("faces/modarchivos/visor.xhtml?idt=".
                        concat(ap.getAnexosProyectoPK().
                                getAnexoId() + "").concat("&tipo=general")));//url
                parrafo.add(chk);
                parrafo.add(Chunk.NEWLINE);
            }
        } else {
//            chk = new Chunk("No se cargaron anexos para este apartado", Constantes.FUENTE.PARRAFO_SUB);
//            parrafo.add(chk);
//            parrafo.add(Chunk.NEWLINE);
        }
        parrafo.setAlignment(Element.ALIGN_CENTER);
        return parrafo;
    }//</editor-fold>

    //<editor-fold defaultstate="collapsed" desc="Agrega anexos de las especificaciones">
    /**
     * Método para desplegar los anexos de la especificación
     *
     * @param url
     * @param folio
     * @param especifiacionId
     * @param serialProyecto
     * @param normaId
     * @return
     */
    public static Paragraph anexoEspecificacion(String url, String folio, String especifiacionId, Short serialProyecto, Short normaId) {
        Paragraph parrafo = new Paragraph();
        Chunk chk;
        List<EspecificacionAnexo> especificacionAnexos = VisorDao.getEspecifiacionesAnexos(folio, especifiacionId, serialProyecto, normaId);
        if (!especificacionAnexos.isEmpty()) {
            for (EspecificacionAnexo ep : especificacionAnexos) {
                chk = new Chunk(ep.getEspecAnexoNombre(), Constantes.FUENTE.PARRAFO_SUB);
                chk.setAnchor(url.concat("faces/modarchivos/visor.xhtml?idt=".
                        concat(ep.getEspecificacionAnexoPK().getEspecificacionAnexoId() + "").concat("&tipo=esp")));//url
                parrafo.add(chk);
                parrafo.add(Chunk.NEWLINE);
            }
        } else {
//            chk = new Chunk("No se cargaron anexos para este apartado", Constantes.FUENTE.PARRAFO_SUB);
//            parrafo.add(chk);
//            parrafo.add(Chunk.NEWLINE);
        }
        parrafo.setAlignment(Element.ALIGN_CENTER);
        return parrafo;
    }//</editor-fold>

    //<editor-fold defaultstate="collapsed" desc="Agrega anexos de las sustancias">
    /**
     * Agregar las hojas de seguridad al apartado de sustancias peligrosas
     *
     * @param folio
     * @param serial
     * @param id
     * @param url
     * @return
     */
    public static Paragraph anexoSustancia(String folio, Short serial, Short id, String url) {
        Paragraph parrafo = new Paragraph();
        Chunk chk;
        List<SustanciaAnexo> sustanciaAnexos = VisorDao.getSustanciaAnexos(folio, serial, id);
        if (!sustanciaAnexos.isEmpty()) {
            for (SustanciaAnexo sa : sustanciaAnexos) {
                chk = new Chunk(sa.getAnexoNombre(), Constantes.FUENTE.PARRAFO_SUB);
                chk.setAnchor(url.concat("faces/modarchivos/visor.xhtml?idt=".
                        concat(sa.getSustanciaAnexoPK().getSustanciaAnexoId() + "").concat("&tipo=sustancia")));//url
                parrafo.add(chk);
                parrafo.add(Chunk.NEWLINE);
            }
        } else {
//            chk = new Chunk("No se cargaron anexos para este apartado", Constantes.FUENTE.PARRAFO_SUB);
//            parrafo.add(chk);
//            parrafo.add(Chunk.NEWLINE);
        }
        parrafo.setAlignment(Element.ALIGN_CENTER);

        return parrafo;
    }//</editor-fold>

}
