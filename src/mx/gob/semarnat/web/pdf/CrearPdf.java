/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.gob.semarnat.web.pdf;

import com.itextpdf.text.Chunk;
import com.itextpdf.text.Document;
import com.itextpdf.text.Element;
import com.itextpdf.text.PageSize;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.Rectangle;
import com.itextpdf.text.pdf.PdfWriter;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.persistence.EntityManager;
import javax.persistence.Query;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import mx.gob.semarnat.dao.PoolEntityManagers;
import mx.gob.semarnat.model.Proyecto;

/**
 * Servlet para la creación del archivo PDF
 * @author Rodrigo
 */
public class CrearPdf extends HttpServlet {
    private Boolean porPeticion = true;

    //<editor-fold defaultstate="collapsed" desc="processRequest">
    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        
        response.setContentType("application/pdf");
        String param = request.getParameter("params");
        String[] args = param.split(",");
        
        generaPDF(args[0], new Short(args[1]), request.getRequestURL().toString(), response, this.getServletContext());
        
    }//</editor-fold>
    
    public byte[] generaPDF(String folio, short serial, String url, HttpServletResponse response, ServletContext servletContext) {
        EntityManager em = PoolEntityManagers.getEmf().createEntityManager();
        Paragraph parrafo = new Paragraph();
        ByteArrayOutputStream  bytesPDF = new ByteArrayOutputStream();
        
        Query q = em.createQuery("SELECT a FROM Proyecto a WHERE a.proyectoPK.folioProyecto=:folio AND a.proyectoPK.serialProyecto=:serial");
        q.setParameter("folio", folio);
        q.setParameter("serial", serial);
        Proyecto p = (Proyecto) q.getSingleResult();
//        em.close();

        try {
            ContenidoPdf cont = new ContenidoPdf(p, url.replace("ResumenPDF", ""));
            //crea documento
            Document document = new Document(PageSize.A4, 50, 50, 70, 55);// L, R, T, B
            //abrir escritura al pdf
            PdfWriter writer;
            if (response != null) {
                writer = PdfWriter.getInstance(document, response.getOutputStream());
            } else {
                writer = PdfWriter.getInstance(document, bytesPDF);
            }
            EncabezadoPdf event = new EncabezadoPdf(servletContext);
            writer.setBoxSize("art", new Rectangle(36, 54, 559, 788));
            writer.setPageEvent(event);
            //abrir documento
            document.open();
            //agregar elementos dentro del documento
            cont.caratula(writer, document);
            document.add(Chunk.NEXTPAGE);
            document.add(cont.indice());
            parrafo.add(Chunk.SPACETABBING);
            Paragraph pp = new Paragraph(new Chunk("Folio: " + folio));
            pp.setAlignment(Element.ALIGN_RIGHT);
            parrafo.add(pp);
            parrafo.add(Chunk.NEWLINE);
            document.add(Chunk.NEWLINE);
            System.out.println("cap 0");
            document.add(parrafo);//Capitulo 0 TEST
            System.out.println("cap 1");
            parrafo = cont.capituloI();
            document.add(parrafo);//capitulo 1
            document.add(Chunk.NEWLINE);
            System.out.println("cap 2");
            parrafo = cont.capituloII();
            document.add(parrafo);//capitulo 2
            System.out.println("cap 3");

            document.add(cont.capituloIII());//capítulo 3
            System.out.println("cap 4");

            document.add(cont.capituloIV());// capítulo 4
            document.add(Chunk.NEWLINE);

            //fin del documento, cierra el documento
            document.close();
        } catch (Exception ex) {
            ex.printStackTrace();
            Logger.getLogger(CrearPdf.class.getName()).log(Level.SEVERE, null, ex);
        } finally {

        }
        return bytesPDF.toByteArray();
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

    public void setPorPeticion(Boolean porPeticion) {
        this.porPeticion = porPeticion;
    }

}
