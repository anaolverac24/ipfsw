/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.gob.semarnat.web.pdf;

import com.itextpdf.text.BadElementException;
import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Element;
import com.itextpdf.text.Image;
import com.itextpdf.text.Phrase;
import com.itextpdf.text.Rectangle;
import com.itextpdf.text.pdf.ColumnText;
import com.itextpdf.text.pdf.PdfPageEventHelper;
import com.itextpdf.text.pdf.PdfWriter;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletContext;

/**
 * Clase para el encabezado y pie de página del archivo PDF
 * @author Admin
 */
public class EncabezadoPdf extends PdfPageEventHelper {

    private final ServletContext ctxt;
    private Integer numPag;

    public EncabezadoPdf(ServletContext c) {
        this.ctxt = c;
    }

    @Override
    public void onOpenDocument(PdfWriter writer, Document document) {
        numPag = -2;
    }

    @Override
    public void onStartPage(PdfWriter writer, Document document) {
        numPag++;
    }

    @Override
    public void onEndPage(PdfWriter writer, Document document) {
        if (document.getPageNumber() > 2) {
            try {
                Rectangle rect = writer.getBoxSize("art");
                String path = ctxt.getRealPath("/assets/images/gobmx/logos/logoSEMARNAT_hoz.png");
                
                Image img = Image.getInstance(path);
                img.scaleToFit(150, 100);
                ColumnText columnText = new ColumnText(writer.getDirectContent());
                columnText.setSimpleColumn(200, 150, 50, 830);//x,y,xPos,yPos
                columnText.addElement(img);
                columnText.setAlignment(Element.ALIGN_CENTER);
                columnText.go();

                ColumnText.showTextAligned(writer.getDirectContent(),
                        Element.ALIGN_CENTER, new Phrase("Recepción, Evaluación y Resolución del Informe Preventivo", Constantes.FUENTE.PARRAFO),
                        310, 815, 0);
                ColumnText.showTextAligned(writer.getDirectContent(),
                        Element.ALIGN_CENTER, new Phrase(
                                String.format("Página %d", numPag), Constantes.FUENTE.PARRAFO),
                        (rect.getLeft() + rect.getRight()) / 2,
                        rect.getBottom() - 18, 0);
            } catch (BadElementException ex) {
                Logger.getLogger(EncabezadoPdf.class.getName()).log(Level.SEVERE, null, ex);
            } catch (IOException ex) {
                Logger.getLogger(EncabezadoPdf.class.getName()).log(Level.SEVERE, null, ex);
            } catch (DocumentException ex) {
                Logger.getLogger(EncabezadoPdf.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }
}
