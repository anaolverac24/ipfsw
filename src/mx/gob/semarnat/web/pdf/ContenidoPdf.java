package mx.gob.semarnat.web.pdf;
    // <editor-fold defaultstate="collapsed" desc="Imports">
import com.itextpdf.text.BaseColor;
import com.itextpdf.text.Chunk;
import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Element;
import com.itextpdf.text.Font;
import com.itextpdf.text.FontProvider;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.Phrase;
import com.itextpdf.text.Section;
import com.itextpdf.text.pdf.ColumnText;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfWriter;
import java.io.IOException;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import javax.persistence.EntityManager;
import javax.persistence.Query;
import mx.gob.semarnat.dao.Capitulo2Dao;
import mx.gob.semarnat.dao.Capitulo3Dao;
import mx.gob.semarnat.dao.Capitulo3DaoCatalogos;
import mx.gob.semarnat.dao.PoolEntityManagers;
import mx.gob.semarnat.dao.ProyectoDao;
import mx.gob.semarnat.model.CatNorma;
import mx.gob.semarnat.model.EstudioRiesgoProyecto;
import mx.gob.semarnat.model.EtapaProyecto;
import mx.gob.semarnat.model.ParqueIndustrialProyecto;
import mx.gob.semarnat.model.PduProyecto;
import mx.gob.semarnat.model.PoetProyecto;
import mx.gob.semarnat.model.Proyecto;
import mx.gob.semarnat.model.RepLegalProyecto;
import mx.gob.semarnat.model.RespTecProyecto;
import mx.gob.semarnat.model.RespTecProyectoPK;
import mx.gob.semarnat.model.catalogos.DelegacionMunicipio;
import mx.gob.semarnat.model.sinatec.Vexdatosusuario;
import mx.gob.semarnat.model.sinatec.Vexdatosusuariorep;
import mx.gob.semarnat.view.Capitulo2View;
//</editor-fold>

/**
 * Clase que carga el contenido de todo el PDF
 * @author Rodrigo
 */
public class ContenidoPdf {

    // <editor-fold defaultstate="collapsed" desc="Declaración de variables">
    private ProyectoDao proyectoDao = new ProyectoDao();
    private static final String CAPITULO_I = "Capítulo I: Datos generales del proyecto, del promovente y del responsable del estudio.";
    private static final String CAPITULO_II = "Capítulo II: Referencias, según corresponda, al o los supuestos del Artículo 31 de la Ley General del Equilibrio Ecológico y la Protección al Ambiente";
    private static final String CAPITULO_III = "Capítulo III: Aspectos técnicos ambientales";
    private static final String CAPITULO_IV = "Estudio de Riesgo";
    private static final HashMap<String, Object> mapa = new HashMap();
    
    //var
    private Paragraph parrafo;
    private Phrase frase;
    private Chunk chk;
    //construct
    private final String url;
    private final Proyecto proyecto;
    private final Capitulo3Dao dao;
    private final Capitulo3DaoCatalogos daoCatalogos;
    private final Capitulo2Dao daoSinatec;//</editor-fold>

    // <editor-fold defaultstate="collapsed" desc="Constructor">
    /**
     * Constructor con parámetros: proyecto y la di
     * @param p proyecto
     * @param url url
     */
    public ContenidoPdf(Proyecto p, String url) {
        mapa.put("font_factory", new ProveedorFuente());
        this.proyecto = p;
        dao = new Capitulo3Dao();
        daoCatalogos = new Capitulo3DaoCatalogos();
        daoSinatec = new Capitulo2Dao();
        this.url = url;
    }// </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="Caratula del archivo PDF">
    /**
     * Caratula del PDF
     *
     * @param writer
     * @throws DocumentException
     */
    public void caratula(PdfWriter writer) throws DocumentException {
        Vexdatosusuario promovente = null;
        if (!((List<Vexdatosusuario>) daoSinatec.datosPromovente(Integer.parseInt(proyecto.getProyectoPK().getFolioProyecto()))).isEmpty()) {
            promovente = (Vexdatosusuario) daoSinatec.datosPromovente(Integer.parseInt(proyecto.getProyectoPK().getFolioProyecto())).get(0);
        }

        SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
        ColumnText columnText = new ColumnText(writer.getDirectContent());
        columnText.setSimpleColumn(500, 300, 80, 650);//x,y,xPos,yPos
        parrafo = new Paragraph(proyecto.getProyNombre() != null ? proyecto.getProyNombre() : "Nombre del proyecto", Constantes.FUENTE.TITULO_CARATULA);
        parrafo.add(Chunk.NEWLINE);
        parrafo.add(Chunk.NEWLINE);
        parrafo.add(new Phrase("Recepción, Evaluación y Resolución del Informe Preventivo", Constantes.FUENTE.TITULO_CARATULA2));
        parrafo.add(Chunk.NEWLINE);
//        parrafo.add(new Phrase(String.format("Tipo: %s \t Sector: %s", new Object[]{proyecto.getNtipo().toString(), proyecto.getNsec().toString()}), Constantes.FUENTE.TITULO_CARATULA2));
//        parrafo.add(Chunk.NEWLINE);
        DelegacionMunicipio dm;
        try {
            dm = (DelegacionMunicipio) daoCatalogos.busca(DelegacionMunicipio.class, proyecto.getProyMunAfectado());
        } catch (Exception e) {
            dm = null;
            e.printStackTrace();
        }
        if (dm != null) {
            parrafo.add(new Phrase(String.format("Entidad: %s \n Municipio: %s\n", new Object[]{dm.getEntidadFederativa().getNombreEntidadFederativa(), dm.getNombreDelegacionMunicipio()}), Constantes.FUENTE.TITULO_CARATULA2));
        }
        if (promovente != null) {
            parrafo.add(new Chunk("Razón social: " + Constantes.tipoFrase(new String[]{promovente.getVcnombre().concat(" ").concat(promovente.getVcapellidopaterno().concat(" ").concat(promovente.getVcapellidomaterno()))}), Constantes.FUENTE.TITULO_CARATULA2));
            parrafo.add(Chunk.NEWLINE);
        }
        parrafo.add(new Phrase(sdf.format(Calendar.getInstance().getTime()), Constantes.FUENTE.TITULO_CARATULA2));

        parrafo.setAlignment(Element.ALIGN_CENTER);
        columnText.addElement(parrafo);
        columnText.setAlignment(Element.ALIGN_CENTER);
        columnText.go();
    }// </editor-fold>
    
    public void caratula(PdfWriter caratulaWriter, Document documento) {
        try {
            EntityManager emCat = PoolEntityManagers.getEmfCat().createEntityManager();
            EntityManager em = PoolEntityManagers.getEmf().createEntityManager();
            EntityManager emSinatec = PoolEntityManagers.getEmfSinatec().createEntityManager();
//            String rutaImg = this.getClass().getClassLoader().getResource("/assets/images/gobmx/logos/logoSEMARNAT_hoz.png").getPath();
//            Image img = Image.getInstance(rutaImg);
//            img.scalePercent(40f);
//            img.setAbsolutePosition(10, 780);
//
//            //Imagen
//            caratulaWriter.getDirectContent().addImage(img);

            //Trámite
            Paragraph CaratulaTramite = new Paragraph();
            CaratulaTramite.setAlignment(Element.ALIGN_CENTER);
            
            String nombreTramite = "";
            Query q = emSinatec.createNativeQuery("SELECT NOMBRE_CORTO FROM VEXDATOSTRAMITE V INNER JOIN CATALOGO_TRAMITES.MODALIDAD M  ON V.BGID_TRAMITE_LK = M.ID_TRAMITE AND V.VCCLAVE_TRAMITE = M.CLAVE_TRAMITE "
                    + "WHERE v.bgtramiteid = :bgtramiteid");
            q.setParameter("bgtramiteid", new BigInteger(proyecto.getProyectoPK().getFolioProyecto()));
            q.setMaxResults(1);
            try {
                nombreTramite = (String)q.getSingleResult();
            } catch (Exception e) {
                nombreTramite = "Sin Datos en SINATEC";
            }
            
            Chunk TituloTramite = new Chunk("TRÁMITE:", Constantes.FUENTE.TITULO_CARATULA);
            Chunk TituloTramite2 = new Chunk(nombreTramite, Constantes.FUENTE.TITULO_CARATULA2);

            CaratulaTramite.add(TituloTramite);
            CaratulaTramite.add(Chunk.NEWLINE);
            CaratulaTramite.add(Chunk.NEWLINE);
            CaratulaTramite.add(TituloTramite2);

            CaratulaTramite.add(Chunk.NEWLINE);
            CaratulaTramite.add(Chunk.NEWLINE);
            CaratulaTramite.add(Chunk.NEWLINE);
            CaratulaTramite.add(Chunk.NEWLINE);
            
            //Proyecto
            Chunk TituloProyecto = new Chunk("PROYECTO: ", Constantes.FUENTE.TITULO_CARATULA);
            Chunk TituloProyecto2 = new Chunk("" + proyecto.getProyNombre() != null ? proyecto.getProyNombre() : "Nombre del proyecto", Constantes.FUENTE.TITULO_CARATULA2);

            CaratulaTramite.add(TituloProyecto);
            CaratulaTramite.add(Chunk.NEWLINE);
            CaratulaTramite.add(Chunk.NEWLINE);
            CaratulaTramite.add(TituloProyecto2);

            CaratulaTramite.add(Chunk.NEWLINE);
            CaratulaTramite.add(Chunk.NEWLINE);
            CaratulaTramite.add(Chunk.NEWLINE);
            CaratulaTramite.add(Chunk.NEWLINE);
            
            //Clave del Proyecto y Número de Bitácora
            if (proyecto.getProyectoPK().getSerialProyecto() >= 3) {
                Chunk cClaveProyecto = new Chunk("CLAVE: ", Constantes.FUENTE.TITULO_CARATULA);
                Chunk cClaveProyecto2 = new Chunk(proyecto.getClaveProyecto(), Constantes.FUENTE.TITULO_CARATULA2);

                CaratulaTramite.add(cClaveProyecto);
                CaratulaTramite.add(cClaveProyecto2);

                CaratulaTramite.add("       ");

                Chunk cBitacoraProyecto = new Chunk("BITÁCORA: ", Constantes.FUENTE.TITULO_CARATULA);
                Chunk cBitacoraProyecto2 = new Chunk(proyecto.getBitacoraProyecto(), Constantes.FUENTE.TITULO_CARATULA2);

                CaratulaTramite.add(cBitacoraProyecto);
                CaratulaTramite.add(cBitacoraProyecto2);

                CaratulaTramite.add(Chunk.NEWLINE);
                CaratulaTramite.add(Chunk.NEWLINE);
                CaratulaTramite.add(Chunk.NEWLINE);
                CaratulaTramite.add(Chunk.NEWLINE);            
            }
            
            //Promovente
            Vexdatosusuario promovente = null;
            if (!((List<Vexdatosusuario>) daoSinatec.datosPromovente(Integer.parseInt(proyecto.getProyectoPK().getFolioProyecto()))).isEmpty()) {
                promovente = (Vexdatosusuario) daoSinatec.datosPromovente(Integer.parseInt(proyecto.getProyectoPK().getFolioProyecto())).get(0);
            }
            if (promovente != null) {
                Chunk cPromovente = new Chunk("PROMOVENTE: ", Constantes.FUENTE.TITULO_CARATULA);
                Chunk cPromovente2 = new Chunk( Constantes.tipoFrase(new String[]{promovente.getVcnombre().concat(" ").concat(promovente.getVcapellidopaterno().concat(" ").concat(promovente.getVcapellidomaterno()))}), 
                        Constantes.FUENTE.TITULO_CARATULA2);

                CaratulaTramite.add(cPromovente);
                CaratulaTramite.add(Chunk.NEWLINE);
                CaratulaTramite.add(Chunk.NEWLINE);
                CaratulaTramite.add(cPromovente2);

                CaratulaTramite.add(Chunk.NEWLINE);
                CaratulaTramite.add(Chunk.NEWLINE);
                CaratulaTramite.add(Chunk.NEWLINE);
                CaratulaTramite.add(Chunk.NEWLINE);
            }
            
            //Clave del Sector y Subsector
            String ssector = "";
            q = emCat.createQuery("SELECT s.sector FROM SectorProyecto s WHERE s.nsec = :nsec");
            q.setParameter("nsec", proyecto.getNsec());
            q.setMaxResults(1);
            ssector = (String)q.getSingleResult();
            
            Chunk cSectorProyecto = new Chunk("SECTOR: ", Constantes.FUENTE.TITULO_CARATULA);
            Chunk cSectorProyecto2 = new Chunk(ssector, Constantes.FUENTE.TITULO_CARATULA2);

            CaratulaTramite.add(cSectorProyecto);
            CaratulaTramite.add(cSectorProyecto2);

            CaratulaTramite.add("         ");

            String ssubSector = "";
            q = emCat.createQuery("SELECT s.subsector FROM SubsectorProyecto s WHERE s.nsub = :nsub");
            q.setMaxResults(1);
            q.setParameter("nsub", proyecto.getNsub());
            ssubSector = (String)q.getSingleResult();            
            
            Chunk cSubsectorProyecto = new Chunk("SUBSECTOR: ", Constantes.FUENTE.TITULO_CARATULA);
            Chunk cSubsectorProyecto2 = new Chunk(ssubSector, Constantes.FUENTE.TITULO_CARATULA2);

            CaratulaTramite.add(cSubsectorProyecto);
            CaratulaTramite.add(cSubsectorProyecto2);

            CaratulaTramite.add(Chunk.NEWLINE);
            CaratulaTramite.add(Chunk.NEWLINE);
            CaratulaTramite.add(Chunk.NEWLINE);
            CaratulaTramite.add(Chunk.NEWLINE);            

            //Rama y tipo
            String srama = "";
            q = emCat.createQuery("SELECT r.rama FROM RamaProyecto r WHERE r.nrama = :nrama");
            q.setMaxResults(1);
            q.setParameter("nrama", proyecto.getNrama());
            srama = (String)q.getSingleResult();            
            
            Chunk cRamaProyecto = new Chunk("RAMA: ", Constantes.FUENTE.TITULO_CARATULA);
            Chunk cramaProyecto2 = new Chunk(srama, Constantes.FUENTE.TITULO_CARATULA2);

            CaratulaTramite.add(cRamaProyecto);
            CaratulaTramite.add(cramaProyecto2);

            CaratulaTramite.add("         ");

            String sTipoProyecto = "";
            q = emCat.createQuery("SELECT t.tipoProyecto FROM TipoProyecto t WHERE t.ntipo = :ntipo");
            q.setMaxResults(1);
            q.setParameter("ntipo", proyecto.getNtipo());
            sTipoProyecto = (String)q.getSingleResult();            

            Chunk cTipoProyecto = new Chunk("TIPO: ", Constantes.FUENTE.TITULO_CARATULA);
            Chunk cTipoProyecto2 = new Chunk(sTipoProyecto, Constantes.FUENTE.TITULO_CARATULA2);

            CaratulaTramite.add(cTipoProyecto);
            CaratulaTramite.add(cTipoProyecto2);

            CaratulaTramite.add(Chunk.NEWLINE);
            CaratulaTramite.add(Chunk.NEWLINE); 
            
            //Ubicación
            PdfPTable tableUniFis = new PdfPTable(3);
            tableUniFis.setWidthPercentage((float) 100);
            
            Paragraph EntidadSIGEIA = new Paragraph(new Chunk("Entidad Federativa", Constantes.FUENTE.TITULO4));
            Paragraph MunicipioSIGEIA = new Paragraph(new Chunk("Municipio", Constantes.FUENTE.TITULO4));
            Paragraph AreaSIGEIA = new Paragraph(new Chunk("Superficie m2", Constantes.FUENTE.TITULO4));

            PdfPCell uh1 = new PdfPCell(EntidadSIGEIA);
            PdfPCell uh2 = new PdfPCell(MunicipioSIGEIA);
            PdfPCell uh3 = new PdfPCell(AreaSIGEIA);

            uh1.setHorizontalAlignment(Element.ALIGN_CENTER);
            uh2.setHorizontalAlignment(Element.ALIGN_CENTER);
            uh3.setHorizontalAlignment(Element.ALIGN_CENTER);

            tableUniFis.addCell(uh1);
            tableUniFis.addCell(uh2);
            tableUniFis.addCell(uh3);
            Query qUbicacion = em.createNativeQuery("SELECT count(*) FROM Mpios_Cruzadavshambre a "
                    + "WHERE a.comp = 'OBRA' and a.num_Folio = :folio and a.version = :serial");
            qUbicacion.setParameter("folio", proyecto.getProyectoPK().getFolioProyecto());
            qUbicacion.setParameter("serial", proyecto.getProyectoPK().getSerialProyecto());
            BigDecimal bdRes = (BigDecimal) qUbicacion.getSingleResult();
            ArrayList<Object[]> lstRes;
            
            if (bdRes.intValue() > 0) {
                qUbicacion = em.createNativeQuery("SELECT a.c_Edomun, a.clv_Munici, a.nom_Estado, a.nom_Munici, to_char(sum(round(a.area,2)),'999G999G999G999D99') "
                        + "FROM Mpios_Cruzadavshambre a WHERE a.comp = 'OBRA' and a.num_Folio = :folio and a.version = :serial  GROUP BY a.c_Edomun, a.clv_Munici, a.nom_Estado, a.nom_Munici");
                qUbicacion.setParameter("folio", proyecto.getProyectoPK().getFolioProyecto());
                qUbicacion.setParameter("serial", proyecto.getProyectoPK().getSerialProyecto());
                lstRes = (ArrayList<Object[]>) qUbicacion.getResultList();
                
            } else {
                qUbicacion = em.createNativeQuery("SELECT a.c_Edomun, a.clv_Munici, a.nom_Estado, a.nom_Munici, to_char(sum(round(a.area,2)),'999G999G999G999D99') "
                        + "FROM Mpios_Cruzadavshambre a WHERE a.comp = 'PREDIO' and a.num_Folio = :folio and a.version = :serial  GROUP BY a.c_Edomun, a.clv_Munici, a.nom_Estado, a.nom_Munici");
                qUbicacion.setParameter("folio", proyecto.getProyectoPK().getFolioProyecto());
                qUbicacion.setParameter("serial", proyecto.getProyectoPK().getSerialProyecto());
                lstRes = (ArrayList<Object[]>) qUbicacion.getResultList();
            }

            //while (lstRes.size() > 0) {
            for (Object[] strRes : lstRes) {
                PdfPCell c1 = new PdfPCell(new Phrase("" + (String)strRes[2]));
                PdfPCell c2 = new PdfPCell(new Phrase("" + (String)strRes[3]));
                PdfPCell c3 = new PdfPCell(new Phrase("" + (String)strRes[4]));
                c3.setHorizontalAlignment(Element.ALIGN_RIGHT);

                tableUniFis.addCell(c1);
                tableUniFis.addCell(c2);
                tableUniFis.addCell(c3);
            }            
            
            Chunk cUbicacionProyecto = new Chunk("UBICACIÓN: ", Constantes.FUENTE.TITULO_CARATULA);
            CaratulaTramite.add(Chunk.NEWLINE);
            CaratulaTramite.add(Chunk.NEWLINE);

            CaratulaTramite.add(cUbicacionProyecto);

//            caratula.add(CaratulaTramite);
//            caratula.add(Chunk.NEWLINE);
//            caratula.add(tableUniFis);
//            
//            Paragraph CaratulaTramite2 = new Paragraph();

            CaratulaTramite.add(tableUniFis);
            
            CaratulaTramite.add(Chunk.NEWLINE);
            CaratulaTramite.add(Chunk.NEWLINE);
            
            //Fecha de ingreso
            q = emSinatec.createQuery("SELECT v.dtfecharegistro FROM Vexdatostramite v WHERE v.bgtramiteid = :bgtramiteid");
            q.setMaxResults(1);
            q.setParameter("bgtramiteid", new BigInteger(proyecto.getProyectoPK().getFolioProyecto()));
            Date fechaRegistro = (Date)q.getSingleResult();
            Locale espanol = new Locale("es","ES");
            SimpleDateFormat sdf = new SimpleDateFormat("d/MMMMM/yyyy  HH:mm",espanol);
            
            Chunk cFechaIngreso = new Chunk("Fecha de ingreso en SEMARNAT: ", Constantes.FUENTE.TITULO_CARATULA);
            Chunk cFechaIngreso2 = new Chunk(sdf.format(fechaRegistro), Constantes.FUENTE.TITULO_CARATULA2);

            CaratulaTramite.add(Chunk.NEWLINE);
            CaratulaTramite.add(Chunk.NEWLINE);
            
            CaratulaTramite.add(cFechaIngreso);
            CaratulaTramite.add(Chunk.NEWLINE);
            CaratulaTramite.add(Chunk.NEWLINE);
            CaratulaTramite.add(cFechaIngreso2);

            CaratulaTramite.setAlignment(Element.ALIGN_CENTER);            
            
            documento.add(CaratulaTramite);
            
        } catch (Exception e)         {
            System.err.println("Ocurrió un error durante la generación de la caratula del reporte PDF");
            e.printStackTrace();
        }
        
    }

    // <editor-fold defaultstate="collapsed" desc="Índice del archivo PDF">
    /**
     * TODO completar metodo
     *
     * @return
     */
    public Paragraph indice() {
        parrafo = new Paragraph();
        Paragraph p = new Paragraph();
        chk = new Chunk("CONTENIDO", Constantes.FUENTE.TITULO1);
        Paragraph tempParagraph = new Paragraph(chk);
        parrafo.add(tempParagraph);
        //capitulos, nuevo parrafo
//        AvanceProyecto ap = (AvanceProyecto) dao.busca(AvanceProyecto.class, new AvanceProyectoPK(proyecto.getProyectoPK().getFolioProyecto(), proyecto.getProyectoPK().getSerialProyecto(), new Short("1")));
//        if (ap != null) {
        chk = new Chunk(CAPITULO_I);
        chk.setLocalGoto("cap1");
        tempParagraph = new Paragraph(chk);
        tempParagraph.setIndentationLeft(20);
        tempParagraph.setSpacingBefore(20);
        parrafo.add(tempParagraph);
        chk = new Chunk("1.1 Datos generales del proyecto");
        chk.setLocalGoto("1.1");
        p.add(chk);
        p.add(Chunk.NEWLINE);
        chk = new Chunk("1.2 Datos generales del promovente");
        chk.setLocalGoto("1.2");
        p.add(chk);
        p.add(Chunk.NEWLINE);
        chk = new Chunk("1.3 Responsable del Informe Preventivo");
        chk.setLocalGoto("1.3");
        p.add(chk);
        p.add(Chunk.NEWLINE);
        p.setIndentationLeft(35f);
        parrafo.add(p);
//        }
////        ap = (AvanceProyecto) dao.busca(AvanceProyecto.class, new AvanceProyectoPK(proyecto.getProyectoPK().getFolioProyecto(),
//                proyecto.getProyectoPK().getSerialProyecto(), new Short("2")));
        p = new Paragraph();
//        if (ap != null) {
        chk = new Chunk(CAPITULO_II);
        chk.setLocalGoto("cap2");
        tempParagraph = new Paragraph(chk);
        tempParagraph.setIndentationLeft(20);
        tempParagraph.setSpacingBefore(20);
        parrafo.add(tempParagraph);
        if (proyecto.getProyNormaId() != null) {
            chk = new Chunk("2.1 Normas Oficiales Mexicanas u otras disposiciones que regulen las "
                    + "emisiones, las descargas o el aprovechamiento de recursos naturales y "
                    + "en general, todos los impactos ambientales relevantes que puedan"
                    + "producir la obra o actividad");
            chk.setLocalGoto("2.1");
            p.add(chk);
        } else {
            chk = new Chunk("2.1 Las obras y/o actividades estén expresamente previstas por "
                    + "un plan parcial de desarrollo urbano o de ordenamiento "
                    + "ecológico que haya sido evaluado por esta Secretaria");
            if (proyecto.getProySupuesto().equals('2')) {                
                chk = new Chunk("2.1 Se trate de instalaciones ubicadas en parques  "
                + "industriales autorizados en los términos de la presente.");
            }
            if (proyecto.getProySupuesto().equals('3')) {
                chk = new Chunk("2.1 Las obras y/o actividades estén expresamente previstas por "
                + "un plan parcial de desarrollo urbano o de ordenamiento "
                + "ecológico que haya sido evaluado por esta Secretaria");
            }
            
            chk.setLocalGoto("2.1.1");
            p.add(chk);
        }
        p.setIndentationLeft(35f);
        parrafo.add(p);
//        }
//        ap = (AvanceProyecto) dao.busca(AvanceProyecto.class, new AvanceProyectoPK(proyecto.getProyectoPK().getFolioProyecto(),
//                proyecto.getProyectoPK().getSerialProyecto(), new Short("31")));
        p = new Paragraph();

//        if (ap != null) {
        chk = new Chunk(CAPITULO_III);
        chk.setLocalGoto("cap3");
        tempParagraph = new Paragraph(chk);
        tempParagraph.setIndentationLeft(20);
        tempParagraph.setSpacingBefore(20);
        parrafo.add(tempParagraph);
        
        chk = new Chunk("3.1 Descripcion General de la Obra o Actividad y/o Proyectos");
        chk.setLocalGoto("3.1");
        p.add(chk);
        p.add(Chunk.NEWLINE);
        
        chk = new Chunk("3.2 Identificación de las sustancias o productos que van a emplearse y que podrían "
                + "provocar un impacto al ambiente, así como sus características físicas y químicas");  
        chk.setLocalGoto("3.2");
        p.add(chk);
        p.add(Chunk.NEWLINE);        
        
        chk = new Chunk("3.3 Identificación y estimación de las emisiones, descargas y residuos cuya "
                + "generación se prevea, así como medidas de control que se pretendan llevar a cabo"); 
        chk.setLocalGoto("3.3");
        p.add(chk);
        p.add(Chunk.NEWLINE);
        
        chk = new Chunk("3.4 Descripción del ambiente y, en su caso, la identificación de otras "
                + "fuentes de emisión de contaminantes existentes en el área de influencia "
                + "del proyecto");
        chk.setLocalGoto("3.4");
        p.add(chk);
        p.add(Chunk.NEWLINE);
        
        
        chk = new Chunk("3.5 Identificación de los impactos ambientales significativos o relevantes "
                + "y determinación de las acciones y medidas para su prevención y "
                + "mitigación");
        chk.setLocalGoto("3.5");
        p.add(chk);
        p.add(Chunk.NEWLINE);
         
        
        chk = new Chunk("3.6 Medidas Preventivas, de Mitigación y/o Compensación ");
        chk.setLocalGoto("3.6");
        p.add(chk);
        p.add(Chunk.NEWLINE);

        
        p.setIndentationLeft(35f);
        parrafo.add(p);
//        }
        p = new Paragraph();
        if (proyecto.getProySupuesto() == '1') {

            if (proyecto.getProyNormaId().equals(new Short("129"))) {
                chk = new Chunk(CAPITULO_IV);
                chk.setLocalGoto("cap4");
                tempParagraph = new Paragraph(chk);
                tempParagraph.setIndentationLeft(20);
                tempParagraph.setSpacingBefore(20);
                parrafo.add(tempParagraph);
                chk = new Chunk("4.1 Datos generales del responsable del estudio de riesgo");
                chk.setLocalGoto("4.1");
                p.add(chk);
                p.add(Chunk.NEWLINE);
                chk = new Chunk("4.2 Escenarios de los riesgos ambientales relacionados con el proyecto");
                chk.setLocalGoto("4.2");
                p.add(chk);
                p.add(Chunk.NEWLINE);
                chk = new Chunk("4.3 Descripción de las medidas de protección en torno a las instalaciones");
                chk.setLocalGoto("4.3");
                p.add(chk);
                p.add(Chunk.NEWLINE);
                chk = new Chunk("4.4 Señalamiento de las medidas de seguridad y preventivas en materia ambiental");
                chk.setLocalGoto("4.4");
                p.add(chk);
                p.add(Chunk.NEWLINE);
                chk = new Chunk("4.5 Resumen");
                chk.setLocalGoto("4.5");
                p.add(chk);
                p.add(Chunk.NEWLINE);
                chk = new Chunk("4.6 Identificación de los Instrumentos");
                chk.setLocalGoto("4.6");
                p.add(chk);
                p.add(Chunk.NEWLINE);                
                p.setIndentationLeft(35f);
                parrafo.add(p);
            }
        }
        parrafo.add(Chunk.NEXTPAGE);
        return parrafo;
    }//</editor-fold>

    // <editor-fold defaultstate="collapsed" desc="Capítulo I">
    /**
     *
     * @return @throws DocumentException
     */
    public Paragraph capituloI() throws DocumentException {
        PojoATabla pojoATabla = new PojoATabla();
        frase = new Phrase();
        parrafo = new Paragraph();
        chk = new Chunk(CAPITULO_I, Constantes.FUENTE.TITULO1);
        chk.setLocalDestination("cap1");
        frase.add(chk);
        frase.add(Chunk.NEWLINE);//fin ancla a cap1
        frase.add(Chunk.NEWLINE);//fin ancla a cap1
        //1.1 Datos Generales del Proyecto
        chk = new Chunk("1.1 Datos Generales del Proyecto", Constantes.FUENTE.TITULO2);
        chk.setLocalDestination("1.1");
        frase.add(chk);
        frase.add(Chunk.NEWLINE);
        //1.1.1 Nombre del proyecto
        frase.add(new Chunk("1.1.1 Nombre del proyecto", Constantes.FUENTE.TITULO3));
        frase.add(Chunk.NEWLINE);
        frase.add(new Chunk(proyecto.getProyNombre() != null ? proyecto.getProyNombre() : "Nombre del proyecto", Constantes.FUENTE.PARRAFO));
        frase.add(Chunk.NEWLINE);
        parrafo.add(frase);
        //Contacto SIGEIA                
        Chunk ContactoSigeia = new Chunk("Contacto SIGEIA",Constantes.FUENTE.TITULO4);
        ContactoSigeia.setAnchor(Capitulo2View.getUrlPdf(proyecto.getProyectoPK().getFolioProyecto(), proyecto.getClaveProyecto()
                                                        ,String.valueOf(proyecto.getProyectoPK().getSerialProyecto()), proyecto.getEstatusProyecto()));
        parrafo.add(ContactoSigeia);
        parrafo.add(Chunk.NEWLINE);
        frase = new Phrase();        
        //1.1.2 Ubicación del proyecto
        frase.add(new Chunk("1.1.2 Ubicación del proyecto", Constantes.FUENTE.TITULO3));
        frase.add(Chunk.NEWLINE);        
        if (proyecto.getProyDomEstablecido().equals('N')) {
            if (proyecto.getProyUbicacionDescrita() != null && !proyecto.getProyUbicacionDescrita().equals("")) {
                frase.add(new Chunk(proyecto.getProyUbicacionDescrita(), Constantes.FUENTE.PARRAFO));
            }
        } else {
            PojoATabla p = new PojoATabla();
            frase.add(p.ubicacionProyecto(proyecto));
//            proyecto.getCveTipoVial();
        }
        frase.add(Chunk.NEWLINE);
        parrafo.add(frase);
        parrafo.add(pojoATabla.referenciaSigeia(proyecto.getProyectoPK().getFolioProyecto(),
                proyecto.getClaveProyecto(), proyecto.getProyectoPK().getSerialProyecto()));
        frase = new Phrase();
        //1.1.3 Superficie total del proyecto
        frase = new Phrase("1.1.3 Superficie total del proyecto", Constantes.FUENTE.TITULO3);
        frase.add(Chunk.NEWLINE);
        parrafo.add(frase);
        parrafo.add(pojoATabla.superficieTotal(proyecto.getProyectoPK().getFolioProyecto(),
                proyecto.getClaveProyecto(), proyecto.getProyectoPK().getSerialProyecto()));
        parrafo.add(Chunk.NEWLINE);
        
        //1.1.4 Superficie total del proyecto
        frase = new Phrase("1.1.4 Planos adicionales del proyecto", Constantes.FUENTE.TITULO3);
        frase.add(Chunk.NEWLINE);
        parrafo.add(frase);
        parrafo.add(Constantes.anexo(url, new Short("2"),
                new Short("1"), new Short("1"),
                new Short("1"),
                proyecto.getProyectoPK().getFolioProyecto(),
                proyecto.getProyectoPK().getSerialProyecto()));
        //1.1.5 Inversión requerida
        frase = new Paragraph(new Chunk("1.1.5 Inversión y Empleos", Constantes.FUENTE.TITULO3));
        frase.add(Chunk.NEWLINE);       
        parrafo.add(frase);
        
        PojoATabla p = new PojoATabla();
        frase.add(p.inversionRequerida(proyecto));
        
        
        //1.1.6 Número de empleados directos e indirectos generados por el desarrollo del proyecto
        frase = new Phrase(new Chunk("1.1.6 Duración del proyecto", Constantes.FUENTE.TITULO3));
        frase.add(Chunk.NEWLINE);
        frase.add(new Chunk(String.format("El proyecto tendrá una duración de %d años y %d meses."
                + "\n Contando con las siguientes etapas: ",
                new Object[]{proyecto.getProyTiempoVidaAnios() != null ? proyecto.getProyTiempoVidaAnios() : 0,
                    proyecto.getProyTiempoVidaMeses() != null ? proyecto.getProyTiempoVidaMeses() : 0}), Constantes.FUENTE.PARRAFO));
        frase.add(Chunk.NEWLINE);
        frase.add(Chunk.NEWLINE);
        parrafo.add(frase);
        parrafo.add(pojoATabla.tablaEtapas(proyecto.getProyectoPK().getFolioProyecto()));
        //-----------1.2 promovente----------------
        Vexdatosusuario promovente = null;
        List<RepLegalProyecto> legalProyecto = (List<RepLegalProyecto>) dao.
                lista_namedQuery("RepLegalProyecto.findByFolioProyecto",
                        new Object[]{proyecto.getProyectoPK().getFolioProyecto()},
                        new String[]{"folioProyecto"});
        if (!((List<Vexdatosusuario>) daoSinatec.datosPromovente(Integer.parseInt(proyecto.getProyectoPK().getFolioProyecto()))).isEmpty()) {
            promovente = (Vexdatosusuario) daoSinatec.datosPromovente(Integer.parseInt(proyecto.getProyectoPK().getFolioProyecto())).get(0);
        }
        chk = new Chunk("1.2 Datos generales del promovente", Constantes.FUENTE.TITULO2);
        chk.setLocalDestination("1.2");
        frase = new Phrase(chk);
        frase.add(Chunk.NEWLINE);
        parrafo.add(frase);
        frase = new Phrase();
        if (promovente != null) {
            String nombProm = null;
            String rfc = null;
            
            nombProm = promovente.getVctipopersona().equals("MOR") == true ? promovente.getVcrazonsocial() : promovente.getVcnombre() + " " + promovente.getVcapellidopaterno() + " " + promovente.getVcapellidomaterno();
            rfc  = promovente.getVctipopersona().equals("MOR") == true ? promovente.getVcrfc() : promovente.getVccurp();
            //1.2.1 Nombre o razón social
            frase.add(new Chunk("1.2.1 Nombre o razón social", Constantes.FUENTE.TITULO3));
            frase.add(Chunk.NEWLINE);                           //tipoPersona.equals("MOR")
            frase.add(new Chunk(Constantes.tipoFrase(new String[]{nombProm}), Constantes.FUENTE.PARRAFO));
            frase.add(Chunk.NEWLINE);
            parrafo.add(frase);
            frase = new Phrase();
            //1.2.2 Registro Federal de Contribuyentes
            frase.add(new Chunk("1.2.2 Registro Federal de Contribuyentes", Constantes.FUENTE.TITULO3));
            frase.add(Chunk.NEWLINE);
            frase.add(new Chunk(rfc.toUpperCase(), Constantes.FUENTE.PARRAFO));
            frase.add(Chunk.NEWLINE);
            parrafo.add(frase);
            
            
            
            
            
            frase = new Phrase();
            //1.2.3 Nombre y cargo del Representante Legal, así como el Registro Federal de Contribuyentes y CURP
            frase.add(new Chunk("1.2.3 Nombre y cargo del Representante Legal, así como el Registro Federal de Contribuyentes y CURP", Constantes.FUENTE.TITULO3));
            frase.add(Chunk.NEWLINE);

            for (RepLegalProyecto rlp : legalProyecto) {
                Vexdatosusuariorep temp = daoSinatec.repLegPorCurp(rlp.getRepLegalProyectoPK().getRfc(),
                        Integer.parseInt(proyecto.getProyectoPK().getFolioProyecto()));

                if (temp.getVccurp().equals(rlp.getRepLegalProyectoPK().getRfc())) {
                    frase.add(new Chunk(Constantes.tipoFrase(new String[]{temp.getVcnombre().concat(" ").concat(temp.getVcapellido1()).concat(" ").concat(temp.getVcapellido2())}), Constantes.FUENTE.PARRAFO));
                    frase.add(Chunk.NEWLINE);
                    frase.add(new Chunk(temp.getVccurp(), Constantes.FUENTE.PARRAFO));
                    frase.add(Chunk.NEWLINE);
                }
            }
            parrafo.add(frase);
            frase = new Phrase();
            //1.2.4 Dirección del promovente para recibir u oír notificaciones
            frase.add(new Chunk("1.2.4 Dirección del promovente para recibir u oír notificaciones", Constantes.FUENTE.TITULO3));
            frase.add(Chunk.NEWLINE);
            parrafo.add(frase);
            parrafo.add(pojoATabla.direccionPromovente(promovente));
            frase = new Phrase();
        }
        //1.3 Responsable del Informe Preventivo
        RespTecProyecto respTecProyecto = (RespTecProyecto) dao.busca(RespTecProyecto.class, new RespTecProyectoPK(proyecto.getProyectoPK().getFolioProyecto(), proyecto.getProyectoPK().getSerialProyecto()));
        if (!legalProyecto.isEmpty() || respTecProyecto != null) {
            chk = new Chunk("1.3 Responsable del Informe Preventivo", Constantes.FUENTE.TITULO2);
            chk.setLocalDestination("1.3");
            frase.add(chk);
            frase.add(Chunk.NEWLINE);
            parrafo.add(frase);
            frase = new Phrase();
            //1.3.1. Nombre o razón social
            frase.add(new Chunk("1.3.1 Nombre o razón social", Constantes.FUENTE.TITULO3));
            frase.add(Chunk.NEWLINE);
            if (!legalProyecto.isEmpty()) {
                if (legalProyecto.get(0).getMismoEstudio().contains("S")) {
                    for (RepLegalProyecto rlp : legalProyecto) {
                        Vexdatosusuariorep temp = daoSinatec.repLegPorCurp(rlp.getRepLegalProyectoPK().getRfc(),
                                Integer.parseInt(proyecto.getProyectoPK().getFolioProyecto()));
                        if (temp.getVccurp().equals(rlp.getRepLegalProyectoPK().getRfc())) {
                            frase.add(new Chunk(Constantes.tipoFrase(new String[]{temp.getVcnombre().concat(" ").concat(temp.getVcapellido1()).concat(" ").concat(temp.getVcapellido2())}), Constantes.FUENTE.PARRAFO));
                            frase.add(Chunk.NEWLINE);
//                frase.add(temp.getVccurp());
                        }
                    }
                }
            } else {
                frase.add(new Chunk(Constantes.tipoFrase(new String[]{respTecProyecto.getRespTecNombre() + " " + respTecProyecto.getRespTecApellido1() + " " + respTecProyecto.getRespTecApellido2()}), Constantes.FUENTE.PARRAFO));
                frase.add(Chunk.NEWLINE);

            }
            parrafo.add(frase);
            frase = new Phrase();
            //1.3.2. RFC
            frase.add(new Chunk("1.3.2 Registro Federal de Contribuyentes", Constantes.FUENTE.TITULO3));
            frase.add(Chunk.NEWLINE);
            if (!legalProyecto.isEmpty() && legalProyecto.get(0).getMismoEstudio().contains("S")) {
                for (RepLegalProyecto rlp : legalProyecto) {
                    Vexdatosusuariorep temp = daoSinatec.repLegPorCurp(rlp.getRepLegalProyectoPK().getRfc(),
                            Integer.parseInt(proyecto.getProyectoPK().getFolioProyecto()));
                    if (temp.getVccurp().equals(rlp.getRepLegalProyectoPK().getRfc())) {
//                frase.add(Constantes.tipoFrase(new String[]{temp.getVcnombre().concat(" ").concat(temp.getVcapellido1()).concat(" ").concat(temp.getVcapellido2())}));
//                frase.add(Chunk.NEWLINE);
                        frase.add(new Chunk(temp.getVccurp(), Constantes.FUENTE.PARRAFO));
                        frase.add(Chunk.NEWLINE);
                    }
                }
            } else {
                String RFCrespTec = "";
                if (respTecProyecto.getRespTecRfc() != null)
                    RFCrespTec = respTecProyecto.getRespTecRfc();
                frase.add(new Chunk(RFCrespTec, Constantes.FUENTE.PARRAFO));
                frase.add(Chunk.NEWLINE);
            }
            parrafo.add(frase);
            frase = new Phrase();
            //1.3.3. NOMBRE
            frase.add(new Chunk("1.3.3 Nombre del Responsable Técnico del estudio, así como su Registro Federal de Contribuyentes y CURP", Constantes.FUENTE.TITULO3));
            frase.add(Chunk.NEWLINE);

            if (!legalProyecto.isEmpty() &&legalProyecto.get(0).getMismoEstudio().contains("S")) {
                for (RepLegalProyecto rlp : legalProyecto) {
                    Vexdatosusuariorep temp = daoSinatec.repLegPorCurp(rlp.getRepLegalProyectoPK().getRfc(),
                            Integer.parseInt(proyecto.getProyectoPK().getFolioProyecto()));
                    if (temp.getVccurp().equals(rlp.getRepLegalProyectoPK().getRfc())) {
                        frase.add(new Chunk(Constantes.tipoFrase(new String[]{temp.getVcnombre().concat(" ").concat(temp.getVcapellido1()).concat(" ").concat(temp.getVcapellido2())}), Constantes.FUENTE.PARRAFO));
                        frase.add(Chunk.NEWLINE);
                        frase.add(new Chunk(temp.getVccurp(), Constantes.FUENTE.PARRAFO));
                        frase.add(Chunk.NEWLINE);
                    }
                }
            } else {
                frase.add(new Chunk(Constantes.tipoFrase(new String[]{respTecProyecto.getRespTecNombre() + " " + respTecProyecto.getRespTecApellido1() + " " + respTecProyecto.getRespTecApellido2()}), Constantes.FUENTE.PARRAFO));
                frase.add(Chunk.NEWLINE);
                frase.add(new Chunk(respTecProyecto.getRespTecRfc(), Constantes.FUENTE.PARRAFO));
                frase.add(Chunk.NEWLINE);
                frase.add(new Chunk(respTecProyecto.getRespTecCurp(), Constantes.FUENTE.PARRAFO));
                frase.add(Chunk.NEWLINE);
            }

            parrafo.add(frase);
        }
        frase = new Phrase();
        //1.3.4. direccion
//        frase.add(new Chunk("1.3.4 Dirección del Responsable del Estudio", Constantes.FUENTE.TITULO3));
//        frase.add(Chunk.NEWLINE);
//        parrafo.add(frase);
//        frase = new Phrase();
        return parrafo;
    }//</editor-fold>

    // <editor-fold defaultstate="collapsed" desc="Capítulo II">
    /**
     *
     * @return parrafo del capitulo 2
     * @throws java.io.IOException
     * @throws com.itextpdf.text.DocumentException
     */
    public Paragraph capituloII() throws IOException, DocumentException {
        PojoATabla pojoATabla= new PojoATabla();
        frase = new Phrase();
        parrafo = new Paragraph();
        parrafo.add(Chunk.NEXTPAGE);
        chk = new Chunk(CAPITULO_II, Constantes.FUENTE.TITULO1);
        chk.setLocalDestination("cap2");
        frase.add(chk);
        frase.add(Chunk.NEWLINE);
        frase.add(Chunk.NEWLINE);//fin ancla a cap2
        parrafo.add(frase);
        if (proyecto.getProyNormaId() != null) {
            //------- tabla de supuestos------
            //parrafo.add(pojoATabla.supuestos(proyecto.getProySupuesto()));
            
            
            
            //2.1 Normas Oficiales Mexicanas u otras disposiciones que regulen las emisiones, las descargas o el aprovechamiento de recursos naturales y en general, todos los impactos ambientales relevantes que puedan producir la obra o actividad
            chk = new Chunk("2.1 Normas Oficiales Mexicanas u otras disposiciones que regulen las emisiones, las descargas o el aprovechamiento de recursos naturales y en general, todos los impactos ambientales relevantes que puedan producir la obra o actividad", Constantes.FUENTE.TITULO2);
            chk.setLocalDestination("2.1");
            frase = new Paragraph(chk);
            frase.add(Chunk.NEWLINE);
            parrafo.add(frase);
            
            //2.1.1 
            frase = new Paragraph(new Chunk(String.format("2.1.1 %s", new Object[]{((CatNorma) dao.busca(CatNorma.class, proyecto.getProyNormaId())).getNormaNombre()}), Constantes.FUENTE.TITULO3));
            frase.add(Chunk.NEWLINE);
            frase.add(new Chunk(((CatNorma) dao.busca(CatNorma.class, proyecto.getProyNormaId())).getNormaDescripcion(), Constantes.FUENTE.PARRAFO));
            frase.add(Chunk.NEWLINE);
            //tabla minerales
            if (proyecto.getProyNormaId() == 120) {
                frase.add(new Phrase("Clima", Constantes.FUENTE.PARRAFO_NEGRITA));
                frase.add(new Phrase("Vegetación", Constantes.FUENTE.PARRAFO_NEGRITA));
                frase.add(new Phrase("Minerales", Constantes.FUENTE.PARRAFO_NEGRITA));
                frase.add(pojoATabla.minerales(proyecto.getProyectoPK().getFolioProyecto(), proyecto.getProyectoPK().getSerialProyecto()));
            }
            if (proyecto.getProyNormaId() == 141) {
//                frase.add(new Phrase("Minerales", Constantes.FUENTE.PARRAFO_NEGRITA));
                frase.add(pojoATabla.minerales(proyecto.getProyectoPK().getFolioProyecto(), proyecto.getProyectoPK().getSerialProyecto()));
            }
            parrafo.add(frase);
            //-----tabla de especificaciones
            parrafo.add(pojoATabla.tablaEspecificaciones(proyecto.getProyectoPK().getFolioProyecto(), proyecto.getProyectoPK().getSerialProyecto(), url));
        }
//--------------------------------------Inicio se quita Paty 
        //2.1.2 Las obras y/o actividades estén expresamente previstas por un plan parcial de desarrollo urbano o de ordenamiento ecológico que haya sido evaluado por esta Secretaria
//        chk = new Chunk((proyecto.getProyNormaId() != null ? "2.1.2" : "2.1")
//                + " Las obras y/o actividades estén expresamente previstas por "
//                + "un plan parcial de desarrollo urbano o de ordenamiento "
//                + "ecológico que haya sido evaluado por esta Secretaria", proyecto.getProyNormaId() != null ? Constantes.FUENTE.TITULO3 : Constantes.FUENTE.TITULO2);
//        chk.setLocalDestination("2.1.1");
//        frase = new Paragraph(chk);
//        frase.add(Chunk.NEWLINE);
//        parrafo.add(frase);
//--------------------------------------Fin se quita Paty 
        //-----parrafo.add(pojoATabla.supuestos(proyecto.getProySupuesto()));
        if (proyecto.getProySupuesto().equals('3')) {
            List<PduProyecto> pdulist = (List<PduProyecto>) dao.lista_namedQuery("PduProyecto.findByFolioProyecto", new Object[]{proyecto.getProyectoPK().getFolioProyecto()}, new String[]{"folioProyecto"});
            
            
            chk = new Chunk("2.1 Las obras y/o actividades estén expresamente previstas por "
            + "un plan parcial de desarrollo urbano o de ordenamiento "
            + "ecológico que haya sido evaluado por esta Secretaria.", proyecto.getProyNormaId() != null ? Constantes.FUENTE.TITULO3 : Constantes.FUENTE.TITULO2);
            chk.setLocalDestination("2.1.1");
            frase = new Paragraph(chk);
            frase.add(Chunk.NEWLINE);
            parrafo.add(frase);
            
            if (!pdulist.isEmpty()) { 
                PduProyecto pdu = pdulist.get(0);
                frase = new Phrase(new Chunk("Programa de Desarrollo Urbano", Constantes.FUENTE.TITULO3));
                frase.add(Chunk.NEWLINE);
                frase.add(new Phrase(new Chunk("Descripción", Constantes.FUENTE.TITULO4)));
                frase.add(Chunk.NEWLINE);
                frase.add(new Phrase(new Chunk(pdu.getCatPdu().getPduDescripcion(), Constantes.FUENTE.TITULO4)));
                frase.add(Chunk.NEWLINE);
                frase.add(new Phrase(new Chunk("Justificación", Constantes.FUENTE.TITULO4)));
                frase.add(Chunk.NEWLINE);
                frase.add(new Chunk(pdu.getPduJustificacion(), Constantes.FUENTE.PARRAFO));
                parrafo.add(frase);
            } else {
                List<PoetProyecto> poet = (List<PoetProyecto>) dao.lista_namedQuery("PoetProyecto.findByFolioProyecto", new Object[]{proyecto.getProyectoPK().getFolioProyecto()}, new String[]{"folioProyecto"});
                if (!poet.isEmpty()) { 
                    frase.add(new Phrase(new Chunk("Programa de Ordenamiento Ecológico de Territorio.", Constantes.FUENTE.TITULO3)));
                    frase.add(Chunk.NEWLINE);
                    frase.add(new Phrase(new Chunk("Descripción", Constantes.FUENTE.TITULO4)));
                    frase.add(Chunk.NEWLINE);
                    frase.add(new Phrase(new Chunk(poet.get(0).getCatPoet().getPoetDescripcion(), Constantes.FUENTE.TITULO4)));
                    frase.add(Chunk.NEWLINE);
                    frase.add(new Phrase(new Chunk("Justificación", Constantes.FUENTE.TITULO4)));
                    frase.add(Chunk.NEWLINE);
                    frase.add(new Chunk(poet.get(0).getPoetJustificacion(), Constantes.FUENTE.PARRAFO));
                    parrafo.add(frase);
                }
            }
        }
        if (proyecto.getProySupuesto().equals('2')) {
            ParqueIndustrialProyecto pi = (ParqueIndustrialProyecto) dao.lista_namedQuery("ParqueIndustrialProyecto.findByFolioProyecto", new Object[]{proyecto.getProyectoPK().getFolioProyecto()}, new String[]{"folioProyecto"}).get(0);
            if (pi != null) {
                chk = new Chunk("2.1 Se trate de instalaciones ubicadas en parques  "
                + "industriales autorizados en los términos de la presente.", proyecto.getProyNormaId() != null ? Constantes.FUENTE.TITULO3 : Constantes.FUENTE.TITULO2);
                chk.setLocalDestination("2.1.1");
                frase = new Paragraph(chk);
                frase.add(Chunk.NEWLINE);
                parrafo.add(frase);
                
                frase = new Phrase("Parques Industriales");
                frase.add(Chunk.NEWLINE);
                frase.add(new Phrase(new Chunk("Descripción", Constantes.FUENTE.TITULO4)));
                frase.add(Chunk.NEWLINE);
                frase.add(new Phrase(new Chunk(pi.getCatParqueIndustrial().getParqueDescripcion(), Constantes.FUENTE.TITULO4)));
                frase.add(Chunk.NEWLINE);
                frase.add(new Phrase(new Chunk("Justificación", Constantes.FUENTE.TITULO4)));
                frase.add(Chunk.NEWLINE);
                frase.add(new Chunk(pi.getParqueIndJustificacion(), Constantes.FUENTE.PARRAFO));
                parrafo.add(frase);
            }
        }
        return parrafo;
    }//</editor-fold>

    // <editor-fold defaultstate="collapsed" desc="Capítulo III">
    /**
     *
     * @return @throws DocumentException
     * @throws IOException
     */
    public Paragraph capituloIII() throws DocumentException, IOException {
        PojoATabla pojoATabla = new PojoATabla();
        frase = new Phrase();
        parrafo = new Paragraph();
        parrafo.add(Chunk.NEXTPAGE);
        chk = new Chunk(CAPITULO_III, Constantes.FUENTE.TITULO1);
        chk.setLocalDestination("cap3");
        frase.add(chk);
        frase.add(Chunk.NEWLINE);
        frase.add(Chunk.NEWLINE);//fin ancla a cap2
        parrafo.add(frase);
        frase = new Phrase();

        //3.1 Descripción General de la Obra o Actividad Proyectada
        chk = new Chunk("3.1 Descripcion General de la Obra o Actividad y/o Proyectos", Constantes.FUENTE.TITULO2);
        chk.setLocalDestination("3.1");
        frase.add(chk);
        frase.add(Chunk.NEWLINE);
        parrafo.add(frase);
        
        
        int i = 1;
        
        
        frase = new Phrase();
        //3.1.1 Dimensiones del Proyecto
//        frase.add(new Chunk("3.1."+i+" Dimensiones del Proyecto", Constantes.FUENTE.TITULO3));
//        frase.add(Chunk.NEWLINE);
//        parrafo.add(frase);
//        frase = new Phrase();
//        i++;
        
        
                //3.1.2 Características del Proyecto
        chk = new Chunk("3.1 Descripcion General de la Obra o Actividad y/o Proyectos", Constantes.FUENTE.TITULO2);
        frase.add(new Chunk("3.1." + i + " Características del Proyecto", Constantes.FUENTE.TITULO3));
        frase.add(Chunk.NEWLINE);
        frase.add(Chunk.NEWLINE);
        parrafo.add(frase);
        parrafo.add(Constantes.htmlConverter(mapa, proyecto.getProyDescParticular()));
        frase = new Phrase();
        frase.add(Chunk.NEWLINE);
        parrafo.add(frase);
        
        frase = new Phrase();
        i++;
        //3.1.3 Uso actual del suelo en el sitio seleccionado  
//        frase.add(new Chunk("3.1."+i+" Uso actual del suelo en el sitio seleccionado", Constantes.FUENTE.TITULO3));
//        frase.add(Chunk.NEWLINE);
//        parrafo.add(frase);
//        frase = new Phrase();
//        i++;
        //3.1.5. Programa de trabajo
        frase.add(new Chunk("3.1." + i + " Programa de trabajo", Constantes.FUENTE.TITULO3));
        frase.add(Chunk.NEWLINE);
        parrafo.add(frase);
        parrafo.add(Constantes.anexo(url, new Short("3"), new Short("1"), new Short("2"), new Short("1"), proyecto.getProyectoPK().getFolioProyecto(), proyecto.getProyectoPK().getSerialProyecto()));
        //parrafo.add(Constantes.anexo(url, new Short("3"), new Short("5"), new Short("1"), new Short("1"), proyecto.getProyectoPK().getFolioProyecto(), proyecto.getProyectoPK().getSerialProyecto()));
        //parrafo.add(Constantes.anexo(url, new Short("3"), new Short("1"), new Short("3"), new Short("1"), proyecto.getProyectoPK().getFolioProyecto(), proyecto.getProyectoPK().getSerialProyecto()));
        //parrafo.add(Constantes.anexo(url, new Short("3"), new Short("1"), new Short("4"), new Short("1"), proyecto.getProyectoPK().getFolioProyecto(), proyecto.getProyectoPK().getSerialProyecto()));
       
        
        
        frase = new Phrase();
         i++;
        List<EtapaProyecto> list = (List<EtapaProyecto>) dao.listado_where_comp(EtapaProyecto.class, "etapaProyectoPK", "folioProyecto", proyecto.getProyectoPK().getFolioProyecto());
        
        for (EtapaProyecto e : list) {
            frase.add(new Phrase(Constantes.tipoFrase(new String[]{String.format("3.1.%d %s", new Object[]{i, e.getCatEtapa().getEtapaDescripcion()})}), Constantes.FUENTE.TITULO3));
            
            frase.add(Chunk.NEWLINE);
            frase.add(new Phrase(String.format("Esta etapa tedrá una duración de %d años y %d meses", new Object[]{e.getAnios(), e.getMeses()}), Constantes.FUENTE.PARRAFO));
            frase.add(Chunk.NEWLINE);
            frase.add(new Phrase(String.format("3.1.%d.1 Agregue cada una de las obras o actividades con la descripción de las mismas", new Object[]{i}), Constantes.FUENTE.TITULO4));
            frase.add(Chunk.NEWLINE);
            frase.add(Constantes.htmlConverter(mapa, e.getEtapaDescripcionObraAct()));
            frase.add(Chunk.NEWLINE);  
            frase.add(Constantes.anexo(url,
                    new Short("3"),
                    new Short("1"),
                    new Integer(e.getCatEtapa().getEtapaId() + 2).shortValue(),
                    new Short("1"),
                    proyecto.getProyectoPK().getFolioProyecto(), 
                    proyecto.getProyectoPK().getSerialProyecto()));
            System.out.println("etapa: 3,1" + new Integer(e.getCatEtapa().getEtapaId() + 2).shortValue());
            frase.add(Chunk.NEWLINE);
            i++;
        }
        parrafo.add(frase);
        
        //3.1.6. Programa de abandono del sitio
//        frase.add(new Chunk("3.1."+i+" Programa de abandono del sitio", Constantes.FUENTE.TITULO3));
//        frase.add(Chunk.NEWLINE);
//        parrafo.add(frase);
//        frase = new Phrase();
//        i++;
                //System.out.println("3.1.7");
        i++;   
        
       
        PojoATabla pojoATablaSust = new PojoATabla();
        frase = new Phrase();
        //3.1.7 Identificación de las sustancias o productos que van a emplearse y que podrían provocar un impacto al ambiente, así como sus características físicas y químicas
        chk = new Chunk("3.2 Identificación de las sustancias o productos que van a emplearse y que podrían provocar un impacto al ambiente, así como sus características físicas y químicas", Constantes.FUENTE.TITULO3);
        chk.setLocalDestination("3.2");
        frase.add(chk);
        
        frase.add(Chunk.NEWLINE);
        parrafo.add(frase);
        parrafo.add(pojoATablaSust.sustancias(proyecto.getProyectoPK().getFolioProyecto(), url));
        frase = new Phrase();
        i++;
        
        //System.out.println("3.1.8");
        
        //3.1.8 Identificación y estimación de las emisiones, descargas y residuos cuya generación se prevea, así como medidas de control que se pretendan llevar a cabo
        chk = new Chunk("3.3 Identificación y estimación de las emisiones, descargas y residuos cuya generación se prevea, así como medidas de control que se pretendan llevar a cabo", Constantes.FUENTE.TITULO3);
        chk.setLocalDestination("3.3");
        frase.add(chk);
        
        frase.add(Chunk.NEWLINE);
        parrafo.add(frase);
        parrafo.add(pojoATabla.emisionResDesc(proyecto.getProyectoPK().getFolioProyecto()));
        frase = new Phrase();
        //3.2 Descripción del ambiente y, en su caso, la identificación de otras fuentes de emisión de contaminantes existentes en el área de influencia del proyecto
        chk = new Chunk("3.4 Descripción del ambiente y, en su caso, la identificación de otras fuentes de emisión de contaminantes existentes en el área de influencia del proyecto", Constantes.FUENTE.TITULO2);
        chk.setLocalDestination("3.4");
        frase.add(chk);
        frase.add(Chunk.NEWLINE);
        frase.add(Chunk.NEWLINE);
        parrafo.add(frase);
        parrafo.add(Constantes.htmlConverter(mapa, proyecto.getProyDescAmbiente()));
        frase = new Phrase();
        frase.add(Chunk.NEWLINE);
        parrafo.add(frase);
        frase = new Phrase();
        //3.3 Identificación de los impactos ambientales significativos o relevantes y determinación de las acciones y medidas para su prevención y mitigación
        chk = new Chunk("3.5 Identificación de los impactos ambientales significativos o relevantes y determinación de las acciones y medidas para su prevención y mitigación", Constantes.FUENTE.TITULO2);
        chk.setLocalDestination("3.5");
        frase.add(chk);
        frase.add(Chunk.NEWLINE);
        parrafo.add(frase);        
        parrafo.add(pojoATabla.impactos(proyecto.getProyectoPK().getFolioProyecto(), 1));        
        parrafo.add(Constantes.anexo(url, new Short("3"), new Short("5"), new Short("1"), new Short("1"), proyecto.getProyectoPK().getFolioProyecto(), proyecto.getProyectoPK().getSerialProyecto()));
       
        frase = new Phrase();
        frase.add(Chunk.NEWLINE);
        //3.4 Medidas Preventivas, de Mitigación y/o Compensación
        chk = new Chunk("3.6 Medidas Preventivas, de Mitigación y/o Compensación", Constantes.FUENTE.TITULO2);
        chk.setLocalDestination("3.6");
        frase.add(chk);
        frase.add(Chunk.NEWLINE);        
        parrafo.add(frase);
        parrafo.add(pojoATabla.impactos(proyecto.getProyectoPK().getFolioProyecto(), 2));
        frase = new Phrase("3.6.1 Descripción de las medidas de compensación o desarrollo de actividades destinadas a la preservación", Constantes.FUENTE.TITULO3);
        frase.add(Chunk.NEWLINE);
        frase.add(Chunk.NEWLINE);
        parrafo.add(frase);
        parrafo.add(Constantes.htmlConverter(mapa, proyecto.getProyDescMedidasCompen()));
        frase = new Phrase();
        frase.add(Chunk.NEWLINE);
        frase.add(new Phrase("3.6.2 Descripción de la protección o conservación de ecosistema que requieran de implementación de dichas actividades", Constantes.FUENTE.TITULO3));
        frase.add(Chunk.NEWLINE);
        parrafo.add(frase);
        parrafo.add(Constantes.htmlConverter(mapa, proyecto.getProyDescProtecConserv()));

        return parrafo;
    }//</editor-fold>

    // <editor-fold defaultstate="collapsed" desc="Capítulo IV(ERA)">
    /**
     *
     * @return @throws IOException
     */
    public Paragraph capituloIV() throws IOException {
        frase = new Phrase();
        parrafo = new Paragraph();
        if (proyecto.getProySupuesto() == '1') {
            if (proyecto.getProyNormaId().equals(new Short("129"))) {
                EstudioRiesgoProyecto estudio = proyectoDao.cargaEstudioRiesgo(proyecto);
                parrafo.add(Chunk.NEXTPAGE);
                chk = new Chunk(CAPITULO_IV, Constantes.FUENTE.TITULO1);
                chk.setLocalDestination("cap4");
                frase.add(chk);
                frase.add(Chunk.NEWLINE);
                frase.add(Chunk.NEWLINE);//fin ancla a cap2
                //4.1 Datos generales del responsable del estudio de riesgo
                chk = new Chunk("4.1 Datos generales del responsable del estudio de riesgo", Constantes.FUENTE.TITULO2);
                chk.setLocalDestination("4.1");
                frase.add(chk);
                frase.add(Chunk.NEWLINE);
                parrafo.add(frase);
                frase = new Phrase();
                //4.1.1 Nombre del responsable del Estudio de Riesgo, así como el Registro Federal de Contribuyentes y CURP
                frase.add(new Chunk("4.1.1 Nombre del responsable del Estudio de Riesgo, así como el Registro Federal de Contribuyentes", Constantes.FUENTE.TITULO3));
                frase.add(Chunk.NEWLINE);
                frase.add(new Chunk(estudio.getEstudioNomRespEla() != null ? estudio.getEstudioNomRespEla() : ""));
                frase.add(Chunk.NEWLINE);
                frase.add(new Chunk(estudio.getEstudioRfcRespEla() != null ? estudio.getEstudioRfcRespEla() : ""));
                frase.add(Chunk.NEWLINE);
                parrafo.add(frase);
                frase = new Phrase();
                //4.1.2 Nombre y c
                frase.add(new Chunk("4.1.2 Nombre y cargo del representante legal, así como el Registro Federal de Contribuyentes y CURP", Constantes.FUENTE.TITULO3));
                frase.add(Chunk.NEWLINE);
                frase.add(new Chunk(estudio.getEstudioNomRpteLegal() != null ? estudio.getEstudioNomRpteLegal() : ""));
                frase.add(Chunk.NEWLINE);
                frase.add(new Chunk(estudio.getEstudioRfcRpte() != null ? estudio.getEstudioRfcRpte() : ""));
                frase.add(Chunk.NEWLINE);
                parrafo.add(frase);
                frase = new Phrase();
                //4.2 Escenarios de los riesgos ambientales relacionados con el proyecto
                chk = new Chunk("4.2 Escenarios de los riesgos ambientales relacionados con el proyecto", Constantes.FUENTE.TITULO2);
                chk.setLocalDestination("4.2");
                frase.add(chk);
                frase.add(Chunk.NEWLINE);
                parrafo.add(frase);
                frase = new Phrase();
                //4.2.1 Descripción del Sistema de Transporte
                frase.add(new Chunk("4.2.1 Descripción del Sistema de Transporte", Constantes.FUENTE.TITULO3));
                frase.add(Chunk.NEWLINE);
                parrafo.add(frase);                
                parrafo.add(Constantes.htmlConverter(mapa, estudio.getEstudioDescSistTansp()));
                parrafo.add(Constantes.anexo(url, new Short("4"), new Short("2"), new Short("1"), new Short("1"), proyecto.getProyectoPK().getFolioProyecto(), proyecto.getProyectoPK().getSerialProyecto()));
                frase = new Phrase();
                //4.2.2 Bases de Diseño
                frase.add(Chunk.NEWLINE);
                frase.add(new Chunk("4.2.2 Bases de Diseño", Constantes.FUENTE.TITULO3));
                frase.add(Chunk.NEWLINE);
                parrafo.add(frase);                
                parrafo.add(Constantes.htmlConverter(mapa, estudio.getEstudioDescBasesDise()));
                parrafo.add(Constantes.anexo(url, new Short("4"), new Short("2"), new Short("2"), new Short("1"), proyecto.getProyectoPK().getFolioProyecto(), proyecto.getProyectoPK().getSerialProyecto()));
                frase = new Phrase();
                frase.add(Chunk.NEWLINE);
                //b4-Hojas de seguridad .- 4.2.3 Condiciones de Operación
                frase.add(new Chunk("4.2.3 Condiciones de Operación", Constantes.FUENTE.TITULO3));
                frase.add(Chunk.NEWLINE);
                parrafo.add(frase);                
                parrafo.add(Constantes.htmlConverter(mapa, estudio.getEstudioOperacion()));
                parrafo.add(Constantes.anexo(url, new Short("4"), new Short("2"), new Short("4"), new Short("1"), proyecto.getProyectoPK().getFolioProyecto(), proyecto.getProyectoPK().getSerialProyecto()));
                frase = new Phrase();
                //4.2.4 Pruebas de Verificación
                frase.add(Chunk.NEWLINE);
                frase.add(new Chunk("4.2.4 Pruebas de Verificación", Constantes.FUENTE.TITULO3));
                frase.add(Chunk.NEWLINE);
                parrafo.add(frase);                
                parrafo.add(Constantes.htmlConverter(mapa, estudio.getEstudioPruebasVerificacion()));
                parrafo.add(Constantes.anexo(url, new Short("4"), new Short("2"), new Short("5"), new Short("1"), proyecto.getProyectoPK().getFolioProyecto(), proyecto.getProyectoPK().getSerialProyecto()));
                frase = new Phrase();
                //4.2.5 Procedimientos y Medidas de Seguridad
                frase.add(Chunk.NEWLINE);
                frase.add(new Chunk("4.2.5 Procedimientos y Medidas de Seguridad", Constantes.FUENTE.TITULO3));
                frase.add(Chunk.NEWLINE);
                parrafo.add(frase);                
                parrafo.add(Constantes.htmlConverter(mapa, estudio.getEstudioProcedimientosMedidas()));
                parrafo.add(Constantes.anexo(url, new Short("4"), new Short("2"), new Short("6"), new Short("1"), proyecto.getProyectoPK().getFolioProyecto(), proyecto.getProyectoPK().getSerialProyecto()));
                frase = new Phrase();
                //4.2.6 Análisis y Evaluación de Riesgos
                frase.add(Chunk.NEWLINE);
                frase.add(new Chunk("4.2.6 Análisis y Evaluación de Riesgos", Constantes.FUENTE.TITULO3));
                frase.add(Chunk.NEWLINE);
                parrafo.add(frase);                
                parrafo.add(Constantes.htmlConverter(mapa, estudio.getEstudioInteraccionesRiesgo()));
                parrafo.add(Constantes.anexo(url, new Short("4"), new Short("2"), new Short("7"), new Short("1"), proyecto.getProyectoPK().getFolioProyecto(), proyecto.getProyectoPK().getSerialProyecto()));
                frase = new Phrase();
                //4.2.7 Metodologías de Identificación y Jerarquización
                frase.add(Chunk.NEWLINE);
                frase.add(new Chunk("4.2.7 Metodologías de Identificación y Jerarquización", Constantes.FUENTE.TITULO3));
                frase.add(Chunk.NEWLINE);
                parrafo.add(frase);                
                parrafo.add(Constantes.htmlConverter(mapa, estudio.getEstudioMetodologiaIdenJerar()));
                parrafo.add(Constantes.anexo(url, new Short("4"), new Short("2"), new Short("8"), new Short("1"), proyecto.getProyectoPK().getFolioProyecto(), proyecto.getProyectoPK().getSerialProyecto()));
                frase = new Phrase();
                frase.add(Chunk.NEWLINE);
                //4.3 Descripción de las medidas de protección en torno a las instalaciones
                chk = new Chunk("4.3 Descripción de las medidas de protección en torno a las instalaciones", Constantes.FUENTE.TITULO2);
                chk.setLocalDestination("4.3");
                frase.add(chk);
                frase.add(Chunk.NEWLINE);
                parrafo.add(frase);
                frase = new Phrase();
                //4.3.1 Radios Ponteciales de Afectación
                frase.add(new Chunk("4.3.1 Radios Ponteciales de Afectación", Constantes.FUENTE.TITULO3));
                frase.add(Chunk.NEWLINE);
                parrafo.add(frase);
                
                parrafo.add(Constantes.htmlConverter(mapa, estudio.getEstudioRadiosPotenciales()));
                parrafo.add(Constantes.anexo(url, new Short("4"), new Short("3"), new Short("1"), new Short("1"), proyecto.getProyectoPK().getFolioProyecto(), proyecto.getProyectoPK().getSerialProyecto()));
                frase = new Phrase();
                frase.add(Chunk.NEWLINE);
                //4.3.2 Interacciones de Riesgo
                frase.add(new Chunk("4.3.2 Interacciones de Riesgo", Constantes.FUENTE.TITULO3));
                frase.add(Chunk.NEWLINE);
                parrafo.add(frase);                
                parrafo.add(Constantes.htmlConverter(mapa, estudio.getEstudioInteraccionesRiesgo()));
                parrafo.add(Constantes.anexo(url, new Short("4"), new Short("3"), new Short("2"), new Short("1"), proyecto.getProyectoPK().getFolioProyecto(), proyecto.getProyectoPK().getSerialProyecto()));
                frase = new Phrase();
                frase.add(Chunk.NEWLINE);
                //4.3.3 Efectos Sobre el Área de Influencia
                frase.add(new Chunk("4.3.3 Efectos Sobre el Área de Influencia", Constantes.FUENTE.TITULO3));
                frase.add(Chunk.NEWLINE);
                parrafo.add(frase);                
                parrafo.add(Constantes.htmlConverter(mapa, estudio.getEstudioEfectosAreaInflu()));
                parrafo.add(Constantes.anexo(url, new Short("4"), new Short("3"), new Short("3"), new Short("1"), proyecto.getProyectoPK().getFolioProyecto(), proyecto.getProyectoPK().getSerialProyecto()));
                frase = new Phrase();
                frase.add(Chunk.NEWLINE);
                //4.4 Señalamiento de las medidas de seguridad y preventivas en materia ambiental
                chk = new Chunk("4.4 Señalamiento de las medidas de seguridad y preventivas en materia ambiental", Constantes.FUENTE.TITULO2);
                chk.setLocalDestination("4.4");
                frase.add(chk);
                frase.add(Chunk.NEWLINE);
                parrafo.add(frase);
                frase = new Phrase();
                //4.4.1 Recomendaciones Técnico-Operativas
                frase.add(new Chunk("4.4.1 Recomendaciones Técnico-Operativas", Constantes.FUENTE.TITULO3));
                frase.add(Chunk.NEWLINE);
                parrafo.add(frase);                
                parrafo.add(Constantes.htmlConverter(mapa, estudio.getEstudioRecomendacionesTecOp()));
                parrafo.add(Constantes.anexo(url, new Short("4"), new Short("4"), new Short("1"), new Short("1"), proyecto.getProyectoPK().getFolioProyecto(), proyecto.getProyectoPK().getSerialProyecto()));
                frase = new Phrase();
                frase.add(Chunk.NEWLINE);
                //4.4.2 Sistemas de Seguridad
                frase.add(new Chunk("4.4.2 Sistemas de Seguridad", Constantes.FUENTE.TITULO3));
                frase.add(Chunk.NEWLINE);
                parrafo.add(frase);                
                parrafo.add(Constantes.htmlConverter(mapa, estudio.getEstudioSistemasSeguridad()));
                parrafo.add(Constantes.anexo(url, new Short("4"), new Short("4"), new Short("2"), new Short("1"), proyecto.getProyectoPK().getFolioProyecto(), proyecto.getProyectoPK().getSerialProyecto()));
                frase = new Phrase();
                frase.add(Chunk.NEWLINE);
                //4.4.3 Medidas Preventivas
                frase.add(new Chunk("4.4.3 Medidas Preventivas", Constantes.FUENTE.TITULO3));
                frase.add(Chunk.NEWLINE);
                parrafo.add(frase);                
                parrafo.add(Constantes.htmlConverter(mapa, estudio.getEstudioMedidasPreventivas()));
                parrafo.add(Constantes.anexo(url, new Short("4"), new Short("4"), new Short("3"), new Short("1"), proyecto.getProyectoPK().getFolioProyecto(), proyecto.getProyectoPK().getSerialProyecto()));
                frase = new Phrase();
                frase.add(Chunk.NEWLINE);
                //4.5 Resumen
                chk = new Chunk("4.5 Resumen", Constantes.FUENTE.TITULO2);
                chk.setLocalDestination("4.5");
                frase.add(chk);
                frase.add(Chunk.NEWLINE);
                parrafo.add(frase);
                parrafo.add(Constantes.htmlConverter(mapa, estudio.getEstudioEfectosAreaInflu()));
                frase = new Phrase();
                //4.5.1 Conclusiones del Estudio de Riesgo Ambiental
                frase.add(new Chunk("4.5.1 Conclusiones del Estudio de Riesgo Ambiental", Constantes.FUENTE.TITULO3));
                frase.add(Chunk.NEWLINE);
                parrafo.add(frase);                
                parrafo.add(Constantes.htmlConverter(mapa, estudio.getEstudioSenalaConclusion()));
                parrafo.add(Constantes.anexo(url, new Short("4"), new Short("5"), new Short("1"), new Short("1"), proyecto.getProyectoPK().getFolioProyecto(), proyecto.getProyectoPK().getSerialProyecto()));
                frase = new Phrase();
                frase.add(Chunk.NEWLINE);
                //4.5.2 Resumen de la Situación General que Presenta el Proyecto en Materia de Riesgo Ambiental
                frase.add(new Chunk("4.5.2 Resumen de la Situación General que Presenta el Proyecto en Materia de Riesgo Ambiental", Constantes.FUENTE.TITULO3));
                frase.add(Chunk.NEWLINE);
                parrafo.add(frase);                
                parrafo.add(Constantes.htmlConverter(mapa, estudio.getEstudioResumenSituacion()));
                parrafo.add(Constantes.anexo(url, new Short("4"), new Short("5"), new Short("2"), new Short("1"), proyecto.getProyectoPK().getFolioProyecto(), proyecto.getProyectoPK().getSerialProyecto()));
                frase = new Phrase();
                frase.add(Chunk.NEWLINE);
                //4.5.3 Informe Técnico Debidamente Llenado
                frase.add(new Chunk("4.5.3 Informe Técnico Debidamente Llenado", Constantes.FUENTE.TITULO3));
                frase.add(Chunk.NEWLINE);
                parrafo.add(frase);                
                parrafo.add(Constantes.htmlConverter(mapa, estudio.getEstudioInformeTecnico()));
                parrafo.add(Constantes.anexo(url, new Short("4"), new Short("5"), new Short("3"), new Short("1"), proyecto.getProyectoPK().getFolioProyecto(), proyecto.getProyectoPK().getSerialProyecto()));
                
                frase = new Phrase();
                frase.add(Chunk.NEWLINE);
                //4.6 Identificación de los Instrumentos
                chk = new Chunk("4.6 Identificación de los Instrumentos", Constantes.FUENTE.TITULO2);
                chk.setLocalDestination("4.6");
                frase.add(chk);
                frase.add(Chunk.NEWLINE);
                parrafo.add(frase);
                
                frase = new Phrase();
                frase.add(new Chunk("4.6.1 Planos de localización y Fotografías", Constantes.FUENTE.TITULO3));
                frase.add(Chunk.NEWLINE);
                parrafo.add(frase);
                parrafo.add(Constantes.anexo(url, new Short("4"), new Short("6"), new Short("1"), new Short("1"), proyecto.getProyectoPK().getFolioProyecto(), proyecto.getProyectoPK().getSerialProyecto()));
                
                frase = new Phrase();
                frase.add(Chunk.NEWLINE);
                frase.add(new Chunk("4.6.2 Otros Anexos", Constantes.FUENTE.TITULO3));
                frase.add(Chunk.NEWLINE);
                parrafo.add(frase);
                parrafo.add(Constantes.anexo(url, new Short("4"), new Short("6"), new Short("2"), new Short("1"), proyecto.getProyectoPK().getFolioProyecto(), proyecto.getProyectoPK().getSerialProyecto()));
            }
        }
        return parrafo;
    }//</editor-fold>

    // <editor-fold defaultstate="collapsed" desc="Clase proveedora de fuentes">
    /**
     * estandariza fuentes para los campos con HTML
     */
    public class ProveedorFuente implements FontProvider {

        public boolean isRegistered(String string) {
            return false;
        }

        public Font getFont(String string, String string1, boolean bln, float f, int i, BaseColor bc) {
            return Constantes.FUENTE.PARRAFO;
        }
    }//</editor-fold>
}
