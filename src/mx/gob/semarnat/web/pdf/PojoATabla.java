/*
 To change this license header, choose License Headers in Project Properties.
 To change this template file, choose Tools | Templates
 and open the template in the editor.
 */
package mx.gob.semarnat.web.pdf;
//<editor-fold defaultstate="collapsed" desc="Imports">
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Element;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.Phrase;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import java.math.BigDecimal;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import mx.gob.semarnat.dao.Capitulo1Dao;
import mx.gob.semarnat.dao.Capitulo2Dao;
import mx.gob.semarnat.dao.Capitulo3Dao;
import mx.gob.semarnat.dao.Capitulo3DaoCatalogos;
import mx.gob.semarnat.dao.EspecifiacionComparator;
import mx.gob.semarnat.model.CatClasificacionMineral;
import mx.gob.semarnat.model.CatEtapa;
import mx.gob.semarnat.model.CatMineralesReservados;
import mx.gob.semarnat.model.ContaminanteProyecto;
import mx.gob.semarnat.model.EspecificacionProyecto;
import mx.gob.semarnat.model.EtapaProyecto;
import mx.gob.semarnat.model.ImpactoProyecto;
import mx.gob.semarnat.model.PreguntaMineral;
import mx.gob.semarnat.model.Proyecto;
import mx.gob.semarnat.model.SustanciaProyecto;
import mx.gob.semarnat.model.catalogos.CatTipoAsen;
import mx.gob.semarnat.model.catalogos.CatUnidadMedida;
import mx.gob.semarnat.model.catalogos.CatVialidad;
import mx.gob.semarnat.model.sinatec.Vexdatosusuario;//</editor-fold>

/**
 * Convierte Consultas a tablas
 * @author Rodrigo
 */
public class PojoATabla {

    private  final Capitulo3Dao dao = new Capitulo3Dao();
    private  final Capitulo1Dao daoCap1 = new Capitulo1Dao();
    private  final Capitulo2Dao daoCap2 = new Capitulo2Dao();
    private  final Capitulo3DaoCatalogos daoCat = new Capitulo3DaoCatalogos();
    
    //<editor-fold defaultstate="collapsed" desc="Tabla de especificaciones">
    /**
     *
     * @param folio
     * @param serial
     * @param url
     * @return
     * @throws DocumentException
     */
    public PdfPTable tablaEspecificaciones(String folio, Short serial, String url) throws DocumentException {
        List<EspecificacionProyecto> list = (List<EspecificacionProyecto>) dao.lista_namedQuery(
                "EspecificacionProyecto.findByFolioProyecto",
                new Object[]{folio}, new String[]{"folioProyecto"});
        PdfPTable tabla = new PdfPTable(2);
        tabla.setWidthPercentage(100);
        tabla.setWidths(new float[]{.35f, 2});
        tabla.setSpacingBefore(15f);
        PdfPCell celda = new PdfPCell(new Phrase("Numeral", Constantes.FUENTE.PARRAFO));
        tabla.addCell(celda);
        celda = new PdfPCell(new Phrase("Especificación/Cumplimiento", Constantes.FUENTE.PARRAFO));
        tabla.addCell(celda);
        String numeral;
        Collections.sort(list, new EspecifiacionComparator());
        for (EspecificacionProyecto ep : list) {
            numeral = ep.getCatEspecificacion().getCatEspecificacionPK().getEspecificacionId();
            if (numeral.contains("\n")) {
                numeral = numeral.replace("\n\n", "\n");
                numeral = numeral.replaceAll("\n", " - ");
            }
            celda = new PdfPCell(new Phrase(numeral, Constantes.FUENTE.PARRAFO));
            if (ep.getCatEspecificacion().getEspecificacionAnexoRequest() == 'N') {
                celda.setRowspan(4);
            } else {
                celda.setRowspan(6);

            }
            tabla.addCell(celda);
            celda = new PdfPCell(new Phrase("Especificación", Constantes.FUENTE.PARRAFO));
            tabla.addCell(celda);
            celda = new PdfPCell(new Phrase(ep.getCatEspecificacion().getEspecificacionDescripcion(), Constantes.FUENTE.PARRAFO));
            tabla.addCell(celda);
            celda = new PdfPCell(new Phrase("Cumplimiento", Constantes.FUENTE.PARRAFO));
            tabla.addCell(celda);
            celda = new PdfPCell(new Phrase(ep.getEspecificacionJustificacion(), Constantes.FUENTE.PARRAFO));
            tabla.addCell(celda);
            if (ep.getCatEspecificacion().getEspecificacionAnexoRequest() == 'S') {
                celda = new PdfPCell(new Phrase("Anexo", Constantes.FUENTE.PARRAFO));
                tabla.addCell(celda);
                celda = new PdfPCell(Constantes.anexoEspecificacion(url, folio, ep.getCatEspecificacion().getCatEspecificacionPK().getEspecificacionId(), serial, ep.getCatEspecificacion().getCatNorma().getNormaId()));
                tabla.addCell(celda);
            }
        }
        return tabla;
    }//</editor-fold>

    //<editor-fold defaultstate="collapsed" desc="Etapas">
    /**
     *
     * @param folio
     * @return Tabla con las etapas del proyecto
     * @throws DocumentException
     */
    public PdfPTable tablaEtapas(String folio) throws DocumentException {
        PdfPTable tabla = new PdfPTable(2);
        tabla.setWidthPercentage(100);
        PdfPCell celda;
        List<EtapaProyecto> list = (List<EtapaProyecto>) dao.listado_where_comp(EtapaProyecto.class, "etapaProyectoPK", "folioProyecto", folio);
        if (!list.isEmpty()) {
            tabla.setWidths(new float[]{.4f, .25f});
            tabla.setSpacingBefore(10f);
            tabla.setSpacingAfter(10f);
            celda = new PdfPCell(new Phrase("Etapa", Constantes.FUENTE.TITULO3));
            celda.setHorizontalAlignment(Element.ALIGN_CENTER);
            tabla.addCell(celda);
            celda = new PdfPCell(new Phrase("Duración", Constantes.FUENTE.TITULO3));
            celda.setHorizontalAlignment(Element.ALIGN_CENTER);
            tabla.addCell(celda);
            for (EtapaProyecto ep : list) {
                celda = new PdfPCell(new Phrase(ep.getCatEtapa().getEtapaDescripcion(), Constantes.FUENTE.PARRAFO));
                tabla.addCell(celda);
                celda = new PdfPCell(new Phrase(ep.getAnios() + " años y " + ep.getMeses() + " meses.", Constantes.FUENTE.PARRAFO));
                tabla.addCell(celda);
            }
        }
        return tabla;
    }//</editor-fold>

    //<editor-fold defaultstate="collapsed" desc="Tabla de emisiones, residuos y descargas">
    /**
     *
     * @param folio
     * @return
     */
    public  PdfPTable emisionResDesc(String folio) {
        PdfPTable tabla = new PdfPTable(3);
        tabla.setSpacingBefore(10f);
        tabla.setSpacingAfter(10f);
        PdfPCell celda;
        tabla.setWidthPercentage(100);
        List<ContaminanteProyecto> cpro = (List<ContaminanteProyecto>) dao.listado_where_comp(ContaminanteProyecto.class, "contaminanteProyectoPK",
                "folioProyecto", folio);
        if (!cpro.isEmpty()) {
            for (ContaminanteProyecto cp : cpro) {
                celda = new PdfPCell(new Phrase("Etapa", Constantes.FUENTE.PARRAFO_NEGRITA));
                celda.setHorizontalAlignment(Element.ALIGN_CENTER);
                tabla.addCell(celda);
                celda = new PdfPCell(new Phrase("Emisión", Constantes.FUENTE.PARRAFO_NEGRITA));
                celda.setHorizontalAlignment(Element.ALIGN_CENTER);
                tabla.addCell(celda);
                celda = new PdfPCell(new Phrase("Cantidad", Constantes.FUENTE.PARRAFO_NEGRITA));
                celda.setHorizontalAlignment(Element.ALIGN_CENTER);
                tabla.addCell(celda);
                celda = new PdfPCell(new Phrase(cp.getCatEtapa().getEtapaDescripcion(), Constantes.FUENTE.PARRAFO));
                tabla.addCell(celda);
                celda = new PdfPCell(new Phrase(cp.getCatContaminante().getContaminanteNombre(), Constantes.FUENTE.PARRAFO));
                tabla.addCell(celda);
                celda = new PdfPCell(new Phrase(cp.getContaminanteCantidad() + " " + ((CatUnidadMedida) daoCat.busca(CatUnidadMedida.class, cp.getCtunClave())).getCtunDesc(), Constantes.FUENTE.PARRAFO));
                tabla.addCell(celda);
                celda = new PdfPCell(new Phrase("Medida de Control", Constantes.FUENTE.PARRAFO_NEGRITA));
                celda.setHorizontalAlignment(Element.ALIGN_CENTER);
                celda.setColspan(3);
                tabla.addCell(celda);
                celda = new PdfPCell(new Phrase(cp.getContaminanteMedidaControl(), Constantes.FUENTE.PARRAFO));
                celda.setColspan(3);
                tabla.addCell(celda);

            }
        }
        return tabla;
    }//</editor-fold>

    //<editor-fold defaultstate="collapsed" desc="Impactos">
    /**
     *
     * @param folio
     * @param tipo
     * @return
     */
    public  PdfPTable impactos(String folio, int tipo) {
        PdfPTable tabla = new PdfPTable(1);
        if (tipo == 1) {
            tabla = new PdfPTable(4);
            tabla.setWidthPercentage(100);
            tabla.setSpacingBefore(10f);
            tabla.setSpacingAfter(15f);
            PdfPCell celda;
            int i = 0, j = 1;
            HashMap<CatEtapa, Integer> relacionEtapas = new HashMap();
            CatEtapa ep = new CatEtapa();
            List<ImpactoProyecto> listaImp = (List<ImpactoProyecto>) dao.listado_where_comp(ImpactoProyecto.class, "impactoProyectoPK",
                    "folioProyecto", folio, "etapaId");
            if (!listaImp.isEmpty()) {
                for (ImpactoProyecto imp : listaImp) {
                    if (j == 1) {
                        ep = imp.getEtapaId();
                    }
                    if (!imp.getEtapaId().equals(ep) || j == listaImp.size()) {
                        relacionEtapas.put(ep, i);
                        ep = imp.getEtapaId();
                        i = 0;
                        if (j == listaImp.size()) {
                            i++;
                            relacionEtapas.put(ep, i);
                        }
                    } else {
                        i++;
                    }
                    j++;
                }
                i = 0;

                celda = new PdfPCell(new Phrase("Etapa", Constantes.FUENTE.PARRAFO_NEGRITA));
                tabla.addCell(celda);
                celda = new PdfPCell(new Phrase("Impacto Ambiental", Constantes.FUENTE.PARRAFO_NEGRITA));
                tabla.addCell(celda);
                celda = new PdfPCell(new Phrase("Medida de mitigación", Constantes.FUENTE.PARRAFO_NEGRITA));
                tabla.addCell(celda);
                celda = new PdfPCell(new Phrase("Medida adicional a los impactos", Constantes.FUENTE.PARRAFO_NEGRITA));
                tabla.addCell(celda);
                for (ImpactoProyecto imp : (List<ImpactoProyecto>) dao.listado_where_comp(ImpactoProyecto.class, "impactoProyectoPK",
                        "folioProyecto", folio, "etapaId")) {
                    if (i == 0) {
                        ep = imp.getEtapaId();
                        celda = new PdfPCell(new Phrase((Constantes.tipoFrase(new String[]{imp.getEtapaId().getEtapaDescripcion()})), Constantes.FUENTE.PARRAFO_NEGRITA));
                        celda.setRowspan(relacionEtapas.get(imp.getEtapaId()));
                        System.out.println("filas del span" + relacionEtapas.get(imp.getEtapaId()));
                        tabla.addCell(celda);
                        i++;
                    }
                    if (!imp.getEtapaId().equals(ep)) {
                        celda = new PdfPCell(new Phrase((Constantes.tipoFrase(new String[]{imp.getEtapaId().getEtapaDescripcion()})), Constantes.FUENTE.PARRAFO_NEGRITA));
                        celda.setRowspan(relacionEtapas.get(imp.getEtapaId()));
                        System.out.println("filas del span" + relacionEtapas.get(imp.getEtapaId()));
                        tabla.addCell(celda);
                        ep = imp.getEtapaId();
                    }

                    celda = new PdfPCell(new Phrase(imp.getImpactoDescripcion(), Constantes.FUENTE.PARRAFO));
                    tabla.addCell(celda);
                    celda = new PdfPCell(new Phrase(imp.getImpactoMedidaMitigacion(), Constantes.FUENTE.PARRAFO));
                    tabla.addCell(celda);
                    if (imp.getImpactoMedidaAdicional() != null) {
                        celda = new PdfPCell(new Phrase(imp.getImpactoMedidaAdicional(), Constantes.FUENTE.PARRAFO));
                        tabla.addCell(celda);
                    } else {
                        celda = new PdfPCell(new Phrase("N/A", Constantes.FUENTE.PARRAFO));
                        tabla.addCell(celda);

                    }
                
                    
                    
                    
                    
                }
                return tabla;
            } else {
                tabla = new PdfPTable(3);
                tabla.setWidthPercentage(100);
                tabla.setSpacingBefore(10f);
                tabla.setSpacingAfter(15f);
                i = 0;
                ep = new CatEtapa();
                for (ImpactoProyecto imp : (List<ImpactoProyecto>) dao.listado_where_comp(ImpactoProyecto.class, "impactoProyectoPK",
                        "folioProyecto", folio, "etapaId")) {
                    if (i == 0) {
                        ep = imp.getEtapaId();
                        celda = new PdfPCell(new Phrase((Constantes.tipoFrase(new String[]{imp.getEtapaId().getEtapaDescripcion()})), Constantes.FUENTE.PARRAFO_NEGRITA));
                        celda.setColspan(3);
                        tabla.addCell(celda);
                        celda = new PdfPCell(new Phrase("Impacto Ambiental", Constantes.FUENTE.PARRAFO_NEGRITA));
                        tabla.addCell(celda);
                        celda = new PdfPCell(new Phrase("Medida de mitigación", Constantes.FUENTE.PARRAFO_NEGRITA));
                        tabla.addCell(celda);
                        celda = new PdfPCell(new Phrase("Medida adicional a los impactos", Constantes.FUENTE.PARRAFO_NEGRITA));
                        tabla.addCell(celda);
                        i++;
                    }
                    if (!imp.getEtapaId().equals(ep)) {
                        celda = new PdfPCell(new Phrase((Constantes.tipoFrase(new String[]{imp.getEtapaId().getEtapaDescripcion()})), Constantes.FUENTE.PARRAFO_NEGRITA));
                        celda.setColspan(3);
                        tabla.addCell(celda);
                        celda = new PdfPCell(new Phrase("Impacto Ambiental", Constantes.FUENTE.PARRAFO_NEGRITA));
                        tabla.addCell(celda);
                        celda = new PdfPCell(new Phrase("Medida de mitigación", Constantes.FUENTE.PARRAFO_NEGRITA));
                        tabla.addCell(celda);
                        celda = new PdfPCell(new Phrase("Medida adicional a los impactos", Constantes.FUENTE.PARRAFO_NEGRITA));
                        tabla.addCell(celda);
                        ep = imp.getEtapaId();
                    }

//                celda = new PdfPCell(new Phrase(Constantes.tipoFrase(new String[]{imp.getEtapaId().getEtapaDescripcion()}), Constantes.FUENTE.PARRAFO));
//                tabla.addCell(celda);
                    celda = new PdfPCell(new Phrase(imp.getImpactoDescripcion(), Constantes.FUENTE.PARRAFO));
                    tabla.addCell(celda);
                    celda = new PdfPCell(new Phrase(imp.getImpactoMedidaMitigacion(), Constantes.FUENTE.PARRAFO));
                    tabla.addCell(celda);
                    if (imp.getImpactoMedidaAdicional() != null) {
                        celda = new PdfPCell(new Phrase(imp.getImpactoMedidaAdicional(), Constantes.FUENTE.PARRAFO));
                        tabla.addCell(celda);
                    } else {
                        celda = new PdfPCell(new Phrase("N/A", Constantes.FUENTE.PARRAFO));
                        tabla.addCell(celda);

                    }
                }
                return tabla;
            }
        }
        return tabla;
    }//</editor-fold>

    //<editor-fold defaultstate="collapsed" desc="Supuestos">
    /**
     *
     * @param supuesto
     * @return
     * @throws com.itextpdf.text.DocumentException
     */
    public PdfPTable supuestos(Character supuesto) throws DocumentException {
        PdfPTable tabla = new PdfPTable(4);
        tabla.setWidthPercentage(100);
        tabla.setWidths(new float[]{.5f, .1f, .1f, 2});
        tabla.setSpacingBefore(10f);
        tabla.setSpacingAfter(15f);
        PdfPCell celda;
        //inicio tabla
        celda = new PdfPCell(new Phrase("Las obras y/o actividades se ajustan a:", Constantes.FUENTE.PARRAFO_NEGRITA));
        celda.setRowspan(3);
        tabla.addCell(celda);
        celda = new PdfPCell(new Phrase(supuesto.equals('1') ? "X" : "", Constantes.FUENTE.TITULO2));
        tabla.addCell(celda);
        celda = new PdfPCell(new Phrase("I", Constantes.FUENTE.PARRAFO));
        tabla.addCell(celda);
        celda = new PdfPCell(new Phrase("Existan normas oficiales mexicanas u otras disposiciones que regulen las emisiones, las descargas, el aprovechamiento de recursos naturales y en general, todos los impactos ambientales relevantes que pueden producir las obras o actividades.", Constantes.FUENTE.PARRAFO));
        tabla.addCell(celda);
        celda = new PdfPCell(new Phrase(supuesto.equals('2') ? "X" : "", Constantes.FUENTE.TITULO2));
        tabla.addCell(celda);
        celda = new PdfPCell(new Phrase("II", Constantes.FUENTE.PARRAFO));
        tabla.addCell(celda);
        celda = new PdfPCell(new Phrase("Se trate de instalaciones ubicadas en parques industriales autorizados por la Secretaría de Medio Ambiente y Recursos Naturales.", Constantes.FUENTE.PARRAFO));
        tabla.addCell(celda);
        celda = new PdfPCell(new Phrase(supuesto.equals('3') ? "X" : "", Constantes.FUENTE.TITULO2));
        tabla.addCell(celda);
        celda = new PdfPCell(new Phrase("III", Constantes.FUENTE.PARRAFO));
        tabla.addCell(celda);
        celda = new PdfPCell(new Phrase("Las obras o actividades de que se trate estén expresamente previstas por un plan parcial de desarrollo urbano o de ordenamiento ecológico que haya sido evaluado por la Secretaría de Medio Ambiente y Recursos Naturales en los términos del artículo 32 de la Ley General del Equilibrio Ecológico y la Protección al Ambiente.", Constantes.FUENTE.PARRAFO));
        tabla.addCell(celda);
        //fin tabla
        return tabla;
    }//</editor-fold>

    //<editor-fold defaultstate="collapsed" desc="Dirección del promovente">
    /**
     *
     * @param promovente
     * @return
     */
    public PdfPTable direccionPromovente(Vexdatosusuario promovente) {
        PdfPTable tabla = new PdfPTable(4);
        tabla.setWidthPercentage(100);
        tabla.setSpacingBefore(10f);
        tabla.setSpacingAfter(15f);
        PdfPCell celda;
        //inicio tabla
        celda = new PdfPCell(new Paragraph(promovente.getVcnombrevialidad(), Constantes.FUENTE.PARRAFO_TITULO));
        tabla.addCell(celda);
        celda = new PdfPCell(new Paragraph(((CatVialidad) daoCat.busca(CatVialidad.class,
                new Short(promovente.getBgcveTipoVialLk().
                        toString()))).getDescripcion(), Constantes.FUENTE.PARRAFO_TITULO));
        tabla.addCell(celda);
        celda = new PdfPCell(new Paragraph(promovente.getVcnumexterior() != null ? promovente.getVcnuminterior() : "S/N", Constantes.FUENTE.PARRAFO_TITULO));
        tabla.addCell(celda);
        celda = new PdfPCell(new Paragraph(promovente.getVcnuminterior() != null ? promovente.getVcnuminterior() : "S/N", Constantes.FUENTE.PARRAFO_TITULO));
        tabla.addCell(celda);
        //
        celda = new PdfPCell(new Paragraph("Vialidad", Constantes.FUENTE.PARRAFO_NEGRITA));
        tabla.addCell(celda);
        celda = new PdfPCell(new Paragraph("Tipo de Vialidad", Constantes.FUENTE.PARRAFO_NEGRITA));
        tabla.addCell(celda);
        celda = new PdfPCell(new Paragraph("Número exterior", Constantes.FUENTE.PARRAFO_NEGRITA));
        tabla.addCell(celda);
        celda = new PdfPCell(new Paragraph("Número interior", Constantes.FUENTE.PARRAFO_NEGRITA));
        tabla.addCell(celda);
        //
        celda = new PdfPCell(new Paragraph(promovente.getVctipoasentamiento(), Constantes.FUENTE.PARRAFO_TITULO));
        tabla.addCell(celda);
        celda = new PdfPCell(new Paragraph(((CatTipoAsen) daoCat.busca(CatTipoAsen.class,
                new Short(promovente.getBgcveTipoAsenLk().
                        toString()))).getNombre(), Constantes.FUENTE.PARRAFO_TITULO));
        tabla.addCell(celda);
        celda = new PdfPCell(new Paragraph(promovente.getVclocalidad(), Constantes.FUENTE.PARRAFO_TITULO));
        tabla.addCell(celda);
        celda = new PdfPCell(new Paragraph(promovente.getVcdCodigoLk(), Constantes.FUENTE.PARRAFO_TITULO));
        tabla.addCell(celda);
        //
        celda = new PdfPCell(new Paragraph("Nombre del asentamiento", Constantes.FUENTE.PARRAFO_NEGRITA));
        tabla.addCell(celda);
        celda = new PdfPCell(new Paragraph("Tipo de asentamiento", Constantes.FUENTE.PARRAFO_NEGRITA));
        tabla.addCell(celda);
        celda = new PdfPCell(new Paragraph("Localidad", Constantes.FUENTE.PARRAFO_NEGRITA));
        tabla.addCell(celda);
        celda = new PdfPCell(new Paragraph("Código Postal", Constantes.FUENTE.PARRAFO_NEGRITA));
        tabla.addCell(celda);
        //fin tabla
        return tabla;
    }//</editor-fold>

    //<editor-fold defaultstate="collapsed" desc="tabla con la ubicación del proyecto">
    /**
     * 
     * @param proyecto
     * @return 
     */
    public PdfPTable ubicacionProyecto(Proyecto proyecto) {
        PdfPTable tabla = new PdfPTable(4);
        tabla.setWidthPercentage(100);
        tabla.setSpacingBefore(10f);
        tabla.setSpacingAfter(15f);
        PdfPCell celda = new PdfPCell(new Phrase(""));
        //inicio tabla
        if (proyecto.getProyNombreAsentamiento() != null) {
            celda = new PdfPCell(new Phrase(!proyecto.getProyNombreAsentamiento().equals("") ? proyecto.getProyNombreAsentamiento() : " ", Constantes.FUENTE.PARRAFO_TITULO));
        }
        celda.setColspan(2);
        tabla.addCell(celda);
        if (proyecto.getProyNombreVialidad() != null) {
            celda = new PdfPCell(new Phrase(!proyecto.getProyNombreVialidad().equals("") ? proyecto.getProyNombreVialidad() : " ", Constantes.FUENTE.PARRAFO_TITULO));
        }
        celda.setColspan(2);
        tabla.addCell(celda);
        celda = new PdfPCell(new Phrase("Asentamiento", Constantes.FUENTE.PARRAFO_NEGRITA));
        celda.setColspan(2);
        tabla.addCell(celda);
        celda = new PdfPCell(new Phrase("Vialidad", Constantes.FUENTE.PARRAFO_NEGRITA));
        celda.setColspan(2);
        tabla.addCell(celda);
        celda = new PdfPCell(new Phrase(proyecto.getProyNumeroExterior() != null ? proyecto.getProyNumeroExterior().toString() : "S/N", Constantes.FUENTE.PARRAFO_TITULO));
        tabla.addCell(celda);
        celda = new PdfPCell(new Phrase(proyecto.getProyNumeroInterior() != null ? proyecto.getProyNumeroInterior().toString() : "S/N", Constantes.FUENTE.PARRAFO_TITULO));
        tabla.addCell(celda);
        celda = new PdfPCell(new Phrase(proyecto.getProyCodigoPostal() != null ? proyecto.getProyCodigoPostal().toString() : " ", Constantes.FUENTE.PARRAFO_TITULO));
        celda.setColspan(2);
        tabla.addCell(celda);
        celda = new PdfPCell(new Phrase("Número exterior", Constantes.FUENTE.PARRAFO_NEGRITA));
        tabla.addCell(celda);
        celda = new PdfPCell(new Phrase("Número interior", Constantes.FUENTE.PARRAFO_NEGRITA));
        tabla.addCell(celda);
        celda = new PdfPCell(new Phrase("Código Postal", Constantes.FUENTE.PARRAFO_NEGRITA));
        celda.setColspan(2);
        tabla.addCell(celda);
        //fin tabla
        return tabla;
    }

    public PdfPTable minerales(String folio, Short serial) {
        PdfPTable tabla = new PdfPTable(2);
        tabla.setWidthPercentage(100);
        tabla.setSpacingBefore(5f);
        tabla.setSpacingAfter(5f);
        PdfPCell celda;
        //inicio tabla
        celda = new PdfPCell(new Phrase("Clasficacion", Constantes.FUENTE.TITULO3));
        celda.setHorizontalAlignment(Element.ALIGN_CENTER);
        tabla.addCell(celda);
        celda = new PdfPCell(new Phrase("Mineral", Constantes.FUENTE.TITULO3));
        celda.setHorizontalAlignment(Element.ALIGN_CENTER);
        tabla.addCell(celda);

        List<PreguntaMineral> m = daoCap1.getMinerales(folio, serial);
        List<CatClasificacionMineral> lista = (List<CatClasificacionMineral>) dao.listado(CatClasificacionMineral.class);

        boolean flag = true;
        Phrase ph = new Phrase();
        celda = new PdfPCell();
        for (CatClasificacionMineral ccm : lista) {
            for (PreguntaMineral t1 : m) {
                CatMineralesReservados t3 = (CatMineralesReservados) dao.busca(CatMineralesReservados.class, t1.getPreguntaMineralPK().getIdMinerales());
                if (flag) {
                    if (ccm.getIdClasificacion().equals(t3.getIdClasificacion())) {
                        celda = new PdfPCell(new Phrase(Constantes.tipoFrase(new String[]{ccm.getDescripcionClasificacion()}), Constantes.FUENTE.PARRAFO_NEGRITA));
                        tabla.addCell(celda);
                        flag = false;
                    }
                }
                if (ccm.getIdClasificacion().equals(t3.getIdClasificacion())) {
                    ph.add(new Phrase(Constantes.tipoFrase(new String[]{t3.getDescripcionMinerales()}) + "\n", Constantes.FUENTE.PARRAFO));
                }
//                CatClasificacionMineral clas = (CatClasificacionMineral) daoCat.busca(CatClasificacionMineral.class, t3.getIdClasificacion());
            }
            celda.addElement(ph);
            flag = true;
            tabla.addCell(celda);
            celda = new PdfPCell();
            ph = new Paragraph();
        }

        PreguntaMineral pm;
        //fin tabla
        return tabla;
    }//</editor-fold>

    //<editor-fold defaultstate="collapsed" desc="Tabla con las Obras">
    /**
     * 
     * @return 
     */
    public PdfPTable obras() {
        PdfPTable tabla = new PdfPTable(6);
        tabla.setWidthPercentage(100);
        tabla.setSpacingBefore(10f);
        tabla.setSpacingAfter(15f);
        PdfPCell celda;
        //inicio tabla
        celda = new PdfPCell(new Phrase("Tipo de Obra", Constantes.FUENTE.TITULO3));
        tabla.addCell(celda);
        celda = new PdfPCell(new Phrase("Dimensiones", Constantes.FUENTE.TITULO3));
        tabla.addCell(celda);
        celda = new PdfPCell(new Phrase("Unidad", Constantes.FUENTE.TITULO3));
        tabla.addCell(celda);
        celda = new PdfPCell(new Phrase("Superficie", Constantes.FUENTE.TITULO3));
        tabla.addCell(celda);
        celda = new PdfPCell(new Phrase("Unidad", Constantes.FUENTE.TITULO3));
        tabla.addCell(celda);
        celda = new PdfPCell(new Phrase("Porcentaje de aceptación", Constantes.FUENTE.TITULO3));
        tabla.addCell(celda);

        //fin tabla
        return tabla;
    }//</editor-fold>

    //<editor-fold defaultstate="collapsed" desc="Tabla de sustancias">
    /**
     *
     * @param folio
     * @param url
     * @return
     */
    public PdfPTable sustancias(String folio, String url) {
        List<SustanciaProyecto> sustancias = (List<SustanciaProyecto>) dao.listado_where_comp(SustanciaProyecto.class, "sustanciaProyectoPK",
                "folioProyecto", folio);
        PdfPTable tabla = new PdfPTable(5);
        tabla.setWidthPercentage(100);
        tabla.setSpacingBefore(10f);
        tabla.setSpacingAfter(15f);
        PdfPCell celda;
        //inicio tabla
        if (!sustancias.isEmpty()) {
            celda = new PdfPCell(new Phrase("Etapa", Constantes.FUENTE.PARRAFO_NEGRITA));
            celda.setHorizontalAlignment(Element.ALIGN_CENTER);
            tabla.addCell(celda);
            celda = new PdfPCell(new Phrase("Descripción", Constantes.FUENTE.PARRAFO_NEGRITA));
            celda.setHorizontalAlignment(Element.ALIGN_CENTER);
            tabla.addCell(celda);
            celda = new PdfPCell(new Phrase("Cantidad", Constantes.FUENTE.PARRAFO_NEGRITA));
            celda.setHorizontalAlignment(Element.ALIGN_CENTER);
            tabla.addCell(celda);
            celda = new PdfPCell(new Phrase("Unidad de medida", Constantes.FUENTE.PARRAFO_NEGRITA));
            celda.setHorizontalAlignment(Element.ALIGN_CENTER);
            tabla.addCell(celda);
            celda = new PdfPCell(new Phrase("Hoja de seguridad", Constantes.FUENTE.PARRAFO_NEGRITA));
            celda.setHorizontalAlignment(Element.ALIGN_CENTER);
            tabla.addCell(celda);
            //fin encabezados
            //contenido
            for (SustanciaProyecto sp : sustancias) {
                celda = new PdfPCell(new Phrase(Constantes.tipoFrase(new String[]{sp.getEtapaId().getEtapaDescripcion()}), Constantes.FUENTE.PARRAFO));
                tabla.addCell(celda);
                if (sp.getSustanciaPromovente() == null) { //descripcion
                    celda = new PdfPCell(new Phrase(Constantes.tipoFrase(new String[]{sp.getSustanciaId().getSustanciaDescripcion()}), Constantes.FUENTE.PARRAFO));
                    tabla.addCell(celda);
                } else {
                    celda = new PdfPCell(new Phrase(Constantes.tipoFrase(new String[]{sp.getSustanciaPromovente()}), Constantes.FUENTE.PARRAFO));
                    tabla.addCell(celda);
                }
                celda = new PdfPCell(new Phrase(sp.getSustanciaCantidadAlmacenada().toString(), Constantes.FUENTE.PARRAFO));
                tabla.addCell(celda);
                celda = new PdfPCell(new Phrase(((CatUnidadMedida) daoCat.busca(CatUnidadMedida.class, sp.getCtunClve())).getCtunDesc(), Constantes.FUENTE.PARRAFO));
                tabla.addCell(celda);
                celda = new PdfPCell(Constantes.anexoSustancia(folio, sp.getSustanciaProyectoPK().getSerialProyecto(), sp.getSustanciaProyectoPK().getSustProyId(), url));
                tabla.addCell(celda);
            }
        }
        //fin tabla
        return tabla;
    }//</editor-fold>
    
    //<editor-fold defaultstate="collapsed" desc="Tabla de datos de la georeferenciacion">
    /**
     * 
     * @param folio
     * @param clave
     * @param serial
     * @return 
     */
    public  PdfPTable referenciaSigeia(String folio, String clave, Short serial) {
        PdfPTable tabla = new PdfPTable(3);
        tabla.setWidthPercentage(100);
        tabla.setSpacingBefore(10f);
        tabla.setSpacingAfter(15f);
        PdfPCell celda;
        List<Object[]> todosEdos;
        todosEdos = daoCap2.edoTodosAfectado(folio, clave, serial);
        //inicio tabla
        if (!todosEdos.isEmpty()) {
            celda = new PdfPCell(new Paragraph("Entidad", Constantes.FUENTE.TITULO3));
            tabla.addCell(celda);
            celda = new PdfPCell(new Paragraph("Municipio", Constantes.FUENTE.TITULO3));
            tabla.addCell(celda);
            celda = new PdfPCell(new Paragraph("Superficie (m2)", Constantes.FUENTE.TITULO3));
            tabla.addCell(celda);
            for (Object[] obj : todosEdos) {
                celda = new PdfPCell(new Paragraph(obj[2].toString(), Constantes.FUENTE.PARRAFO));
                tabla.addCell(celda);
                celda = new PdfPCell(new Paragraph(obj[3].toString(), Constantes.FUENTE.PARRAFO));
                tabla.addCell(celda);
                celda = new PdfPCell(new Paragraph(Constantes.DOS_DECIMALES_COMAS.format(obj[4]), Constantes.FUENTE.PARRAFO));
                celda.setHorizontalAlignment(Element.ALIGN_RIGHT);
                tabla.addCell(celda);
            }
        }
        //fin tabla
        return tabla;
    }//</editor-fold>

    //<editor-fold defaultstate="collapsed" desc="Tabla de la superficie total del proyecto">
    /**
     * 
     * @param folio
     * @param clave
     * @param serial
     * @return 
     */
    public  PdfPTable superficieTotal(String folio, String clave, Short serial) {
        PdfPTable tabla = new PdfPTable(3);
        tabla.setWidthPercentage(100);
        tabla.setSpacingBefore(10f);
        tabla.setSpacingAfter(15f);
        PdfPCell celda;
        List<Object[]> proySigSupProy;
        proySigSupProy = daoCap2.proySigSupProyecto(folio, clave, serial);
        //inicio tabla
        if (!proySigSupProy.isEmpty()) {
            celda = new PdfPCell(new Phrase("Componente", Constantes.FUENTE.TITULO3));
            tabla.addCell(celda);
            celda = new PdfPCell(new Phrase("Descripción", Constantes.FUENTE.TITULO3));
            tabla.addCell(celda);
            celda = new PdfPCell(new Phrase("Superficie (m2)", Constantes.FUENTE.TITULO3));
            tabla.addCell(celda);
            for (Object[] pss : proySigSupProy) {
                celda = new PdfPCell(new Phrase(pss[0].toString(), Constantes.FUENTE.PARRAFO));
                tabla.addCell(celda);
                celda = new PdfPCell(new Phrase(pss[1].toString(), Constantes.FUENTE.PARRAFO));
                tabla.addCell(celda);
                celda = new PdfPCell(new Phrase(Constantes.DOS_DECIMALES_COMAS.format(BigDecimal.class.cast(pss[2])), Constantes.FUENTE.PARRAFO));
                celda.setHorizontalAlignment(Element.ALIGN_RIGHT);
                tabla.addCell(celda);
            }
            List<Object[]> proySigSupProyPredios1 = daoCap2.proySigSumPredios(folio, clave, serial);
            celda = new PdfPCell(new Paragraph("Superficie total predio:", Constantes.FUENTE.TITULO3));
            celda.setColspan(2);
            tabla.addCell(celda);
            celda = new PdfPCell(new Paragraph(Constantes.DOS_DECIMALES_COMAS.format(proySigSupProyPredios1.get(0)[1]), Constantes.FUENTE.PARRAFO));
            celda.setHorizontalAlignment(Element.ALIGN_RIGHT);
            tabla.addCell(celda);
            List<Object[]> proySigSupProyObras1 = daoCap2.proySigSumObras(folio, clave, serial);
            celda = new PdfPCell(new Paragraph("Superficie total obras:", Constantes.FUENTE.TITULO3));
            celda.setColspan(2);
            tabla.addCell(celda);
            celda = new PdfPCell(new Paragraph(Constantes.DOS_DECIMALES_COMAS.format(proySigSupProyObras1.get(0)[1]), Constantes.FUENTE.PARRAFO));
            celda.setHorizontalAlignment(Element.ALIGN_RIGHT);
            tabla.addCell(celda);
        }
        //fin tabla
        return tabla;
    }//</editor-fold>
    
    
    //<editor-fold defaultstate="collapsed" desc="Inversión requerida">
    /**
     *
     * @param proyecto
     * @return
     */
    public PdfPTable inversionRequerida(Proyecto proyecto) {
        PdfPTable tabla = new PdfPTable(2);
        tabla.setWidthPercentage(100);
        tabla.setSpacingBefore(10f);
        tabla.setSpacingAfter(15f);
        PdfPCell celda;
        Double inverTot = 0.0;
        Integer empleosTot = 0;
        //inicio tabla
        inverTot = proyecto.getProyInversionRequerida() != null && proyecto.getProyMedidasPrevencion() != null ? proyecto.getProyInversionRequerida().doubleValue() + proyecto.getProyMedidasPrevencion().doubleValue() : null;
        empleosTot = proyecto.getProyEmpleosPermanentes() != null && proyecto.getProyEmpleosTemporales() != null ? proyecto.getProyEmpleosPermanentes().intValue() + proyecto.getProyEmpleosTemporales().intValue() : null;
        celda = new PdfPCell(new Paragraph("Costo de la Inversión Requerida (M.N.)", Constantes.FUENTE.PARRAFO_NEGRITA));
        tabla.addCell(celda);  //proyecto.proyInversionRequerida
        celda = new PdfPCell(new Paragraph("$" + proyecto.getProyInversionRequerida() != null ? proyecto.getProyInversionRequerida().toString() : null, Constantes.FUENTE.PARRAFO));
        tabla.addCell(celda);
        celda = new PdfPCell(new Paragraph("Costos de Medidas de Prevención y Mitigación (M.N.)", Constantes.FUENTE.PARRAFO_NEGRITA));
        tabla.addCell(celda);
        celda = new PdfPCell(new Paragraph("$" + proyecto.getProyMedidasPrevencion() != null ? proyecto.getProyMedidasPrevencion().toString() : null, Constantes.FUENTE.PARRAFO));
        tabla.addCell(celda);
        celda = new PdfPCell(new Paragraph("Inversión Total (M.N.)", Constantes.FUENTE.PARRAFO_NEGRITA));
        tabla.addCell(celda);
        celda = new PdfPCell(new Paragraph("$" + inverTot.toString(), Constantes.FUENTE.PARRAFO));
        tabla.addCell(celda);
        celda = new PdfPCell(new Paragraph("Empleos Permanetes", Constantes.FUENTE.PARRAFO_NEGRITA));
        tabla.addCell(celda);
        celda = new PdfPCell(new Paragraph(proyecto.getProyEmpleosPermanentes() != null ? proyecto.getProyEmpleosPermanentes().toString() : null, Constantes.FUENTE.PARRAFO));
        tabla.addCell(celda);
        celda = new PdfPCell(new Paragraph("Empleos Temporales", Constantes.FUENTE.PARRAFO_NEGRITA));
        tabla.addCell(celda);
        celda = new PdfPCell(new Paragraph(proyecto.getProyEmpleosTemporales() != null ? proyecto.getProyEmpleosTemporales().toString() : null, Constantes.FUENTE.PARRAFO));
        tabla.addCell(celda);
        celda = new PdfPCell(new Paragraph("Empleos Totales", Constantes.FUENTE.PARRAFO_NEGRITA));
        tabla.addCell(celda);
        celda = new PdfPCell(new Paragraph(empleosTot.toString(), Constantes.FUENTE.PARRAFO));
        tabla.addCell(celda);
  
        
        //fin tabla
        return tabla;
    }//</editor-fold>

}
