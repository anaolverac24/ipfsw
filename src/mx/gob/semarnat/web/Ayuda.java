/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.gob.semarnat.web;

import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import mx.gob.semarnat.dao.ProyectoDao;

/**
 *
 * @author mauricio
 */
public class Ayuda extends HttpServlet {

    private final ProyectoDao proyectoDao = new ProyectoDao();

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        PrintWriter out = response.getWriter();
        try {
            String nsec = request.getParameter("nsec");
            String cap = request.getParameter("capitulo");
            String sub = request.getParameter("subcapitulo");
            String sec = request.getParameter("seccion");

            short capitulo = (cap == null || cap.equals("")) ? -1 : Short.valueOf(cap);
            short subCapitulo = (sub == null || sub.equals("")) ? -1 : Short.valueOf(sub);
            short seccion = (sec == null || sec.equals("")) ? -1 : Short.valueOf(sec);
            short nsub = (nsec == null || nsec.equals("")) ? -1 : Short.valueOf(nsec);

            System.out.println("Salida");
            System.out.println("Ayuda");
            out.println("<p>");
            out.println(proyectoDao.getAyuda(nsub, capitulo, subCapitulo, seccion));
            out.println("</p>");

        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            out.close();
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
