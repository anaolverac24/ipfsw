/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.gob.semarnat.web;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.Query;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import mx.gob.semarnat.dao.PoolEntityManagers;
import mx.gob.semarnat.model.AnexosProyecto;
import mx.gob.semarnat.model.EspecificacionAnexo;
import mx.gob.semarnat.model.SustanciaAnexo;

/**
 *
 * @author mauricio
 */
public class Preview extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String a = request.getParameter("idt");
        String tipo = request.getParameter("tipo");

        EntityManager em = PoolEntityManagers.getEmf().createEntityManager();

        String filePath = "";
        String ext = "";
        String nombre = "";

        if (tipo.equals("general")) {
            AnexosProyecto e;
            Query q = em.createQuery("SELECT a FROM AnexosProyecto a WHERE a.anexosProyectoPK.anexoId = :url");
            q.setParameter("url", Short.valueOf(a.trim()));
            try {
                e = (AnexosProyecto) q.getSingleResult();
                nombre = e.getAnexoNombre();
            } catch (Exception err) {
                System.out.println("No ENCONTRADA");
                Query q2 = em.createQuery("SELECT e FROM AnexosProyecto e");
                List<AnexosProyecto> l = q2.getResultList();
                e = l.get(0);
            }
            filePath = e.getAnexoUrl();
            ext = e.getAnexoExtension().toLowerCase().trim();
        } else {
            if (tipo.equals("sustancia")) {
                Query q = em.createQuery("SELECT s FROM SustanciaAnexo s WHERE s.sustanciaAnexoPK.sustanciaAnexoId = :url");
                q.setParameter("url", Short.valueOf(a.trim()));
                SustanciaAnexo s;
                try {
                    s = (SustanciaAnexo) q.getSingleResult();
                    nombre = s.getAnexoNombre();
                } catch (Exception e) {
                    System.out.println("NO encontrado");
                    Query q2 = em.createQuery("SELECT s FROM SustanciaAnexo s");
                    List<SustanciaAnexo> l = q2.getResultList();
                    s = l.get(0);
                }
                filePath = s.getAnexoUrl();
                ext = s.getAnexoExtension().toLowerCase().trim();
            } else {
                Query q = em.createQuery("SELECT e FROM EspecificacionAnexo e WHERE e.especificacionAnexoPK.especificacionAnexoId = :url");
                q.setParameter("url", Short.valueOf(a.trim()));
                EspecificacionAnexo e;
                try {
                    e = (EspecificacionAnexo) q.getSingleResult();
                    nombre = e.getEspecAnexoNombre();
                } catch (Exception err) {
                    System.out.println("No ENCONTRADA");
                    Query q2 = em.createQuery("SELECT e FROM EspecificacionAnexo e");
                    List<EspecificacionAnexo> l = q2.getResultList();
                    e = l.get(0);
                }
                filePath = e.getEspecAnexoUrl();
                ext = e.getEspecAnexoExtension().toLowerCase().trim();
            }
        }

        System.out.println("" + filePath);
        File downloadFile = new File(filePath);
        FileInputStream inStream = new FileInputStream(downloadFile);

        String mimeType = "application/octet";
        System.out.println("ext " + ext);

        if (ext.equals("jpg") || ext.equals("png") || ext.equals("gif")) {
            mimeType = "image/" + ext;
        } else {
            if (ext.equals("pdf")) {
                mimeType = "application/pdf";
            } else {
                response.setHeader("Content-Disposition", "attachment; filename=" + nombre);
                System.out.println("nombre:" + nombre);
            }
        }

        response.setContentType(mimeType);
        System.out.println("mimeType " + mimeType);

        response.setContentLength((int) downloadFile.length());
        OutputStream outStream = response.getOutputStream();

        byte[] buffer = new byte[4096];
        int bytesRead = -1;

        while ((bytesRead = inStream.read(buffer)) != -1) {
            outStream.write(buffer, 0, bytesRead);
        }

        inStream.close();
        outStream.close();

    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>
}
