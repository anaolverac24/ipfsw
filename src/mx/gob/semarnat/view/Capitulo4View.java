/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.gob.semarnat.view;

import java.io.Serializable;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import javax.persistence.EntityManager;
import mx.gob.semarnat.dao.Capitulo1Dao;
import mx.gob.semarnat.dao.Capitulo3Dao;
import mx.gob.semarnat.dao.PoolEntityManagers;
import mx.gob.semarnat.dao.ProyectoDao;
import mx.gob.semarnat.dao.SinatecProcedure;
import mx.gob.semarnat.model.EstudioRiesgoProyecto;
import mx.gob.semarnat.model.Proyecto;
import org.primefaces.context.RequestContext;

/**
 *
 * @author mauricio
 */
@ManagedBean(name = "capitulo4View")
@ViewScoped
public class Capitulo4View implements Serializable {

    private final ProyectoDao proyectoDao = new ProyectoDao();
    private EstudioRiesgoProyecto estudio = new EstudioRiesgoProyecto();
    private Proyecto proyecto;
    private final Capitulo1Dao daoCap1 = new Capitulo1Dao();
    private final Capitulo3Dao dao = new Capitulo3Dao();

    //Constructor
    public Capitulo4View() {
        //cargar proyecto
        FacesContext fContext = FacesContext.getCurrentInstance();
        fContext.getExternalContext().getSessionMap().get("userFolioProy");
        String folioProyecto = (String) fContext.getExternalContext().getSessionMap().get("folioProyecto");
        Short serialProyecto = (Short) fContext.getExternalContext().getSessionMap().get("serialProyecto");

        proyecto = daoCap1.cargaProyecto(folioProyecto, serialProyecto);
        estudio = proyectoDao.cargaEstudioRiesgo(proyecto);
    }

    public void guardar() {
        EntityManager em = PoolEntityManagers.getEmf().createEntityManager();
        em.getTransaction().begin();
        em.merge(estudio);
        em.getTransaction().commit();
        em.close();
        dao.guardarAvance(proyecto, new Short("25"), "4");
        RequestContext reqcontEnv = RequestContext.getCurrentInstance();
        //método para guardar avance en sinatec y verificar
        SinatecProcedure sp = new SinatecProcedure();
        int avance = Integer.parseInt(proyectoDao.avanceProyecto(proyecto));
        sp.ejecutaSP(Integer.parseInt(proyecto.getProyectoPK().getFolioProyecto()), avance);
        if (avance == 99) {
            reqcontEnv.execute("alert('Ha completado el registro de datos del Informe Preventivo.')");
        }
        //terminan procedimientos con SINATEC

        reqcontEnv.execute("alert('Su informacion ha sido guardada con exito.')");

    }

    /**
     * @return the estudio
     */
    public EstudioRiesgoProyecto getEstudio() {
        return estudio;
    }

    /**
     * @param estudio the estudio to set
     */
    public void setEstudio(EstudioRiesgoProyecto estudio) {
        this.estudio = estudio;
    }

    /**
     * @return the proyecto
     */
    public Proyecto getProyecto() {
        return proyecto;
    }

    /**
     * @param proyecto the proyecto to set
     */
    public void setProyecto(Proyecto proyecto) {
        this.proyecto = proyecto;
    }
}
