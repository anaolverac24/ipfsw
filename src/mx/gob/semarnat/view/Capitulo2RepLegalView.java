/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.gob.semarnat.view;

import java.util.ArrayList;
import java.util.List;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import javax.faces.model.ListDataModel;
import javax.persistence.NoResultException;
import mx.gob.semarnat.dao.Capitulo1Dao;
import mx.gob.semarnat.dao.Capitulo2Dao;
import mx.gob.semarnat.dao.Capitulo3Dao;
import mx.gob.semarnat.dao.ProyectoDao;
import mx.gob.semarnat.dao.SinatecProcedure;
import mx.gob.semarnat.dao.VisorDao;
import mx.gob.semarnat.model.Proyecto;
import mx.gob.semarnat.model.RepLegalProyecto;
import mx.gob.semarnat.model.RepLegalProyectoPK;
import mx.gob.semarnat.model.RespTecProyecto;
import mx.gob.semarnat.model.RespTecProyectoPK;
import mx.gob.semarnat.model.sinatec.Vexdatosusuariorep;
import org.primefaces.context.RequestContext;
import org.primefaces.model.SelectableDataModel;

/**
 * @author Rodrigo
 */
@ManagedBean(name = "cap2RepLegal")
@ViewScoped
public class Capitulo2RepLegalView {

    //------------------para guardado de tabla proyecto  
    private final ProyectoDao proyectoDao = new ProyectoDao();
    private Proyecto proyecto;
    private final Capitulo1Dao daoCap1 = new Capitulo1Dao();

    private RepresentanteDataModel representantes;
    private List<Vexdatosusuariorep> representantesSelect;
    private RespTecProyecto respTecProyecto = new RespTecProyecto();
    private String curp;

    private final Capitulo2Dao dao = new Capitulo2Dao();
    private final Capitulo3Dao dao3 = new Capitulo3Dao();

    private Boolean flag = null;

    public Capitulo2RepLegalView() {
        //cargar proyecto
        FacesContext fContext = FacesContext.getCurrentInstance();
        fContext.getExternalContext().getSessionMap().get("userFolioProy");
        String folioProyecto = (String) fContext.getExternalContext().getSessionMap().get("folioProyecto");
        Short serialProyecto = (Short) fContext.getExternalContext().getSessionMap().get("serialProyecto");
        proyecto = daoCap1.cargaProyecto(folioProyecto, serialProyecto);
        //fin de la carga del proyecto

        //representantes legales desde BD
        representantesSelect = new ArrayList();
        //carga el data model, recibe la lista global
        representantes = new RepresentanteDataModel(dao.datosPrepLeg(Integer.parseInt(proyecto.getProyectoPK().getFolioProyecto())));

        //carga representantes legales previamente seleccionados
        for (RepLegalProyecto repl
                : (List<RepLegalProyecto>) dao3.listado_where_comp(RepLegalProyecto.class,
                        "repLegalProyectoPK", "folioProyecto",
                        proyecto.getProyectoPK().getFolioProyecto())) {

            representantesSelect.add(representantes.getRowData((repl.getRepLegalProyectoPK().getRfc()
                    + "/"
                    + repl.getRepLegalProyectoPK().getFolioProyecto())));
            if (repl.getMismoEstudio().equals("S")) {
                curp = repl.getRepLegalProyectoPK().getRfc();
                flag = true;
            }
        }
        //carga responsable tecnico
        respTecProyecto = (RespTecProyecto) dao.busca(RespTecProyecto.class, new RespTecProyectoPK(proyecto.getProyectoPK().getFolioProyecto(), proyecto.getProyectoPK().getSerialProyecto()));
        if (respTecProyecto != null) {
            flag = false;
        } else {
            respTecProyecto = new RespTecProyecto();
        }
    }

    public void guardar() {
        RequestContext reqcontEnv = RequestContext.getCurrentInstance();
        if (proyecto.getProySupuesto() != null) {
            try {
                //borra datos existentes
                dao.borraRepLegal(proyecto.getProyectoPK().getFolioProyecto());
                //guarda datos de los representantes legales seleccionados
                for (Vexdatosusuariorep rep : representantesSelect) {
                    System.out.println("rep "+ rep);
                    RepLegalProyecto temp = new RepLegalProyecto();
                    
                    System.out.println("curp  --->  " + rep.getVccurp());
                    
                    temp.setRepLegalProyectoPK(new RepLegalProyectoPK(proyecto.getProyectoPK().getFolioProyecto(),
                            proyecto.getProyectoPK().getSerialProyecto(),
                            rep.getVccurp()));
                    temp.setRfc(rep.getVccurp());
                    temp.setProyecto(proyecto);
                    temp.setMismoEstudio("N");
                    dao.modifica(temp);
                }
//                dao.guardarAvance(proyecto, new Short(proyecto.getProyNormaId() == 129 ? "8" : "11"));
                if (proyecto.getProySupuesto() == '1') {
                    dao.guardarAvance(proyecto, new Short(proyecto.getProyNormaId() == 129 ? "8" : "11"));
                } else {
                    dao.guardarAvance(proyecto, new Short("11"));
                }
                //método para guardar avance en sinatec y verificar
                SinatecProcedure sp = new SinatecProcedure();
                int avance = Integer.parseInt(proyectoDao.avanceProyecto(proyecto));
                sp.ejecutaSP(Integer.parseInt(proyecto.getProyectoPK().getFolioProyecto()), avance);
                //terminan procedimientos con SINATEC
                reqcontEnv.execute("alert('Su información ha sido guardada con éxito.')");
                if (avance == 99) {
                    reqcontEnv.execute("alert('Ha completado el registro de datos del Informe Preventivo.')");
                }
            } catch (Exception e) {
                reqcontEnv.execute("alert('No se ha podido guardar la información, Intente más tarde.')");
                e.printStackTrace();
            }
        } else {
            reqcontEnv.execute("alert('Capture el capítulo 1 antes de proceder con los demás capítulos')");
        }
    }

    public void guardarREEIA() {
        RequestContext reqcontEnv = RequestContext.getCurrentInstance();
        RepLegalProyecto temp;
        if (proyecto.getProySupuesto() != null) {
            try {
//            dao.modifica(proyecto);
//            RespTecProyecto ap = ;
                if (flag) {
                    //elimina al responsable de la tabla
                    dao3.elimina(RespTecProyecto.class, new RespTecProyectoPK(proyecto.getProyectoPK().getFolioProyecto(),
                            proyecto.getProyectoPK().getSerialProyecto()));
                    if (curp != null) {//modifica en BD el status del rep legal                        
                        int vInicio = curp.length()-18;
                        String vCurp = curp.substring(vInicio,curp.length());
                        temp = (RepLegalProyecto) dao3.busca(RepLegalProyecto.class, new RepLegalProyectoPK(proyecto.getProyectoPK().getFolioProyecto(), proyecto.getProyectoPK().getSerialProyecto(), vCurp));                        
                        System.out.println("rfc--->   " + curp + " longitud " + curp.length() + " sólo la curp --> " + vCurp);
                        temp.setMismoEstudio("S");
                        dao.modifica(temp);
                    }
                } else {
//                dao.agrega(respTecProyecto);
                    //agrega o modifica al responsable
                    respTecProyecto.setRespTecProyectoPK(new RespTecProyectoPK(proyecto.getProyectoPK().getFolioProyecto(), proyecto.getProyectoPK().getSerialProyecto()));
                    dao.modifica(respTecProyecto);
                    //quita al representante legal que es representante
//                temp = (RepLegalProyecto) dao3.listado_where_comp(RepLegalProyecto.class, "repLegalProyectoPK", "rfc", curp).get(0);
                    try {
                        temp = (RepLegalProyecto) dao3.busca(RepLegalProyecto.class, new RepLegalProyectoPK(proyecto.getProyectoPK().getFolioProyecto(), proyecto.getProyectoPK().getSerialProyecto(), curp));
                        temp.setMismoEstudio("N");
                        dao3.modifica(temp);
                    } catch (NullPointerException e) {
                    }
                }
//                dao.guardarAvance(proyecto, new Short(proyecto.getProyNormaId() == 129 ? "8" : "11"));
                if (proyecto.getProySupuesto() == '1') {
                    dao.guardarAvance(proyecto, new Short(proyecto.getProyNormaId() == 129 ? "8" : "11"));
                } else {
                    dao.guardarAvance(proyecto, new Short("11"));
                }
                //método para guardar avance en sinatec y verificar
                SinatecProcedure sp = new SinatecProcedure();
                int avance = Integer.parseInt(proyectoDao.avanceProyecto(proyecto));
                sp.ejecutaSP(Integer.parseInt(proyecto.getProyectoPK().getFolioProyecto()), avance);
                reqcontEnv.execute("alert('Su información ha sido guardada con éxito.')");
                if (avance == 99) {
                    reqcontEnv.execute("alert('Ha completado el registro de datos del Informe Preventivo.')");
                }
            } catch (Exception e) {
                reqcontEnv.execute("alert('No se ha podido guardar la información, Intente más tarde.')");
                e.printStackTrace();
            }
        } else {
            reqcontEnv.execute("alert('Capture el capítulo 1 antes de proceder con los demás capítulos')");
        }
    }
    //getters y setters

    public Boolean getFlag() {
        return flag;
    }

    public void setFlag(Boolean flag) {
        this.flag = flag;
    }

    public String getCurp() {
        return curp;
    }

    public void setCurp(String curp) {
        this.curp = curp;
    }

    public RepresentanteDataModel getRepresentantes() {
        return representantes;
    }

    public void setRepresentantes(RepresentanteDataModel representantes) {
        this.representantes = representantes;
    }

    public List<Vexdatosusuariorep> getRepresentantesSelect() {
        return representantesSelect;
    }

    public void setRepresentantesSelect(List<Vexdatosusuariorep> representantesSelect) {
        this.representantesSelect = representantesSelect;
    }

    public RespTecProyecto getRespTecProyecto() {
        return respTecProyecto;
    }

    public void setRespTecProyecto(RespTecProyecto respTecProyecto) {
        this.respTecProyecto = respTecProyecto;
    }

    //data model
    public class RepresentanteDataModel extends ListDataModel<Vexdatosusuariorep> implements SelectableDataModel<Vexdatosusuariorep> {

        public RepresentanteDataModel() {
        }

        public RepresentanteDataModel(List<Vexdatosusuariorep> list) {
            super(list);
        }

        public Object getRowKey(Vexdatosusuariorep t) {
            String s = t.getVccurp() + "/" + t.getBgtramiteid();
            return s;
        }

        public Vexdatosusuariorep getRowData(String string) {
            String s[];
            s = string.split("/");
            try {
                return (Vexdatosusuariorep) dao.repLegPorCurp(s[0], Integer.parseInt(s[1]));
//            return (Vexdatosusuariorep) dao.datosPrepLeg(Integer.parseInt(string)).get(0);
            } catch (NoResultException e) {
                return new Vexdatosusuariorep();
            }
        }

    }

}
