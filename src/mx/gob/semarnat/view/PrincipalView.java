/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.gob.semarnat.view;

import Token.DecToken;
import java.io.FileOutputStream;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import javax.persistence.EntityManager;
import javax.persistence.Query;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;

import mx.gob.semarnat.dao.Capitulo2Dao;
import mx.gob.semarnat.dao.Capitulo3Dao;
import mx.gob.semarnat.dao.PoolEntityManagers;
import mx.gob.semarnat.dao.ProyectoDao;
import mx.gob.semarnat.dao.SinatecProcedure;
import mx.gob.semarnat.dao.VisorDao;
import mx.gob.semarnat.model.AnexosProyecto;
import mx.gob.semarnat.model.AnexosProyectoPK;
import mx.gob.semarnat.model.CatParametro;
import mx.gob.semarnat.model.EvaluacionProyecto;
import mx.gob.semarnat.model.Proyecto;
import mx.gob.semarnat.model.catalogos.SubsectorProyecto;
import mx.gob.semarnat.web.pdf.CrearPdf;
import org.primefaces.context.RequestContext;

/**
 *
 * @author mauricio
 */
@ManagedBean(name = "principalView")
@ViewScoped
public class PrincipalView implements Serializable {

    private final ProyectoDao proyectoDao = new ProyectoDao();
    private final Capitulo2Dao capitulo2Dao = new Capitulo2Dao();

    private String folio;
    private String message = "";
    private Integer VerLote = 0;

    private String folioProyecto;
    private Short serialProyecto;

    private Proyecto proyecto;
    private Boolean finTramite = false;
    private Boolean pdf = false;
    private Boolean capitulo1 = true;
    private Boolean capitulo2 = true;
    private Boolean capitulo3 = true;
    private Boolean capitulo4 = false;
    /**
     * Subsector del proyecto.
     */
    private SubsectorProyecto subsectorProyecto;


    public PrincipalView() {
        HttpServletRequest req = (HttpServletRequest) FacesContext.getCurrentInstance().getExternalContext().getRequest();
         String q = req.getQueryString();        
        String userFolioProy = q.substring(q.indexOf("folioProy")+"folioProy".length()+1);
        
        //Primero se desencripta el folio del proyecto
         System.out.println("folio Ant -- > " + userFolioProy);        
                                    
        //Primero se desencripta el folio del proyecto                                       
        try
        {
            DecToken decToken = new DecToken ();            
            String vFolioEncriptado = userFolioProy;
                    
            userFolioProy = decToken.DecryptCentralToken(vFolioEncriptado);

            System.out.println("desencriptado--->    " + userFolioProy);
            
            
        }catch (Exception msg){
            msg.printStackTrace();
        }         
        
        System.out.println("FOLIOPROY:::" + userFolioProy);
        FacesContext context = FacesContext.getCurrentInstance();
        context.getExternalContext().getSessionMap().put("userFolioProy", userFolioProy);
        SinatecProcedure sp = new SinatecProcedure();

        // Busca proyecto
        proyecto = proyectoDao.cargaProyecto(userFolioProy);

        // Si no existe crearlo
        if (proyecto == null) {
            proyecto = new Proyecto(userFolioProy, (short) 1, "0");
            proyecto.setEstatusProyecto("A");
            proyectoDao.persistProyecto(proyecto);
        } 

        //Solicita el Lote y sino existe en la tabla proyecto lo inserta
        if (proyecto.getProyLote() == null || proyecto.getProyLote() == 0) {
            sp.solicitudCarga(Integer.parseInt(userFolioProy), "Incializa_Lote", "Informe Preventivo", VerLote);
            //System.out.println("Folio Proyecto --->  " +  userFolioProy + "Inicializa lote::: ---->" + VerLote.toString()); 
        }

        //Una vez creado el proyecto le actualiza el lote para que sea un dato inicial
        //procedimiento SIANTEC
        capitulo1 = proyecto.getEstatusProyecto().contains("A");
        capitulo2 = proyecto.getEstatusProyecto().contains("A");
        capitulo3 = proyecto.getEstatusProyecto().contains("A");
        pdf = Integer.parseInt(proyectoDao.avanceProyecto(proyecto)) >= 60;
        finTramite = Integer.parseInt(proyectoDao.avanceProyecto(proyecto)) == 99;
//
//        // Validar si esta cerrado
//        if (proyecto.getEstatusProyecto().equals("C")) {
//            System.out.println("Folio cerrado");
//        } else {
//            System.out.println("Folio abierto");
//
//        }

//       
        context.getExternalContext().getSessionMap().put("folioProyecto", proyecto.getProyectoPK().getFolioProyecto());
        context.getExternalContext().getSessionMap().put("serialProyecto", proyecto.getProyectoPK().getSerialProyecto());

        if (proyecto.getProyNormaId() != null && proyecto.getEstatusProyecto().contains("A")) {
            if (proyecto.getProyNormaId() == 129) {
                capitulo4 = true;
                RequestContext reqcontEnv = RequestContext.getCurrentInstance();
                reqcontEnv.execute("menu4('" + proyecto.getProyNormaId() + "')");
                System.out.println("Norma seleccionada: " + proyecto.getProyNormaId());
            }
        }
    }

    private int avance;

    @Deprecated
    public void _finaliza() {
        RequestContext reqcontEnv = RequestContext.getCurrentInstance();
        System.out.println("--Cerrando trámite--");
        if (proyecto.getProyLote() != null) {
            SinatecProcedure sp = new SinatecProcedure();
            int noArch = VisorDao.tamanioLista(VisorDao.getAnexos(new Short("0"), new Short("0"), new Short("0"), new Short("0"), proyecto.getProyectoPK().getFolioProyecto(), proyecto.getProyectoPK().getSerialProyecto()))
                    + VisorDao.tamanioLista(VisorDao.getEspecifiacionesAnexos(proyecto.getProyectoPK().getFolioProyecto(), null, proyecto.getProyectoPK().getSerialProyecto(), null));
            //ejecuta SP cuando completa el trámite
            int i = sp.sp_fin(proyecto.getProyLote(), noArch);
            pdf = true;
            System.out.println(i == 0 ? "--Procedimiento OK--" : "Error al ejecutar el procedimiento");
            if (i == 0) {
                Capitulo3Dao dao3 = new Capitulo3Dao();
                dao3.guardarAvance(proyecto, new Short("1"), "0");
                proyecto.setEstatusProyecto("C");
                dao3.modifica(proyecto);
                //Proceso de captura completo
                reqcontEnv.execute("alert('Ha completado el registro de datos del Informe Preventivo')");
            } else {
                reqcontEnv.execute("alert('Error al ejecutar el procedimiento de finalización del trámite')");

            }
        } else {
            reqcontEnv.execute("alert('No se ha completado el registro, intente de nuevo más tarde')");
        }
    }
    
    public void finaliza() {
        RequestContext reqcontEnv = RequestContext.getCurrentInstance();
        String vFolio = getProyecto().getProyectoPK().getFolioProyecto();
        short vSerial = getProyecto().getProyectoPK().getSerialProyecto();
        String vBitacoraProyecto = getProyecto().getBitacoraProyecto();
        SinatecProcedure sp = new SinatecProcedure();
        String vMensaje = "";
        
        System.out.println("--Validaciones y procesos antes del cerrado de trámite--");
        System.out.println("--Folio trámite--" + vFolio + "  serial  " + vSerial); 
        //Validación de existencia de adjuntos
        int noArch = 0;
        noArch = proyectoDao.conteoAdjuntos(vFolio, vSerial, null).intValue();    
        
        if (noArch > 0) {
            //Generación de reporte de resumen
            System.out.println("-------Generación de reporte de resumen--------");
            generaReporteResumen(vFolio, vSerial);
            
            Long LoteProyecto = null;
            Integer vContador = 0;
            
            if (vSerial == 1) {
                //Aviso al Sinatec de los archivos que se adjuntaron
                ArrayList<Object[]> lstAdjuntos = proyectoDao.obtenerAdjuntos(vFolio, vSerial);
                int idx = 0;
                Integer iLote = null;
                for (Object[] adjunto : lstAdjuntos) {
                    System.out.println("-------Procedimeinto SINATEC PSTFOLIODOCSATLOTE_RG Serial 1--------");
                    sp.solicitudCarga(Integer.parseInt(vFolio), adjunto[0] != null ? (String)adjunto[0] : "", (String)adjunto[1], iLote);
                    if (iLote == null) {
                        Long lLote = proyectoDao.obtenerLoteProyecto(vFolio, vSerial);
                        iLote = lLote != null ? lLote.intValue() : null;
                        System.out.println("Lote:  " + iLote );
                    }
                    System.out.println("Adjunto ---> " + idx + ": Archivo: " + adjunto[0] != null ? (String)adjunto[0] : "" + " Descripción: " + adjunto[1] != null ? (String)adjunto[1] : "" );
                    vContador += 1;
                    idx++;
                }
                
                LoteProyecto = new Long(iLote);//getProy().getProyLote();
                //se realiza de nuevo el conteo de archivos, para tomar en cuenta el archivo de resumen de PDF
                //de la MIA que se adjunto en el procedimeinto generaReporteResumen(vFolio, vSerial);
                noArch = proyectoDao.conteoAdjuntos(vFolio, vSerial, null).intValue(); 
            } else if (vSerial == 3) {
                System.out.println("-------Procedimeinto SINATEC PSTFOLIODOCSATLOTE_RG Serial 3--------");
                vMensaje = sp.solicitudCarga(Integer.parseInt(vFolio), "FINALIZA_TRAMITE", vBitacoraProyecto, null);
                EvaluacionProyecto EvaProy = proyectoDao.datosEvaProy(vBitacoraProyecto);
                LoteProyecto = EvaProy.getEvaLoteInfoAdicional();
                noArch = 0;
            }
            System.out.println("No. de Archivos DESPUES de generar Resumen de MIA en PDF--" + noArch);
            System.out.println("--Cerrando trámite--" + vMensaje);

            System.out.println("Serial " + vSerial + "Bitacora Proyecto " + vBitacoraProyecto + "lote 2 ---> " + LoteProyecto);
        
            //ejecuta SP cuando completa el trámite
            int i = sp.sp_fin(proyecto.getProyLote(), noArch); //-1;//
            pdf = true;
            System.out.println(i == 0 ? "--Procedimiento OK--" : "Error al ejecutar el procedimiento Número de archivos reportados " + vContador + " archivos contabilizados " + noArch);
            if (i == 0) {
                Capitulo3Dao dao3 = new Capitulo3Dao();
                dao3.guardarAvance(proyecto, new Short("1"), "0");
                proyecto.setEstatusProyecto("C");
                dao3.modifica(proyecto);
                //Proceso de captura completo
                reqcontEnv.execute("alert('Ha completado el registro de datos del Informe Preventivo')");
                System.out.println("Se ha completado el registro de datos de la Manifestación de Impacto Ambiental");
            } else {
                reqcontEnv.execute("alert('No se ha completado el registro, intente de nuevo más tarde')");
                System.out.println("No se ha completado el registro de datos del Informe Preventivo");
            }
        } else {
            reqcontEnv.execute("alert('El trámite no cuenta con ningún adjunto, no es posible cerrarlo')");
            System.out.println("El trámite no cuenta con ningún adjunto, no es posible cerrarlo");

        }
        
        
    }
    
    private void generaReporteResumen(String folio, short serial) {
        try {
            ServletContext servletContext = (ServletContext) FacesContext.getCurrentInstance().getExternalContext().getContext();
            EntityManager em = PoolEntityManagers.getEmf().createEntityManager();
            Query q = em.createQuery("SELECT a FROM CatParametro a WHERE a.parDescripcion = 'ADJUNTOS'");
            CatParametro c = (CatParametro) q.getSingleResult();
            String UPLOAD_DIRECTORY = c.getParRuta();
//            String UPLOAD_DIRECTORY = "C:/wars/";

            CrearPdf r = new CrearPdf();
            byte[] pdf;
            //========================= DESARROLLO =========================
            pdf = r.generaPDF(folio, serial,"http://appsdev.semarnat.gob.mx:8080/InformePreventivo/", null, servletContext);
            //========================= PRODUCCION =========================
            //pdf = r.generaPDF(folio, serial,"https://mia.semarnat.gob.mx:8443/InformePreventivo/", null, servletContext);
            
            
            String nombre = "Informe preventivo.pdf";
            String ruta = UPLOAD_DIRECTORY + folio + "/" + nombre;
            String descripcion = "Este archivo contiene toda la información del trámite electrónico.";
            
            Short idAnexo = proyectoDao.numRegAnexo();
            AnexosProyecto anexo = new AnexosProyecto(new AnexosProyectoPK((short)0, (short)0, (short)0, (short)0, idAnexo, folio, serial));
            anexo.setAnexoDescripcion(descripcion);
            anexo.setAnexoNombre(nombre);
            anexo.setAnexoUrl(ruta);
            anexo.setAnexoExtension("pdf");
            anexo.setAnexoTamanio(BigDecimal.ONE);
            proyectoDao.merge(anexo);
            System.out.println("Se inserta registro de archivo 0(PDF de resumen de MIA) en tabla Anexos Proyecto " + folio + "  Serial: " + serial);
            System.out.println("Se sube archivo 0(PDF de resumen de MIA) a Servidor con ruta:  " + ruta);
            FileOutputStream fos = new FileOutputStream(ruta);
            fos.write(pdf);
            fos.close();
        
        } catch (Exception e) {
            System.out.println("Ocurrió un error durante la generación del reporte de resumen");
            e.printStackTrace();
        }
    }    

//    public void cargaProyecto() {
//
//        // Busca proyecto
//        proyecto = ProyectoDao.cargaProyecto(folio);
//
//        // Si no existe crearlo
//        if (proyecto == null) {
//            proyecto = new Proyecto(folio, (short) 1);
//            proyecto.setEstatusProyecto("A");
//            ProyectoDao.persistProyecto(proyecto);
//        }
//
//        // Validar si esta cerrado
//        if (proyecto.getEstatusProyecto().equals("C")) {
//            System.out.println("Folio cerrado");
//        } else {
//            System.out.println("Folio abierto");
//
//        }
//
//        folioProyecto = proyecto.getProyectoPK().getFolioProyecto();
//        serialProyecto = proyecto.getProyectoPK().getSerialProyecto();
//    }
    public Boolean getNorma129() {
        if (proyecto.getProyNormaId() != null && proyecto.getEstatusProyecto().contains("A")) {
            if (proyecto.getProyNormaId() == 129) {
                return true;
            } else {
                return false;
            }
        } else {
            return false;
        }
    }

    public Boolean getError() {
        if (message.length() > 0) {
            return true;
        }
        return false;
    }
//

    public void guardarProyecto() {
//        ProyectoDao.persistProyecto(proyecto);
    }

    /**
     * @return the proyecto
     */
    public Proyecto getProyecto() {
        return proyecto;
    }

    /**
     * @param proyecto the proyecto to set
     */
    public void setProyecto(Proyecto proyecto) {
        this.proyecto = proyecto;
    }

    /**
     * @return the folio
     */
    public String getFolio() {
        return folio;
    }

    /**
     * @param folio the folio to set
     */
    public void setFolio(String folio) {
        this.folio = folio;
//        cargaProyecto();
    }

    /**
     * @return the message
     */
    public String getMessage() {
        return message;
    }

    /**
     * @param message the message to set
     */
    public void setMessage(String message) {
        this.message = message;
    }

    /**
     * @return the capitulo2
     */
    public Boolean getCapitulo2() {
        return proyecto.getEstatusProyecto().contains("A");
    }

    /**
     * @param capitulo2 the capitulo2 to set
     */
    public void setCapitulo2(Boolean capitulo2) {
        this.capitulo2 = capitulo2;
    }

    /**
     * @return the capitulo3
     */
    public Boolean getCapitulo3() {
        return proyecto.getEstatusProyecto().contains("A");
    }

    /**
     * @param capitulo3 the capitulo3 to set
     */
    public void setCapitulo3(Boolean capitulo3) {
        this.capitulo3 = capitulo3;
    }

    /**
     * @return the capitulo4
     */
    public Boolean getCapitulo4() {
        return proyecto.getEstatusProyecto().contains("A");
    }

    /**
     * @param capitulo4 the capitulo4 to set
     */
    public void setCapitulo4(Boolean capitulo4) {
        this.capitulo4 = capitulo4;
    }

    /**
     * @return the capitulo1
     */
    public Boolean getCapitulo1() {
        return proyecto.getEstatusProyecto().contains("A");
    }

    /**
     * @param capitulo1 the capitulo1 to set
     */
    public void setCapitulo1(Boolean capitulo1) {
        this.capitulo1 = capitulo1;
    }

    /**
     * @return the folioProyecto
     */
    public String getFolioProyecto() {
        return folioProyecto;
    }

    /**
     * @param folioProyecto the folioProyecto to set
     */
    public void setFolioProyecto(String folioProyecto) {
        this.folioProyecto = folioProyecto;
    }

    /**
     * @return the serialProyecto
     */
    public Short getSerialProyecto() {
        return serialProyecto;
    }

    /**
     * @param serialProyecto the serialProyecto to set
     */
    public void setSerialProyecto(Short serialProyecto) {
        this.serialProyecto = serialProyecto;
    }

    /**
     * @return the avance
     */
    public int getAvance() {
        if (proyecto != null) {
            String a = proyectoDao.avanceProyecto(proyecto);
            return Integer.parseInt(a);
        }
        return 0;
    }

    /**
     * @param avance the avance to set
     */
    public void setAvance(int avance) {
        this.avance = avance;
    }

    public Boolean getFinTramite() {
        return Integer.parseInt(proyectoDao.avanceProyecto(proyecto)) == 99;
    }

    public void setFinTramite(Boolean finTramite) {
        this.finTramite = finTramite;
    }

    public Boolean getPdf() {
        return Integer.parseInt(proyectoDao.avanceProyecto(proyecto)) >= 60;
    }

    public void setPdf(Boolean pdf) {
        this.pdf = pdf;
    }

    public String getTime() {
        return "" + new Date().getTime();
    }

	/**
	 * @return the subsectorProyecto
	 */
	public SubsectorProyecto getSubsectorProyecto() {
		return subsectorProyecto;
	}

	/**
	 * @param subsectorProyecto the subsectorProyecto to set
	 */
	public void setSubsectorProyecto(SubsectorProyecto subsectorProyecto) {
		this.subsectorProyecto = subsectorProyecto;
	}
}
