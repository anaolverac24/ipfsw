/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.gob.semarnat.view.converters;

import javax.faces.bean.ManagedBean;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.FacesConverter;
import mx.gob.semarnat.dao.Capitulo3DaoCatalogos;
import mx.gob.semarnat.model.catalogos.CatUnidadMedida;


@ManagedBean(name = "catUnidadConverter")
@FacesConverter(value = "catUnidadConverter")
public class CatUnidadMedidaConverter implements Converter{
    
    private Capitulo3DaoCatalogos dao = new Capitulo3DaoCatalogos();

    public Object getAsObject(FacesContext context, UIComponent component, String value) {
        Object o =dao.busca(CatUnidadMedida.class, new Short(value));
        return o instanceof CatUnidadMedida ? ((CatUnidadMedida)o).getCtunClve() : null;
    }

    public String getAsString(FacesContext context, UIComponent component, Object value) {
        return value+"";
    }
    
}
