/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.gob.semarnat.view.converters;

import javax.faces.bean.ManagedBean;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.FacesConverter;
import javax.persistence.EntityManager;
import mx.gob.semarnat.dao.PoolEntityManagers;
import mx.gob.semarnat.model.CatTipoClima;
import mx.gob.semarnat.model.CatTipoVegetacion;

/**
 *
 * @author mauricio
 */
@ManagedBean(name = "catTipoClimaConverter")
@FacesConverter("catTipoClimaConverter")
public class CatTipoClimaConverter implements Converter {

    @Override
    public Object getAsObject(FacesContext ctx, UIComponent component, String value) {
        EntityManager em = PoolEntityManagers.getEmf().createEntityManager();

        Object ob = em.find(CatTipoClima.class, new Short(value));

        return ob;
    }

    @Override
    public String getAsString(FacesContext fc, UIComponent uic, Object o) {
        return ((CatTipoClima) o).getClimaId()+ "";
    }

}
