/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.gob.semarnat.view.converters;

import javax.faces.bean.ManagedBean;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.FacesConverter;
import mx.gob.semarnat.dao.Capitulo3Dao;
import mx.gob.semarnat.model.CatMineralesReservados;


/**
 *
 * @author Admin
 */
@ManagedBean(name = "catMineralConv")
@FacesConverter("catMineralConv")
public class CatMineralConverter implements Converter{
    private Capitulo3Dao dao = new Capitulo3Dao();

    public Object getAsObject(FacesContext context, UIComponent component, String value) {
        
        Object ob = dao.busca(CatMineralesReservados.class, value);
        Short id = ((CatMineralesReservados)ob).getIdMinerales();
       
        return id;
    }

    public String getAsString(FacesContext context, UIComponent component, Object value) {
        return value+"";
    }
    
}
