/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package mx.gob.semarnat.view.converters;

import javax.faces.bean.ManagedBean;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.FacesConverter;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import mx.gob.semarnat.dao.PoolEntityManagers;
import mx.gob.semarnat.model.CatPdu;

/**
 *
 * @author Rengerden
 */
@ManagedBean(name = "catPduConverterBean")
@FacesConverter(value = "catPduConverter", forClass = CatPdu.class )
public class CatPduConverter implements Converter {


    @Override
    public Object getAsObject(FacesContext ctx, UIComponent component, String value) {
        EntityManager em = PoolEntityManagers.getEmf().createEntityManager();
        Object ob = em.find(CatPdu.class, new Short(value));

        return ob;
    }

    @Override
    public String getAsString(FacesContext fc, UIComponent uic, Object o) {
        return ((CatPdu) o).getPduId() + "";
    }

}