/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.gob.semarnat.view.converters;

import javax.faces.bean.ManagedBean;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.FacesConverter;
import mx.gob.semarnat.dao.Capitulo3Dao;
import mx.gob.semarnat.model.CatEtapa;

/**
 *
 * @author Admin
 */
@ManagedBean(name = "converterCatEtapa")
@FacesConverter("converterCatEtapa")
public class CatEtapaConverter implements Converter {   
    private Capitulo3Dao dao = new Capitulo3Dao();

    public Object getAsObject(FacesContext context, UIComponent component, String value) {
        return dao.busca(CatEtapa.class, new Short(value));
    }

    public String getAsString(FacesContext context, UIComponent component, Object value) {
        return (((CatEtapa)value).getEtapaId()).toString();
    }
}
