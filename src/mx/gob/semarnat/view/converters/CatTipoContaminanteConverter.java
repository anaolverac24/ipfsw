/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.gob.semarnat.view.converters;

import javax.faces.bean.ManagedBean;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.FacesConverter;
import mx.gob.semarnat.dao.Capitulo3Dao;
import mx.gob.semarnat.model.CatContaminante;

/**
 *
 * @author Admin
 */
@ManagedBean(name = "catTipoContaminanteConverter")
@FacesConverter("catTipoContaminanteConverter")
public class CatTipoContaminanteConverter implements Converter{
    private Capitulo3Dao dao = new Capitulo3Dao();

    public Object getAsObject(FacesContext context, UIComponent component, String value) {
        Object o = dao.busca(CatContaminante.class, new Short(value));
        return o instanceof CatContaminante?o:null;
    }

    public String getAsString(FacesContext context, UIComponent component, Object value) {
        return value+"";
    }
}
