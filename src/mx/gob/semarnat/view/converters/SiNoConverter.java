/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.gob.semarnat.view.converters;

import javax.faces.bean.ManagedBean;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.FacesConverter;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import mx.gob.semarnat.model.CatNorma;

/**
 *
 * @author mauricio
 */
@ManagedBean(name = "siNoConverterBean")
@FacesConverter(value = "siNoConverter")
public class SiNoConverter implements Converter {

    @Override
    public Object getAsObject(FacesContext ctx, UIComponent component, String value) {
//        EntityManager em = emf.createEntityManager();
//        Object ob = em.find(CatNorma.class, new Short(value));
//        em.close();
        Character ch = value.charAt(0);
        return ch;
    }

    @Override
    public String getAsString(FacesContext fc, UIComponent uic, Object o) {
        return ( o.toString());
    }

}
