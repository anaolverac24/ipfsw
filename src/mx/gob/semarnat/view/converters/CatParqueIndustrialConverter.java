/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.gob.semarnat.view.converters;

import javax.faces.bean.ManagedBean;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.FacesConverter;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import mx.gob.semarnat.dao.PoolEntityManagers;

import mx.gob.semarnat.model.CatParqueIndustrial;

/**
 *
 * @author Rengerden
 */
@ManagedBean(name = "catParqueConverterBean")
@FacesConverter(value = "catParqueConverter", forClass = CatParqueIndustrial.class)
public class CatParqueIndustrialConverter implements Converter {

    public static EntityManagerFactory emf = PoolEntityManagers.getEmf();

    @Override
    public Object getAsObject(FacesContext ctx, UIComponent component, String value) {
        EntityManager em = emf.createEntityManager();
        Object ob = em.find(CatParqueIndustrial.class, new Short(value));
        em.close();

        return ob;
    }

    @Override
    public String getAsString(FacesContext fc, UIComponent uic, Object o) {
        return ((CatParqueIndustrial) o).getParqueId().toString() + "";

    }

}
