/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.gob.semarnat.view.converters;

import javax.faces.bean.ManagedBean;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.FacesConverter;
import javax.persistence.EntityManager;
import mx.gob.semarnat.dao.PoolEntityManagers;
import mx.gob.semarnat.model.CatPdu;
import mx.gob.semarnat.model.CatTipoVegetacion;

/**
 *
 * @author mauricio
 */
@ManagedBean(name = "catTipoVegetaConverter")
@FacesConverter("catTipoVegetaConverter")
public class CatTipoVegetaConverter implements Converter {

    @Override
    public Object getAsObject(FacesContext ctx, UIComponent component, String value) {
        EntityManager em = PoolEntityManagers.getEmf().createEntityManager();

        Object ob = em.find(CatTipoVegetacion.class, new Short(value));

        return ob;
    }

    @Override
    public String getAsString(FacesContext fc, UIComponent uic, Object o) {
        return ((CatTipoVegetacion) o).getVegetacionId() + "";
    }

}
