/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.gob.semarnat.view.converters;

import javax.faces.bean.ManagedBean;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.FacesConverter;
import mx.gob.semarnat.dao.Capitulo3Dao;
import mx.gob.semarnat.model.CatSustanciaAltamRiesgosa;

/**
 *
 * @author Admin
 */
@ManagedBean(name = "convSustAltamR")
@FacesConverter("convSustAltamR")
public class CatSustAltamRiesgConverter implements Converter{
    private Capitulo3Dao dao = new Capitulo3Dao();

    public Object getAsObject(FacesContext context, UIComponent component, String value) {
        return value != null?
                dao.busca(CatSustanciaAltamRiesgosa.class, new Short(value))
                :new CatSustanciaAltamRiesgosa();
    }

    public String getAsString(FacesContext context, UIComponent component, Object value) {
        return value instanceof CatSustanciaAltamRiesgosa ?
                ((CatSustanciaAltamRiesgosa)value).getSustanciaId().toString() :
                "9999";
    }
    
}
