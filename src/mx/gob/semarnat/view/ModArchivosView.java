/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.gob.semarnat.view;

import java.util.ArrayList;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.RequestScoped;
import mx.gob.semarnat.dao.VisorDao;
import mx.gob.semarnat.model.AnexosProyecto;
import mx.gob.semarnat.model.EspecificacionAnexo;
import mx.gob.semarnat.model.SustanciaAnexo;

/**
 *
 * @author mauricio
 */
@ManagedBean(name = "modArchivosView")
@RequestScoped
public class ModArchivosView {

    private String folioProyecto;
    private String especifiacionId;
    private Short serialProyecto;
    private Short normaId;

    private List<EspecificacionAnexo> especificacionAnexos = new ArrayList<EspecificacionAnexo>();
    private List<AnexosProyecto> anexosProyecto = new ArrayList<AnexosProyecto>();
    private List<SustanciaAnexo> sustanciaAnexos  = new ArrayList();
    
    private Boolean verEspAnexos;
    private Boolean verAnexos;
    private Boolean verHojaSeguridad;

    @ManagedProperty("#{param.info}")
    private String info;

    @PostConstruct
    public void init() {
        String[] args = info.split(",");

        if (args.length == 4) {
//            System.out.println("BEAN _ INFO " + info);

            folioProyecto = args[1];
            especifiacionId = args[0];
            serialProyecto = new Short(args[2]);
            normaId = new Short(args[3]);

//            System.out.println("Folio " + folioProyecto);
//            System.out.println("Espec " + especifiacionId);
//            System.out.println("Serial " + serialProyecto);
//            System.out.println("Norma " + normaId);

            especificacionAnexos = VisorDao.getEspecifiacionesAnexos(folioProyecto, especifiacionId, serialProyecto, normaId);
        }

        if (args.length == 6) {
            String folioProyecto = "";
            Short serialProyecto = (short) 0;
            short capituloId = (short) 0;
            short subcapituloId = (short) 0;
            short seccionId = (short) 0;
            short apartadoId = (short) 0;
            capituloId = new Short(args[0]);
            subcapituloId = new Short(args[1]);
            seccionId = new Short(args[2]);
            apartadoId = new Short(args[3]);
            folioProyecto = args[4];
            serialProyecto = new Short(args[5]);
            anexosProyecto = VisorDao.getAnexos(capituloId, subcapituloId, seccionId, apartadoId, folioProyecto, serialProyecto);
            
        }
        if(args.length==3){
            Short sustanciaProyId;
            folioProyecto = args[0];
            serialProyecto = new Short(args[1]);
            sustanciaProyId = new Short(args[2]);
            sustanciaAnexos = VisorDao.getSustanciaAnexos(folioProyecto, serialProyecto, sustanciaProyId);
        }

        verEspAnexos = !especificacionAnexos.isEmpty(); //verEspAnexos = !especificacionAnexos.isEmpty();
        verAnexos = !anexosProyecto.isEmpty();
//        if(sustanciaAnexos.isEmpty()){
//            verHojaSeguridad = false;
//        }else{
//            verHojaSeguridad = true;
//        }
        verHojaSeguridad = !sustanciaAnexos.isEmpty();
    }

    public ModArchivosView() {
//        System.out.println("info " + info);
    }

    /**
     * @return the info
     */
    public String getInfo() {
        return info;
    }

    /**
     * @param info the info to set
     */
    public void setInfo(String info) {
        this.info = info;
    }

    /**
     * @return the especificacionAnexos
     */
    public List<EspecificacionAnexo> getEspecificacionAnexos() {
        return especificacionAnexos;
    }

    /**
     * @param especificacionAnexos the especificacionAnexos to set
     */
    public void setEspecificacionAnexos(List<EspecificacionAnexo> especificacionAnexos) {
        this.especificacionAnexos = especificacionAnexos;
    }

    /**
     * @return the verEspAnexos
     */
    public Boolean getVerEspAnexos() {
        return verEspAnexos;
    }

    /**
     * @param verEspAnexos the verEspAnexos to set
     */
    public void setVerEspAnexos(Boolean verEspAnexos) {
        this.verEspAnexos = verEspAnexos;
    }

    /**
     * @return the anexosProyecto
     */
    public List<AnexosProyecto> getAnexosProyecto() {
        return anexosProyecto;
    }

    /**
     * @param anexosProyecto the anexosProyecto to set
     */
    public void setAnexosProyecto(List<AnexosProyecto> anexosProyecto) {
        this.anexosProyecto = anexosProyecto;
    }

    /**
     * @return the verAnexos
     */
    public Boolean getVerAnexos() {
        return verAnexos;
    }

    /**
     * @param verAnexos the verAnexos to set
     */
    public void setVerAnexos(Boolean verAnexos) {
        this.verAnexos = verAnexos;
    }

    public List<SustanciaAnexo> getSustanciaAnexos() {
        return sustanciaAnexos;
    }

    public void setSustanciaAnexos(List<SustanciaAnexo> sustanciaAnexos) {
        this.sustanciaAnexos = sustanciaAnexos;
    }

    public Boolean getVerHojaSeguridad() {
        return verHojaSeguridad;
    }

    public void setVerHojaSeguridad(Boolean verHojaSeguridad) {
        this.verHojaSeguridad = verHojaSeguridad;
    }
    
}
