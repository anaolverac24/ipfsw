/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.gob.semarnat.view;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import javax.persistence.EntityExistsException;
import mx.gob.semarnat.dao.Capitulo1Dao;
import mx.gob.semarnat.dao.Capitulo3Dao;
import mx.gob.semarnat.dao.ProyectoDao;
import mx.gob.semarnat.dao.SinatecProcedure;
import mx.gob.semarnat.model.ImpactoProyecto;
import mx.gob.semarnat.model.ImpactoProyectoPK;
import mx.gob.semarnat.model.Proyecto;
import org.primefaces.context.RequestContext;

/**
 * @author Admin
 */
@ManagedBean(name = "cap35")
@ViewScoped
public class Cap35_IdenImpAmb implements Serializable {

    private final ProyectoDao proyectoDao = new ProyectoDao();
    private List<IdenImpAmbHelper> impactoProyectos;
    private List<ImpactoProyecto> impactosBD;
    private IdenImpAmbHelper impactoProyectoSelect;
    private boolean nuevo = false;
    private Capitulo3Dao dao = new Capitulo3Dao();
    // Proyecto recupera el proycto de la sesion
    private final Capitulo1Dao daoCap1 = new Capitulo1Dao();
    private final Proyecto proyecto;

    public Cap35_IdenImpAmb() {
        //cargar proyecto
        FacesContext fContext = FacesContext.getCurrentInstance();
        fContext.getExternalContext().getSessionMap().get("userFolioProy");
        String folioProyecto = (String) fContext.getExternalContext().getSessionMap().get("folioProyecto");
        Short serialProyecto = (Short) fContext.getExternalContext().getSessionMap().get("serialProyecto");

        proyecto = daoCap1.cargaProyecto(folioProyecto, serialProyecto);
        //fin de carga de proyecto

        impactosBD = (List<ImpactoProyecto>) dao.listado_where_comp(ImpactoProyecto.class, new String[]{"impactoProyectoPK", "impactoProyectoPK"},
                new String[]{"folioProyecto", "serialProyecto"},
                new Object[]{proyecto.getProyectoPK().getFolioProyecto(), proyecto.getProyectoPK().getSerialProyecto()});

        int i = 0;
        impactoProyectos = new ArrayList();
        for (ImpactoProyecto cp : impactosBD) {
            impactoProyectos.add(new IdenImpAmbHelper(cp, i));
            i++;
        }
        if (impactosBD.isEmpty()) {
            nuevo = true;
            impactoProyectos = new ArrayList();//inicializa arreglo
            impactoProyectos.add(new IdenImpAmbHelper(new ImpactoProyecto(), 0));
        } else {
            impactosBD = new ArrayList();
            for (IdenImpAmbHelper ch : impactoProyectos) {
                impactosBD.add(ch.impacto);
            }
        }
    }

    public void agregaImp() {//agregar elemento a la tabla
        int i = impactoProyectos.get(impactoProyectos.size() - 1).getId();
        impactoProyectos.add(new IdenImpAmbHelper(new ImpactoProyecto(), i + 1));
    }

    public void quitarImp() {//quitar elemento de la tabla
        impactoProyectos.remove(impactoProyectoSelect);
    }

    public void guardar() {
        int i = 0;
        RequestContext reqcontEnv = RequestContext.getCurrentInstance();
        if (proyecto.getProySupuesto() != null) {
            if (nuevo) {
                try {
                    //registros nuevos sin historicos
                    for (IdenImpAmbHelper ip : impactoProyectos) {
                        ip.impacto.setProyecto(proyecto);
                        ip.impacto.setImpactoProyectoPK(new ImpactoProyectoPK(proyecto.getProyectoPK().getFolioProyecto(),
                                proyecto.getProyectoPK().getSerialProyecto(), new Short(i + "")));
                        dao.agrega(ip.impacto);
                        i++;
                    }
//                    dao.guardarAvance(proyecto, new Short(proyecto.getProyNormaId() == 129 ? "4" : "6"), "35");
                    if (proyecto.getProySupuesto() == '1') {
                        dao.guardarAvance(proyecto, new Short(proyecto.getProyNormaId() == 129 ? "4" : "6"), "35");
                    } else {
                        dao.guardarAvance(proyecto, new Short("6"), "35");
                    }
                    //método para guardar avance en sinatec y verificar
                    SinatecProcedure sp = new SinatecProcedure();
                    int avance = Integer.parseInt(proyectoDao.avanceProyecto(proyecto));
                    sp.ejecutaSP(Integer.parseInt(proyecto.getProyectoPK().getFolioProyecto()), avance);
                    reqcontEnv.execute("alert('Su información ha sido guardada con éxito.');");
                    if (avance == 99) {
                        reqcontEnv.execute("alert('Ha completado el registro de datos del Informe Preventivo.')");
                    }
                    //terminan procedimientos con SINATEC
                } catch (Exception e) {
                    reqcontEnv.execute("alert('No se ha podido guardar la información, Intente más tarde.');");
                }
            } else {
                try {
                    impactosBD = (List<ImpactoProyecto>) dao.listado_where_comp(ImpactoProyecto.class, new String[]{"impactoProyectoPK", "impactoProyectoPK"},
                            new String[]{"folioProyecto", "serialProyecto"},
                            new Object[]{proyecto.getProyectoPK().getFolioProyecto(), proyecto.getProyectoPK().getSerialProyecto()});
                    //borra registros historicos
                    for (ImpactoProyecto imp : impactosBD) {
                        dao.elimina(ImpactoProyecto.class, imp.getImpactoProyectoPK());
                    }
                    //carga registros actuales
                    if (!getImpactoProyectos().isEmpty()) {
                        for (IdenImpAmbHelper ip : impactoProyectos) {
                            try {
                                ip.impacto.setProyecto(proyecto);
                                ip.impacto.setImpactoProyectoPK(new ImpactoProyectoPK(proyecto.getProyectoPK().getFolioProyecto(),
                                        proyecto.getProyectoPK().getSerialProyecto(), new Short(i + "")));
                                dao.agrega(ip.impacto);
                                i++;
                            } catch (EntityExistsException ex) {
                                dao.modifica(ip.impacto);
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        }
                    }
                    //método para guardar avance en sinatec y verificar
                    SinatecProcedure sp = new SinatecProcedure();
                    int avance = Integer.parseInt(proyectoDao.avanceProyecto(proyecto));
                    sp.ejecutaSP(Integer.parseInt(proyecto.getProyectoPK().getFolioProyecto()), avance);
                    reqcontEnv.execute("alert('Su información ha sido guardada con éxito.')");
                    if (avance == 99) {
                        reqcontEnv.execute("alert('Ha completado el registro de datos del Informe Preventivo.')");
                    }
                    //terminan procedimientos con SINATEC
                } catch (Exception e) {
                    reqcontEnv.execute("alert('No se ha podido guardar la información, Intente más tarde.')");
                }
            }
        } else {
            reqcontEnv.execute("alert('Capture el capítulo 1 antes de proceder con los demás capítulos')");
        }
    }

    // getters y setters
    public Proyecto getProyecto() {
        return proyecto;
    }

    public List<IdenImpAmbHelper> getImpactoProyectos() {
        return impactoProyectos;
    }

    public void setImpactoProyectos(List<IdenImpAmbHelper> impactoProyectos) {
        this.impactoProyectos = impactoProyectos;
    }

    public IdenImpAmbHelper getImpactoProyectoSelect() {
        return impactoProyectoSelect;
    }

    public void setImpactoProyectoSelect(IdenImpAmbHelper impactoProyectoSelect) {
        this.impactoProyectoSelect = impactoProyectoSelect;
    }

    //clase de ayuda para impactos
    public class IdenImpAmbHelper {

        private ImpactoProyecto impacto;
        private Integer id;

        public IdenImpAmbHelper(ImpactoProyecto impacto, Integer id) {
            this.impacto = impacto;
            this.id = id;
        }

        public Integer getId() {
            return id;
        }

        public void setId(Integer id) {
            this.id = id;
        }

        public ImpactoProyecto getImpacto() {
            return impacto;
        }

        public void setImpacto(ImpactoProyecto impacto) {
            this.impacto = impacto;
        }
    }

}
