/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.gob.semarnat.view;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import mx.gob.semarnat.dao.Capitulo1Dao;
import mx.gob.semarnat.dao.Capitulo3Dao;
import mx.gob.semarnat.dao.Capitulo3DaoCatalogos;
import mx.gob.semarnat.dao.ProyectoDao;
import mx.gob.semarnat.dao.SinatecProcedure;
import mx.gob.semarnat.model.CatEtapa;
import mx.gob.semarnat.model.CatSustanciaAltamRiesgosa;
import mx.gob.semarnat.model.EtapaProyecto;
import mx.gob.semarnat.model.Proyecto;
import mx.gob.semarnat.model.SustanciaProyecto;
import mx.gob.semarnat.model.SustanciaProyectoPK;
import mx.gob.semarnat.model.catalogos.CatUnidadMedida;
import org.primefaces.context.RequestContext;

/**
 * @author Admin
 */
@ManagedBean(name = "cap32")
@ViewScoped
public class Cap32_Sustan_bean implements Serializable {
  
    private final ProyectoDao proyectoDao = new ProyectoDao();
    private List<SustanciaHelper> sustanciaProyectos;
    private SustanciaHelper sustanciaSeleccionada;
    private List<CatEtapa> catEtapa;
    private final Capitulo3Dao dao = new Capitulo3Dao();
    private final Capitulo3DaoCatalogos daoCat = new Capitulo3DaoCatalogos();
    private final Capitulo1Dao daoCap1 = new Capitulo1Dao();
    private List<CatUnidadMedida> catUnidades;
    // Proyecto recupera el proyecto de la sesion
    private final Proyecto proyecto;
    private boolean nuevo = false;
    private boolean supera = false;

    public Cap32_Sustan_bean() {
        //cargar proyecto
        FacesContext fContext = FacesContext.getCurrentInstance();
        fContext.getExternalContext().getSessionMap().get("userFolioProy");
        String folioProyecto = (String) fContext.getExternalContext().getSessionMap().get("folioProyecto");
        Short serialProyecto = (Short) fContext.getExternalContext().getSessionMap().get("serialProyecto");

        proyecto = daoCap1.cargaProyecto(folioProyecto, serialProyecto);
        catEtapa = new ArrayList();
        catUnidades = (List<CatUnidadMedida>) daoCat.listado(CatUnidadMedida.class);
        cargaLista();
        //carga sustancias a la variable
        sustanciaProyectos = new ArrayList();
        for (SustanciaProyecto sp : (List<SustanciaProyecto>) dao.listado_where_comp(SustanciaProyecto.class, new String[]{"sustanciaProyectoPK", "sustanciaProyectoPK"},
                new String[]{"folioProyecto", "serialProyecto"},
                new Object[]{proyecto.getProyectoPK().getFolioProyecto(), proyecto.getProyectoPK().getSerialProyecto()})) {
            /**
             * aqui debe de ir el campo de la BD que hace referencia al anexo
             */
            sustanciaProyectos.add(new SustanciaHelper(sp, sp.getSustanciaId().getSustanciaId().equals(new Short("9999")), 0));//,sp.getAnexoId));
        }
        if (sustanciaProyectos.isEmpty()) {
            //agrega una sustancia vacía si no hay datos desde BD}
            /**
             * Ver si hay registros en tabla de anexos, poner id del anexo en
             * vez del 0
             */
            sustanciaProyectos.add(new SustanciaHelper(new SustanciaProyecto(), false, 0));
            nuevo = true;
        }
    }
    //carga lista de etapas

    private void cargaLista() {
        List<EtapaProyecto> etapa;
        try {
            etapa = (List<EtapaProyecto>) dao.listado_where_comp(EtapaProyecto.class, "etapaProyectoPK", "folioProyecto", proyecto.getProyectoPK().getFolioProyecto());
            System.out.println("cantidad de etapas: " + etapa.size());
            for (EtapaProyecto e : etapa) {
                catEtapa.add(e.getCatEtapa());
            }
        } catch (NullPointerException e) {
        } catch (Exception e) {
        }
    }

    //autocompletar
    public List<CatSustanciaAltamRiesgosa> completarSustancias(String qry) {
        List<CatSustanciaAltamRiesgosa> list = (List<CatSustanciaAltamRiesgosa>) dao.listado_like(CatSustanciaAltamRiesgosa.class,
                "sustanciaDescripcion", qry.concat("%").trim().toUpperCase());
        if (list.isEmpty()) {
            if (list == null) {
                list = new ArrayList();
                list.add((CatSustanciaAltamRiesgosa) dao.busca(CatSustanciaAltamRiesgosa.class, new Short("9999")));
            } else {
                list.add((CatSustanciaAltamRiesgosa) dao.busca(CatSustanciaAltamRiesgosa.class, new Short("9999")));

            }
        }
        return list;
    }

    public void addSustancia() {
        if (!isSupera()) {
            sustanciaProyectos.add(new SustanciaHelper(new SustanciaProyecto(), false, 0));
        }
    }

    public String removeSustancia() {
        if (sustanciaSeleccionada != null) {
            dao.eliminarAnexo(sustanciaSeleccionada.getModel());
            sustanciaProyectos.remove(sustanciaSeleccionada);
        }
        return null;
    }
    
    //verificar si supera la cantidad permitida
    public void verificaCantidad() {
        for (SustanciaHelper sp : sustanciaProyectos) {
            try {
                if (sp.getModel().getSustanciaCantidadAlmacenada().doubleValue()
                        > sp.getModel().getSustanciaId().getSustanciaCantidad().doubleValue()
                        && !sp.getOtra()) {
                    if ( proyecto.getProyNormaId() != null && proyecto.getProyNormaId().equals((short)129)) {
                        supera = false;
                    } else {
                        supera = true;
                    }
                } else {
                    supera = false;
                }
            } catch (NullPointerException e) {
            }
        }
    }

    public void guardar() {
        List<SustanciaProyecto> sustanciaProyectosBD;
        RequestContext reqcontEnv = RequestContext.getCurrentInstance();
        if (proyecto.getProySupuesto() != null) {
            if (nuevo) {
                try {
                    for (SustanciaHelper sp : sustanciaProyectos) {
                        SustanciaProyecto temp = sp.model;
                        temp.setSustanciaProyectoPK(new SustanciaProyectoPK(
                                proyecto.getProyectoPK().getFolioProyecto(),
                                proyecto.getProyectoPK().getSerialProyecto(),
                                sp.model.getSustanciaId().getSustanciaId()));
                        dao.agrega(temp);
                    }
//                    dao.guardarAvance(proyecto, new Short(proyecto.getProyNormaId() == 129 ? "4" : "6"), "32");
                    if (proyecto.getProySupuesto() == '1') {
                        dao.guardarAvance(proyecto, new Short(proyecto.getProyNormaId() == 129 ? "4" : "6"), "32");
                    } else {
                        dao.guardarAvance(proyecto, new Short("6"), "32");
                    }
                    reqcontEnv.execute("alert('Su información ha sido guardada con éxito.');");

                } catch (Exception e) {
                    reqcontEnv.execute("alert('No se ha podido guardar la información, Intente más tarde.');");
                }
            } else {
                try {
                    sustanciaProyectosBD = (List<SustanciaProyecto>) dao.listado_where_comp(SustanciaProyecto.class, new String[]{"sustanciaProyectoPK", "sustanciaProyectoPK"},
                            new String[]{"folioProyecto", "serialProyecto"},
                            new Object[]{proyecto.getProyectoPK().getFolioProyecto(), proyecto.getProyectoPK().getSerialProyecto()});
                    //carga registros actuales 
                    int i = 0;
                    
                    for (SustanciaProyecto ssp : sustanciaProyectosBD) {
                        for (SustanciaHelper sp : sustanciaProyectos) {
                            SustanciaProyecto temp = sp.model;
                            if (temp.getSustanciaProyectoPK() != null) {
                                if (temp.getSustanciaProyectoPK().equals(ssp.getSustanciaProyectoPK())) {
                                    dao.modifica(temp);
                                    i++;
                                    System.out.println("modifico");
                                }
                            } else {
                                temp.setSustanciaProyectoPK(new SustanciaProyectoPK(
                                        proyecto.getProyectoPK().getFolioProyecto(),
                                        proyecto.getProyectoPK().getSerialProyecto(),
                                        new Short("0")));
                                dao.agrega(temp);
                                System.out.println("agrega nuevo");
                            }
                        }
                        if(i==0){
                            dao.elimina(SustanciaProyecto.class, ssp.getSustanciaProyectoPK());
                            System.out.println("elimino");
                        }
                        i=0;
                    }
                    //borra registros anteriores si vacían la lista o solo hay nuevos registros
//                    if (sustanciaProyectos.isEmpty() || i == 0) {
//                        for (SustanciaProyecto spb : sustanciaProyectosBD) {
//                            dao.elimina(SustanciaProyecto.class, spb.getSustanciaProyectoPK());
//                        }
//                    }
                    reqcontEnv.execute("alert('Su información ha sido guardada con éxito.');");
                    sustanciaProyectos = new ArrayList<SustanciaHelper>();
                    for (SustanciaProyecto sp : (List<SustanciaProyecto>) dao.listado_where_comp(SustanciaProyecto.class, new String[]{"sustanciaProyectoPK", "sustanciaProyectoPK"},
                            new String[]{"folioProyecto", "serialProyecto"},
                            new Object[]{proyecto.getProyectoPK().getFolioProyecto(), proyecto.getProyectoPK().getSerialProyecto()})) {
                        sustanciaProyectos.add(new SustanciaHelper(sp, sp.getSustanciaId().getSustanciaId().equals(new Short("9999")), 0));
                    }

                } catch (Exception e) {
                    reqcontEnv.execute("alert('No se ha podido guardar la información, Intente más tarde.');");
                    e.printStackTrace();
                }
            }
            //método para guardar avance en sinatec y verificar
            SinatecProcedure sp = new SinatecProcedure();
            int avance = Integer.parseInt(proyectoDao.avanceProyecto(proyecto));
            sp.ejecutaSP(Integer.parseInt(proyecto.getProyectoPK().getFolioProyecto()), avance);
            if (avance == 99) {
                reqcontEnv.execute("alert('Ha completado el registro de datos del Informe Preventivo.')");
            }
            //terminan procedimientos con SINATEC
        } else {
            reqcontEnv.execute("alert('Capture el capítulo 1 antes de proceder con los demás capítulos')");
        }
    }

    //getter y setter
    public List<SustanciaHelper> getSustanciaProyectos() {
        return sustanciaProyectos;
    }

    public void setSustanciaProyectos(List<SustanciaHelper> sustanciaProyectos) {
        this.sustanciaProyectos = sustanciaProyectos;
    }

    public SustanciaHelper getSustanciaSeleccionada() {
        return sustanciaSeleccionada;
    }

    public void setSustanciaSeleccionada(SustanciaHelper sustanciaSeleccionada) {
        this.sustanciaSeleccionada = sustanciaSeleccionada;
    }

    public List<CatEtapa> getCatEtapa() {
        return catEtapa;
    }

    public void setCatEtapa(List<CatEtapa> catEtapa) {
        this.catEtapa = catEtapa;
    }

    public List<CatUnidadMedida> getCatUnidades() {
        return catUnidades;
    }

    public void setCatUnidades(List<CatUnidadMedida> catUnidades) {
        this.catUnidades = catUnidades;
    }

    public boolean isSupera() {
        return supera;
    }

    public Proyecto getProyecto() {
        return proyecto;
    }

    public void setSupera(boolean supera) {
        this.supera = supera;
    }
//    public Boolean getNorma(){
//        return proyecto.getProyNormaId()!=null;
//    }

    public class SustanciaHelper {

        private SustanciaProyecto model;
        private Boolean otra;
        private Integer idAnexo;

        public SustanciaHelper(SustanciaProyecto model, Boolean otra, Integer idAnexo) {
            this.model = model;
            this.otra = otra;
            this.idAnexo = idAnexo;
        }

        public SustanciaProyecto getModel() {
            return model;
        }

        public void setModel(SustanciaProyecto model) {
            this.model = model;
        }

        public Boolean getOtra() {
            if (getModel().getSustanciaId() != null) {
                otra = getModel().getSustanciaId().getSustanciaId().equals(new Short("9999"));
            }
            return otra;
        }

        public boolean getAnexo() {
            return model.getSustanciaProyectoPK() != null;
        }

        public void setOtra(Boolean otra) {
            this.otra = otra;
        }

        public Integer getIdAnexo() {
            return idAnexo;
        }

        public void setIdAnexo(Integer idAnexo) {
            this.idAnexo = idAnexo;
        }
        
        public void actualizaCtunClve() {
            CatUnidadMedida unidadMedida;
//            if (!otra) {
                if (getModel().getSustanciaId() != null && getModel().getSustanciaId().getSustanciaId() != 9999) {
                    unidadMedida = obtieneSustancia(getModel().getSustanciaId().getSustanciaUnidad());
                    if (unidadMedida != null) {
                        getModel().setCtunClve(unidadMedida.getCtunClve());
                    }
                }
//            }
        }
    }
    
    private CatUnidadMedida obtieneSustancia(String nombre) {
        CatUnidadMedida unidad = null;
        for(CatUnidadMedida unidadMedida : catUnidades) {
            if (unidadMedida.getCtunAbre().trim().compareToIgnoreCase(nombre.trim()) == 0 || 
                    unidadMedida.getCtunDesc().trim().compareToIgnoreCase(nombre.trim()) == 0 ) {
                unidad = unidadMedida;
                break;
            }
        }
        
        return unidad;
    }
}
