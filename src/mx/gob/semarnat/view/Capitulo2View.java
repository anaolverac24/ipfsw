/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.gob.semarnat.view;

import mx.gob.semarnat.view.util.Constantes;
import java.io.Serializable;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.List;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import javax.faces.event.ValueChangeEvent;
import javax.faces.model.SelectItem;
import javax.persistence.EntityManager;
import mx.gob.semarnat.dao.Capitulo1Dao;
import mx.gob.semarnat.dao.Capitulo2Dao;
import mx.gob.semarnat.dao.PoolEntityManagers;
import mx.gob.semarnat.dao.ProyectoDao;
import mx.gob.semarnat.dao.SinatecProcedure;
import mx.gob.semarnat.model.AnexosProyecto;
import mx.gob.semarnat.model.ProySubsectorGuias;
import mx.gob.semarnat.model.Proyecto;
import mx.gob.semarnat.model.Proysig;
import mx.gob.semarnat.model.catalogos.CatTipoAsen;
import mx.gob.semarnat.model.catalogos.CatVialidad;
import mx.gob.semarnat.model.catalogos.RamaProyecto;
import mx.gob.semarnat.model.catalogos.SectorProyecto;
import mx.gob.semarnat.model.catalogos.SubsectorProyecto;
import mx.gob.semarnat.model.catalogos.TipoProyecto;
import org.primefaces.context.RequestContext;

/**
 *
 * @author Case Solutions 10
 */
@ManagedBean(name = "capitulo2View")
@ViewScoped

public class Capitulo2View implements Serializable {
	private Boolean desactivaBtnGuia=true; //desactiva boton Guia PDF
	private List<ProySubsectorGuias>GuiasPdf = new ArrayList<ProySubsectorGuias>();
	private ArrayList<String>guiasPdfValidas=new ArrayList<String>();
	
	
    private final ProyectoDao proyectoDao = new ProyectoDao();

    private final Capitulo2Dao dao = new Capitulo2Dao();

    private final List<Proysig> proySigList = new ArrayList<Proysig>();
    private final Proysig proySig = new Proysig();
    private List<Object[]> edoAfectado = new ArrayList();
    private List<Object[]> todosEdos = new ArrayList();

    private List<SelectItem> opcRama = new ArrayList<SelectItem>();
    private List<SubsectorProyecto> subsector;
    // define si se ve el tipo de domicilion exacto, para su llenado
    private Boolean localiz = null;
    // define si se ve el tipo de domicilion NO exacto, para su llenado
    private Boolean localizNo = null;

    // para la seleccion de referencia de subsector
    private String referencia;
    // la selecion de la subsector
    private final SubsectorProyecto ssSeleccionada = new SubsectorProyecto();
    // para la seleccion de referencia de rama
    private String referenciaRama;

    // la selecion de la rama
    private RamaProyecto ramaSeleccionada = new RamaProyecto();

    // para la seleccion de referencia de rama
    private List<Object[]> sectorProyecto;
    
    // para la seleccion de referencia de tipo de domicilio
    private Character tipoDomic;

//---------------------superficie total del proyecto---------------
    private List<Object[]> proySigSupProy = new ArrayList();

    //---------------variables para inserción------------
    private String nombreProy;
    private String tipoProyecto;
    private String tipoVialidad;
    private String nombVialidad;
    private String numInterior;
    private String numExterior;
    private String tipoAsentamiento;
    private String nombAsentamiento;
    private String codigoPostal;
    private String direccCompleta;
    
    //Clave del tramite y Subsector
    private Integer claveTramite = 0;
    private short subsec = 0;
    
//------------------para guardado de tabla proyecto  
    private Proyecto proyecto;
    private final Capitulo1Dao daoCap1 = new Capitulo1Dao();

//-----------------para seccion de planos adicionales---
    private List<AnexosProyecto> anexosProyecto;
    private AnexosProyecto planoAdicSelect;

//--------------------------------------------------------------    
    private Boolean btnGuardar = true; // define si se puede ver el boton de guardar

    private final EntityManager enMgr = PoolEntityManagers.getEmf().createEntityManager();
//-----------------------Variable para recuperación del LOTE del proyecto---------------------
    private String vLote = "0";

    //Bloquear
    private Boolean blos = false;
    
    
    /**
     * Permite indicar si se muestra una tabla o una etiqueta del subsector.
     */
    private Boolean existeSector;
    /**
     * Permite indicar si se muestra un combo o una etiqueta del subsector.
     */
    private Boolean existeSubSector;      
    /**
     * Permite indicar si se muestran los datos del sector, subsector, tipo y rama del proyecto.
     */
    private Boolean existeNormaProyecto;
    
    /**
     * Objeto para mostrar el sector del proyecto consultado. 
     */
    private SectorProyecto sectorProyectoConsultado;
    /**
     * Objeto para mostrar el subsector del proyecto consultado.
     */
    private SubsectorProyecto subSectorProyectoConsultado;
    /**
     * Objeto para mostrar la rama del proyecto consultado.
     */
    private RamaProyecto ramaProyectoConsultado;
    /**
     * Objeto para mostrar el tipo de proyecto del proyecto consultado.
     */
    private TipoProyecto tipoProyectoConsultado;
    
    public void bloquea() {
        blos = true;
    }

    public Capitulo2View() {
        anexosProyecto = new ArrayList();
        //------------cargar proyecto
        FacesContext fContext = FacesContext.getCurrentInstance();
        fContext.getExternalContext().getSessionMap().get("userFolioProy");
        String folioProyecto = (String) fContext.getExternalContext().getSessionMap().get("folioProyecto");
        Short serialProyecto = (Short) fContext.getExternalContext().getSessionMap().get("serialProyecto");        
        
        try {
        proyecto = daoCap1.cargaProyecto(folioProyecto, serialProyecto);
        } catch (Exception e) {
            System.out.println("No encontrado");
        }
        
        //Recuperación del Lote del Proyecto
        /*
         if(proyecto.getProyLote() != null){
         vLote = proyecto.getProyLote().toString();
         }else{
         vLote = "0";
         }
         */
        //------------fin de la carga del proyecto
        todosEdos = estadosAfetados();
        proySigSupProy = dao.proySigSupProyecto(proyecto.getProyectoPK().getFolioProyecto(), proyecto.getClaveProyecto(), proyecto.getProyectoPK().getSerialProyecto());

        //Si el proyecto existe
        if (proyecto != null) {
        	//Si el proyecto contiene una norma.
        	if (proyecto.getProyNormaId() != null) {
        		//Se habilitan las banderas para mostrar los labels con los datos del sector y subsector del proyecto
        		existeNormaProyecto = true;
                
                try {
                	//Se consulta los datos del subsector del proyecto.
					subSectorProyectoConsultado = dao.consultarSubSectorProyecto(proyecto.getNsub());
					sectorProyectoConsultado = dao.consultarSectorProyecto(proyecto.getNsec());
					ramaProyectoConsultado = dao.consultarRamaProyectoPorIdSubSector(proyecto.getNsub(), proyecto.getNrama());
					tipoProyectoConsultado = dao.consultarTipoProyectoPorRama(proyecto.getNtipo(), proyecto.getNrama());
					
				} catch (Exception e) {
					FacesContext.getCurrentInstance().addMessage(
							null, new FacesMessage(null, "Error al consultar el subsector del proyecto", null));
					
					e.printStackTrace();
				}
                
    		} else {
    			//Si no contiene norma del proyecto, entonces se muestran combos.
    			existeNormaProyecto = false;
    		}
		}
        
        if (proyecto.getProyDomEstablecido() != null) {
            System.out.println("Valor " + tipoDomic);
            //Carga Normas Oficiales
            if (proyecto.getProyDomEstablecido().equals('S')) {  //SI tiene domicilio establecido
                localiz = true;
                localizNo = false;
            }
            if (proyecto.getProyDomEstablecido().equals('N')) {  //NO tiene domicilio establecido
                localizNo = true;
                localiz = false;
            }
        }

        nombreProy = proyecto.getProyNombre();
        tipoProyecto = proyecto.getNtipo() != null ? proyecto.getNtipo().toString() : null;
        
        System.out.println("Folio: " + folioProyecto);
        System.out.println("Serial: " + serialProyecto);
        System.out.println("SubSector: " + subsec);
        System.out.println("Clave del Tramite: " + claveTramite);
        
        buscaGuiasPdf();
        
    }

    public void buscaGuiasPdf(){
    	try{    
        	//Obtiene Guias PDF
    		System.out.println("..........iniciando busqueda de guias PDF");
    		if(proyecto.getNsub() != null){
        	GuiasPdf=dao.getUrlGuiaPdf(new Integer(proyecto.getNsub()), 63);
        	System.out.println("Se encontraron "+GuiasPdf.size()+" guias PDF");
        	//getDesactivaBtnGuia();
        	validaGuiasPdf();
    		}else{
    			System.out.println("No se encontro el SubSector");
    		}
        } catch (Exception e) {    		
    			e.printStackTrace();
    	}
    }
    
    public void validaGuiasPdf(){
    	try{
    		if(!guiasPdfValidas.isEmpty())
    			guiasPdfValidas.clear();
    		
    		System.out.println("..........iniciando validacion de guias PDF encontradas");
    		for(ProySubsectorGuias guia:GuiasPdf){
        		if(guia.getId().getUrlGuia()!=null && guia.getId().getUrlGuia().startsWith("http://") && guia.getId().getUrlGuia().endsWith(".pdf")){
        			guiasPdfValidas.add(guia.getId().getUrlGuia());
        		}
        	}
    		System.out.println("..Existen "+ guiasPdfValidas.size()+" validas");
    	}catch(Exception e){
    		FacesContext.getCurrentInstance().addMessage(
    				null, new FacesMessage(null, "Error al validar las Guias PDF", null));
        			e.printStackTrace();
    	}
    }
    
    public void abreGuias(){
    	for(int i=0; i<guiasPdfValidas.size();i++){
    		System.out.println("Abriendo guia: "+guiasPdfValidas.get(i));
    		RequestContext.getCurrentInstance().execute("window.open('" + guiasPdfValidas.get(i) + "')");
    	}
    }
    
        
    public void guardar() {
        RequestContext reqcontEnv = RequestContext.getCurrentInstance();
        if (proyecto.getProySupuesto() != null) {
            try {
                if (((Proyecto) dao.busca(Proyecto.class, proyecto.getProyectoPK())).getProyLote() != null) {
                    Long lote;
                    lote = ((Proyecto) dao.busca(Proyecto.class, proyecto.getProyectoPK())).getProyLote();
                    proyecto.setProyLote(lote);
                }

                if (proyecto.getProyDomEstablecido().equals("N")) { //domicilio establecido
                    proyecto.setProyUbicacionDescrita("");
                }
                if (proyecto.getProyDomEstablecido().equals("S")) { //domicilio establecido
                    Short varShort = new Short("0");
                    proyecto.setCveTipoVial(varShort);
                    proyecto.setProyNombreVialidad(" ");
                    proyecto.setProyNumeroExterior(0);
                    proyecto.setProyNumeroInterior(0);
                    proyecto.setTipoAsentamientoId(varShort);
                    proyecto.setProyNombreAsentamiento("");
                    proyecto.setProyCodigoPostal(0);
                }
                if (sectorProyecto != null) {
                    proyecto.setNsec((Short) sectorProyecto.get(0)[0]);
                }
                
                //Si el proyecto no contiene un subsector
                if (subSectorProyectoConsultado == null) {
                	
				}
                
                if (edoAfectado != null) {
                    if (edoAfectado.get(0)[1] != null) {
                        proyecto.setProyEntAfectado(edoAfectado.get(0)[1].toString().substring(0, 2));
                        proyecto.setProyMunAfectado(edoAfectado.get(0)[1].toString());
                        if (proyecto.getProySupuesto() == '1') {
                            dao.guardarAvance(proyecto, new Short(proyecto.getProyNormaId() == 129 ? "9" : "11"));
                        } else {
                            dao.guardarAvance(proyecto, new Short("11"));
                        }
                    }
                }
                //guarda proyecto
                proyecto.setProyNombre(nombreProy);
                proyecto.setNtipo(new Short(tipoProyecto));
                
                dao.modifica(proyecto);											
                
                //método para guardar avance en sinatec y verificar
                SinatecProcedure sp = new SinatecProcedure();
                int avance = Integer.parseInt(proyectoDao.avanceProyecto(proyecto));
                sp.ejecutaSP(Integer.parseInt(proyecto.getProyectoPK().getFolioProyecto()), avance);

                reqcontEnv.execute("alert('Su información ha sido guardada con éxito.')");
                if (avance == 99) {
                    reqcontEnv.execute("alert('Ha completado el registro de datos del Informe Preventivo.')");
                }
                //terminan procedimientos con SINATEC
                System.out.println("Guardado .. ok");
            } catch (Exception e) {
                reqcontEnv.execute("alert('No se ha podido guardar la información, Intente más tarde.')");
                e.printStackTrace();
            }
        } else {
            reqcontEnv.execute("alert('Capture el capítulo 1 antes de proceder con los demás capítulos')");
        }
    }

    /**
     *
     * @return Url de SIGEIA
     * @throws NoSuchAlgorithmException
     */
    public String getUrl() throws NoSuchAlgorithmException {
        String url = Constantes.URL_SIGEIA;
        String secureParam = "usuario=" + Constantes.USUARIO_SIGEIA
                + "&password=" + Constantes.PASSWORD_SIGEIA
                + "&noFolio=" + proyecto.getProyectoPK().getFolioProyecto()
                + "&claveProyecto=" + proyecto.getClaveProyecto()
                + "&version=" + proyecto.getProyectoPK().getSerialProyecto()
                + "&tipoTramite=" + Constantes.TRAMITE
                + "&estatus=" + proyecto.getEstatusProyecto();
        return url.concat(secureParam);
    }

    public static String getUrlPdf(String vFolio, String vClave, String vSerial, String vEstatus) {
        String url = Constantes.URL_SIGEIA;
        String secureParam = "usuario=" + Constantes.USUARIO_SIGEIA
                + "&password=" + Constantes.PASSWORD_SIGEIA
                + "&noFolio=" + vFolio
                + "&claveProyecto="+vClave+"&version=" + vSerial
                + "&tipoTramite=" + Constantes.TRAMITE
                + "&estatus=" + vEstatus;
        return url.concat(secureParam);
    }
    
    /**
     * @return the edoAfectado
     */
    public List<Object[]> getEdoAfectado() {
        return edoAfectado;
    }

    public final List<Object[]> estadosAfetados() {
//        List<Object[]> p = dao.proySigSum(getProyecto().getProyectoPK().getFolioProyecto(), getProyecto().getClaveProyecto(), getProyecto().getProyectoPK().getSerialProyecto());

        List<Object[]> pObra = dao.mpiosSumObra(getProyecto().getProyectoPK().getFolioProyecto(), getProyecto().getClaveProyecto(), getProyecto().getProyectoPK().getSerialProyecto());
        List<Object[]> pPredio = dao.mpiosSumPred(getProyecto().getProyectoPK().getFolioProyecto(), getProyecto().getClaveProyecto(), getProyecto().getProyectoPK().getSerialProyecto());

        setEdoAfectado(null);
        todosEdos = null;
        String mensaje = "";

        Integer id;
//
////        String aux2;
//
        //---------------si trae obras o predios
        if (!pObra.isEmpty() || !pPredio.isEmpty()) {
            //----------si trae obras Y predios
            if (!pObra.isEmpty() && !pPredio.isEmpty()) {
                id = Integer.parseInt(pObra.get(pObra.size() - 1)[0].toString());
                if (id != 0) {
                    edoAfectado = dao.edoMasAfectado(getProyecto().getProyectoPK().getFolioProyecto(), getProyecto().getClaveProyecto(), getProyecto().getProyectoPK().getSerialProyecto(), id);
                }
            }
            //----------si trae obras Y NO predios
            if (!pObra.isEmpty() && pPredio.isEmpty()) {
                id = Integer.parseInt(pObra.get(pObra.size() - 1)[0].toString());
                if (id != 0) {
                    edoAfectado = dao.edoMasAfectado(getProyecto().getProyectoPK().getFolioProyecto(), getProyecto().getClaveProyecto(), getProyecto().getProyectoPK().getSerialProyecto(), id);
                }
            }
            //----------si NO trae obras Y SI predios
            if (pObra.isEmpty() && !pPredio.isEmpty()) {
                id = Integer.parseInt(pPredio.get(pPredio.size() - 1)[0].toString());
                if (id != 0) {
                    edoAfectado = dao.edoMasAfectado(getProyecto().getProyectoPK().getFolioProyecto(), getProyecto().getClaveProyecto(), getProyecto().getProyectoPK().getSerialProyecto(), id);
                    setEdoAfectado(edoAfectado);
                }
            }
        }

        todosEdos = dao.edoTodosAfectado(getProyecto().getProyectoPK().getFolioProyecto(), getProyecto().getClaveProyecto(), getProyecto().getProyectoPK().getSerialProyecto());

        if (pObra.isEmpty() || pPredio.isEmpty()) {
            mensaje = "Debe georefenciar correctamente su proyecto";
            todosEdos = null;
        }
        if (mensaje.length() != 0) {
            FacesMessage message = new FacesMessage(FacesMessage.SEVERITY_INFO, "Debe georefenciar correctamente su proyecto", null);
            FacesContext.getCurrentInstance().addMessage(null, message);
        }
        return todosEdos;
    }

    /**
     * @return the todosEdos
     */
    public List<Object[]> getTodosEdos() {
        return todosEdos;
    }

    /**
     * @return the proyecto
     */
    public Proyecto getProyecto() {
        return proyecto;
    }

    public void setProyecto(Proyecto proyecto) {
        this.proyecto = proyecto;
    }

    public SelectItem[] getSubSectorSelectOne() {

        List<SubsectorProyecto> lista;// = new ArrayList<SubsectorProyecto>();
        lista = dao.todosCatSubSec();

        int size = lista.size() + 1;
        SelectItem[] items = new SelectItem[size];
        int i = 0;
        items[0] = new SelectItem("0", "-- Seleccione --");
        i++;
        for (SubsectorProyecto x : lista) {

            items[i++] = new SelectItem(x.getNsub().toString(), x.getSubsector());
        }
        return items;

    }

    public SelectItem[] getRamaSelectOne() {
        SelectItem[] items = null;
        if (proyecto.getNsub() != null) {

            List<RamaProyecto> lista;// = new ArrayList<RamaProyecto>();
            short idSubsec = proyecto.getNsub();   //Short.parseShort(referencia);

            lista = dao.rama(idSubsec);

            int size = lista.size() + 1;
            items = new SelectItem[size];
            int i = 0;
            //if (selectOne) {
            items[0] = new SelectItem("0", "-- Seleccione --");
            i++;
            //}
            for (RamaProyecto x : lista) {

                items[i++] = new SelectItem(x.getNrama().toString(), x.getRama());
            }

        }

        return items;
    }

    public SelectItem[] getTipoProySelectOne() {
        SelectItem[] items = null;
        if (proyecto.getNrama() != null) {
            List<TipoProyecto> lista;// = new ArrayList<TipoProyecto>();
            short idRama = proyecto.getNrama();   //Short.parseShort(referenciaRama);

            lista = dao.todosTipProySubSec(idRama);

            int size = lista.size() + 1;
            items = new SelectItem[size];
            int i = 0;
            //if (selectOne) {
            items[0] = new SelectItem("0", "-- Seleccione --");
            i++;
            //}
            for (TipoProyecto x : lista) {

                items[i++] = new SelectItem(x.getNtipo().toString(), x.getTipoProyecto());
            }

        }

        return items;
    }

    // Selecciona referencia
    public void seleccionaReferencia() {

    }

    /**
     * @return the opcRama
     */
    public List<SelectItem> getopcRama() {
        return opcRama;
    }

    /**
     * @param opcSubsector the opcRama to set
     */
    public void setOpcopcRama(List<SelectItem> opcSubsector) {
        this.opcRama = opcSubsector;
    }

    /**
     * @return the referencia
     */
    public String getReferencia() {
        return referencia;
    }

    /**
     * @param referencia the referencia to set
     */
    public void setReferencia(String referencia) {
        this.referencia = referencia;
    }

    /**
     * @return the localiz
     */
    public Boolean getLocaliz() {
        return localiz;
    }

    /**
     * @param localiz the nom to set
     */
    public void setLocaliz(Boolean localiz) {
        this.localiz = localiz;
    }

    /**
     * @return the ramaSeleccionada
     */
    public RamaProyecto getRamaSeleccionada() {
        return ramaSeleccionada;
    }

    /**
     * @param ramaSeleccionada the ramaSeleccionada to set
     */
    public void setRamaSeleccionada(RamaProyecto ramaSeleccionada) {
        this.ramaSeleccionada = ramaSeleccionada;
    }

    /**
     * @param edoAfectado the edoAfectado to set
     */
    public void setEdoAfectado(List<Object[]> edoAfectado) {
        this.edoAfectado = edoAfectado;

    }

    /**
     * @return the referenciaRama
     */
    public String getReferenciaRama() {
        return referenciaRama;
    }

    /**
     * @param referenciaRama the referenciaRama to set
     */
    public void setReferenciaRama(String referenciaRama) {
        this.referenciaRama = referenciaRama;
    }

    /**
     * @return the sectorProyecto
     */
    public List<Object[]> getSectorProyecto() {
        //Carga Subsectores  
        if (proyecto.getNsub() != null) {
            if (proyecto.getNsub() != 0) {
                short idSubsec = proyecto.getNsub(); //Short.parseShort(referencia);
                List<Object[]> p = dao.sector(idSubsec);
                //JOptionPane.showMessageDialog(null, "Debe:  " + p.size(), "Error", JOptionPane.INFORMATION_MESSAGE);
                if (p.size() > 0) {
                    sectorProyecto = p;
                    sectorProyectoConsultado = new SectorProyecto();
                }
            } else {
                sectorProyecto = null;
            }
        }

        return sectorProyecto;
    }

    /**
     * @param sectorProyecto the sectorProyecto to set
     */
    public void setSectorProyecto(List<Object[]> sectorProyecto) {

        this.sectorProyecto = sectorProyecto;
    }

//----------------para seccion domicilio    
    /**
     * @return the tipoDomic
     */
    public Character getTipoDomic() {
        return tipoDomic;
    }

    /**
     * @param tipoDomic the tipoDomic to set
     */
    public void setTipoDomic(Character tipoDomic) {
        this.tipoDomic = tipoDomic;
    }

    // Selecciona referencia
    public void seleccionaDomicilio() {
        localiz = false;
        localizNo = false;
        if (proyecto.getProyDomEstablecido() != null) {
            System.out.println("Valor " + tipoDomic);
            //Carga Normas Oficiales
            if (proyecto.getProyDomEstablecido().toString().equals("S")) {  //SI tiene domicilio establecido
                localiz = true;
                localizNo = false;
            }
            if (proyecto.getProyDomEstablecido().toString().equals("N")) {  //NO tiene domicilio establecido
                localizNo = true;
                localiz = false;
            }
        }
    }

    /**
     * @return the localizNo
     */
    public Boolean getLocalizNo() {
        return localizNo;
    }

    /**
     * @param localizNo the localizNo to set
     */
    public void setLocalizNo(Boolean localizNo) {
        this.localizNo = localizNo;
    }

//---------------------seccion de catalogos
    public SelectItem[] getVialidadSelectOne() {

        List<CatVialidad> lista1;// = new ArrayList<CatVialidad>();
        lista1 = dao.todosCatVialidad();

        int size = lista1.size() + 1;
        SelectItem[] items = new SelectItem[size];
        int i = 0;
        items[0] = new SelectItem("0", "-- Seleccione --");
        i++;
        for (CatVialidad x : lista1) {

            items[i++] = new SelectItem(x.getCveTipoVial().toString(), x.getDescripcion());
        }
        return items;

    }

    public SelectItem[] getTipAseSelectOne() {

        List<CatTipoAsen> lista1;// = new ArrayList<CatTipoAsen>();
        lista1 = dao.todosCatAsent();

        int size = lista1.size() + 1;
        SelectItem[] items = new SelectItem[size];
        int i = 0;
        //if (selectOne) {
        items[0] = new SelectItem("0", "-- Seleccione --");
        i++;
        //}
        for (CatTipoAsen x : lista1) {

            items[i++] = new SelectItem(x.getCveTipoAsen().toString(), x.getNombre());
        }
        return items;

    }

    /**
     * @return the proySigSupProy
     */
    public List<Object[]> getProySigSupProy() {
        return proySigSupProy;
    }

    /**
     * @param proySigSupProy the proySigSupProy to set
     */
    public void setProySigSupProy(List<Object[]> proySigSupProy) {
        this.proySigSupProy = proySigSupProy;
    }

    /**
     * @return the proySigSupProyObras
     */
    public List<Object[]> getProySigSupProyObras() {
        List<Object[]> proySigSupProyObras1 = dao.proySigSumObras(getProyecto().getProyectoPK().getFolioProyecto(), getProyecto().getClaveProyecto(), getProyecto().getProyectoPK().getSerialProyecto());
        return proySigSupProyObras1;
    }

    /**
     * @return the proySigSupProyPredios
     */
    public List<Object[]> getProySigSupProyPredios() {
        List<Object[]> proySigSupProyPredios1 = dao.proySigSumPredios(getProyecto().getProyectoPK().getFolioProyecto(), getProyecto().getClaveProyecto(), getProyecto().getProyectoPK().getSerialProyecto());
        return proySigSupProyPredios1;
    }

    //------------------------------setes y getter's de variables para guardar
    /**
     * @return the nombreProy
     */
    public String getNombreProy() {
        return nombreProy;
    }

    /**
     * @param nombreProy the nombreProy to set
     */
    public void setNombreProy(String nombreProy) {
        this.nombreProy = nombreProy;
    }

    /**
     * @return the tipoProyecto
     */
    public String getTipoProyecto() {
        return tipoProyecto;
    }

    /**
     * @param tipoProyecto the tipoProyecto to set
     */
    public void setTipoProyecto(String tipoProyecto) {
        this.tipoProyecto = tipoProyecto;
    }

    /**
     * @return the tipoVialidad
     */
    public String getTipoVialidad() {
        return tipoVialidad;
    }

    /**
     * @param tipoVialidad the tipoVialidad to set
     */
    public void setTipoVialidad(String tipoVialidad) {
        this.tipoVialidad = tipoVialidad;
    }

    /**
     * @return the nombVialidad
     */
    public String getNombVialidad() {
        return nombVialidad;
    }

    /**
     * @param nombVialidad the nombVialidad to set
     */
    public void setNombVialidad(String nombVialidad) {
        this.nombVialidad = nombVialidad;
    }

    /**
     * @return the numInterior
     */
    public String getNumInterior() {
        return numInterior;
    }

    /**
     * @param numInterior the numInterior to set
     */
    public void setNumInterior(String numInterior) {
        this.numInterior = numInterior;
    }

    /**
     * @return the numExterior
     */
    public String getNumExterior() {
        return numExterior;
    }

    /**
     * @param numExterior the numExterior to set
     */
    public void setNumExterior(String numExterior) {
        this.numExterior = numExterior;
    }

    /**
     * @return the tipoAsentamiento
     */
    public String getTipoAsentamiento() {
        return tipoAsentamiento;
    }

    /**
     * @param tipoAsentamiento the tipoAsentamiento to set
     */
    public void setTipoAsentamiento(String tipoAsentamiento) {
        this.tipoAsentamiento = tipoAsentamiento;
    }

    /**
     * @return the nombAsentamiento
     */
    public String getNombAsentamiento() {
        return nombAsentamiento;
    }

    /**
     * @param nombAsentamiento the nombAsentamiento to set
     */
    public void setNombAsentamiento(String nombAsentamiento) {
        this.nombAsentamiento = nombAsentamiento;
    }

    /**
     * @return the codigoPostal
     */
    public String getCodigoPostal() {
        return codigoPostal;
    }

    /**
     * @param codigoPostal the codigoPostal to set
     */
    public void setCodigoPostal(String codigoPostal) {
        this.codigoPostal = codigoPostal;
    }

    /**
     * @return the direccCompleta
     */
    public String getDireccCompleta() {
        return direccCompleta;
    }

    /**
     * @param direccCompleta the direccCompleta to set
     */
    public void setDireccCompleta(String direccCompleta) {
        this.direccCompleta = direccCompleta;
    }

//-------------------planos adicionales
    public void addPlano() {
        anexosProyecto.add(new AnexosProyecto());

    }

    public String removePlano() {
        if (planoAdicSelect != null) {
            anexosProyecto.remove(planoAdicSelect);
        }
        return null;
    }

    /**
     * @return the planoAdicSelect
     */
    public AnexosProyecto getPlanoAdicSelect() {
        return planoAdicSelect;
    }

    /**
     * @param planoAdicSelect the planoAdicSelect to set
     */
    public void setPlanoAdicSelect(AnexosProyecto planoAdicSelect) {
        this.planoAdicSelect = planoAdicSelect;
    }

    /**
     * @return the anexosProyecto
     */
    public List<AnexosProyecto> getAnexosProyecto() {
        return anexosProyecto;
    }

    /**
     * @param anexosProyecto the anexosProyecto to set
     */
    public void setAnexosProyecto(List<AnexosProyecto> anexosProyecto) {
        this.anexosProyecto = anexosProyecto;
    }

    public Double getTotalInv() {
        return proyecto.getProyInversionRequerida() != null
                && proyecto.getProyMedidasPrevencion() != null
                        ? proyecto.getProyInversionRequerida().doubleValue()
                        + proyecto.getProyMedidasPrevencion().doubleValue() : null;
    }

    public Integer getTotalEmpleos() {
        return proyecto.getProyEmpleosPermanentes() != null
                && proyecto.getProyEmpleosTemporales() != null
                        ? proyecto.getProyEmpleosPermanentes()
                        + proyecto.getProyEmpleosTemporales() : null;

    }

    /**
     * @return the btnGuardar
     */
    public Boolean getBtnGuardar() {
        return btnGuardar;
    }

    /**
     * @param btnGuardar the btnGuardar to set
     */
    public void setBtnGuardar(Boolean btnGuardar) {
        this.btnGuardar = btnGuardar;
    }

    /**
     * @return the blos
     */
    public Boolean getBlos() {
        return blos;
    }

    /**
     * @param blos the blos to set
     */
    public void setBlos(Boolean blos) {
        this.blos = blos;
    }

	/**
	 * @return the existeSector
	 */
	public Boolean getExisteSector() {
		return existeSector;
	}

	/**
	 * @param existeSector the existeSector to set
	 */
	public void setExisteSector(Boolean existeSector) {
		this.existeSector = existeSector;
	}

	/**
	 * @return the existeSubSector
	 */
	public Boolean getExisteSubSector() {
		return existeSubSector;
	}

	/**
	 * @param existeSubSector the existeSubSector to set
	 */
	public void setExisteSubSector(Boolean existeSubSector) {
		this.existeSubSector = existeSubSector;
	}

	/**
	 * @return the sectorProyectoConsultado
	 */
	public SectorProyecto getSectorProyectoConsultado() {
		return sectorProyectoConsultado;
	}

	/**
	 * @param sectorProyectoConsultado the sectorProyectoConsultado to set
	 */
	public void setSectorProyectoConsultado(SectorProyecto sectorProyectoConsultado) {
		this.sectorProyectoConsultado = sectorProyectoConsultado;
	}

	/**
	 * @return the subsectorProyecto
	 */
	public SubsectorProyecto getSubsectorProyectoConsultado() {
		return subSectorProyectoConsultado;
	}

	/**
	 * @param subsectorProyecto the subsectorProyecto to set
	 */
	public void setSubsectorProyectoConsultado(SubsectorProyecto subsectorProyecto) {
		this.subSectorProyectoConsultado = subsectorProyecto;
	}

	/**
	 * @return the existeNormaProyecto
	 */
	public Boolean getExisteNormaProyecto() {
		return existeNormaProyecto;
	}

	/**
	 * @param existeNormaProyecto the existeNormaProyecto to set
	 */
	public void setExisteNormaProyecto(Boolean existeNormaProyecto) {
		this.existeNormaProyecto = existeNormaProyecto;
	}

	/**
	 * @return the subSectorProyectoConsultado
	 */
	public SubsectorProyecto getSubSectorProyectoConsultado() {
		return subSectorProyectoConsultado;
	}

	/**
	 * @param subSectorProyectoConsultado the subSectorProyectoConsultado to set
	 */
	public void setSubSectorProyectoConsultado(SubsectorProyecto subSectorProyectoConsultado) {
		this.subSectorProyectoConsultado = subSectorProyectoConsultado;
	}

	/**
	 * @return the ramaProyectoConsultado
	 */
	public RamaProyecto getRamaProyectoConsultado() {
		return ramaProyectoConsultado;
	}

	/**
	 * @param ramaProyectoConsultado the ramaProyectoConsultado to set
	 */
	public void setRamaProyectoConsultado(RamaProyecto ramaProyectoConsultado) {
		this.ramaProyectoConsultado = ramaProyectoConsultado;
	}

	/**
	 * @return the tipoProyectoConsultado
	 */
	public TipoProyecto getTipoProyectoConsultado() {
		return tipoProyectoConsultado;
	}

	/**
	 * @param tipoProyectoConsultado the tipoProyectoConsultado to set
	 */
	public void setTipoProyectoConsultado(TipoProyecto tipoProyectoConsultado) {
		this.tipoProyectoConsultado = tipoProyectoConsultado;
	}

	public Boolean getDesactivaBtnGuia() {
		System.out.println("Id Subsector= "+proyecto.getNsub());
    	System.out.println("Clave de tramite= "+66);
    	System.out.println("Existen "+GuiasPdf.size()+" guias");
    	
		if(!guiasPdfValidas.isEmpty()){
			desactivaBtnGuia=false;
		}else{
			desactivaBtnGuia=true;
		}
		return desactivaBtnGuia;
	}

	public void setDesactivaBtnGuia(Boolean desactivaBtnGuia) {
		this.desactivaBtnGuia = desactivaBtnGuia;
	}

	public List<ProySubsectorGuias> getGuiasPdf() {
		return GuiasPdf;
	}

	public void setGuiasPdf(List<ProySubsectorGuias> guiasPdf) {
		GuiasPdf = guiasPdf;
	}

	public ArrayList<String> getGuiasPdfValidas() {
		return guiasPdfValidas;
	}

	public void setGuiasPdfValidas(ArrayList<String> guiasPdfValidas) {
		this.guiasPdfValidas = guiasPdfValidas;
	}
	
	
}
