/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.gob.semarnat.view;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import javax.persistence.EntityExistsException;
import mx.gob.semarnat.dao.Capitulo1Dao;
import mx.gob.semarnat.dao.Capitulo3Dao;
import mx.gob.semarnat.dao.ProyectoDao;
import mx.gob.semarnat.dao.SinatecProcedure;
import mx.gob.semarnat.model.CatContaminante;
import mx.gob.semarnat.model.CatTipoContaminante;
import mx.gob.semarnat.model.ContaminanteProyecto;
import mx.gob.semarnat.model.ContaminanteProyectoPK;
import mx.gob.semarnat.model.EtapaProyecto;
import mx.gob.semarnat.model.Proyecto;
import org.primefaces.context.RequestContext;

/**
 *
 * @author Admin
 */
@ManagedBean(name = "cap33")
@ViewScoped
public class Cap33_EmisionResDes implements Serializable {

    private final ProyectoDao proyectoDao = new ProyectoDao();
    private Capitulo3Dao dao = new Capitulo3Dao();
    private List<ContaminantesHelper> contaminanteProyectos;
    private List<ContaminanteProyecto> contaminanteProyectosBD;
    private ContaminantesHelper contaminanteProyectoSelect;
    private List<EtapaProyecto> etapaProyectos;
    private boolean nuevo = false;
    // Proyecto recupera el proyecto de la sesion
    private final Capitulo1Dao daoCap1 = new Capitulo1Dao();
    private final Proyecto proyecto;

    public Cap33_EmisionResDes() {
        etapaProyectos = new ArrayList();
        contaminanteProyectos = new ArrayList();
        //cargar proyecto
        FacesContext fContext = FacesContext.getCurrentInstance();
        fContext.getExternalContext().getSessionMap().get("userFolioProy");
        String folioProyecto = (String) fContext.getExternalContext().getSessionMap().get("folioProyecto");
        Short serialProyecto = (Short) fContext.getExternalContext().getSessionMap().get("serialProyecto");

        proyecto = daoCap1.cargaProyecto(folioProyecto, serialProyecto);

        int i = 0;
        for (ContaminanteProyecto cp : (List<ContaminanteProyecto>) dao.listado_where_comp(ContaminanteProyecto.class, new String[]{"contaminanteProyectoPK", "contaminanteProyectoPK"},
                new String[]{"folioProyecto", "serialProyecto"},
                new Object[]{proyecto.getProyectoPK().getFolioProyecto(), proyecto.getProyectoPK().getSerialProyecto()})) {
            contaminanteProyectos.add(new ContaminantesHelper(cp,
                    (List<CatContaminante>) dao.listado_where(CatContaminante.class, "tipoContaminanteId",
                            dao.busca(CatTipoContaminante.class, cp.getCatContaminante().getTipoContaminanteId().getTipoContaminanteId())), cp.getCatContaminante().getTipoContaminanteId().getTipoContaminanteId().toString(),
                    (List<CatTipoContaminante>) dao.listado(CatTipoContaminante.class), i));
            i++;
        }
        if (contaminanteProyectos.isEmpty()) {
            List<CatTipoContaminante> listado = (List<CatTipoContaminante>) dao.listado(CatTipoContaminante.class);
            contaminanteProyectos.add(new ContaminantesHelper(new ContaminanteProyecto(),
                    (List<CatContaminante>) dao.listado_where(CatContaminante.class, "tipoContaminanteId", listado.get(0)), "",
                    listado,
                    0));
            nuevo = true;
        } else {
            contaminanteProyectosBD = new ArrayList();
            for (ContaminantesHelper ch : contaminanteProyectos) {
                contaminanteProyectosBD.add(ch.contaminante);
            }
        }
    }

    public void dropResiduo() {
        contaminanteProyectos.remove(contaminanteProyectoSelect);
    }

    public void addResiduo() {
        int i = contaminanteProyectos.get(contaminanteProyectos.size() - 1).getId();
        i++;
        List<CatTipoContaminante> listado = (List<CatTipoContaminante>) dao.listado(CatTipoContaminante.class);
        contaminanteProyectos.add(new ContaminantesHelper(new ContaminanteProyecto(),
                (List<CatContaminante>) dao.listado_where(CatContaminante.class, "tipoContaminanteId", listado.get(0)), "",
                listado,
                i));
    }

    public void guardar() {
        RequestContext reqcontEnv = RequestContext.getCurrentInstance();
        if (proyecto.getProySupuesto() != null) {
            if (nuevo) {
                try {
                    for (ContaminantesHelper cp : contaminanteProyectos) {
                        cp.getContaminante().setContaminanteProyectoPK(
                                new ContaminanteProyectoPK(
                                        proyecto.getProyectoPK().getFolioProyecto(),
                                        proyecto.getProyectoPK().getSerialProyecto(),
                                        cp.getContaminante().getCatEtapa().getEtapaId(),
                                        cp.getContaminante().getCatContaminante().getContaminanteId()));
                        dao.agrega(cp.contaminante);
                    }
//                    dao.guardarAvance(proyecto, new Short(proyecto.getProyNormaId() == 129 ? "4" : "6"), "33");
                    if (proyecto.getProySupuesto() == '1') {
                        dao.guardarAvance(proyecto, new Short(proyecto.getProyNormaId() == 129 ? "4" : "6"), "33");
                    } else {
                        dao.guardarAvance(proyecto, new Short("6"), "33");
                    }
                    reqcontEnv.execute("alert('Su información ha sido guardada con éxito.');");

                } catch (Exception e) {
                    e.printStackTrace();
                    reqcontEnv.execute("alert('No se ha podido guardar la información, Intente más tarde.');");

                }
            } else {
                try {
                    for (ContaminanteProyecto cpp : contaminanteProyectosBD) {
                        dao.elimina(ContaminanteProyecto.class, cpp.getContaminanteProyectoPK());
                    }
                    for (ContaminantesHelper cp : contaminanteProyectos) {
                        try {
                            cp.getContaminante().setContaminanteProyectoPK(
                                    new ContaminanteProyectoPK(
                                            proyecto.getProyectoPK().getFolioProyecto(),
                                            proyecto.getProyectoPK().getSerialProyecto(),
                                            cp.getContaminante().getCatEtapa().getEtapaId(),
                                            cp.getContaminante().getCatContaminante().getContaminanteId()));
                            dao.agrega(cp.contaminante);
                        } catch (EntityExistsException ex) {
                            dao.modifica(cp.contaminante);
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                    reqcontEnv.execute("alert('Su informacion ha sido guardada con exito.');");

                } catch (Exception e) {
                    e.printStackTrace();
                    reqcontEnv.execute("alert('No se ha podido guardar la informacion, Intente mas tarde.');");

                }
            }
            //método para guardar avance en sinatec y verificar
            SinatecProcedure sp = new SinatecProcedure();
            int avance = Integer.parseInt(proyectoDao.avanceProyecto(proyecto));
            sp.ejecutaSP(Integer.parseInt(proyecto.getProyectoPK().getFolioProyecto()), avance);
            if (avance == 99) {
                reqcontEnv.execute("alert('Ha completado el registro de datos del Informe Preventivo.')");
            }
            //terminan procedimientos con SINATEC
        } else {
            reqcontEnv.execute("alert('Capture el capítulo 1 antes de proceder con los demás capítulos')");
        }
    }
//getter y setter

    public List<ContaminantesHelper> getContaminanteProyectos() {
        return contaminanteProyectos;
    }

    public void setContaminanteProyectos(List<ContaminantesHelper> contaminanteProyectos) {
        this.contaminanteProyectos = contaminanteProyectos;
    }

    public ContaminantesHelper getContaminanteProyectoSelect() {
        return contaminanteProyectoSelect;
    }

    public void setContaminanteProyectoSelect(ContaminantesHelper contaminanteProyectoSelect) {
        this.contaminanteProyectoSelect = contaminanteProyectoSelect;
    }

    public List<EtapaProyecto> getEtapaProyectos() {
        return etapaProyectos;
    }

    public void setEtapaProyectos(List<EtapaProyecto> etapaProyectos) {
        this.etapaProyectos = etapaProyectos;
    }
    //metodo para selectOneDinamico

    public void cargaContaminantes() {
        for (ContaminantesHelper cH : getContaminanteProyectos()) {
            cH.setCatContaminante((List<CatContaminante>) dao.listado_where(CatContaminante.class, "tipoContaminanteId",
                    dao.busca(CatTipoContaminante.class, new Short(cH.seleccion))));

        }
    }

    //clase de ayuda-----------
    public class ContaminantesHelper {

        private ContaminanteProyecto contaminante;
        private List<CatContaminante> catContaminante;
        private String seleccion;
        private Integer id;
        private List<CatTipoContaminante> catTipoContaminante;

        public ContaminantesHelper(ContaminanteProyecto contaminante, List<CatContaminante> catContaminante, String seleccion, List<CatTipoContaminante> catTipoContaminante, Integer id) {
            this.contaminante = contaminante;
            this.catContaminante = catContaminante;
            this.seleccion = seleccion;
            this.id = id;
            this.catTipoContaminante = catTipoContaminante;
        }

        public ContaminanteProyecto getContaminante() {
            return contaminante;
        }

        public void setContaminante(ContaminanteProyecto contaminante) {
            this.contaminante = contaminante;
        }

        public List<CatContaminante> getCatContaminante() {
            return catContaminante;
        }

        public void setCatContaminante(List<CatContaminante> catContaminante) {
            this.catContaminante = catContaminante;
        }

        public List<CatTipoContaminante> getCatTipoContaminante() {
            return catTipoContaminante;
        }

        public void setCatTipoContaminante(List<CatTipoContaminante> catTipoContaminante) {
            this.catTipoContaminante = catTipoContaminante;
        }

        public String getSeleccion() {
            return seleccion;
        }

        public void setSeleccion(String seleccion) {
            this.seleccion = seleccion;
        }

        public Integer getId() {
            return id;
        }

        public void setId(Integer id) {
            this.id = id;
        }
    }
}
