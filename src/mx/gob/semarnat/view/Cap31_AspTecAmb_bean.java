/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.gob.semarnat.view;

import java.io.Serializable;
import java.util.List;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import mx.gob.semarnat.dao.Capitulo1Dao;
import mx.gob.semarnat.dao.Capitulo3Dao;
import mx.gob.semarnat.dao.ProyectoDao;
import mx.gob.semarnat.dao.SinatecProcedure;
import mx.gob.semarnat.model.CatEtapa;
import mx.gob.semarnat.model.EtapaProyecto;
import mx.gob.semarnat.model.EtapaProyectoPK;
import mx.gob.semarnat.model.Proyecto;
import org.primefaces.context.RequestContext;

/**
 *
 * @author Administrador
 */
@ManagedBean(name = "cap31")
@ViewScoped
public class Cap31_AspTecAmb_bean implements Serializable {

    private ProyectoDao proyectoDao = new ProyectoDao();
    private static final long serialVersionUID = 20111020L;
    private Boolean banderas[];
    private Boolean nuevo[];
    private String anios[];
    private String meses[];
    private EtapaProyecto preparacionSitio = new EtapaProyecto();
    private EtapaProyecto construccion = new EtapaProyecto();
    private EtapaProyecto operaMantenim = new EtapaProyecto();
    private EtapaProyecto abandono = new EtapaProyecto();
    private final Capitulo3Dao dao = new Capitulo3Dao();
    // Proyecto recupera el proycto de la sesion
    private Proyecto proyecto;
    private final Capitulo1Dao daoCap1 = new Capitulo1Dao();

    public Cap31_AspTecAmb_bean() {
        //cargar proyecto
        FacesContext fContext = FacesContext.getCurrentInstance();
        fContext.getExternalContext().getSessionMap().get("userFolioProy");
        String folioProyecto = (String) fContext.getExternalContext().getSessionMap().get("folioProyecto");
        Short serialProyecto = (Short) fContext.getExternalContext().getSessionMap().get("serialProyecto");

        proyecto = daoCap1.cargaProyecto(folioProyecto, serialProyecto);
        //fin de la carga del proyecto
        banderas = new Boolean[]{false, false, false, false};
        anios = new String[]{null, null, null, null, null};
        meses = new String[]{null, null, null, null, null};
        cargaLista();
    }

    public void actualizaVista() {
        for (int i = 1; i <= 4; i++) {
            if (anios[i] != null) {
                banderas[i - 1] = Integer.parseInt("".equals(anios[i]) ? "-1" : anios[i]) > 0;
            }
            if (meses[i] != null) {
                if (!banderas[i - 1] || anios[i] == null || anios[i].equals("")) {
                    banderas[i - 1] = Integer.parseInt("".equals(meses[i]) ? "-1" : meses[i]) > 0;
                }
            }
        }
    }

    private void cargaLista() {
        nuevo = new Boolean[]{true, true, true, true};
        List<EtapaProyecto> etapa;
        if (proyecto.getProyTiempoVidaAnios() != null && proyecto.getProyTiempoVidaMeses() != null) {
            anios[0] = proyecto.getProyTiempoVidaAnios().toString();
            meses[0] = proyecto.getProyTiempoVidaMeses().toString();
        }

        try {
            etapa = (List<EtapaProyecto>) dao.listado_where_comp(EtapaProyecto.class, "etapaProyectoPK", "folioProyecto", proyecto.getProyectoPK().getFolioProyecto());

            for (EtapaProyecto e : etapa) {
                nuevo[(int) e.getCatEtapa().getEtapaId() - 1] = false;
                anios[(int) e.getCatEtapa().getEtapaId()] = e.getAnios() + "";
                meses[(int) e.getCatEtapa().getEtapaId()] = e.getMeses() + "";
                banderas[(int) e.getCatEtapa().getEtapaId() - 1] = e.getAnios() > 0 || e.getMeses() > 0;
                switch ((int) e.getCatEtapa().getEtapaId()) {
                    case 1:
                        preparacionSitio = e;
                        break;
                    case 2:
                        construccion = e;
                        break;
                    case 3:
                        operaMantenim = e;
                        break;
                    case 4:
                        abandono = e;
                        break;
                }
            }
        } catch (NullPointerException e) {
        } catch (Exception e) {
        }
    }

    public void guardar() {
        RequestContext reqcontEnv = RequestContext.getCurrentInstance();
        if (proyecto.getProySupuesto() != null) {
            try {
                Proyecto p = (Proyecto) daoCap1.getObject(new Proyecto(), proyecto.getProyectoPK());
                System.out.println("LOTE: " + p.getProyLote());
                if (proyecto.getProySupuesto() == '1') {
                    dao.guardarAvance(proyecto, new Short(proyecto.getProyNormaId() == 129 ? "5" : "6"), "31");
                }else{
                    dao.guardarAvance(proyecto, new Short("6"), "31");
                }

                proyecto.setProyTiempoVidaAnios(new Short(anios[0].equals("") ? "0" : anios[0]));
                proyecto.setProyTiempoVidaMeses(new Short(meses[0].equals("") ? "0" : meses[0]));
                if (p.getProyLote() != null) {
                    Integer lote;
                    lote = p.getProyLote().intValue();
                    if (proyecto.getProyLote() == null) {
                        proyecto.setProyLote(lote.longValue());
                    }
                }

                dao.modifica(proyecto);//actualiza el proyecto, pone duracion

                if (banderas[0] || !nuevo[0]) {//pasa si aplica la etapa de preparación
                    preparacionSitio.setProyecto(proyecto);
//                System.out.println("prepara: " + preparacionSitio.getEtapaDescripcionObraAct());
                    preparacionSitio.setAnios(Short.parseShort(anios[1].equals("") ? "0" : anios[1]));
                    preparacionSitio.setMeses(Short.parseShort(meses[1].equals("") ? "0" : meses[1]));
                    preparacionSitio.setCatEtapa((CatEtapa) dao.busca(CatEtapa.class, new Short("1")));
                    preparacionSitio.setEtapaProyectoPK(new EtapaProyectoPK(proyecto.getProyectoPK().getFolioProyecto(), proyecto.getProyectoPK().getSerialProyecto(), preparacionSitio.getCatEtapa().getEtapaId()));
                    //verificar si tiene duración, sino, borra el registro y todas sus dependencias
                    if (preparacionSitio.getAnios() > 0 || preparacionSitio.getMeses() > 0) {
                        dao.modifica(preparacionSitio); //actualiza si tiene duración
                    } else {//borra si no tiene duración
                        dao.eliminaSustancias(proyecto.getProyectoPK().getSerialProyecto(), proyecto.getProyectoPK().getFolioProyecto(), preparacionSitio.getCatEtapa());
                        dao.eliminaImpacto(proyecto.getProyectoPK().getSerialProyecto(), proyecto.getProyectoPK().getFolioProyecto(), preparacionSitio.getCatEtapa());
                        dao.elimina(EtapaProyecto.class, preparacionSitio.getEtapaProyectoPK());

                    }
                }
                if (banderas[1] || !nuevo[1]) {//pasa si aplica la etapa de construccion
                    construccion.setProyecto(proyecto);
                    construccion.setAnios(Short.parseShort(anios[2].equals("") ? "0" : anios[2]));
                    construccion.setMeses(Short.parseShort(meses[2].equals("") ? "0" : meses[2]));
                    construccion.setCatEtapa((CatEtapa) dao.busca(CatEtapa.class, new Short("2")));
                    construccion.setEtapaProyectoPK(new EtapaProyectoPK(proyecto.getProyectoPK().getFolioProyecto(), proyecto.getProyectoPK().getSerialProyecto(), construccion.getCatEtapa().getEtapaId()));
                    //verificar si tiene duración, sino, borra el registro y todas sus dependencias
                    if (construccion.getAnios() > 0 || construccion.getMeses() > 0) {
                        dao.modifica(construccion); //actualiza si tiene duración
                    } else {//borra si no tiene duración
                        dao.eliminaSustancias(proyecto.getProyectoPK().getSerialProyecto(), proyecto.getProyectoPK().getFolioProyecto(), construccion.getCatEtapa());
                        dao.eliminaImpacto(proyecto.getProyectoPK().getSerialProyecto(), proyecto.getProyectoPK().getFolioProyecto(), construccion.getCatEtapa());
                        dao.elimina(EtapaProyecto.class, construccion.getEtapaProyectoPK());

                    }
                }
                if (banderas[2] || !nuevo[2]) {//pasa si aplica la etapa de operacion y mantenimeinto
                    operaMantenim.setProyecto(proyecto);
                    operaMantenim.setAnios(Short.parseShort(anios[3].equals("") ? "0" : anios[3]));
                    operaMantenim.setMeses(Short.parseShort(meses[3].equals("") ? "0" : meses[3]));
                    operaMantenim.setCatEtapa((CatEtapa) dao.busca(CatEtapa.class, new Short("3")));
                    operaMantenim.setEtapaProyectoPK(new EtapaProyectoPK(proyecto.getProyectoPK().getFolioProyecto(), proyecto.getProyectoPK().getSerialProyecto(), operaMantenim.getCatEtapa().getEtapaId()));
                    //verificar si tiene duración, sino, borra el registro y todas sus dependencias
                    if (operaMantenim.getAnios() > 0 || operaMantenim.getMeses() > 0) {
                        dao.modifica(operaMantenim); //actualiza si tiene duración
                    } else {//borra si no tiene duración
                        dao.eliminaSustancias(proyecto.getProyectoPK().getSerialProyecto(), proyecto.getProyectoPK().getFolioProyecto(), operaMantenim.getCatEtapa());
                        dao.eliminaImpacto(proyecto.getProyectoPK().getSerialProyecto(), proyecto.getProyectoPK().getFolioProyecto(), operaMantenim.getCatEtapa());
                        dao.elimina(EtapaProyecto.class, operaMantenim.getEtapaProyectoPK());

                    }
                }
                if (banderas[3] || !nuevo[3]) {//pasa si aplica la etapa de abandono
                    abandono.setProyecto(proyecto);
                    abandono.setAnios(Short.parseShort(anios[4].equals("") ? "0" : anios[4]));
                    abandono.setMeses(Short.parseShort(meses[4].equals("") ? "0" : meses[4]));
                    abandono.setCatEtapa((CatEtapa) dao.busca(CatEtapa.class, new Short("4")));
                    abandono.setEtapaProyectoPK(new EtapaProyectoPK(proyecto.getProyectoPK().getFolioProyecto(), proyecto.getProyectoPK().getSerialProyecto(), abandono.getCatEtapa().getEtapaId()));
                    //verificar si tiene duración, sino, borra el registro y todas sus dependencias
                    if (abandono.getAnios() > 0 || abandono.getMeses() > 0) {
                        dao.modifica(abandono); //actualiza si tiene duración
                    } else {//borra si no tiene duración
                        dao.eliminaSustancias(proyecto.getProyectoPK().getSerialProyecto(), proyecto.getProyectoPK().getFolioProyecto(), abandono.getCatEtapa());
                        dao.eliminaImpacto(proyecto.getProyectoPK().getSerialProyecto(), proyecto.getProyectoPK().getFolioProyecto(), abandono.getCatEtapa());
                        dao.elimina(EtapaProyecto.class, abandono.getEtapaProyectoPK());

                    }
                }
                reqcontEnv.execute("alert('Su información ha sido guardada con éxito.')");
            } catch (Exception e) {
                e.printStackTrace();
                reqcontEnv.execute("alert('No se ha podido guardar la información, Intente más tarde.')");
            }
            proyectoDao.avanceProyecto(proyecto);
            //método para guardar avance en sinatec y verificar
            SinatecProcedure sp = new SinatecProcedure();
            int avance = Integer.parseInt(proyectoDao.avanceProyecto(proyecto));
            sp.ejecutaSP(Integer.parseInt(proyecto.getProyectoPK().getFolioProyecto()), avance);
            if (avance == 99) {
                reqcontEnv.execute("alert('Ha completado el registro de datos del Informe Preventivo.')");
            }
            //terminan procedimientos con SINATEC
        } else {
            reqcontEnv.execute("alert('Capture el capítulo 1 antes de proceder con los demás capítulos')");
        }
    }

//getters y setters 
    public Boolean[] getBanderas() {
        return banderas;
    }

    public void setBanderas(Boolean[] banderas) {
        this.banderas = banderas;
    }

    public String[] getAnios() {
        return anios;
    }

    public String[] getMeses() {
        return meses;
    }

    public void setAnios(String[] anios) {
        this.anios = anios;
    }

    public void setMeses(String[] meses) {
        this.meses = meses;
    }

    public Proyecto getProyecto() {
        return proyecto;
    }

    public void setProyecto(Proyecto proyecto) {
        this.proyecto = proyecto;
    }

    public EtapaProyecto getPreparacionSitio() {
        return preparacionSitio;
    }

    public void setPreparacionSitio(EtapaProyecto preparacionSitio) {
        this.preparacionSitio = preparacionSitio;
    }

    public EtapaProyecto getConstruccion() {
        return construccion;
    }

    public void setConstruccion(EtapaProyecto construccion) {
        this.construccion = construccion;
    }

    public EtapaProyecto getOperaMantenim() {
        return operaMantenim;
    }

    public void setOperaMantenim(EtapaProyecto operaMantenim) {
        this.operaMantenim = operaMantenim;
    }

    public EtapaProyecto getAbandono() {
        return abandono;
    }

    public void setAbandono(EtapaProyecto abandono) {
        this.abandono = abandono;
    }
}
