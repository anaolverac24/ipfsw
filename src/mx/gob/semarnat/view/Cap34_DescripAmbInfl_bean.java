/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.gob.semarnat.view;

import javax.faces.bean.ManagedBean;
import javax.faces.context.FacesContext;
import mx.gob.semarnat.dao.Capitulo1Dao;
import mx.gob.semarnat.dao.Capitulo3Dao;
import mx.gob.semarnat.dao.ProyectoDao;
import mx.gob.semarnat.dao.SinatecProcedure;
import mx.gob.semarnat.model.Proyecto;
import org.primefaces.context.RequestContext;

/**
 *
 * @author Admin
 */
@ManagedBean(name = "cap34")
public class Cap34_DescripAmbInfl_bean {

    private final ProyectoDao proyectoDao = new ProyectoDao();
    private Capitulo3Dao dao = new Capitulo3Dao();
    // Proyecto recupera el proycto de la sesion
    private final Capitulo1Dao daoCap1 = new Capitulo1Dao();
    private String procedencia;

    private Proyecto proyecto;

    public Cap34_DescripAmbInfl_bean() {
        //cargar proyecto
        FacesContext fContext = FacesContext.getCurrentInstance();
        fContext.getExternalContext().getSessionMap().get("userFolioProy");
        String folioProyecto = (String) fContext.getExternalContext().getSessionMap().get("folioProyecto");
        Short serialProyecto = (Short) fContext.getExternalContext().getSessionMap().get("serialProyecto");

        proyecto = daoCap1.cargaProyecto(folioProyecto, serialProyecto);

    }

    public void guarda() {
        RequestContext reqcontEnv = RequestContext.getCurrentInstance();
        if (proyecto.getProySupuesto() != null) {
            try {
                dao.modifica(proyecto);
//            reqcontEnv.execute("$('#guarda').removeAttr('disabled');");
                if (proyecto.getProyDescAmbiente() != null || (proyecto.getProyDescMedidasCompen() != null && proyecto.getProyDescProtecConserv() != null)) {
                    //verifica que estén llenos todos los campos para establecer avance
                    if (procedencia != null) {//agrega avance a proyecto
                        if (proyecto.getProySupuesto() == '1') {
                            dao.guardarAvance(proyecto, new Short(proyecto.getProyNormaId() == 129 ? "4" : "5"), "34");
                        } else {
                            dao.guardarAvance(proyecto, new Short("5"), "34");
                        }
//                        dao.guardarAvance(proyecto, new Short(proyecto.getProyNormaId() == 129 ? "4" : "5"), "34");
                    } else {
//                        dao.guardarAvance(proyecto, new Short(proyecto.getProyNormaId() == 129 ? "4" : "5"), "36");
                        if (proyecto.getProySupuesto() == '1') {
                            dao.guardarAvance(proyecto, new Short(proyecto.getProyNormaId() == 129 ? "4" : "5"), "36");
                        } else {
                            dao.guardarAvance(proyecto, new Short("5"), "36");
                        }
                    }
                }
                //método para guardar avance en sinatec y verificar
                SinatecProcedure sp = new SinatecProcedure();
                int avance = Integer.parseInt(proyectoDao.avanceProyecto(proyecto));
                sp.ejecutaSP(Integer.parseInt(proyecto.getProyectoPK().getFolioProyecto()), avance);
                reqcontEnv.execute("alert('Su información ha sido guardada con éxito.');");
                if (avance == 99) {
                    reqcontEnv.execute("alert('Ha completado el registro de datos del Informe Preventivo.')");
                }
                //terminan procedimientos con SINATEC
            } catch (Exception e) {
                reqcontEnv.execute("alert('No se ha podido guardar la información, Intente más tarde.');");
                e.printStackTrace();
            }

        } else {
            reqcontEnv.execute("alert('Capture el capítulo 1 antes de proceder con los demás capítulos')");
        }
    }

    public Proyecto getProyecto() {
        return proyecto;
    }

    public void setProyecto(Proyecto proyecto) {
        this.proyecto = proyecto;
    }

    public String getProcedencia() {
        return procedencia;
    }

    public void setProcedencia(String procedencia) {
        this.procedencia = procedencia;
    }

}
