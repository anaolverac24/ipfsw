/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.gob.semarnat.view;

import javax.faces.bean.ManagedBean;
import javax.faces.context.FacesContext;
import mx.gob.semarnat.dao.Capitulo1Dao;
import mx.gob.semarnat.dao.Capitulo2Dao;
import mx.gob.semarnat.dao.Capitulo3DaoCatalogos;
import mx.gob.semarnat.model.Proyecto;
import mx.gob.semarnat.model.catalogos.CatTipoAsen;
import mx.gob.semarnat.model.catalogos.CatVialidad;
import mx.gob.semarnat.model.sinatec.Vexdatosusuario;

/**
 *
 * @author Rodrigo
 */
@ManagedBean(name = "promoventeView")
public class Capitulo2DatosPromovente {

    //------------------para guardado de tabla proyecto  
    private final Proyecto proyecto;
    private final Capitulo1Dao daoCap1 = new Capitulo1Dao();
    private final Capitulo3DaoCatalogos daoCat = new Capitulo3DaoCatalogos();
    private Vexdatosusuario promovente;
    private Boolean vacio;

    private final Capitulo2Dao dao = new Capitulo2Dao();

    public Capitulo2DatosPromovente() {
        //cargar proyecto
        FacesContext fContext = FacesContext.getCurrentInstance();
        fContext.getExternalContext().getSessionMap().get("userFolioProy");
        String folioProyecto = (String) fContext.getExternalContext().getSessionMap().get("folioProyecto");
        Short serialProyecto = (Short) fContext.getExternalContext().getSessionMap().get("serialProyecto");
        proyecto = daoCap1.cargaProyecto(folioProyecto, serialProyecto);
        //fin de la carga del proyecto
        try {
            promovente = (Vexdatosusuario) dao.
                    datosPromovente(Integer.
                            parseInt(proyecto.getProyectoPK().
                                    getFolioProyecto())).get(0);
            vacio = false;
        } catch (IndexOutOfBoundsException e) {
            promovente = new Vexdatosusuario();
            vacio = true;
        }

    }

    public String getVialidad() {
        return ((CatVialidad) daoCat.busca(CatVialidad.class,
                new Short(promovente.getBgcveTipoVialLk().
                        toString()))).getDescripcion();
    }

    public String getAsentamiento() {
        return ((CatTipoAsen) daoCat.busca(CatTipoAsen.class,
                new Short(promovente.getBgcveTipoAsenLk().
                        toString()))).getNombre();
    }

    public Vexdatosusuario getPromovente() {
        return promovente;
    }

    public void setPromovente(Vexdatosusuario promovente) {
        this.promovente = promovente;
    }

    public Boolean getVacio() {
        return vacio;
    }

    public void setVacio(Boolean vacio) {
        this.vacio = vacio;
    }

}
