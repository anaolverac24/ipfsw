/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.gob.semarnat.view;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.TreeSet;

import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;
import javax.faces.model.SelectItem;
import javax.persistence.EntityExistsException;
import mx.gob.semarnat.dao.Capitulo1Dao;
import mx.gob.semarnat.dao.Capitulo3Dao;
import mx.gob.semarnat.dao.CatNormaAsociacionDao;
import mx.gob.semarnat.dao.EspecifiacionComparator;
import mx.gob.semarnat.model.CatClasificacionMineral;
import mx.gob.semarnat.model.CatEspecificacion;
import mx.gob.semarnat.model.CatMineralesReservados;
import mx.gob.semarnat.model.CatNorma;
import mx.gob.semarnat.model.CatNormaAsociacion;
import mx.gob.semarnat.model.CatObra;
import mx.gob.semarnat.model.CatParqueIndustrial;
import mx.gob.semarnat.model.CatPdu;
import mx.gob.semarnat.model.CatPoet;
import mx.gob.semarnat.model.CatPregunta;
import mx.gob.semarnat.model.CatTipoClima;
import mx.gob.semarnat.model.CatTipoVegetacion;
import mx.gob.semarnat.model.EspecificacionProyecto;
import mx.gob.semarnat.model.ParqueIndustrialProyecto;
import mx.gob.semarnat.model.ParqueIndustrialProyectoPK;
import mx.gob.semarnat.model.PduProyecto;
import mx.gob.semarnat.model.PduProyectoPK;
import mx.gob.semarnat.model.PoetProyecto;
import mx.gob.semarnat.model.PoetProyectoPK;
import mx.gob.semarnat.model.PreguntaClima;
import mx.gob.semarnat.model.PreguntaClimaHelper;
import mx.gob.semarnat.model.PreguntaClimaPK;
import mx.gob.semarnat.model.PreguntaMineral;
import mx.gob.semarnat.model.PreguntaMineralPK;
import mx.gob.semarnat.model.PreguntaObra;
import mx.gob.semarnat.model.PreguntaObraHelper;
import mx.gob.semarnat.model.PreguntaObraPK;
import mx.gob.semarnat.model.PreguntaProyecto;
import mx.gob.semarnat.model.PreguntaVegetacion;
import mx.gob.semarnat.model.PreguntaVegetacionHelper;
import mx.gob.semarnat.model.PreguntaVegetacionPK;
import mx.gob.semarnat.model.Proyecto;
import mx.gob.semarnat.model.catalogos.CatUnidadMedida;
import oracle.jdbc.proxy.annotation.Post;

import org.primefaces.context.RequestContext;

/**
 *
 * @author Case Solutions 10
 */
@ManagedBean(name = "supuestosView")
@ViewScoped
public class Capitulo1View implements Serializable {

    private final Capitulo1Dao dao;
    private final Capitulo3Dao dao3;
    // Proyecto en sesion
    private Proyecto proyecto;
    // Supuestos
    private Boolean panelMsg = false;  // define si se puede ver el panel de las normas
    private Boolean panelNorm = false;  // define si se puede ver el panel de las normas
    private Boolean panelPreg = false; // define si se puede ver el panel de las normas
    private Boolean panelEspe = false; // define si se puede ver el panel de especificaciones
    private Boolean btnGuardar = false; // define si se puede ver el boton de guardar
    private String referencia; // para la seleccion de referencia
    private Boolean disa = false;
    private Boolean btnGuarda1 = false;
    private Boolean btnGuarda2 = false;
    private List<SelectItem> catNom = new LinkedList<SelectItem>(); // catalogo de normas
    private CatNorma normaSeleccionada = new CatNorma(); // la selecion de nom
    // Supuesto 2
    private Boolean panelSup2 = false;
    private List<SelectItem> catParques = new LinkedList<SelectItem>(); // catalogo de parques
    private ParqueIndustrialProyecto parque = new ParqueIndustrialProyecto();
    // Supuesto 3
    private Boolean panelSup3 = false;
    private Boolean panelSup31 = false;
    private Boolean panelSup32 = false;
    private List<SelectItem> catPDU = new LinkedList<SelectItem>(); // catalogo de parques
    private List<SelectItem> catPODS = new LinkedList<SelectItem>(); // catalogo de parques
    private String selecionaOpcion3 = "";
    private PduProyecto pduProyecto = new PduProyecto();
    private PoetProyecto poetProyecto = new PoetProyecto();
    //normas estaticas 120
    private List<SelectItem> catTipoClima = new LinkedList<SelectItem>(); // catalogo de parques
    private List<SelectItem> catTipoVegetacion = new LinkedList<SelectItem>(); // catalogo de parques
    private PreguntaVegetacion pregVegetacion = new PreguntaVegetacion();
    private PreguntaClima preClima = new PreguntaClima();
    private CatTipoClima tipoClimaSel = new CatTipoClima();
    private CatTipoVegetacion tipoVegetaSel = new CatTipoVegetacion();

    private List<SelectItem> catTipoMineral = new LinkedList<SelectItem>(); // catalogo de parques
    private List<SelectItem> catTipoObra = new LinkedList<SelectItem>(); // catalogo de parques
    private List<CatClasificacionMineral> clasificacionMineral;
    private List<CatTipoClima> tipoClimaList;
    private List<CatTipoVegetacion> tipoVegetacionList;
    private List<CatObra> tipObraList;
    private String[] estadoMineralSelect;
    private boolean mineralReservado = false;
    private Boolean flagNormCientoV = false;
    private List<PreguntaObraHelper> listaObras = new ArrayList();
    private Character respuestas[] = new Character[2];
    private PreguntaObraHelper obra;
    private PreguntaClimaHelper clima = new PreguntaClimaHelper(0, new PreguntaClima(new PreguntaClimaPK()));
    private List<PreguntaVegetacionHelper> listaVege;
    private PreguntaVegetacionHelper vege;
    private static List<CatUnidadMedida> listaUnidades;
    //norma 141
    private Boolean flagNorm141 = false;
    private List<MineralHelper> lista;
    private List<CatClasificacionMineral> listaClasif;
    private ArrayList<String> catalogoMinerales;
    //fin variables normas estaticas
    private Boolean verMinSel = false;
    private String msgMinSel = "";
    private List<PreguntaObra> obras = new ArrayList<PreguntaObra>();
    
    /**
     * Permite obtener el folio del proyecto de la url de la pagina.
     */
    private String folioProy;
    

    /**
     * Constructor
     *
     * Recupera el proyecto de la sesion
     */
    public Capitulo1View() {

        // Recupera proyecto de sesion
        FacesContext context = FacesContext.getCurrentInstance();
        context.getExternalContext().getSessionMap().get("userFolioProy");
        String folioProyecto = (String) context.getExternalContext().getSessionMap().get("folioProyecto");
        Short serialProyecto = (Short) context.getExternalContext().getSessionMap().get("serialProyecto");

        dao = new Capitulo1Dao();
        dao3 = new Capitulo3Dao();
        proyecto = dao.cargaProyecto(folioProyecto, serialProyecto);

        tipoClimaList = dao.tipoClima();
        tipoVegetacionList = dao.tipoVegetacion();
        tipObraList = dao.tipoObra();
        listaUnidades = dao.unidadMedida("volúmen");

        // Ordenar especifucaciones
//        if (proyecto.getEspecificacionProyectoList() != null && !proyecto.getEspecificacionProyectoList().isEmpty()){
//            Collections.sort(proyecto.getEspecificacionProyectoList(), new EspecifiacionComparator());
//        }
        //inicializacion de listas
        listaObras = new ArrayList();
        listaVege = new ArrayList();

        try {
            validaNorma(proyecto.getProyNormaId());
            normaSeleccionada = new CatNorma(proyecto.getProyNormaId());
        } catch (Exception err) {
            //nop
        }

        // carga Normas
        List<CatNorma> noms = dao.listaNormas();
        CatNorma n0 = new CatNorma();
        n0.setNormaDescripcion("");
        n0.setNormaId((short) 0);
        SelectItem t = new SelectItem(n0, "Seleccionar NOM");
        t.setNoSelectionOption(true);
        getCatNom().add(t);

        for (CatNorma n : noms) {
            SelectItem tmp = new SelectItem(n, n.getNormaNombre());
            getCatNom().add(tmp);
            System.out.println(" " + n);
        }

        // vegetacion
        catTipoVegetacion = new LinkedList<SelectItem>();
        SelectItem tmp1 = new SelectItem(new CatTipoVegetacion(), "Seleccione el tipo de vegetacion");
        catTipoVegetacion.add(tmp1);

        for (CatTipoVegetacion c : tipoVegetacionList) {
            SelectItem tmp = new SelectItem(c, c.getVegetacionDescripcion());
            catTipoVegetacion.add(tmp);
        }

        // clima
        catTipoClima = new LinkedList<SelectItem>();
        SelectItem tmp2 = new SelectItem(new CatTipoClima(), "Seleccione el tipo de clima");
        catTipoClima.add(tmp2);
        for (CatTipoClima c : tipoClimaList) {
            SelectItem tmp = new SelectItem(c, c.getClimaDescripcion());
            catTipoClima.add(tmp);
        }
        // Parque Industrial
        try {
            parque = (ParqueIndustrialProyecto) dao.getObject(new ParqueIndustrialProyecto(), new ParqueIndustrialProyectoPK(proyecto.getProyectoPK().getFolioProyecto(), proyecto.getProyectoPK().getSerialProyecto(), (short) 1));
            System.out.println("Recuperado parque");
            if (parque == null) {
//                parque = new ParqueIndustrialProyecto();
                parque = new ParqueIndustrialProyecto(proyecto.getProyectoPK().getFolioProyecto(), proyecto.getProyectoPK().getSerialProyecto(), (short) 1);
            }
        } catch (Exception err) {
            System.out.println("Creando nueva parque");
            parque = new ParqueIndustrialProyecto(proyecto.getProyectoPK().getFolioProyecto(), proyecto.getProyectoPK().getSerialProyecto(), (short) 1);
        }
        //PDU
        try {
            pduProyecto = (PduProyecto) dao.getObject(new PduProyecto(), new PduProyectoPK(proyecto.getProyectoPK().getFolioProyecto(), proyecto.getProyectoPK().getSerialProyecto(), (short) 1));
            System.out.println("Recuperado pdu");
            if (pduProyecto == null) {
//                pduProyecto = new PduProyecto();
                pduProyecto = new PduProyecto(proyecto.getProyectoPK().getFolioProyecto(), proyecto.getProyectoPK().getSerialProyecto(), (short) 1);
            }
        } catch (Exception err) {
            pduProyecto = new PduProyecto(proyecto.getProyectoPK().getFolioProyecto(), proyecto.getProyectoPK().getSerialProyecto(), (short) 1);
            System.out.println("Creando nueva pdu");
        }

        // POET
        try {
            poetProyecto = (PoetProyecto) dao.getObject(new PoetProyecto(), new PoetProyectoPK(proyecto.getProyectoPK().getFolioProyecto(), proyecto.getProyectoPK().getSerialProyecto(), (short) 1));
            System.out.println("Recuperado poet");
            if (poetProyecto == null) {
//                poetProyecto = new PoetProyecto();
                poetProyecto = new PoetProyecto(proyecto.getProyectoPK().getFolioProyecto(), proyecto.getProyectoPK().getSerialProyecto(), (short) 1);
            }
        } catch (Exception err) {
            poetProyecto = new PoetProyecto(proyecto.getProyectoPK().getFolioProyecto(), proyecto.getProyectoPK().getSerialProyecto(), (short) 1);
            System.out.println("Creando nueva poet");
        }

        // Carga Supuestos si ya fueron guardados
        if (proyecto.getProySupuesto() != null) {
            btnGuarda1 = false;
            disa = true;
            referencia = proyecto.getProySupuesto() + "";

            if (proyecto.getProySupuesto().equals('1')) {
                if (proyecto.getProyNormaId() != null) {
                    normaSeleccionada = dao.getNorma(proyecto.getProyNormaId());
                }

                if (normaSeleccionada.getNormaId() != null) {
                    setClasificacionMineral((List<CatClasificacionMineral>) dao3.listado(CatClasificacionMineral.class));
                    setLista((List<MineralHelper>) new ArrayList());
                    setEstadoMineralSelect(null);
                    //Collections.sort(proyecto.getEspecificacionProyectoList(), new EspecifiacionComparator());
                    String espe;
                    for (EspecificacionProyecto ep : proyecto.getEspecificacionProyectoList()) {
                        espe = ep.getCatEspecificacion().getCatEspecificacionPK().getEspecificacionId();
                        if (espe.contains("\n")) {
                            espe = espe.replace("\n\n", "\n");
                            ep.getCatEspecificacion().getCatEspecificacionPK().setEspecificacionId(espe.replace("\n", " - "));
                        }
                    }
                    if (normaSeleccionada.getNormaId() == 120) {
                        flagNormCientoV = true;
                        if (listaObras.isEmpty()) {
                            agregaObra();
                            agregaVege();
                        }
                        panelEspe = true;
                        btnGuardar = true;

                    } else if (normaSeleccionada.getNormaId() == 141) {
                        flagNorm141 = true;
                        panelEspe = true;
                        btnGuardar = true;

                    } else {
                        panelNorm = true;
                        panelPreg = true;
                        panelEspe = true;
                        btnGuardar = true;

                    }
                    if (proyecto.getProyNormaId() == 120) {
                        preClima = dao.getPreguntaClima(folioProyecto, serialProyecto);
                        pregVegetacion = dao.getPreguntaVegetacion(folioProyecto, serialProyecto);
                        tipoClimaSel = dao.getCatTpoclima(preClima.getPreguntaClimaPK().getClimaId());
                        tipoVegetaSel = dao.getCatTipoVegetacion(pregVegetacion.getPreguntaVegetacionPK().getPreguntaId());
                        respuestas[0] = 'N';
                        respuestas[1] = 'S';
                        flagNormCientoV = true;
                        mineralReservado = false;

                        String minsel = "";
                        List<PreguntaMineral> m = dao.getMinerales(folioProyecto, serialProyecto);
                        for (PreguntaMineral t1 : m) {
                            CatMineralesReservados t3 = dao.getMineral(t1.getPreguntaMineralPK().getIdMinerales());
                            minsel += t3.getDescripcionMinerales() + ",";
                        }

                        obras = dao.getObras(folioProyecto, serialProyecto);
                        msgMinSel = minsel;
                        verMinSel = true;
                        panelNorm = true;

                    }
                    if (proyecto.getProyNormaId() == 141) {
                        preClima = dao.getPreguntaClima(folioProyecto, serialProyecto);
                        pregVegetacion = dao.getPreguntaVegetacion(folioProyecto, serialProyecto);
//                tipoClimaSel = dao.getCatTpoclima(preClima.getPreguntaClimaPK().getClimaId());
//                tipoVegetaSel = dao.getCatTipoVegetacion(pregVegetacion.getPreguntaVegetacionPK().getPreguntaId());
                        respuestas[0] = 'S';
                        respuestas[1] = 'S';
                        flagNorm141 = true;
                        mineralReservado = false;

                        String minsel = "";
                        List<PreguntaMineral> m = dao.getMinerales(folioProyecto, serialProyecto);
                        for (PreguntaMineral t1 : m) {
                            CatMineralesReservados t3 = dao.getMineral(t1.getPreguntaMineralPK().getIdMinerales());
                            minsel += t3.getDescripcionMinerales() + ",";
                        }

                        obras = dao.getObras(folioProyecto, serialProyecto);
                        msgMinSel = minsel;
                        verMinSel = true;
                        panelNorm = true;
                    }
                }

            }
            if (proyecto.getProySupuesto().equals('2')) {
                panelSup2 = true;
                btnGuarda2 = true;
                List<CatParqueIndustrial> parques = dao.getListObject(new CatParqueIndustrial());
                CatParqueIndustrial p = new CatParqueIndustrial();
                p.setParqueDescripcion("Seleccione parque industrial");
                p.setParqueId((short) 0);
                SelectItem x = new SelectItem(p, "Seleccionar Parque Industrial");
                catParques.add(x);

                for (CatParqueIndustrial c : parques) {
                    SelectItem tmp = new SelectItem(c, c.getParqueDescripcion());
                    catParques.add(tmp);
                }

                // Parque Industrial
                try {
                    parque = (ParqueIndustrialProyecto) dao.getObject(new ParqueIndustrialProyecto(), new ParqueIndustrialProyectoPK(proyecto.getProyectoPK().getFolioProyecto(), proyecto.getProyectoPK().getSerialProyecto(), (short) 1));
                    System.out.println("Recuperado parque");
                    if (parque == null) {
                        parque = new ParqueIndustrialProyecto(proyecto.getProyectoPK().getFolioProyecto(), proyecto.getProyectoPK().getSerialProyecto(), (short) 1);

                    }
                } catch (Exception err) {
                    System.out.println("Creando nueva parque");
                    parque = new ParqueIndustrialProyecto(proyecto.getProyectoPK().getFolioProyecto(), proyecto.getProyectoPK().getSerialProyecto(), (short) 1);
                }

            }
            if (proyecto.getProySupuesto().equals('3')) {
                panelSup3 = true;
                btnGuarda2 = true;

                catPDU.clear();
                catPODS.clear();
                List<CatPdu> pdus = dao.getListObject(new CatPdu());
                for (CatPdu cp : pdus) {
                    SelectItem tmp = new SelectItem(cp, cp.getPduDescripcion());
                    catPDU.add(tmp);
                }
                List<CatPoet> poets = dao.getListObject(new CatPoet());
                for (CatPoet cpoet : poets) {
                    SelectItem tmp = new SelectItem(cpoet, cpoet.getPoetDescripcion());
                    catPODS.add(tmp);
                }
                //PDU
                try {
                    pduProyecto = (PduProyecto) dao.getObject(new PduProyecto(), new PduProyectoPK(proyecto.getProyectoPK().getFolioProyecto(), proyecto.getProyectoPK().getSerialProyecto(), (short) 1));
                    System.out.println("Recuperado pdu");
                } catch (Exception err) {
                    pduProyecto = new PduProyecto(proyecto.getProyectoPK().getFolioProyecto(), proyecto.getProyectoPK().getSerialProyecto(), (short) 1);
                    System.out.println("Creando nueva pdu");
                }

                // POET
                try {
                    poetProyecto = (PoetProyecto) dao.getObject(new PoetProyecto(), new PoetProyectoPK(proyecto.getProyectoPK().getFolioProyecto(), proyecto.getProyectoPK().getSerialProyecto(), (short) 1));
                    System.out.println("Recuperado poet");
                } catch (Exception err) {
                    poetProyecto = new PoetProyecto(proyecto.getProyectoPK().getFolioProyecto(), proyecto.getProyectoPK().getSerialProyecto(), (short) 1);
                    System.out.println("Creando nueva poet");
                }
            }

        }

    }
    
    @PostConstruct
    public void init() {
      System.out.println(folioProy);
	}

    /**
     * Valida si la norma requiere actualizar el menu principal
     *
     * @param normaId
     */
    public void validaNorma(Short normaId) {
        if (normaId != null) {
            if (normaId == 129) {
                RequestContext reqcontEnv = RequestContext.getCurrentInstance();
                reqcontEnv.execute("parent.menu4('" + normaSeleccionada.getNormaId() + "')");
            }
        }
    }

    /**
     * Seleccion la referencia (1,2,3)
     */
    public void seleccionaReferencia() {
        System.out.println("Valor " + referencia);

        flagNorm141 = false;
        flagNormCientoV = false;
        panelNorm = false;
        panelEspe = false;
        panelPreg = false;
        panelSup2 = false;
        panelSup3 = false;
        panelSup31 = false;
        panelSup32 = false;
        btnGuardar = false;
        btnGuarda2 = false;
        panelMsg = false;

        //Carga Normas Oficiales
        if (referencia.equals("1")) {

            panelNorm = true;
        }
        if (referencia.equals("2")) {
            panelNorm = false;

            getCatParques().clear();
            List<CatParqueIndustrial> parques = dao.getListObject(new CatParqueIndustrial());
            CatParqueIndustrial p = new CatParqueIndustrial();
            p.setParqueDescripcion("Seleccione parque industrial");
            p.setParqueId((short) 0);
            SelectItem t = new SelectItem(p, "Seleccionar Parque Industrial");
            getCatParques().add(t);

            for (CatParqueIndustrial c : parques) {
                SelectItem tmp = new SelectItem(c, c.getParqueDescripcion());
                getCatParques().add(tmp);
            }
            panelSup2 = true;
            btnGuarda2 = true;
        }
        if (referencia.equals("3")) {
            panelSup3 = true;
            btnGuarda2 = true;

            getCatPDU().clear();
            getCatPODS().clear();
            List<CatPdu> pdus = dao.getListObject(new CatPdu());
            for (CatPdu t : pdus) {
                SelectItem tmp = new SelectItem(t, t.getPduDescripcion());
                getCatPDU().add(tmp);
            }
            List<CatPoet> poets = dao.getListObject(new CatPoet());
            for (CatPoet t : poets) {
                SelectItem tmp = new SelectItem(t, t.getPoetDescripcion());
                getCatPODS().add(tmp);
            }

        }
    }

    /**
     * Selecciona la norma oficial
     */
    public void seleccionaNom() {
        if (proyecto.getPreguntaProyectoList() == null) {
            proyecto.setPreguntaProyectoList(new LinkedList<PreguntaProyecto>());
        }
        if (proyecto.getEspecificacionProyectoList() == null) {
            proyecto.setEspecificacionProyectoList(new TreeSet<EspecificacionProyecto>());
        }
        proyecto.getPreguntaProyectoList().clear();
        proyecto.getEspecificacionProyectoList().clear();

//        dao.limpiarPreguntasProyecto(proyecto);
        panelMsg = false;
        panelPreg = false;
        panelEspe = false;
        flagNorm141 = false;
        flagNormCientoV = false;
        btnGuarda1 = false;
        btnGuardar = false;
        mineralReservado = false;
        respuestas = new Character[2];

        RequestContext reqcontEnv = RequestContext.getCurrentInstance();
        if (normaSeleccionada != null && normaSeleccionada.getNormaId() != null) {
            reqcontEnv.execute("parent.oculta_menu4('" + normaSeleccionada.getNormaId() + "');");
        }

        if (normaSeleccionada != null && normaSeleccionada.getNormaId() != null) {
            proyecto.setProyNormaId(normaSeleccionada.getNormaId());

            // Carga preguntas segun el id de la nom
            List<CatPregunta> p = dao.preguntas(normaSeleccionada.getNormaId() + "");
            for (CatPregunta t : p) {
                PreguntaProyecto pp = new PreguntaProyecto(proyecto.getProyectoPK().getFolioProyecto(), proyecto.getProyectoPK().getSerialProyecto(), t.getPreguntaId());
                pp.setCatPregunta(t);
                pp.setProyecto(proyecto);
                proyecto.getPreguntaProyectoList().add(pp);
            }

            // Dinamicas
            if (normaSeleccionada.getNormaId() != 120 && normaSeleccionada.getNormaId() != 141) {
                if (!proyecto.getPreguntaProyectoList().isEmpty()) {
                    panelPreg = true;
                }
            } else {
                // ** Estaticas

                setClasificacionMineral((List<CatClasificacionMineral>) dao3.listado(CatClasificacionMineral.class));
                setLista((List<MineralHelper>) new ArrayList());
                setEstadoMineralSelect(null);

                if (normaSeleccionada.getNormaId() == 120) {
                    flagNormCientoV = true;
                    if (listaObras.isEmpty()) {
                        agregaObra();
                        agregaVege();
                    }
                }
                if (normaSeleccionada.getNormaId() == 141) {
                    flagNorm141 = true;
                }
            }

        }
    }

    /**
     * Selecciona la pregunta del cuestionario
     */
    public void seleccionaPregunta() {
        boolean eva = true;
        boolean te = true;

        if (normaSeleccionada.getNormaId() != 120 && normaSeleccionada.getNormaId() != 141) {

            for (PreguntaProyecto p : proyecto.getPreguntaProyectoList()) {
                if (p.getPreguntaRespuesta() != null) {
                    if (p.getPreguntaRespuesta().equals(p.getCatPregunta().getPreguntaContinua())) {
                        te &= true;
                    } else {
                        te &= false;
                    }
                }
            }

            panelMsg = !te;

            for (PreguntaProyecto p : proyecto.getPreguntaProyectoList()) {
                if (p.getCatPregunta().getPreguntaContinua().equals(p.getPreguntaRespuesta())) {
                    eva &= true;
                } else {
                    eva &= false;
                }
            }

        } else {

            //Verifica si selecciona otro 120
            if (normaSeleccionada.getNormaId() == 120) {
                eva &= (respuestas[0] != null && respuestas[0] == 'N');
                eva &= (respuestas[1] != null && respuestas[1] == 'S');

                eva &= (tipoClimaSel != null && tipoClimaSel.getClimaDescripcion() != null);
                eva &= !(tipoClimaSel != null && tipoClimaSel.getClimaDescripcion() != null && (tipoClimaSel.getClimaDescripcion().trim().equals("OTRO") || tipoClimaSel.getClimaDescripcion().trim().equals("Seleccione el tipo de clima") || tipoClimaSel.getClimaDescripcion().trim().equals("")));
                eva &= (tipoVegetaSel != null && tipoVegetaSel.getVegetacionDescripcion() != null);
                eva &= !(tipoVegetaSel != null && tipoVegetaSel.getVegetacionDescripcion() != null && (tipoVegetaSel.getVegetacionDescripcion().trim().equals("OTRO") || tipoVegetaSel.getVegetacionDescripcion().trim().equals("Seleccion el tipo de vegetacion") || tipoVegetaSel.getVegetacionDescripcion().trim().equals("Seleccion el tipo de vegetacion")));

                if (respuestas[0] != null) {
                    te &= (respuestas[0] == 'N');
                }
                if (respuestas[1] != null) {
                    te &= respuestas[1] == 'S';
                }
                if (tipoClimaSel != null && tipoClimaSel.getClimaDescripcion() != null) {
                    te &= !(tipoClimaSel.getClimaDescripcion().trim().equals("OTRO") || tipoClimaSel.getClimaDescripcion().trim().equals("Seleccione el tipo de clima") || tipoClimaSel.getClimaDescripcion().trim().equals(""));

                }
                if (tipoVegetaSel != null && tipoVegetaSel.getVegetacionDescripcion() != null) {
                    te &= !(tipoVegetaSel.getVegetacionDescripcion().trim().equals("OTRO") || tipoVegetaSel.getVegetacionDescripcion().trim().equals("Seleccion el tipo de vegetacion") || tipoVegetaSel.getVegetacionDescripcion().trim().equals("Seleccion el tipo de vegetacion"));
                }

            }
            if (normaSeleccionada.getNormaId() == 141) {
                eva &= (respuestas[0] != null && respuestas[0] == 'S');
                eva &= (respuestas[1] != null && respuestas[1] == 'S');

                if (respuestas[0] != null) {
                    te &= (respuestas[0] == 'S');
                }
                if (respuestas[1] != null) {
                    te &= respuestas[1] == 'S';
                }

            }

            panelMsg = !te;
            mineralReservado = eva;
        }

        System.out.println("Continuar despues" + eva);

        if (eva) {
            //Selecciona Especificaciones de la nom
            if (proyecto.getEspecificacionProyectoList().isEmpty()) {
                List<CatEspecificacion> e = dao.especificaciones(normaSeleccionada.getNormaId());
                for (CatEspecificacion es : e) {
                    EspecificacionProyecto esp = new EspecificacionProyecto(proyecto.getProyectoPK().getFolioProyecto(), proyecto.getProyectoPK().getSerialProyecto(), es.getCatEspecificacionPK().getEspecificacionId(), normaSeleccionada.getNormaId());
                    esp.setCatEspecificacion(es);
                    //Se hace relacion con el proyecto
                    esp.setProyecto(proyecto);
                    proyecto.getEspecificacionProyectoList().add(esp);
                }
            }
            if (proyecto.getEspecificacionProyectoList() != null & !proyecto.getEspecificacionProyectoList().isEmpty()) {
//                panelEspe = true;
            }

//            btnGuardar = true;
            panelMsg = false;
            btnGuarda1 = true;

        } else {
//            btnGuardar = eva;
//            panelEspe = eva;
        }

        //Guarda para poder tener relaciones en adjuntos
//        if (eva) {
        btnGuarda1 = eva;
//        } 
        
    }

    public void guardar1AE(ActionEvent actionEvent) {
        if (normaSeleccionada != null) {
            proyecto.setProyNormaId(normaSeleccionada.getNormaId());
        }

        btnGuarda1 = false;
        disa = true;
        dao.guardarCap1(proyecto, null, null);
        btnGuardar = true;
        panelEspe = true;

        validaNorma(proyecto.getProyNormaId());

    }

    /**
     * Permite guardar la informacion del proyecto del Capitulo 1.
     * @param evt
     */
    public void guarar1(ActionEvent evt) {
        if (normaSeleccionada != null) {
        	
        	try {
        		
            	CatNormaAsociacionDao catNormaAsociacionDao = new CatNormaAsociacionDao();
            	// consulta la asociacion de la norma seleccionada
				CatNormaAsociacion catNormaAsociacion = catNormaAsociacionDao.consultarNormaAsociacionPorID(normaSeleccionada.getNormaId());
				// Si existe la asociacion de la norma.
				if (catNormaAsociacion != null) {
					//Se asignan los datos de la seccion, subseccion, tipo y rama del proyecto.
					proyecto.setNsec(catNormaAsociacion.getNsec());
					proyecto.setNsub(catNormaAsociacion.getNsub());
					proyecto.setNtipo(catNormaAsociacion.getNtipo());
					proyecto.setNrama(catNormaAsociacion.getNrama());					
				}
				
			} catch (Exception e) {
				FacesContext.getCurrentInstance().addMessage(
						null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error al consultar la norma asociacion del proyecto", null));
				return;
			}
        	
            proyecto.setProyNormaId(normaSeleccionada.getNormaId());

            if (normaSeleccionada.getNormaId() == 120) {
                proyecto.getPreguntaProyectoList().get(0).setPreguntaRespuesta('N');
                proyecto.getPreguntaProyectoList().get(1).setPreguntaRespuesta('S');
                preClima = new PreguntaClima(proyecto.getProyectoPK().getFolioProyecto(), proyecto.getProyectoPK().getSerialProyecto(), proyecto.getPreguntaProyectoList().get(0).getCatPregunta().getPreguntaId(), tipoClimaSel.getClimaId());
                pregVegetacion = new PreguntaVegetacion(proyecto.getProyectoPK().getFolioProyecto(), proyecto.getProyectoPK().getSerialProyecto(), proyecto.getPreguntaProyectoList().get(0).getCatPregunta().getPreguntaId(), tipoVegetaSel.getVegetacionId());
            }
        }

        btnGuarda1 = false;
        disa = true;
        dao.guardarCap1(proyecto, null, null);
        btnGuardar = true;
        panelEspe = true;
        validaNorma(proyecto.getProyNormaId());

        if (normaSeleccionada != null && normaSeleccionada.getNormaId() == 120) {
            dao.merge(preClima);
            dao.merge(pregVegetacion);
        }

    }

    public void guardarSup2y3() {
        btnGuarda2 = false;
        disa = true;
        if (proyecto.getProySupuesto() == null) {
            dao.guardarCap1(proyecto, null, null);
//        validaNorma(proyecto.getProyNormaId());
            switch (proyecto.getProySupuesto()) {
                case '2':
                    dao.persist(parque);
                    break;
                case '3':
                    if (pduProyecto.getPduJustificacion() == null) {
                        dao.persist(pduProyecto);
                    } else {
                        dao.persist(poetProyecto);
                    }
                    break;
            }
        } else {
            switch (proyecto.getProySupuesto()) {
                case '2':
                    dao.upsert(parque);
                    break;
                case '3':
                    if (pduProyecto.getPduJustificacion() == null) {
                        dao.upsert(pduProyecto);
                    } else {
                        dao.upsert(poetProyecto);
                    }
                    break;
            }
        }
    }

    //normas estéticas
    public void cargaMinerales() {
        String label;
        getLista().clear();
        for (String estadoMineralSelect1 : getEstadoMineralSelect()) {
            label = ((CatClasificacionMineral) dao3.busca(CatClasificacionMineral.class, new Short(estadoMineralSelect1))).
                    getDescripcionClasificacion();
            getLista().
                    add(new MineralHelper((List<CatMineralesReservados>) dao3.listado_where(CatMineralesReservados.class, "idClasificacion", new Short(estadoMineralSelect1)),
                                    new ArrayList(), label.length() > 50 ? label.substring(0, 50).concat("...") : label));
        }

    }

    public void agregaObra() {
        if (!getListaObras().isEmpty()) {
            int id;
            PreguntaObra p = new PreguntaObra();
            PreguntaObraPK pk = new PreguntaObraPK();
            p.setPreguntaObraPK(pk);
            id = (listaObras.get((listaObras.size() - 1))).getId();
            id++;
            listaObras.add(new PreguntaObraHelper(id, p));
        } else {
            listaObras.add(new PreguntaObraHelper(0, new PreguntaObra(new PreguntaObraPK())));
        }
    }

    public void quitaObra() {
        System.out.println("ObraId: " + obra.getId());
        listaObras.remove(obra);
    }

    public void agregaVege() {
        if (!listaVege.isEmpty()) {
            int id;
            id = (listaVege.get((listaVege.size() - 1))).getId();
            id++;
            listaVege.add(new PreguntaVegetacionHelper(id, new PreguntaVegetacion(new PreguntaVegetacionPK())));
        } else {
            listaVege.add(new PreguntaVegetacionHelper(0, new PreguntaVegetacion(new PreguntaVegetacionPK())));
        }
    }

    public void quitaVege() {

        listaVege.remove(vege);
    }

    // Supuesto 3
    public void seleccionaSup3() {
        panelSup31 = false;
        panelSup32 = false;

        if (selecionaOpcion3.equals("pdu")) {
            panelSup31 = true;
        }
        if (selecionaOpcion3.equals("poet")) {
            panelSup32 = true;

        }
    }

    //fin normas estaticas
    public void guardar(ActionEvent evt) {
        System.out.println("Iniciar guardado (0)");

        RequestContext reqcontEnv = RequestContext.getCurrentInstance();
        if (dao.cargaProyecto(proyecto.getProyectoPK().getFolioProyecto(),
                proyecto.getProyectoPK().getSerialProyecto()).getProyLote() != null) {
            Integer lote;
            lote = dao.cargaProyecto(proyecto.getProyectoPK().getFolioProyecto(),
                    proyecto.getProyectoPK().getSerialProyecto()).getProyLote().intValue();
            proyecto.setProyLote(lote.longValue());
        }
        proyecto.setProySupuesto(referencia.charAt(0));
        try {
            dao.guardarCap1(proyecto, null, null);

            // Supuesto 2
            if (proyecto.getProySupuesto() == '2') {
                parque.setProyecto(proyecto);
                dao.upsert(parque);
            }

            // Supuesto 3
            if (proyecto.getProySupuesto() == '3') {
                if (selecionaOpcion3.equals("pdu")) {
                    proyecto.setProySupuesto('U');
                    pduProyecto.setProyecto(proyecto);
                    dao.upsert(pduProyecto);
                }
                if (selecionaOpcion3.equals("poet")) {
                    proyecto.setProySupuesto('T');
                    poetProyecto.setProyecto(proyecto);
                    dao.upsert(poetProyecto);
                }
            }

            if (normaSeleccionada.getNormaId() != null) {
//                dao.limpiarPreguntasProyecto(proyecto);
                //carga respuestas a las preguntas estaticas de 120 y 141
                if (normaSeleccionada.getNormaId() == 120) {
                    System.out.println("norma 120");

                    proyecto.getPreguntaProyectoList().get(0).setPreguntaRespuesta(getRespuestas()[0]);
                    proyecto.getPreguntaProyectoList().get(1).setPreguntaRespuesta(getRespuestas()[1]);
//                    proyecto.getPreguntaProyectoList().get(2).setPreguntaRespuesta('S');
                }
                if (normaSeleccionada.getNormaId() == 141) {
                    proyecto.getPreguntaProyectoList().get(0).setPreguntaRespuesta(getRespuestas()[0]);
                    proyecto.getPreguntaProyectoList().get(1).setPreguntaRespuesta(getRespuestas()[1]);
                }

                if (normaSeleccionada.getNormaId() == 120) {
                    System.out.println("norma 120, obras");

                    //guardar obras
                    for (PreguntaObraHelper p : listaObras) {
                        p.getModel().getPreguntaObraPK().setFolioProyecto(proyecto.getProyectoPK().getFolioProyecto());
                        p.getModel().getPreguntaObraPK().setSerialProyecto(proyecto.getProyectoPK().getSerialProyecto());
                        p.getModel().getPreguntaObraPK().setPreguntaId(proyecto.getPreguntaProyectoList().get(1).getCatPregunta().getPreguntaId());
                        System.out.println("Obra " + p);

//                        EntityManager em = PoolEntityManagers.getEmf().createEntityManager();
//                        Query q = "DELETE FROM";
                        dao.merge(p.getModel());
                    }

//                    //guardar clima
//                    clima.getModel().getPreguntaClimaPK().setFolioProyecto(proyecto.getProyectoPK().getFolioProyecto());
//                    clima.getModel().getPreguntaClimaPK().setSerialProyecto(proyecto.getProyectoPK().getSerialProyecto());
//                    clima.getModel().getPreguntaClimaPK().setPreguntaId(proyecto.getPreguntaProyectoList().get(0).getCatPregunta().getPreguntaId());
//                    dao.merge(clima.getModel());
//                    //guardar tipo de vegetacion
//                    for (PreguntaVegetacionHelper p : listaVege) {
//                        p.getModel().getPreguntaVegetacionPK().setFolioProyecto(proyecto.getProyectoPK().getFolioProyecto());
//                        p.getModel().getPreguntaVegetacionPK().setSerialProyecto(proyecto.getProyectoPK().getSerialProyecto());
//                        p.getModel().getPreguntaVegetacionPK().setPreguntaId((proyecto.getPreguntaProyectoList().get(0).getCatPregunta().getPreguntaId()));
//                        System.out.println(" PreguntaVegetacion " + p.toString());
//                        try {
//                            dao.persist(p.getModel());
//                        } catch (EntityExistsException e) {
//                            dao3.modifica(p.getModel());
//                        }
//                    }
                    //guardar minerales
                    System.out.println("norma 120, minerales");

                    PreguntaMineral mineral;
                    for (MineralHelper mh : getLista()) {
                        for (String pm : mh.listaIds) {
                            mineral = new PreguntaMineral(new PreguntaMineralPK(proyecto.getProyectoPK().getFolioProyecto(),
                                    proyecto.getProyectoPK().getSerialProyecto(),
                                    proyecto.getPreguntaProyectoList().get(0).getCatPregunta().getPreguntaId(),
                                    new Short(pm)));
                            try {
                                dao.persist(mineral);
                            } catch (EntityExistsException e) {
                                dao3.modifica(mineral);
                            }
                        }
                    }
                    if (normaSeleccionada.getNormaId() == 141) {
                        PreguntaMineral mineral1;
                        for (String pm : catalogoMinerales) {
                            mineral1 = new PreguntaMineral(new PreguntaMineralPK(proyecto.getProyectoPK().getFolioProyecto(),
                                    proyecto.getProyectoPK().getSerialProyecto(),
                                    proyecto.getPreguntaProyectoList().get(0).getCatPregunta().getPreguntaId(),
                                    new Short(pm)));

                            dao.merge(mineral1);
                        }
                    }
                }
            }

            System.out.println("Guardado(0) ... ok");

            validaNorma(proyecto.getProyNormaId());
            disa = true;

            reqcontEnv.execute("alert('Su información ha sido guardada con éxito.')");
        } catch (Exception e) {
            reqcontEnv.execute("alert('No se ha podido guardar la información, Intente más tarde.')");
            e.printStackTrace();
        }
    }

    public void guardar2() {
        proyecto.setProySupuesto(referencia.charAt(0));
        try {

            dao.guardarCap1(proyecto, null, null);

            if (normaSeleccionada.getNormaId() != null) {
                validaNorma(normaSeleccionada.getNormaId());
            }

            // Supuesto 2
            if (proyecto.getProySupuesto() == '2') {
                parque.setProyecto(proyecto);
                dao.upsert(parque);
            }

            // Supuesto 3
            if (proyecto.getProySupuesto() == '3') {
                if (selecionaOpcion3.equals("pdu")) {
                    proyecto.setProySupuesto('U');
                    pduProyecto.setProyecto(proyecto);
                    dao.upsert(pduProyecto);
                }
                if (selecionaOpcion3.equals("poet")) {
                    proyecto.setProySupuesto('T');
                    poetProyecto.setProyecto(proyecto);
                    dao.upsert(poetProyecto);
                }
            }

            if (normaSeleccionada.getNormaId() != null) {

                //carga respuestas a las preguntas estaticas de 120 y 141
                if (normaSeleccionada.getNormaId() == 120) {
                    proyecto.getPreguntaProyectoList().get(0).setPreguntaRespuesta(getRespuestas()[0]);
                    proyecto.getPreguntaProyectoList().get(1).setPreguntaRespuesta(getRespuestas()[1]);
                    proyecto.getPreguntaProyectoList().get(2).setPreguntaRespuesta('S');
                }
                if (normaSeleccionada.getNormaId() == 141) {
                    proyecto.getPreguntaProyectoList().get(0).setPreguntaRespuesta(getRespuestas()[0]);
                    proyecto.getPreguntaProyectoList().get(1).setPreguntaRespuesta(getRespuestas()[1]);
                }

                if (normaSeleccionada.getNormaId() == 120) {
                    //guardar obras
                    for (PreguntaObraHelper p : listaObras) {
                        p.getModel().getPreguntaObraPK().setFolioProyecto(proyecto.getProyectoPK().getFolioProyecto());
                        p.getModel().getPreguntaObraPK().setSerialProyecto(proyecto.getProyectoPK().getSerialProyecto());
                        p.getModel().getPreguntaObraPK().setPreguntaId(proyecto.getPreguntaProyectoList().get(1).getCatPregunta().getPreguntaId());
                        System.out.println("Obra " + p);
                        dao.merge(p.getModel());
                    }

                    //guardar clima
                    clima.getModel().getPreguntaClimaPK().setFolioProyecto(proyecto.getProyectoPK().getFolioProyecto());
                    clima.getModel().getPreguntaClimaPK().setSerialProyecto(proyecto.getProyectoPK().getSerialProyecto());
                    clima.getModel().getPreguntaClimaPK().setPreguntaId(proyecto.getPreguntaProyectoList().get(0).getCatPregunta().getPreguntaId());
                    dao.merge(clima.getModel());

                    //guardar tipo de vegetacion
                    for (PreguntaVegetacionHelper p : listaVege) {
                        p.getModel().getPreguntaVegetacionPK().setFolioProyecto(proyecto.getProyectoPK().getFolioProyecto());
                        p.getModel().getPreguntaVegetacionPK().setSerialProyecto(proyecto.getProyectoPK().getSerialProyecto());
                        p.getModel().getPreguntaVegetacionPK().setPreguntaId((proyecto.getPreguntaProyectoList().get(0).getCatPregunta().getPreguntaId()));
                        System.out.println(" PreguntaVegetacion " + p.toString());
                        dao.merge(p.getModel());
                    }

                    //guardar minerales
                    PreguntaMineral mineral;
                    for (MineralHelper mh : getLista()) {
                        for (String pm : mh.listaIds) {
                            mineral = new PreguntaMineral(new PreguntaMineralPK(proyecto.getProyectoPK().getFolioProyecto(),
                                    proyecto.getProyectoPK().getSerialProyecto(),
                                    proyecto.getPreguntaProyectoList().get(0).getCatPregunta().getPreguntaId(),
                                    new Short(pm)));

                            dao.merge(mineral);
                        }
                    }
                    if (normaSeleccionada.getNormaId() == 141) {
                        PreguntaMineral mineral1;
                        for (String pm : catalogoMinerales) {
                            mineral1 = new PreguntaMineral(new PreguntaMineralPK(proyecto.getProyectoPK().getFolioProyecto(),
                                    proyecto.getProyectoPK().getSerialProyecto(),
                                    proyecto.getPreguntaProyectoList().get(0).getCatPregunta().getPreguntaId(),
                                    new Short(pm)));

                            dao.merge(mineral1);
                        }
                    }
                }
            }

            RequestContext reqcontEnv = RequestContext.getCurrentInstance();

            if (normaSeleccionada.getNormaId() == 129) {
                reqcontEnv.execute("menu4('129')");

            }
            reqcontEnv.execute("alert('Su informacion ha sido guardada con exito.')");

        } catch (Exception e) {
            RequestContext reqcontEnv = RequestContext.getCurrentInstance();
            reqcontEnv.execute("alert('No se ha podido guardar la informacion, Intente mas tarde.')");

            e.printStackTrace();
        }

    }

    /**
     * @return the referencia
     */
    public String getReferencia() {
        return referencia;
    }

    /**
     * @param referencia the referencia to set
     */
    public void setReferencia(String referencia) {
        this.referencia = referencia;
        proyecto.setProySupuesto(referencia.charAt(0));
    }

    /**
     * @return the catNom
     */
    public List<SelectItem> getCatNom() {
        return catNom;
    }

    /**
     * @param catNom the catNom to set
     */
    public void setCatNom(List<SelectItem> catNom) {
        this.catNom = catNom;
    }

    /**
     * @return the normaSeleccionada
     */
    public CatNorma getNormaSeleccionada() {
        return normaSeleccionada;
    }

    /**
     * @param normaSeleccionada the normaSeleccionada to set
     */
    public void setNormaSeleccionada(CatNorma normaSeleccionada) {
        this.normaSeleccionada = normaSeleccionada;
    }
//
//    /**
//     * @return the preguntas
//     */
//    public List<PreguntaProyecto> getPreguntas() {
//        return preguntas;
//    }
//
//    /**
//     * @param preguntas the preguntas to set
//     */
//    public void setPreguntas(List<PreguntaProyecto> preguntas) {
//        this.preguntas = preguntas;
//    }

//    /**
//     * @return the especifiaciones
//     */
//    public List<EspecificacionProyecto> getEspecifiaciones() {
//        return especifiaciones;
//    }
//
//    /**
//     * @param especifiaciones the especifiaciones to set
//     */
//    public void setEspecifiaciones(List<EspecificacionProyecto> especifiaciones) {
//        this.especifiaciones = especifiaciones;
//    }
    /**
     * @return the proyecto
     */
    public Proyecto getProyecto() {
        return proyecto;
    }

    /**
     * @param proyecto the proyecto to set
     */
    public void setProyecto(Proyecto proyecto) {
        this.proyecto = proyecto;
    }

    /**
     * @return the panelNorm
     */
    public Boolean getPanelNorm() {
        return panelNorm;
    }

    /**
     * @param panelNorm the panelNorm to set
     */
    public void setPanelNorm(Boolean panelNorm) {
        this.panelNorm = panelNorm;
    }

    /**
     * @return the panelPreg
     */
    public Boolean getPanelPreg() {
        return panelPreg;
    }

    /**
     * @param panelPreg the panelPreg to set
     */
    public void setPanelPreg(Boolean panelPreg) {
        this.panelPreg = panelPreg;
    }

    /**
     * @return the panelEspe
     */
    public Boolean getPanelEspe() {
        return panelEspe;
    }

    /**
     * @param panelEspe the panelEspe to set
     */
    public void setPanelEspe(Boolean panelEspe) {
        this.panelEspe = panelEspe;
    }

    /**
     * @return the btnGuardar
     */
    public Boolean getBtnGuardar() {
        return btnGuardar;
    }

    /**
     * @param btnGuardar the btnGuardar to set
     */
    public void setBtnGuardar(Boolean btnGuardar) {
        this.btnGuardar = btnGuardar;
    }

    public Boolean getFlagNormCientoV() {
        return flagNormCientoV;
    }

    public void setFlagNormCientoV(Boolean flagNormCientoV) {
        this.flagNormCientoV = flagNormCientoV;
    }

    public Boolean getFlagNorm141() {
        return flagNorm141;
    }

    public void setFlagNorm141(Boolean flagNorm141) {
        this.flagNorm141 = flagNorm141;
    }

    public List<PreguntaObraHelper> getListaObras() {
        return listaObras;
    }

    public void setListaObras(List<PreguntaObraHelper> listaObras) {
        this.listaObras = listaObras;
    }

    public PreguntaObraHelper getObra() {
        return obra;
    }

    public void setObra(PreguntaObraHelper obra) {
        this.obra = obra;
    }

    public PreguntaClimaHelper getClima() {
        return clima;
    }

    public void setClima(PreguntaClimaHelper clima) {
        this.clima = clima;
    }

    public List<PreguntaVegetacionHelper> getListaVege() {
        return listaVege;
    }

    public void setListaVege(List<PreguntaVegetacionHelper> listaVege) {
        this.listaVege = listaVege;
    }

    public PreguntaVegetacionHelper getVege() {
        return vege;
    }

    public void setVege(PreguntaVegetacionHelper vege) {
        this.vege = vege;
    }

    public List<CatTipoClima> getTipoClimaList() {
        return tipoClimaList;
    }

    public List<CatTipoVegetacion> getTipoVegetacionList() {
        return tipoVegetacionList;
    }

    public List<CatObra> getTipObraList() {
        return tipObraList;
    }

    public List<CatUnidadMedida> getListaUnidades() {
        return listaUnidades;
    }

    public List<CatClasificacionMineral> getListaClasif() {
        return listaClasif;
    }

    public void setListaClasif(List<CatClasificacionMineral> listaClasif) {
        this.listaClasif = listaClasif;
    }

    /**
     * @return the panelMsg
     */
    public Boolean getPanelMsg() {
        return panelMsg;
    }

    /**
     * @param panelMsg the panelMsg to set
     */
    public void setPanelMsg(Boolean panelMsg) {
        this.panelMsg = panelMsg;
    }

    /**
     * @param estadoMineralSelect the estadoMineralSelect to set
     */
    public void setEstadoMineralSelect(String[] estadoMineralSelect) {
        this.estadoMineralSelect = estadoMineralSelect;
    }

    /**
     * @return the clasificacionMineral
     */
    public List<CatClasificacionMineral> getClasificacionMineral() {
        return clasificacionMineral;
    }

    /**
     * @param clasificacionMineral the clasificacionMineral to set
     */
    public void setClasificacionMineral(List<CatClasificacionMineral> clasificacionMineral) {
        this.clasificacionMineral = clasificacionMineral;
    }

    /**
     * @return the mineralReservado
     */
    public boolean isMineralReservado() {
        return mineralReservado;
    }

    /**
     * @param mineralReservado the mineralReservado to set
     */
    public void setMineralReservado(boolean mineralReservado) {
        this.mineralReservado = mineralReservado;
    }

    public Character[] getRespuestas() {
        return respuestas;
    }

    public void setRespuestas(Character[] respuestas) {
        this.respuestas = respuestas;
    }

    public int getTamanioLista() {
        return lista.size();
    }

    /**
     * @return the estadoMineralSelect
     */
    public String[] getEstadoMineralSelect() {
        return estadoMineralSelect;
    }

    /**
     * @return the panelSup2
     */
    public Boolean getPanelSup2() {
        return panelSup2;
    }

    /**
     * @param panelSup2 the panelSup2 to set
     */
    public void setPanelSup2(Boolean panelSup2) {
        this.panelSup2 = panelSup2;
    }

    /**
     * @return the catParques
     */
    public List<SelectItem> getCatParques() {
        return catParques;
    }

    /**
     * @param catParques the catParques to set
     */
    public List<MineralHelper> getLista() {
        return lista;
    }

    public void setCatParques(List<SelectItem> catParques) {
        this.catParques = catParques;
    }

    /**
     * @param lista the lista to set
     */
    public void setLista(List<MineralHelper> lista) {
        this.lista = lista;
    }

    /**
     * @return the mineralReservado
     */
    public ParqueIndustrialProyecto getParque() {
        return parque;
    }

    /**
     * @param parque the parque to set
     */
    public void setParque(ParqueIndustrialProyecto parque) {
        this.parque = parque;
    }

    //mostrar mineral
    public List<String> getNomMineral() {
        List<String> min = new ArrayList();

        if (lista != null) {
            for (MineralHelper mh : lista) {
                for (String s : mh.listaIds) {
                    min.add(((CatMineralesReservados) dao3.busca(CatMineralesReservados.class, new Short(s))).getDescripcionMinerales());
                }
            }
        }

        return min;
    }

    /**
     * @return the panelSup3
     */
    public Boolean getPanelSup3() {
        return panelSup3;
    }

    /**
     * @param panelSup3 the panelSup3 to set
     */
    public void setPanelSup3(Boolean panelSup3) {
        this.panelSup3 = panelSup3;
    }

    /**
     * @return the panelSup31
     */
    public Boolean getPanelSup31() {
        return panelSup31;
    }

    /**
     * @param panelSup31 the panelSup31 to set
     */
    public void setPanelSup31(Boolean panelSup31) {
        this.panelSup31 = panelSup31;
    }

    /**
     * @return the panelSup32
     */
    public Boolean getPanelSup32() {
        return panelSup32;
    }

    /**
     * @param panelSup32 the panelSup32 to set
     */
    public void setPanelSup32(Boolean panelSup32) {
        this.panelSup32 = panelSup32;
    }

    /**
     * @return the catPDU
     */
    public List<SelectItem> getCatPDU() {
        return catPDU;
    }

    /**
     * @param catPDU the catPDU to set
     */
    public void setCatPDU(List<SelectItem> catPDU) {
        this.catPDU = catPDU;
    }

    /**
     * @return the selecionaOpcion3
     */
    public String getSelecionaOpcion3() {
        return selecionaOpcion3;
    }

    /**
     * @param selecionaOpcion3 the selecionaOpcion3 to set
     */
    public void setSelecionaOpcion3(String selecionaOpcion3) {
        this.selecionaOpcion3 = selecionaOpcion3;
    }

    /**
     * @return the catPODS
     */
    public List<SelectItem> getCatPODS() {
        return catPODS;
    }

    /**
     * @param catPODS the catPODS to set
     */
    public void setCatPODS(List<SelectItem> catPODS) {
        this.catPODS = catPODS;
    }

    /**
     * @return the pduProyecto
     */
    public PduProyecto getPduProyecto() {
        return pduProyecto;
    }

    /**
     * @param pduProyecto the pduProyecto to set
     */
    public void setPduProyecto(PduProyecto pduProyecto) {
        this.pduProyecto = pduProyecto;
    }

    /**
     * @return the poetProteco
     */
    public PoetProyecto getPoetProyecto() {
        return poetProyecto;
    }

    /**
     * @param poetProteco the poetProteco to set
     */
    public void setPoetProteco(PoetProyecto poetProyecto) {
        this.poetProyecto = poetProyecto;

    }

    /**
     * @return the disa
     */
    public Boolean getDisa() {
        return disa;
    }

    /**
     * @param disa the disa to set
     */
    public void setDisa(Boolean disa) {
        this.disa = disa;
    }

    /**
     * @return the btnGuarda1
     */
    public Boolean getBtnGuarda1() {
        return btnGuarda1;
    }

    /**
     * @param btnGuarda1 the btnGuarda1 to set
     */
    public void setBtnGuarda1(Boolean btnGuarda1) {
        this.btnGuarda1 = btnGuarda1;
    }

    /**
     * @return the btnGuarda2
     */
    public Boolean getBtnGuarda2() {
        return btnGuarda2;
    }

    /**
     * @param btnGuarda2 the btnGuarda2 to set
     */
    public void setBtnGuarda2(Boolean btnGuarda2) {
        this.btnGuarda2 = btnGuarda2;
    }

    /**
     * @return the catTipoClima
     */
    public List<SelectItem> getCatTipoClima() {
        return catTipoClima;
    }

    /**
     * @param catTipoClima the catTipoClima to set
     */
    public void setCatTipoClima(List<SelectItem> catTipoClima) {
        this.catTipoClima = catTipoClima;
    }

    /**
     * @return the catTipoMineral
     */
    public List<SelectItem> getCatTipoMineral() {
        return catTipoMineral;
    }

    /**
     * @param catTipoMineral the catTipoMineral to set
     */
    public void setCatTipoMineral(List<SelectItem> catTipoMineral) {
        this.catTipoMineral = catTipoMineral;
    }

    /**
     * @return the catTipoVegetacion
     */
    public List<SelectItem> getCatTipoVegetacion() {
        return catTipoVegetacion;
    }

    /**
     * @param catTipoVegetacion the catTipoVegetacion to set
     */
    public void setCatTipoVegetacion(List<SelectItem> catTipoVegetacion) {
        this.catTipoVegetacion = catTipoVegetacion;
    }

    /**
     * @return the catTipoObra
     */
    public List<SelectItem> getCatTipoObra() {
        return catTipoObra;
    }

    /**
     * @param catTipoObra the catTipoObra to set
     */
    public void setCatTipoObra(List<SelectItem> catTipoObra) {
        this.catTipoObra = catTipoObra;
    }

    /**
     * @return the pregVegetacion
     */
    public PreguntaVegetacion getPregVegetacion() {
        return pregVegetacion;
    }

    /**
     * @param pregVegetacion the pregVegetacion to set
     */
    public void setPregVegetacion(PreguntaVegetacion pregVegetacion) {
        this.pregVegetacion = pregVegetacion;
    }

    /**
     * @return the preClima
     */
    public PreguntaClima getPreClima() {
        return preClima;
    }

    /**
     * @param preClima the preClima to set
     */
    public void setPreClima(PreguntaClima preClima) {
        this.preClima = preClima;
    }

    /**
     * @return the tipoClimaSel
     */
    public CatTipoClima getTipoClimaSel() {
        return tipoClimaSel;
    }

    /**
     * @param tipoClimaSel the tipoClimaSel to set
     */
    public void setTipoClimaSel(CatTipoClima tipoClimaSel) {
        this.tipoClimaSel = tipoClimaSel;
    }

    /**
     * @return the tipoVegetaSel
     */
    public CatTipoVegetacion getTipoVegetaSel() {
        return tipoVegetaSel;
    }

    /**
     * @param tipoVegetaSel the tipoVegetaSel to set
     */
    public void setTipoVegetaSel(CatTipoVegetacion tipoVegetaSel) {
        this.tipoVegetaSel = tipoVegetaSel;
    }

    /**
     * @return the verMinSel
     */
    public Boolean getVerMinSel() {
        return verMinSel;
    }

    /**
     * @param verMinSel the verMinSel to set
     */
    public void setVerMinSel(Boolean verMinSel) {
        this.verMinSel = verMinSel;
    }

    /**
     * @return the msgMinSel
     */
    public String getMsgMinSel() {
        return msgMinSel;
    }

    /**
     * @param msgMinSel the msgMinSel to set
     */
    public void setMsgMinSel(String msgMinSel) {
        this.msgMinSel = msgMinSel;
    }

    /**
     * @return the obras
     */
    public List<PreguntaObra> getObras() {
        return obras;
    }

    /**
     * @param obras the obras to set
     */
    public void setObras(List<PreguntaObra> obras) {
        this.obras = obras;
    }

    public ArrayList<String> getCatalogoMinerales() {
        return catalogoMinerales;
    }

    public void setCatalogoMinerales(ArrayList<String> catalogoMinerales) {
        for (String c : catalogoMinerales) {
            if (!this.catalogoMinerales.contains(c)) {
                this.catalogoMinerales.add(c);
            }
        }
    }

    public class MineralHelper {//clase de apoyo para mostrar los minerales

        private List<CatMineralesReservados> listaMinerales;
        private ArrayList<String> listaIds;
        private String catalogo;

        public MineralHelper() {
        }

        public MineralHelper(List<CatMineralesReservados> listaMinerales, ArrayList<String> listaIds, String catalogo) {
            this.listaMinerales = listaMinerales;
            this.listaIds = listaIds;
            this.catalogo = catalogo;
        }

        public ArrayList<String> getListaIds() {
            return listaIds;
        }

        public void setListaIds(ArrayList<String> listaIds) {
            this.listaIds = listaIds;
        }

        public List<CatMineralesReservados> getListaMinerales() {
            return listaMinerales;
        }

        public void setListaMinerales(List<CatMineralesReservados> listaMinerales) {
            this.listaMinerales = listaMinerales;
        }

        public String getCatalogo() {
            return catalogo;
        }

        public void setCatalogo(String catalogo) {
            this.catalogo = catalogo;
        }
    }

	/**
	 * @return the folioProy
	 */
	public String getFolioProy() {
		return folioProy;
	}

	/**
	 * @param folioProy the folioProy to set
	 */
	public void setFolioProy(String folioProy) {
		this.folioProy = folioProy;
	}
}
