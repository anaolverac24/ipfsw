/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package mx.gob.semarnat.model;

import java.io.Serializable;
import java.math.BigInteger;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author mauricio
 */
@Entity
@Table(name = "OGMMPUN")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Ogmmpun.findAll", query = "SELECT o FROM Ogmmpun o"),
    @NamedQuery(name = "Ogmmpun.findByObjectid12", query = "SELECT o FROM Ogmmpun o WHERE o.objectid12 = :objectid12"),
    @NamedQuery(name = "Ogmmpun.findByObjectid1", query = "SELECT o FROM Ogmmpun o WHERE o.objectid1 = :objectid1"),
    @NamedQuery(name = "Ogmmpun.findById", query = "SELECT o FROM Ogmmpun o WHERE o.id = :id"),
    @NamedQuery(name = "Ogmmpun.findByObjectid", query = "SELECT o FROM Ogmmpun o WHERE o.objectid = :objectid"),
    @NamedQuery(name = "Ogmmpun.findByComp", query = "SELECT o FROM Ogmmpun o WHERE o.comp = :comp"),
    @NamedQuery(name = "Ogmmpun.findByDescrip", query = "SELECT o FROM Ogmmpun o WHERE o.descrip = :descrip"),
    @NamedQuery(name = "Ogmmpun.findByProy", query = "SELECT o FROM Ogmmpun o WHERE o.proy = :proy"),
    @NamedQuery(name = "Ogmmpun.findByRowlayer", query = "SELECT o FROM Ogmmpun o WHERE o.rowlayer = :rowlayer"),
    @NamedQuery(name = "Ogmmpun.findByShape", query = "SELECT o FROM Ogmmpun o WHERE o.shape = :shape")})
public class Ogmmpun implements Serializable {
    private static final long serialVersionUID = 1L;
    @Basic(optional = false)
    @Column(name = "OBJECTID_12")
    private BigInteger objectid12;
    @Column(name = "OBJECTID_1")
    private Long objectid1;
    @Column(name = "ID")
    private Integer id;
    @Id
    @Basic(optional = false)
    @Column(name = "OBJECTID")
    private Long objectid;
    @Column(name = "COMP")
    private String comp;
    @Column(name = "DESCRIP")
    private String descrip;
    @Column(name = "PROY")
    private String proy;
    @Column(name = "ROWLAYER")
    private Long rowlayer;
    @Column(name = "SHAPE")
    private BigInteger shape;
    @OneToMany(mappedBy = "ogmmpunObjectid")
    private List<Ogmsig> ogmsigList;

    public Ogmmpun() {
    }

    public Ogmmpun(Long objectid) {
        this.objectid = objectid;
    }

    public Ogmmpun(Long objectid, BigInteger objectid12) {
        this.objectid = objectid;
        this.objectid12 = objectid12;
    }

    public BigInteger getObjectid12() {
        return objectid12;
    }

    public void setObjectid12(BigInteger objectid12) {
        this.objectid12 = objectid12;
    }

    public Long getObjectid1() {
        return objectid1;
    }

    public void setObjectid1(Long objectid1) {
        this.objectid1 = objectid1;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Long getObjectid() {
        return objectid;
    }

    public void setObjectid(Long objectid) {
        this.objectid = objectid;
    }

    public String getComp() {
        return comp;
    }

    public void setComp(String comp) {
        this.comp = comp;
    }

    public String getDescrip() {
        return descrip;
    }

    public void setDescrip(String descrip) {
        this.descrip = descrip;
    }

    public String getProy() {
        return proy;
    }

    public void setProy(String proy) {
        this.proy = proy;
    }

    public Long getRowlayer() {
        return rowlayer;
    }

    public void setRowlayer(Long rowlayer) {
        this.rowlayer = rowlayer;
    }

    public BigInteger getShape() {
        return shape;
    }

    public void setShape(BigInteger shape) {
        this.shape = shape;
    }

    @XmlTransient
    public List<Ogmsig> getOgmsigList() {
        return ogmsigList;
    }

    public void setOgmsigList(List<Ogmsig> ogmsigList) {
        this.ogmsigList = ogmsigList;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (objectid != null ? objectid.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Ogmmpun)) {
            return false;
        }
        Ogmmpun other = (Ogmmpun) object;
        if ((this.objectid == null && other.objectid != null) || (this.objectid != null && !this.objectid.equals(other.objectid))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "mx.gob.semarnat.model.Ogmmpun[ objectid=" + objectid + " ]";
    }
    
}
