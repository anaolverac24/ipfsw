/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package mx.gob.semarnat.model;

import java.io.Serializable;
import java.math.BigInteger;
import java.util.Date;
import java.util.List;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author mauricio
 */
@Entity
@Table(name = "REG_TERR_PRIORI")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "RegTerrPriori.findAll", query = "SELECT r FROM RegTerrPriori r"),
    @NamedQuery(name = "RegTerrPriori.findByNumFolio", query = "SELECT r FROM RegTerrPriori r WHERE r.regTerrPrioriPK.numFolio = :numFolio"),
    @NamedQuery(name = "RegTerrPriori.findByCveProy", query = "SELECT r FROM RegTerrPriori r WHERE r.regTerrPrioriPK.cveProy = :cveProy"),
    @NamedQuery(name = "RegTerrPriori.findByCveArea", query = "SELECT r FROM RegTerrPriori r WHERE r.regTerrPrioriPK.cveArea = :cveArea"),
    @NamedQuery(name = "RegTerrPriori.findByVersion", query = "SELECT r FROM RegTerrPriori r WHERE r.regTerrPrioriPK.version = :version"),
    @NamedQuery(name = "RegTerrPriori.findByNombre", query = "SELECT r FROM RegTerrPriori r WHERE r.nombre = :nombre"),
    @NamedQuery(name = "RegTerrPriori.findByClave", query = "SELECT r FROM RegTerrPriori r WHERE r.clave = :clave"),
    @NamedQuery(name = "RegTerrPriori.findBySupEa", query = "SELECT r FROM RegTerrPriori r WHERE r.supEa = :supEa"),
    @NamedQuery(name = "RegTerrPriori.findByProy", query = "SELECT r FROM RegTerrPriori r WHERE r.proy = :proy"),
    @NamedQuery(name = "RegTerrPriori.findByComp", query = "SELECT r FROM RegTerrPriori r WHERE r.comp = :comp"),
    @NamedQuery(name = "RegTerrPriori.findByDescrip", query = "SELECT r FROM RegTerrPriori r WHERE r.descrip = :descrip"),
    @NamedQuery(name = "RegTerrPriori.findByAreabuffer", query = "SELECT r FROM RegTerrPriori r WHERE r.areabuffer = :areabuffer"),
    @NamedQuery(name = "RegTerrPriori.findByArea", query = "SELECT r FROM RegTerrPriori r WHERE r.area = :area"),
    @NamedQuery(name = "RegTerrPriori.findByFechaHora", query = "SELECT r FROM RegTerrPriori r WHERE r.fechaHora = :fechaHora")})
public class RegTerrPriori implements Serializable {
    private static final long serialVersionUID = 1L;
    @EmbeddedId
    protected RegTerrPrioriPK regTerrPrioriPK;
    @Column(name = "NOMBRE")
    private String nombre;
    @Column(name = "CLAVE")
    private BigInteger clave;
    @Column(name = "SUP_EA")
    private BigInteger supEa;
    @Column(name = "PROY")
    private String proy;
    @Column(name = "COMP")
    private String comp;
    @Column(name = "DESCRIP")
    private String descrip;
    @Column(name = "AREABUFFER")
    private BigInteger areabuffer;
    @Column(name = "AREA")
    private BigInteger area;
    @Column(name = "FECHA_HORA")
    @Temporal(TemporalType.TIMESTAMP)
    private Date fechaHora;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "regTerrPriori")
    private List<SigeiaAnalisis> sigeiaAnalisisList;

    public RegTerrPriori() {
    }

    public RegTerrPriori(RegTerrPrioriPK regTerrPrioriPK) {
        this.regTerrPrioriPK = regTerrPrioriPK;
    }

    public RegTerrPriori(String numFolio, String cveProy, String cveArea, short version) {
        this.regTerrPrioriPK = new RegTerrPrioriPK(numFolio, cveProy, cveArea, version);
    }

    public RegTerrPrioriPK getRegTerrPrioriPK() {
        return regTerrPrioriPK;
    }

    public void setRegTerrPrioriPK(RegTerrPrioriPK regTerrPrioriPK) {
        this.regTerrPrioriPK = regTerrPrioriPK;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public BigInteger getClave() {
        return clave;
    }

    public void setClave(BigInteger clave) {
        this.clave = clave;
    }

    public BigInteger getSupEa() {
        return supEa;
    }

    public void setSupEa(BigInteger supEa) {
        this.supEa = supEa;
    }

    public String getProy() {
        return proy;
    }

    public void setProy(String proy) {
        this.proy = proy;
    }

    public String getComp() {
        return comp;
    }

    public void setComp(String comp) {
        this.comp = comp;
    }

    public String getDescrip() {
        return descrip;
    }

    public void setDescrip(String descrip) {
        this.descrip = descrip;
    }

    public BigInteger getAreabuffer() {
        return areabuffer;
    }

    public void setAreabuffer(BigInteger areabuffer) {
        this.areabuffer = areabuffer;
    }

    public BigInteger getArea() {
        return area;
    }

    public void setArea(BigInteger area) {
        this.area = area;
    }

    public Date getFechaHora() {
        return fechaHora;
    }

    public void setFechaHora(Date fechaHora) {
        this.fechaHora = fechaHora;
    }

    @XmlTransient
    public List<SigeiaAnalisis> getSigeiaAnalisisList() {
        return sigeiaAnalisisList;
    }

    public void setSigeiaAnalisisList(List<SigeiaAnalisis> sigeiaAnalisisList) {
        this.sigeiaAnalisisList = sigeiaAnalisisList;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (regTerrPrioriPK != null ? regTerrPrioriPK.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof RegTerrPriori)) {
            return false;
        }
        RegTerrPriori other = (RegTerrPriori) object;
        if ((this.regTerrPrioriPK == null && other.regTerrPrioriPK != null) || (this.regTerrPrioriPK != null && !this.regTerrPrioriPK.equals(other.regTerrPrioriPK))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "mx.gob.semarnat.model.RegTerrPriori[ regTerrPrioriPK=" + regTerrPrioriPK + " ]";
    }
    
}
