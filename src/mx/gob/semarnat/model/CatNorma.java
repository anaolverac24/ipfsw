/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package mx.gob.semarnat.model;

import java.io.Serializable;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author mauricio
 */
@Entity
@Table(name = "CAT_NORMA")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "CatNorma.findAll", query = "SELECT c FROM CatNorma c"),
    @NamedQuery(name = "CatNorma.findByNormaId", query = "SELECT c FROM CatNorma c WHERE c.normaId = :normaId"),
    @NamedQuery(name = "CatNorma.findByNormaDescripcion", query = "SELECT c FROM CatNorma c WHERE c.normaDescripcion = :normaDescripcion"),
    @NamedQuery(name = "CatNorma.findByTipoTramite", query = "SELECT c FROM CatNorma c WHERE c.tipoTramite = :tipoTramite"),
    @NamedQuery(name = "CatNorma.findByNormaDinamica", query = "SELECT c FROM CatNorma c WHERE c.normaDinamica = :normaDinamica")})
public class CatNorma implements Serializable {
    @Column(name = "NORMA_DINAMICA")
    private Character normaDinamica;
    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @Column(name = "NORMA_ID")
    private Short normaId;
    @Column(name = "NORMA_DESCRIPCION")
    private String normaDescripcion;
    @Column(name = "TIPO_TRAMITE")
    private Character tipoTramite;
    
    @Column(name = "NORMA_NOMBRE")
    private String normaNombre;
    
    
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "catNorma")
    private List<CatEspecificacion> catEspecificacionList;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "normaId")
    private List<CatPregunta> catPreguntaList;

    public CatNorma() {
    }

    public CatNorma(Short normaId) {
        this.normaId = normaId;
    }

    public Short getNormaId() {
        return normaId;
    }

    public void setNormaId(Short normaId) {
        this.normaId = normaId;
    }

    public String getNormaDescripcion() {
        return normaDescripcion;
    }

    public void setNormaDescripcion(String normaDescripcion) {
        this.normaDescripcion = normaDescripcion;
    }

    public Character getTipoTramite() {
        return tipoTramite;
    }

    public void setTipoTramite(Character tipoTramite) {
        this.tipoTramite = tipoTramite;
    }

    public Character getNormaDinamica() {
        return normaDinamica;
    }

    public void setNormaDinamica(Character normaDinamica) {
        this.normaDinamica = normaDinamica;
    }

    @XmlTransient
    public List<CatEspecificacion> getCatEspecificacionList() {
        return catEspecificacionList;
    }

    public void setCatEspecificacionList(List<CatEspecificacion> catEspecificacionList) {
        this.catEspecificacionList = catEspecificacionList;
    }

    @XmlTransient
    public List<CatPregunta> getCatPreguntaList() {
        return catPreguntaList;
    }

    public void setCatPreguntaList(List<CatPregunta> catPreguntaList) {
        this.catPreguntaList = catPreguntaList;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (normaId != null ? normaId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof CatNorma)) {
            return false;
        }
        CatNorma other = (CatNorma) object;
        if ((this.normaId == null && other.normaId != null) || (this.normaId != null && !this.normaId.equals(other.normaId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "mx.gob.semarnat.model.CatNorma[ normaId=" + normaId + " ]";
    }

    /**
     * @return the normaNombre
     */
    public String getNormaNombre() {
        return normaNombre;
    }

    /**
     * @param normaNombre the normaNombre to set
     */
    public void setNormaNombre(String normaNombre) {
        this.normaNombre = normaNombre;
    }

    
    
}
