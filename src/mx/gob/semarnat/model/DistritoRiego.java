/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package mx.gob.semarnat.model;

import java.io.Serializable;
import java.math.BigInteger;
import java.util.Date;
import java.util.List;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author mauricio
 */
@Entity
@Table(name = "DISTRITO_RIEGO")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "DistritoRiego.findAll", query = "SELECT d FROM DistritoRiego d"),
    @NamedQuery(name = "DistritoRiego.findByNumFolio", query = "SELECT d FROM DistritoRiego d WHERE d.distritoRiegoPK.numFolio = :numFolio"),
    @NamedQuery(name = "DistritoRiego.findByCveProy", query = "SELECT d FROM DistritoRiego d WHERE d.distritoRiegoPK.cveProy = :cveProy"),
    @NamedQuery(name = "DistritoRiego.findByCveArea", query = "SELECT d FROM DistritoRiego d WHERE d.distritoRiegoPK.cveArea = :cveArea"),
    @NamedQuery(name = "DistritoRiego.findByVersion", query = "SELECT d FROM DistritoRiego d WHERE d.distritoRiegoPK.version = :version"),
    @NamedQuery(name = "DistritoRiego.findByClave", query = "SELECT d FROM DistritoRiego d WHERE d.clave = :clave"),
    @NamedQuery(name = "DistritoRiego.findByNombre", query = "SELECT d FROM DistritoRiego d WHERE d.nombre = :nombre"),
    @NamedQuery(name = "DistritoRiego.findByNumusua", query = "SELECT d FROM DistritoRiego d WHERE d.numusua = :numusua"),
    @NamedQuery(name = "DistritoRiego.findBySupEa", query = "SELECT d FROM DistritoRiego d WHERE d.supEa = :supEa"),
    @NamedQuery(name = "DistritoRiego.findByProy", query = "SELECT d FROM DistritoRiego d WHERE d.proy = :proy"),
    @NamedQuery(name = "DistritoRiego.findByComp", query = "SELECT d FROM DistritoRiego d WHERE d.comp = :comp"),
    @NamedQuery(name = "DistritoRiego.findByDescrip", query = "SELECT d FROM DistritoRiego d WHERE d.descrip = :descrip"),
    @NamedQuery(name = "DistritoRiego.findByAreabuffer", query = "SELECT d FROM DistritoRiego d WHERE d.areabuffer = :areabuffer"),
    @NamedQuery(name = "DistritoRiego.findByArea", query = "SELECT d FROM DistritoRiego d WHERE d.area = :area"),
    @NamedQuery(name = "DistritoRiego.findByFechaHora", query = "SELECT d FROM DistritoRiego d WHERE d.fechaHora = :fechaHora")})
public class DistritoRiego implements Serializable {
    private static final long serialVersionUID = 1L;
    @EmbeddedId
    protected DistritoRiegoPK distritoRiegoPK;
    @Column(name = "CLAVE")
    private String clave;
    @Column(name = "NOMBRE")
    private String nombre;
    @Column(name = "NUMUSUA")
    private BigInteger numusua;
    @Column(name = "SUP_EA")
    private BigInteger supEa;
    @Column(name = "PROY")
    private String proy;
    @Column(name = "COMP")
    private String comp;
    @Column(name = "DESCRIP")
    private String descrip;
    @Column(name = "AREABUFFER")
    private BigInteger areabuffer;
    @Column(name = "AREA")
    private BigInteger area;
    @Column(name = "FECHA_HORA")
    @Temporal(TemporalType.TIMESTAMP)
    private Date fechaHora;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "distritoRiego")
    private List<SigeiaAnalisis> sigeiaAnalisisList;

    public DistritoRiego() {
    }

    public DistritoRiego(DistritoRiegoPK distritoRiegoPK) {
        this.distritoRiegoPK = distritoRiegoPK;
    }

    public DistritoRiego(String numFolio, String cveProy, String cveArea, short version) {
        this.distritoRiegoPK = new DistritoRiegoPK(numFolio, cveProy, cveArea, version);
    }

    public DistritoRiegoPK getDistritoRiegoPK() {
        return distritoRiegoPK;
    }

    public void setDistritoRiegoPK(DistritoRiegoPK distritoRiegoPK) {
        this.distritoRiegoPK = distritoRiegoPK;
    }

    public String getClave() {
        return clave;
    }

    public void setClave(String clave) {
        this.clave = clave;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public BigInteger getNumusua() {
        return numusua;
    }

    public void setNumusua(BigInteger numusua) {
        this.numusua = numusua;
    }

    public BigInteger getSupEa() {
        return supEa;
    }

    public void setSupEa(BigInteger supEa) {
        this.supEa = supEa;
    }

    public String getProy() {
        return proy;
    }

    public void setProy(String proy) {
        this.proy = proy;
    }

    public String getComp() {
        return comp;
    }

    public void setComp(String comp) {
        this.comp = comp;
    }

    public String getDescrip() {
        return descrip;
    }

    public void setDescrip(String descrip) {
        this.descrip = descrip;
    }

    public BigInteger getAreabuffer() {
        return areabuffer;
    }

    public void setAreabuffer(BigInteger areabuffer) {
        this.areabuffer = areabuffer;
    }

    public BigInteger getArea() {
        return area;
    }

    public void setArea(BigInteger area) {
        this.area = area;
    }

    public Date getFechaHora() {
        return fechaHora;
    }

    public void setFechaHora(Date fechaHora) {
        this.fechaHora = fechaHora;
    }

    @XmlTransient
    public List<SigeiaAnalisis> getSigeiaAnalisisList() {
        return sigeiaAnalisisList;
    }

    public void setSigeiaAnalisisList(List<SigeiaAnalisis> sigeiaAnalisisList) {
        this.sigeiaAnalisisList = sigeiaAnalisisList;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (distritoRiegoPK != null ? distritoRiegoPK.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof DistritoRiego)) {
            return false;
        }
        DistritoRiego other = (DistritoRiego) object;
        if ((this.distritoRiegoPK == null && other.distritoRiegoPK != null) || (this.distritoRiegoPK != null && !this.distritoRiegoPK.equals(other.distritoRiegoPK))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "mx.gob.semarnat.model.DistritoRiego[ distritoRiegoPK=" + distritoRiegoPK + " ]";
    }
    
}
