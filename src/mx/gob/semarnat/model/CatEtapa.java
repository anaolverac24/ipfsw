/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package mx.gob.semarnat.model;

import java.io.Serializable;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author mauricio
 */
@Entity
@Table(name = "CAT_ETAPA")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "CatEtapa.findAll", query = "SELECT c FROM CatEtapa c"),
    @NamedQuery(name = "CatEtapa.findByEtapaId", query = "SELECT c FROM CatEtapa c WHERE c.etapaId = :etapaId"),
    @NamedQuery(name = "CatEtapa.findByEtapaDescripcion", query = "SELECT c FROM CatEtapa c WHERE c.etapaDescripcion = :etapaDescripcion")})
public class CatEtapa implements Serializable {
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "catEtapa")
    private List<ContaminanteProyecto> contaminanteProyectoList;
    @OneToMany(mappedBy = "etapaId")
    private List<ImpactoProyecto> impactoProyectoList;
    @OneToMany(mappedBy = "etapaId")
    private List<SustanciaProyecto> sustanciaProyectoList;
    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @Column(name = "ETAPA_ID")
    private Short etapaId;
    @Basic(optional = false)
    @Column(name = "ETAPA_DESCRIPCION")
    private String etapaDescripcion;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "catEtapa")
    private List<EtapaProyecto> etapaProyectoList;

    public CatEtapa() {
    }

    public CatEtapa(Short etapaId) {
        this.etapaId = etapaId;
    }

    public CatEtapa(Short etapaId, String etapaDescripcion) {
        this.etapaId = etapaId;
        this.etapaDescripcion = etapaDescripcion;
    }

    public Short getEtapaId() {
        return etapaId;
    }

    public void setEtapaId(Short etapaId) {
        this.etapaId = etapaId;
    }

    public String getEtapaDescripcion() {
        return etapaDescripcion;
    }

    public void setEtapaDescripcion(String etapaDescripcion) {
        this.etapaDescripcion = etapaDescripcion;
    }

    @XmlTransient
    public List<EtapaProyecto> getEtapaProyectoList() {
        return etapaProyectoList;
    }

    public void setEtapaProyectoList(List<EtapaProyecto> etapaProyectoList) {
        this.etapaProyectoList = etapaProyectoList;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (etapaId != null ? etapaId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        if (!(object instanceof CatEtapa)) {
            return false;
        }
        CatEtapa other = (CatEtapa) object;
        if ((this.etapaId == null && other.etapaId != null) || (this.etapaId != null && !this.etapaId.equals(other.etapaId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return etapaId.toString();
    }

    @XmlTransient
    public List<ImpactoProyecto> getImpactoProyectoList() {
        return impactoProyectoList;
    }

    public void setImpactoProyectoList(List<ImpactoProyecto> impactoProyectoList) {
        this.impactoProyectoList = impactoProyectoList;
    }

    @XmlTransient
    public List<SustanciaProyecto> getSustanciaProyectoList() {
        return sustanciaProyectoList;
    }

    public void setSustanciaProyectoList(List<SustanciaProyecto> sustanciaProyectoList) {
        this.sustanciaProyectoList = sustanciaProyectoList;
    }

    @XmlTransient
    public List<ContaminanteProyecto> getContaminanteProyectoList() {
        return contaminanteProyectoList;
    }

    public void setContaminanteProyectoList(List<ContaminanteProyecto> contaminanteProyectoList) {
        this.contaminanteProyectoList = contaminanteProyectoList;
    }
    
}
