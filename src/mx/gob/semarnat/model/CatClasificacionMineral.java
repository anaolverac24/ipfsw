/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.gob.semarnat.model;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Admin
 */
@Entity
@Table(name = "CAT_CLASIFICACION_MINERAL")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "CatClasificacionMineral.findAll", query = "SELECT c FROM CatClasificacionMineral c"),
    @NamedQuery(name = "CatClasificacionMineral.findByIdClasificacion", query = "SELECT c FROM CatClasificacionMineral c WHERE c.idClasificacion = :idClasificacion"),
    @NamedQuery(name = "CatClasificacionMineral.findByDescripcionClasificacion", query = "SELECT c FROM CatClasificacionMineral c WHERE c.descripcionClasificacion = :descripcionClasificacion")})
public class CatClasificacionMineral implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @Column(name = "ID_CLASIFICACION")
    private Short idClasificacion;
    @Column(name = "DESCRIPCION_CLASIFICACION")
    private String descripcionClasificacion;

    public CatClasificacionMineral() {
    }

    public CatClasificacionMineral(Short idClasificacion) {
        this.idClasificacion = idClasificacion;
    }

    public Short getIdClasificacion() {
        return idClasificacion;
    }

    public void setIdClasificacion(Short idClasificacion) {
        this.idClasificacion = idClasificacion;
    }

    public String getDescripcionClasificacion() {
        return descripcionClasificacion;
    }

    public void setDescripcionClasificacion(String descripcionClasificacion) {
        this.descripcionClasificacion = descripcionClasificacion;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idClasificacion != null ? idClasificacion.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof CatClasificacionMineral)) {
            return false;
        }
        CatClasificacionMineral other = (CatClasificacionMineral) object;
        if ((this.idClasificacion == null && other.idClasificacion != null) || (this.idClasificacion != null && !this.idClasificacion.equals(other.idClasificacion))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "mx.gob.semarnat.model.CatClasificacionMineral[ idClasificacion=" + idClasificacion + " ]";
    }
    
}
