/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package mx.gob.semarnat.model;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author mauricio
 */
@Entity
@Table(name = "CAT_PARAMETRO")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "CatParametro.findAll", query = "SELECT c FROM CatParametro c"),
    @NamedQuery(name = "CatParametro.findByParId", query = "SELECT c FROM CatParametro c WHERE c.parId = :parId"),
    @NamedQuery(name = "CatParametro.findByParDescripcion", query = "SELECT c FROM CatParametro c WHERE c.parDescripcion = :parDescripcion"),
    @NamedQuery(name = "CatParametro.findByParConsecutivo", query = "SELECT c FROM CatParametro c WHERE c.parConsecutivo = :parConsecutivo"),
    @NamedQuery(name = "CatParametro.findByParRuta", query = "SELECT c FROM CatParametro c WHERE c.parRuta = :parRuta")})
public class CatParametro implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @Column(name = "PAR_ID")
    private Short parId;
    @Column(name = "PAR_DESCRIPCION")
    private String parDescripcion;
    @Column(name = "PAR_CONSECUTIVO")
    private Long parConsecutivo;
    @Basic(optional = false)
    @Column(name = "PAR_RUTA")
    private String parRuta;

    public CatParametro() {
    }

    public CatParametro(Short parId) {
        this.parId = parId;
    }

    public CatParametro(Short parId, String parRuta) {
        this.parId = parId;
        this.parRuta = parRuta;
    }

    public Short getParId() {
        return parId;
    }

    public void setParId(Short parId) {
        this.parId = parId;
    }

    public String getParDescripcion() {
        return parDescripcion;
    }

    public void setParDescripcion(String parDescripcion) {
        this.parDescripcion = parDescripcion;
    }

    public Long getParConsecutivo() {
        return parConsecutivo;
    }

    public void setParConsecutivo(Long parConsecutivo) {
        this.parConsecutivo = parConsecutivo;
    }

    public String getParRuta() {
        return parRuta;
    }

    public void setParRuta(String parRuta) {
        this.parRuta = parRuta;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (parId != null ? parId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof CatParametro)) {
            return false;
        }
        CatParametro other = (CatParametro) object;
        if ((this.parId == null && other.parId != null) || (this.parId != null && !this.parId.equals(other.parId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.model.CatParametro[ parId=" + parId + " ]";
    }
    
}
