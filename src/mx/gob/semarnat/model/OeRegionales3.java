/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package mx.gob.semarnat.model;

import java.io.Serializable;
import java.math.BigInteger;
import java.util.Date;
import java.util.List;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author mauricio
 */
@Entity
@Table(name = "OE_REGIONALES_3")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "OeRegionales3.findAll", query = "SELECT o FROM OeRegionales3 o"),
    @NamedQuery(name = "OeRegionales3.findByNumFolio", query = "SELECT o FROM OeRegionales3 o WHERE o.oeRegionales3PK.numFolio = :numFolio"),
    @NamedQuery(name = "OeRegionales3.findByCveProy", query = "SELECT o FROM OeRegionales3 o WHERE o.oeRegionales3PK.cveProy = :cveProy"),
    @NamedQuery(name = "OeRegionales3.findByCveArea", query = "SELECT o FROM OeRegionales3 o WHERE o.oeRegionales3PK.cveArea = :cveArea"),
    @NamedQuery(name = "OeRegionales3.findByVersion", query = "SELECT o FROM OeRegionales3 o WHERE o.oeRegionales3PK.version = :version"),
    @NamedQuery(name = "OeRegionales3.findByNomOe", query = "SELECT o FROM OeRegionales3 o WHERE o.nomOe = :nomOe"),
    @NamedQuery(name = "OeRegionales3.findByTipoOe", query = "SELECT o FROM OeRegionales3 o WHERE o.tipoOe = :tipoOe"),
    @NamedQuery(name = "OeRegionales3.findByUga", query = "SELECT o FROM OeRegionales3 o WHERE o.uga = :uga"),
    @NamedQuery(name = "OeRegionales3.findByUgaComp", query = "SELECT o FROM OeRegionales3 o WHERE o.ugaComp = :ugaComp"),
    @NamedQuery(name = "OeRegionales3.findByPolitica", query = "SELECT o FROM OeRegionales3 o WHERE o.politica = :politica"),
    @NamedQuery(name = "OeRegionales3.findByPolMapear", query = "SELECT o FROM OeRegionales3 o WHERE o.polMapear = :polMapear"),
    @NamedQuery(name = "OeRegionales3.findByUsoPred", query = "SELECT o FROM OeRegionales3 o WHERE o.usoPred = :usoPred"),
    @NamedQuery(name = "OeRegionales3.findByCriterios", query = "SELECT o FROM OeRegionales3 o WHERE o.criterios = :criterios"),
    @NamedQuery(name = "OeRegionales3.findBySupEa", query = "SELECT o FROM OeRegionales3 o WHERE o.supEa = :supEa"),
    @NamedQuery(name = "OeRegionales3.findByProy", query = "SELECT o FROM OeRegionales3 o WHERE o.proy = :proy"),
    @NamedQuery(name = "OeRegionales3.findByComp", query = "SELECT o FROM OeRegionales3 o WHERE o.comp = :comp"),
    @NamedQuery(name = "OeRegionales3.findByDescrip", query = "SELECT o FROM OeRegionales3 o WHERE o.descrip = :descrip"),
    @NamedQuery(name = "OeRegionales3.findByAreabuffer", query = "SELECT o FROM OeRegionales3 o WHERE o.areabuffer = :areabuffer"),
    @NamedQuery(name = "OeRegionales3.findByArea", query = "SELECT o FROM OeRegionales3 o WHERE o.area = :area"),
    @NamedQuery(name = "OeRegionales3.findByFechaHora", query = "SELECT o FROM OeRegionales3 o WHERE o.fechaHora = :fechaHora")})
public class OeRegionales3 implements Serializable {
    private static final long serialVersionUID = 1L;
    @EmbeddedId
    protected OeRegionales3PK oeRegionales3PK;
    @Column(name = "NOM_OE")
    private String nomOe;
    @Column(name = "TIPO_OE")
    private String tipoOe;
    @Column(name = "UGA")
    private String uga;
    @Column(name = "UGA_COMP")
    private String ugaComp;
    @Column(name = "POLITICA")
    private String politica;
    @Column(name = "POL_MAPEAR")
    private String polMapear;
    @Column(name = "USO_PRED")
    private String usoPred;
    @Column(name = "CRITERIOS")
    private String criterios;
    @Column(name = "SUP_EA")
    private BigInteger supEa;
    @Column(name = "PROY")
    private String proy;
    @Column(name = "COMP")
    private String comp;
    @Column(name = "DESCRIP")
    private String descrip;
    @Column(name = "AREABUFFER")
    private BigInteger areabuffer;
    @Column(name = "AREA")
    private BigInteger area;
    @Column(name = "FECHA_HORA")
    @Temporal(TemporalType.TIMESTAMP)
    private Date fechaHora;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "oeRegionales3")
    private List<SigeiaAnalisis> sigeiaAnalisisList;

    public OeRegionales3() {
    }

    public OeRegionales3(OeRegionales3PK oeRegionales3PK) {
        this.oeRegionales3PK = oeRegionales3PK;
    }

    public OeRegionales3(String numFolio, String cveProy, String cveArea, short version) {
        this.oeRegionales3PK = new OeRegionales3PK(numFolio, cveProy, cveArea, version);
    }

    public OeRegionales3PK getOeRegionales3PK() {
        return oeRegionales3PK;
    }

    public void setOeRegionales3PK(OeRegionales3PK oeRegionales3PK) {
        this.oeRegionales3PK = oeRegionales3PK;
    }

    public String getNomOe() {
        return nomOe;
    }

    public void setNomOe(String nomOe) {
        this.nomOe = nomOe;
    }

    public String getTipoOe() {
        return tipoOe;
    }

    public void setTipoOe(String tipoOe) {
        this.tipoOe = tipoOe;
    }

    public String getUga() {
        return uga;
    }

    public void setUga(String uga) {
        this.uga = uga;
    }

    public String getUgaComp() {
        return ugaComp;
    }

    public void setUgaComp(String ugaComp) {
        this.ugaComp = ugaComp;
    }

    public String getPolitica() {
        return politica;
    }

    public void setPolitica(String politica) {
        this.politica = politica;
    }

    public String getPolMapear() {
        return polMapear;
    }

    public void setPolMapear(String polMapear) {
        this.polMapear = polMapear;
    }

    public String getUsoPred() {
        return usoPred;
    }

    public void setUsoPred(String usoPred) {
        this.usoPred = usoPred;
    }

    public String getCriterios() {
        return criterios;
    }

    public void setCriterios(String criterios) {
        this.criterios = criterios;
    }

    public BigInteger getSupEa() {
        return supEa;
    }

    public void setSupEa(BigInteger supEa) {
        this.supEa = supEa;
    }

    public String getProy() {
        return proy;
    }

    public void setProy(String proy) {
        this.proy = proy;
    }

    public String getComp() {
        return comp;
    }

    public void setComp(String comp) {
        this.comp = comp;
    }

    public String getDescrip() {
        return descrip;
    }

    public void setDescrip(String descrip) {
        this.descrip = descrip;
    }

    public BigInteger getAreabuffer() {
        return areabuffer;
    }

    public void setAreabuffer(BigInteger areabuffer) {
        this.areabuffer = areabuffer;
    }

    public BigInteger getArea() {
        return area;
    }

    public void setArea(BigInteger area) {
        this.area = area;
    }

    public Date getFechaHora() {
        return fechaHora;
    }

    public void setFechaHora(Date fechaHora) {
        this.fechaHora = fechaHora;
    }

    @XmlTransient
    public List<SigeiaAnalisis> getSigeiaAnalisisList() {
        return sigeiaAnalisisList;
    }

    public void setSigeiaAnalisisList(List<SigeiaAnalisis> sigeiaAnalisisList) {
        this.sigeiaAnalisisList = sigeiaAnalisisList;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (oeRegionales3PK != null ? oeRegionales3PK.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof OeRegionales3)) {
            return false;
        }
        OeRegionales3 other = (OeRegionales3) object;
        if ((this.oeRegionales3PK == null && other.oeRegionales3PK != null) || (this.oeRegionales3PK != null && !this.oeRegionales3PK.equals(other.oeRegionales3PK))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "mx.gob.semarnat.model.OeRegionales3[ oeRegionales3PK=" + oeRegionales3PK + " ]";
    }
    
}
