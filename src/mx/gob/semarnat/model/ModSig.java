/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package mx.gob.semarnat.model;

import java.io.Serializable;
import java.math.BigInteger;
import java.util.List;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author mauricio
 */
@Entity
@Table(name = "MOD_SIG")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "ModSig.findAll", query = "SELECT m FROM ModSig m"),
    @NamedQuery(name = "ModSig.findByObjectid", query = "SELECT m FROM ModSig m WHERE m.modSigPK.objectid = :objectid"),
    @NamedQuery(name = "ModSig.findByNumFolio", query = "SELECT m FROM ModSig m WHERE m.modSigPK.numFolio = :numFolio"),
    @NamedQuery(name = "ModSig.findByCveProy", query = "SELECT m FROM ModSig m WHERE m.modSigPK.cveProy = :cveProy"),
    @NamedQuery(name = "ModSig.findByCveArea", query = "SELECT m FROM ModSig m WHERE m.modSigPK.cveArea = :cveArea"),
    @NamedQuery(name = "ModSig.findByVersion", query = "SELECT m FROM ModSig m WHERE m.modSigPK.version = :version"),
    @NamedQuery(name = "ModSig.findByDescrip", query = "SELECT m FROM ModSig m WHERE m.descrip = :descrip"),
    @NamedQuery(name = "ModSig.findByComp", query = "SELECT m FROM ModSig m WHERE m.comp = :comp"),
    @NamedQuery(name = "ModSig.findByResolucion", query = "SELECT m FROM ModSig m WHERE m.resolucion = :resolucion"),
    @NamedQuery(name = "ModSig.findByIdRes", query = "SELECT m FROM ModSig m WHERE m.idRes = :idRes"),
    @NamedQuery(name = "ModSig.findByShape", query = "SELECT m FROM ModSig m WHERE m.shape = :shape"),
    @NamedQuery(name = "ModSig.findByProy", query = "SELECT m FROM ModSig m WHERE m.proy = :proy"),
    @NamedQuery(name = "ModSig.findByShapeArea", query = "SELECT m FROM ModSig m WHERE m.shapeArea = :shapeArea"),
    @NamedQuery(name = "ModSig.findByShapeLen", query = "SELECT m FROM ModSig m WHERE m.shapeLen = :shapeLen")})
public class ModSig implements Serializable {
    private static final long serialVersionUID = 1L;
    @EmbeddedId
    protected ModSigPK modSigPK;
    @Column(name = "DESCRIP")
    private String descrip;
    @Column(name = "COMP")
    private String comp;
    @Column(name = "RESOLUCION")
    private String resolucion;
    @Column(name = "ID_RES")
    private Integer idRes;
    @Column(name = "SHAPE")
    private BigInteger shape;
    @Column(name = "PROY")
    private String proy;
    @Column(name = "SHAPE_AREA")
    private BigInteger shapeArea;
    @Column(name = "SHAPE_LEN")
    private BigInteger shapeLen;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "modSig")
    private List<SigeiaAnalisis> sigeiaAnalisisList;
    @JoinColumn(name = "MOD_LIN_ID_RES", referencedColumnName = "ID_RES")
    @ManyToOne
    private ModLin modLinIdRes;
    @JoinColumn(name = "MOD_POL_ID_RES", referencedColumnName = "ID_RES")
    @ManyToOne
    private ModPol modPolIdRes;
    @JoinColumn(name = "MOD_PTS_ID_RES", referencedColumnName = "ID_RES")
    @ManyToOne
    private ModPts modPtsIdRes;

    public ModSig() {
    }

    public ModSig(ModSigPK modSigPK) {
        this.modSigPK = modSigPK;
    }

    public ModSig(BigInteger objectid, String numFolio, String cveProy, String cveArea, short version) {
        this.modSigPK = new ModSigPK(objectid, numFolio, cveProy, cveArea, version);
    }

    public ModSigPK getModSigPK() {
        return modSigPK;
    }

    public void setModSigPK(ModSigPK modSigPK) {
        this.modSigPK = modSigPK;
    }

    public String getDescrip() {
        return descrip;
    }

    public void setDescrip(String descrip) {
        this.descrip = descrip;
    }

    public String getComp() {
        return comp;
    }

    public void setComp(String comp) {
        this.comp = comp;
    }

    public String getResolucion() {
        return resolucion;
    }

    public void setResolucion(String resolucion) {
        this.resolucion = resolucion;
    }

    public Integer getIdRes() {
        return idRes;
    }

    public void setIdRes(Integer idRes) {
        this.idRes = idRes;
    }

    public BigInteger getShape() {
        return shape;
    }

    public void setShape(BigInteger shape) {
        this.shape = shape;
    }

    public String getProy() {
        return proy;
    }

    public void setProy(String proy) {
        this.proy = proy;
    }

    public BigInteger getShapeArea() {
        return shapeArea;
    }

    public void setShapeArea(BigInteger shapeArea) {
        this.shapeArea = shapeArea;
    }

    public BigInteger getShapeLen() {
        return shapeLen;
    }

    public void setShapeLen(BigInteger shapeLen) {
        this.shapeLen = shapeLen;
    }

    @XmlTransient
    public List<SigeiaAnalisis> getSigeiaAnalisisList() {
        return sigeiaAnalisisList;
    }

    public void setSigeiaAnalisisList(List<SigeiaAnalisis> sigeiaAnalisisList) {
        this.sigeiaAnalisisList = sigeiaAnalisisList;
    }

    public ModLin getModLinIdRes() {
        return modLinIdRes;
    }

    public void setModLinIdRes(ModLin modLinIdRes) {
        this.modLinIdRes = modLinIdRes;
    }

    public ModPol getModPolIdRes() {
        return modPolIdRes;
    }

    public void setModPolIdRes(ModPol modPolIdRes) {
        this.modPolIdRes = modPolIdRes;
    }

    public ModPts getModPtsIdRes() {
        return modPtsIdRes;
    }

    public void setModPtsIdRes(ModPts modPtsIdRes) {
        this.modPtsIdRes = modPtsIdRes;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (modSigPK != null ? modSigPK.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof ModSig)) {
            return false;
        }
        ModSig other = (ModSig) object;
        if ((this.modSigPK == null && other.modSigPK != null) || (this.modSigPK != null && !this.modSigPK.equals(other.modSigPK))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "mx.gob.semarnat.model.ModSig[ modSigPK=" + modSigPK + " ]";
    }
    
}
