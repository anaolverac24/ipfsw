/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package mx.gob.semarnat.model;

import java.io.Serializable;
import java.math.BigInteger;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author mauricio
 */
@Entity
@Table(name = "OGMLIN")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Ogmlin.findAll", query = "SELECT o FROM Ogmlin o"),
    @NamedQuery(name = "Ogmlin.findByObjectid1", query = "SELECT o FROM Ogmlin o WHERE o.objectid1 = :objectid1"),
    @NamedQuery(name = "Ogmlin.findByObjectid", query = "SELECT o FROM Ogmlin o WHERE o.objectid = :objectid"),
    @NamedQuery(name = "Ogmlin.findByDescrip", query = "SELECT o FROM Ogmlin o WHERE o.descrip = :descrip"),
    @NamedQuery(name = "Ogmlin.findByComp", query = "SELECT o FROM Ogmlin o WHERE o.comp = :comp"),
    @NamedQuery(name = "Ogmlin.findByProy", query = "SELECT o FROM Ogmlin o WHERE o.proy = :proy"),
    @NamedQuery(name = "Ogmlin.findByShape", query = "SELECT o FROM Ogmlin o WHERE o.shape = :shape"),
    @NamedQuery(name = "Ogmlin.findByShapeLen", query = "SELECT o FROM Ogmlin o WHERE o.shapeLen = :shapeLen")})
public class Ogmlin implements Serializable {
    private static final long serialVersionUID = 1L;
    @Basic(optional = false)
    @Column(name = "OBJECTID_1")
    private BigInteger objectid1;
    @Id
    @Basic(optional = false)
    @Column(name = "OBJECTID")
    private Long objectid;
    @Column(name = "DESCRIP")
    private String descrip;
    @Column(name = "COMP")
    private String comp;
    @Column(name = "PROY")
    private String proy;
    @Column(name = "SHAPE")
    private BigInteger shape;
    @Column(name = "SHAPE_LEN")
    private BigInteger shapeLen;
    @OneToMany(mappedBy = "ogmlinObjectid")
    private List<Ogmsig> ogmsigList;

    public Ogmlin() {
    }

    public Ogmlin(Long objectid) {
        this.objectid = objectid;
    }

    public Ogmlin(Long objectid, BigInteger objectid1) {
        this.objectid = objectid;
        this.objectid1 = objectid1;
    }

    public BigInteger getObjectid1() {
        return objectid1;
    }

    public void setObjectid1(BigInteger objectid1) {
        this.objectid1 = objectid1;
    }

    public Long getObjectid() {
        return objectid;
    }

    public void setObjectid(Long objectid) {
        this.objectid = objectid;
    }

    public String getDescrip() {
        return descrip;
    }

    public void setDescrip(String descrip) {
        this.descrip = descrip;
    }

    public String getComp() {
        return comp;
    }

    public void setComp(String comp) {
        this.comp = comp;
    }

    public String getProy() {
        return proy;
    }

    public void setProy(String proy) {
        this.proy = proy;
    }

    public BigInteger getShape() {
        return shape;
    }

    public void setShape(BigInteger shape) {
        this.shape = shape;
    }

    public BigInteger getShapeLen() {
        return shapeLen;
    }

    public void setShapeLen(BigInteger shapeLen) {
        this.shapeLen = shapeLen;
    }

    @XmlTransient
    public List<Ogmsig> getOgmsigList() {
        return ogmsigList;
    }

    public void setOgmsigList(List<Ogmsig> ogmsigList) {
        this.ogmsigList = ogmsigList;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (objectid != null ? objectid.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Ogmlin)) {
            return false;
        }
        Ogmlin other = (Ogmlin) object;
        if ((this.objectid == null && other.objectid != null) || (this.objectid != null && !this.objectid.equals(other.objectid))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "mx.gob.semarnat.model.Ogmlin[ objectid=" + objectid + " ]";
    }
    
}
