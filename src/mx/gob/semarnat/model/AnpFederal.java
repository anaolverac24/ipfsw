/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package mx.gob.semarnat.model;

import java.io.Serializable;
import java.math.BigInteger;
import java.util.Date;
import java.util.List;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author mauricio
 */
@Entity
@Table(name = "ANP_FEDERAL")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "AnpFederal.findAll", query = "SELECT a FROM AnpFederal a"),
    @NamedQuery(name = "AnpFederal.findByNumFolio", query = "SELECT a FROM AnpFederal a WHERE a.anpFederalPK.numFolio = :numFolio"),
    @NamedQuery(name = "AnpFederal.findByCveProy", query = "SELECT a FROM AnpFederal a WHERE a.anpFederalPK.cveProy = :cveProy"),
    @NamedQuery(name = "AnpFederal.findByCveArea", query = "SELECT a FROM AnpFederal a WHERE a.anpFederalPK.cveArea = :cveArea"),
    @NamedQuery(name = "AnpFederal.findByVersion", query = "SELECT a FROM AnpFederal a WHERE a.anpFederalPK.version = :version"),
    @NamedQuery(name = "AnpFederal.findByNombre", query = "SELECT a FROM AnpFederal a WHERE a.nombre = :nombre"),
    @NamedQuery(name = "AnpFederal.findByCatDecret", query = "SELECT a FROM AnpFederal a WHERE a.catDecret = :catDecret"),
    @NamedQuery(name = "AnpFederal.findByCatManejo", query = "SELECT a FROM AnpFederal a WHERE a.catManejo = :catManejo"),
    @NamedQuery(name = "AnpFederal.findByUltDecret", query = "SELECT a FROM AnpFederal a WHERE a.ultDecret = :ultDecret"),
    @NamedQuery(name = "AnpFederal.findBySupEa", query = "SELECT a FROM AnpFederal a WHERE a.supEa = :supEa"),
    @NamedQuery(name = "AnpFederal.findByProy", query = "SELECT a FROM AnpFederal a WHERE a.proy = :proy"),
    @NamedQuery(name = "AnpFederal.findByComp", query = "SELECT a FROM AnpFederal a WHERE a.comp = :comp"),
    @NamedQuery(name = "AnpFederal.findByDescrip", query = "SELECT a FROM AnpFederal a WHERE a.descrip = :descrip"),
    @NamedQuery(name = "AnpFederal.findByAreabuffer", query = "SELECT a FROM AnpFederal a WHERE a.areabuffer = :areabuffer"),
    @NamedQuery(name = "AnpFederal.findByArea", query = "SELECT a FROM AnpFederal a WHERE a.area = :area"),
    @NamedQuery(name = "AnpFederal.findByFechaHora", query = "SELECT a FROM AnpFederal a WHERE a.fechaHora = :fechaHora")})
public class AnpFederal implements Serializable {
    private static final long serialVersionUID = 1L;
    @EmbeddedId
    protected AnpFederalPK anpFederalPK;
    @Column(name = "NOMBRE")
    private String nombre;
    @Column(name = "CAT_DECRET")
    private String catDecret;
    @Column(name = "CAT_MANEJO")
    private String catManejo;
    @Column(name = "ULT_DECRET")
    @Temporal(TemporalType.TIMESTAMP)
    private Date ultDecret;
    @Column(name = "SUP_EA")
    private BigInteger supEa;
    @Column(name = "PROY")
    private String proy;
    @Column(name = "COMP")
    private String comp;
    @Column(name = "DESCRIP")
    private String descrip;
    @Column(name = "AREABUFFER")
    private BigInteger areabuffer;
    @Column(name = "AREA")
    private BigInteger area;
    @Column(name = "FECHA_HORA")
    @Temporal(TemporalType.TIMESTAMP)
    private Date fechaHora;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "anpFederal")
    private List<SigeiaAnalisis> sigeiaAnalisisList;

    public AnpFederal() {
    }

    public AnpFederal(AnpFederalPK anpFederalPK) {
        this.anpFederalPK = anpFederalPK;
    }

    public AnpFederal(String numFolio, String cveProy, String cveArea, short version) {
        this.anpFederalPK = new AnpFederalPK(numFolio, cveProy, cveArea, version);
    }

    public AnpFederalPK getAnpFederalPK() {
        return anpFederalPK;
    }

    public void setAnpFederalPK(AnpFederalPK anpFederalPK) {
        this.anpFederalPK = anpFederalPK;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getCatDecret() {
        return catDecret;
    }

    public void setCatDecret(String catDecret) {
        this.catDecret = catDecret;
    }

    public String getCatManejo() {
        return catManejo;
    }

    public void setCatManejo(String catManejo) {
        this.catManejo = catManejo;
    }

    public Date getUltDecret() {
        return ultDecret;
    }

    public void setUltDecret(Date ultDecret) {
        this.ultDecret = ultDecret;
    }

    public BigInteger getSupEa() {
        return supEa;
    }

    public void setSupEa(BigInteger supEa) {
        this.supEa = supEa;
    }

    public String getProy() {
        return proy;
    }

    public void setProy(String proy) {
        this.proy = proy;
    }

    public String getComp() {
        return comp;
    }

    public void setComp(String comp) {
        this.comp = comp;
    }

    public String getDescrip() {
        return descrip;
    }

    public void setDescrip(String descrip) {
        this.descrip = descrip;
    }

    public BigInteger getAreabuffer() {
        return areabuffer;
    }

    public void setAreabuffer(BigInteger areabuffer) {
        this.areabuffer = areabuffer;
    }

    public BigInteger getArea() {
        return area;
    }

    public void setArea(BigInteger area) {
        this.area = area;
    }

    public Date getFechaHora() {
        return fechaHora;
    }

    public void setFechaHora(Date fechaHora) {
        this.fechaHora = fechaHora;
    }

    @XmlTransient
    public List<SigeiaAnalisis> getSigeiaAnalisisList() {
        return sigeiaAnalisisList;
    }

    public void setSigeiaAnalisisList(List<SigeiaAnalisis> sigeiaAnalisisList) {
        this.sigeiaAnalisisList = sigeiaAnalisisList;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (anpFederalPK != null ? anpFederalPK.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof AnpFederal)) {
            return false;
        }
        AnpFederal other = (AnpFederal) object;
        if ((this.anpFederalPK == null && other.anpFederalPK != null) || (this.anpFederalPK != null && !this.anpFederalPK.equals(other.anpFederalPK))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "mx.gob.semarnat.model.AnpFederal[ anpFederalPK=" + anpFederalPK + " ]";
    }
    
}
