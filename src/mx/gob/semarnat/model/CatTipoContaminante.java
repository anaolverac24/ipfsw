/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package mx.gob.semarnat.model;

import java.io.Serializable;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author mauricio
 */
@Entity
@Table(name = "CAT_TIPO_CONTAMINANTE")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "CatTipoContaminante.findAll", query = "SELECT c FROM CatTipoContaminante c"),
    @NamedQuery(name = "CatTipoContaminante.findByTipoContaminanteId", query = "SELECT c FROM CatTipoContaminante c WHERE c.tipoContaminanteId = :tipoContaminanteId"),
    @NamedQuery(name = "CatTipoContaminante.findByTipoContaminanteDescripcion", query = "SELECT c FROM CatTipoContaminante c WHERE c.tipoContaminanteDescripcion = :tipoContaminanteDescripcion")})
public class CatTipoContaminante implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @Column(name = "TIPO_CONTAMINANTE_ID")
    private Short tipoContaminanteId;
    @Basic(optional = false)
    @Column(name = "TIPO_CONTAMINANTE_DESCRIPCION")
    private String tipoContaminanteDescripcion;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "tipoContaminanteId")
    private List<CatContaminante> catContaminanteList;

    public CatTipoContaminante() {
    }

    public CatTipoContaminante(Short tipoContaminanteId) {
        this.tipoContaminanteId = tipoContaminanteId;
    }

    public CatTipoContaminante(Short tipoContaminanteId, String tipoContaminanteDescripcion) {
        this.tipoContaminanteId = tipoContaminanteId;
        this.tipoContaminanteDescripcion = tipoContaminanteDescripcion;
    }

    public Short getTipoContaminanteId() {
        return tipoContaminanteId;
    }

    public void setTipoContaminanteId(Short tipoContaminanteId) {
        this.tipoContaminanteId = tipoContaminanteId;
    }

    public String getTipoContaminanteDescripcion() {
        return tipoContaminanteDescripcion;
    }

    public void setTipoContaminanteDescripcion(String tipoContaminanteDescripcion) {
        this.tipoContaminanteDescripcion = tipoContaminanteDescripcion;
    }

    @XmlTransient
    public List<CatContaminante> getCatContaminanteList() {
        return catContaminanteList;
    }

    public void setCatContaminanteList(List<CatContaminante> catContaminanteList) {
        this.catContaminanteList = catContaminanteList;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (tipoContaminanteId != null ? tipoContaminanteId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof CatTipoContaminante)) {
            return false;
        }
        CatTipoContaminante other = (CatTipoContaminante) object;
        if ((this.tipoContaminanteId == null && other.tipoContaminanteId != null) || (this.tipoContaminanteId != null && !this.tipoContaminanteId.equals(other.tipoContaminanteId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return tipoContaminanteId+"";
    }
    
}
