/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package mx.gob.semarnat.model;

import java.io.Serializable;
import java.math.BigInteger;
import java.util.Date;
import java.util.List;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author mauricio
 */
@Entity
@Table(name = "USO_SUELO_VEGET")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "UsoSueloVeget.findAll", query = "SELECT u FROM UsoSueloVeget u"),
    @NamedQuery(name = "UsoSueloVeget.findByNumFolio", query = "SELECT u FROM UsoSueloVeget u WHERE u.usoSueloVegetPK.numFolio = :numFolio"),
    @NamedQuery(name = "UsoSueloVeget.findByCveProy", query = "SELECT u FROM UsoSueloVeget u WHERE u.usoSueloVegetPK.cveProy = :cveProy"),
    @NamedQuery(name = "UsoSueloVeget.findByCveArea", query = "SELECT u FROM UsoSueloVeget u WHERE u.usoSueloVegetPK.cveArea = :cveArea"),
    @NamedQuery(name = "UsoSueloVeget.findByVersion", query = "SELECT u FROM UsoSueloVeget u WHERE u.usoSueloVegetPK.version = :version"),
    @NamedQuery(name = "UsoSueloVeget.findByClave", query = "SELECT u FROM UsoSueloVeget u WHERE u.clave = :clave"),
    @NamedQuery(name = "UsoSueloVeget.findByClavefot", query = "SELECT u FROM UsoSueloVeget u WHERE u.clavefot = :clavefot"),
    @NamedQuery(name = "UsoSueloVeget.findByTipInfo", query = "SELECT u FROM UsoSueloVeget u WHERE u.tipInfo = :tipInfo"),
    @NamedQuery(name = "UsoSueloVeget.findByTipEcov", query = "SELECT u FROM UsoSueloVeget u WHERE u.tipEcov = :tipEcov"),
    @NamedQuery(name = "UsoSueloVeget.findByAgecosis", query = "SELECT u FROM UsoSueloVeget u WHERE u.agecosis = :agecosis"),
    @NamedQuery(name = "UsoSueloVeget.findByTipages", query = "SELECT u FROM UsoSueloVeget u WHERE u.tipages = :tipages"),
    @NamedQuery(name = "UsoSueloVeget.findByTipVeg", query = "SELECT u FROM UsoSueloVeget u WHERE u.tipVeg = :tipVeg"),
    @NamedQuery(name = "UsoSueloVeget.findByDesveg", query = "SELECT u FROM UsoSueloVeget u WHERE u.desveg = :desveg"),
    @NamedQuery(name = "UsoSueloVeget.findByFaseVs", query = "SELECT u FROM UsoSueloVeget u WHERE u.faseVs = :faseVs"),
    @NamedQuery(name = "UsoSueloVeget.findByTipPlan", query = "SELECT u FROM UsoSueloVeget u WHERE u.tipPlan = :tipPlan"),
    @NamedQuery(name = "UsoSueloVeget.findByTipCul1", query = "SELECT u FROM UsoSueloVeget u WHERE u.tipCul1 = :tipCul1"),
    @NamedQuery(name = "UsoSueloVeget.findByTipCul2", query = "SELECT u FROM UsoSueloVeget u WHERE u.tipCul2 = :tipCul2"),
    @NamedQuery(name = "UsoSueloVeget.findByOtros", query = "SELECT u FROM UsoSueloVeget u WHERE u.otros = :otros"),
    @NamedQuery(name = "UsoSueloVeget.findByCUYV", query = "SELECT u FROM UsoSueloVeget u WHERE u.cUYV = :cUYV"),
    @NamedQuery(name = "UsoSueloVeget.findByTipoGen", query = "SELECT u FROM UsoSueloVeget u WHERE u.tipoGen = :tipoGen"),
    @NamedQuery(name = "UsoSueloVeget.findBySupEa", query = "SELECT u FROM UsoSueloVeget u WHERE u.supEa = :supEa"),
    @NamedQuery(name = "UsoSueloVeget.findByProy", query = "SELECT u FROM UsoSueloVeget u WHERE u.proy = :proy"),
    @NamedQuery(name = "UsoSueloVeget.findByComp", query = "SELECT u FROM UsoSueloVeget u WHERE u.comp = :comp"),
    @NamedQuery(name = "UsoSueloVeget.findByDescrip", query = "SELECT u FROM UsoSueloVeget u WHERE u.descrip = :descrip"),
    @NamedQuery(name = "UsoSueloVeget.findByAreabuffer", query = "SELECT u FROM UsoSueloVeget u WHERE u.areabuffer = :areabuffer"),
    @NamedQuery(name = "UsoSueloVeget.findByArea", query = "SELECT u FROM UsoSueloVeget u WHERE u.area = :area"),
    @NamedQuery(name = "UsoSueloVeget.findByFechaHora", query = "SELECT u FROM UsoSueloVeget u WHERE u.fechaHora = :fechaHora")})
public class UsoSueloVeget implements Serializable {
    private static final long serialVersionUID = 1L;
    @EmbeddedId
    protected UsoSueloVegetPK usoSueloVegetPK;
    @Column(name = "CLAVE")
    private String clave;
    @Column(name = "CLAVEFOT")
    private String clavefot;
    @Column(name = "TIP_INFO")
    private String tipInfo;
    @Column(name = "TIP_ECOV")
    private String tipEcov;
    @Column(name = "AGECOSIS")
    private String agecosis;
    @Column(name = "TIPAGES")
    private String tipages;
    @Column(name = "TIP_VEG")
    private String tipVeg;
    @Column(name = "DESVEG")
    private String desveg;
    @Column(name = "FASE_VS")
    private String faseVs;
    @Column(name = "TIP_PLAN")
    private String tipPlan;
    @Column(name = "TIP_CUL1")
    private String tipCul1;
    @Column(name = "TIP_CUL2")
    private String tipCul2;
    @Column(name = "OTROS")
    private String otros;
    @Column(name = "C_U_Y_V")
    private String cUYV;
    @Column(name = "TIPO_GEN")
    private String tipoGen;
    @Column(name = "SUP_EA")
    private BigInteger supEa;
    @Column(name = "PROY")
    private String proy;
    @Column(name = "COMP")
    private String comp;
    @Column(name = "DESCRIP")
    private String descrip;
    @Column(name = "AREABUFFER")
    private BigInteger areabuffer;
    @Column(name = "AREA")
    private BigInteger area;
    @Column(name = "FECHA_HORA")
    @Temporal(TemporalType.TIMESTAMP)
    private Date fechaHora;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "usoSueloVeget")
    private List<SigeiaAnalisis> sigeiaAnalisisList;

    public UsoSueloVeget() {
    }

    public UsoSueloVeget(UsoSueloVegetPK usoSueloVegetPK) {
        this.usoSueloVegetPK = usoSueloVegetPK;
    }

    public UsoSueloVeget(String numFolio, String cveProy, String cveArea, short version) {
        this.usoSueloVegetPK = new UsoSueloVegetPK(numFolio, cveProy, cveArea, version);
    }

    public UsoSueloVegetPK getUsoSueloVegetPK() {
        return usoSueloVegetPK;
    }

    public void setUsoSueloVegetPK(UsoSueloVegetPK usoSueloVegetPK) {
        this.usoSueloVegetPK = usoSueloVegetPK;
    }

    public String getClave() {
        return clave;
    }

    public void setClave(String clave) {
        this.clave = clave;
    }

    public String getClavefot() {
        return clavefot;
    }

    public void setClavefot(String clavefot) {
        this.clavefot = clavefot;
    }

    public String getTipInfo() {
        return tipInfo;
    }

    public void setTipInfo(String tipInfo) {
        this.tipInfo = tipInfo;
    }

    public String getTipEcov() {
        return tipEcov;
    }

    public void setTipEcov(String tipEcov) {
        this.tipEcov = tipEcov;
    }

    public String getAgecosis() {
        return agecosis;
    }

    public void setAgecosis(String agecosis) {
        this.agecosis = agecosis;
    }

    public String getTipages() {
        return tipages;
    }

    public void setTipages(String tipages) {
        this.tipages = tipages;
    }

    public String getTipVeg() {
        return tipVeg;
    }

    public void setTipVeg(String tipVeg) {
        this.tipVeg = tipVeg;
    }

    public String getDesveg() {
        return desveg;
    }

    public void setDesveg(String desveg) {
        this.desveg = desveg;
    }

    public String getFaseVs() {
        return faseVs;
    }

    public void setFaseVs(String faseVs) {
        this.faseVs = faseVs;
    }

    public String getTipPlan() {
        return tipPlan;
    }

    public void setTipPlan(String tipPlan) {
        this.tipPlan = tipPlan;
    }

    public String getTipCul1() {
        return tipCul1;
    }

    public void setTipCul1(String tipCul1) {
        this.tipCul1 = tipCul1;
    }

    public String getTipCul2() {
        return tipCul2;
    }

    public void setTipCul2(String tipCul2) {
        this.tipCul2 = tipCul2;
    }

    public String getOtros() {
        return otros;
    }

    public void setOtros(String otros) {
        this.otros = otros;
    }

    public String getCUYV() {
        return cUYV;
    }

    public void setCUYV(String cUYV) {
        this.cUYV = cUYV;
    }

    public String getTipoGen() {
        return tipoGen;
    }

    public void setTipoGen(String tipoGen) {
        this.tipoGen = tipoGen;
    }

    public BigInteger getSupEa() {
        return supEa;
    }

    public void setSupEa(BigInteger supEa) {
        this.supEa = supEa;
    }

    public String getProy() {
        return proy;
    }

    public void setProy(String proy) {
        this.proy = proy;
    }

    public String getComp() {
        return comp;
    }

    public void setComp(String comp) {
        this.comp = comp;
    }

    public String getDescrip() {
        return descrip;
    }

    public void setDescrip(String descrip) {
        this.descrip = descrip;
    }

    public BigInteger getAreabuffer() {
        return areabuffer;
    }

    public void setAreabuffer(BigInteger areabuffer) {
        this.areabuffer = areabuffer;
    }

    public BigInteger getArea() {
        return area;
    }

    public void setArea(BigInteger area) {
        this.area = area;
    }

    public Date getFechaHora() {
        return fechaHora;
    }

    public void setFechaHora(Date fechaHora) {
        this.fechaHora = fechaHora;
    }

    @XmlTransient
    public List<SigeiaAnalisis> getSigeiaAnalisisList() {
        return sigeiaAnalisisList;
    }

    public void setSigeiaAnalisisList(List<SigeiaAnalisis> sigeiaAnalisisList) {
        this.sigeiaAnalisisList = sigeiaAnalisisList;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (usoSueloVegetPK != null ? usoSueloVegetPK.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof UsoSueloVeget)) {
            return false;
        }
        UsoSueloVeget other = (UsoSueloVeget) object;
        if ((this.usoSueloVegetPK == null && other.usoSueloVegetPK != null) || (this.usoSueloVegetPK != null && !this.usoSueloVegetPK.equals(other.usoSueloVegetPK))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "mx.gob.semarnat.model.UsoSueloVeget[ usoSueloVegetPK=" + usoSueloVegetPK + " ]";
    }
    
}
