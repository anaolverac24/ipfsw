/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package mx.gob.semarnat.model;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Collection;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author Paty
 */
@Entity
@Table(name = "EVALUACION_PROYECTO")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "EvaluacionProyecto.findAll", query = "SELECT e FROM EvaluacionProyecto e"),
    @NamedQuery(name = "EvaluacionProyecto.findByBitacoraProyecto", query = "SELECT e FROM EvaluacionProyecto e WHERE e.bitacoraProyecto = :bitacoraProyecto"),
    @NamedQuery(name = "EvaluacionProyecto.findByEvaSistesisProyecto", query = "SELECT e FROM EvaluacionProyecto e WHERE e.evaSistesisProyecto = :evaSistesisProyecto"),
    @NamedQuery(name = "EvaluacionProyecto.findByEvaValCritPromovente", query = "SELECT e FROM EvaluacionProyecto e WHERE e.evaValCritPromovente = :evaValCritPromovente"),
    @NamedQuery(name = "EvaluacionProyecto.findByEvaValCritEvaluador", query = "SELECT e FROM EvaluacionProyecto e WHERE e.evaValCritEvaluador = :evaValCritEvaluador"),
    @NamedQuery(name = "EvaluacionProyecto.findByEvaMontCritPromovente", query = "SELECT e FROM EvaluacionProyecto e WHERE e.evaMontCritPromovente = :evaMontCritPromovente"),
    @NamedQuery(name = "EvaluacionProyecto.findByEvaMontCritEvaluador", query = "SELECT e FROM EvaluacionProyecto e WHERE e.evaMontCritEvaluador = :evaMontCritEvaluador")})
public class EvaluacionProyecto implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @Column(name = "BITACORA_PROYECTO")
    private String bitacoraProyecto;
    @Column(name = "EVA_SISTESIS_PROYECTO")
    private String evaSistesisProyecto;
    @Column(name = "EVA_VAL_CRIT_PROMOVENTE")
    private Short evaValCritPromovente;
    @Column(name = "EVA_VAL_CRIT_EVALUADOR")
    private Short evaValCritEvaluador;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Column(name = "EVA_MONT_CRIT_PROMOVENTE")
    private BigDecimal evaMontCritPromovente;
    @Column(name = "EVA_MONT_CRIT_EVALUADOR")
    private BigDecimal evaMontCritEvaluador;
    @Column(name = "EVA_LOTE_INFO_ADICIONAL")
    private Long evaLoteInfoAdicional;    
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "evaluacionProyecto")
    private Collection<ComentarioProyecto> comentarioProyectoCollection;

    public EvaluacionProyecto() {
    }

    public EvaluacionProyecto(String bitacoraProyecto) {
        this.bitacoraProyecto = bitacoraProyecto;
    }

    public String getBitacoraProyecto() {
        return bitacoraProyecto;
    }

    public void setBitacoraProyecto(String bitacoraProyecto) {
        this.bitacoraProyecto = bitacoraProyecto;
    }

    public String getEvaSistesisProyecto() {
        return evaSistesisProyecto;
    }

    public void setEvaSistesisProyecto(String evaSistesisProyecto) {
        this.evaSistesisProyecto = evaSistesisProyecto;
    }

    public Short getEvaValCritPromovente() {
        return evaValCritPromovente;
    }

    public void setEvaValCritPromovente(Short evaValCritPromovente) {
        this.evaValCritPromovente = evaValCritPromovente;
    }

    public Short getEvaValCritEvaluador() {
        return evaValCritEvaluador;
    }

    public void setEvaValCritEvaluador(Short evaValCritEvaluador) {
        this.evaValCritEvaluador = evaValCritEvaluador;
    }

    public BigDecimal getEvaMontCritPromovente() {
        return evaMontCritPromovente;
    }

    public void setEvaMontCritPromovente(BigDecimal evaMontCritPromovente) {
        this.evaMontCritPromovente = evaMontCritPromovente;
    }

    public BigDecimal getEvaMontCritEvaluador() {
        return evaMontCritEvaluador;
    }

    public void setEvaMontCritEvaluador(BigDecimal evaMontCritEvaluador) {
        this.evaMontCritEvaluador = evaMontCritEvaluador;
    }
    
    public Long getEvaLoteInfoAdicional() {
        return evaLoteInfoAdicional;
    }

    public void setEvaLoteInfoAdicional(Long evaLoteInfoAdicional) {
        this.evaLoteInfoAdicional = evaLoteInfoAdicional;
    }
    @XmlTransient
    public Collection<ComentarioProyecto> getComentarioProyectoCollection() {
        return comentarioProyectoCollection;
    }

    public void setComentarioProyectoCollection(Collection<ComentarioProyecto> comentarioProyectoCollection) {
        this.comentarioProyectoCollection = comentarioProyectoCollection;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (bitacoraProyecto != null ? bitacoraProyecto.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof EvaluacionProyecto)) {
            return false;
        }
        EvaluacionProyecto other = (EvaluacionProyecto) object;
        if ((this.bitacoraProyecto == null && other.bitacoraProyecto != null) || (this.bitacoraProyecto != null && !this.bitacoraProyecto.equals(other.bitacoraProyecto))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "mx.gob.semarnat.model.EvaluacionProyecto[ bitacoraProyecto=" + bitacoraProyecto + " ]";
    }
    
}
