/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package mx.gob.semarnat.model;

import java.io.Serializable;
import java.util.Collection;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author Rengerden
 */
@Entity
@Table(name = "CAT_POET")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "CatPoet.findAll", query = "SELECT c FROM CatPoet c"),
    @NamedQuery(name = "CatPoet.findByPoetId", query = "SELECT c FROM CatPoet c WHERE c.poetId = :poetId"),
    @NamedQuery(name = "CatPoet.findByPoetDescripcion", query = "SELECT c FROM CatPoet c WHERE c.poetDescripcion = :poetDescripcion")})
public class CatPoet implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @Column(name = "POET_ID")
    private Short poetId;
    @Basic(optional = false)
    @Column(name = "POET_DESCRIPCION")
    private String poetDescripcion;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "catPoet")
    private Collection<PoetProyecto> poetProyectoCollection;

    public CatPoet() {
    }

    public CatPoet(Short poetId) {
        this.poetId = poetId;
    }

    public CatPoet(Short poetId, String poetDescripcion) {
        this.poetId = poetId;
        this.poetDescripcion = poetDescripcion;
    }

    public Short getPoetId() {
        return poetId;
    }

    public void setPoetId(Short poetId) {
        this.poetId = poetId;
    }

    public String getPoetDescripcion() {
        return poetDescripcion;
    }

    public void setPoetDescripcion(String poetDescripcion) {
        this.poetDescripcion = poetDescripcion;
    }

    @XmlTransient
    public Collection<PoetProyecto> getPoetProyectoCollection() {
        return poetProyectoCollection;
    }

    public void setPoetProyectoCollection(Collection<PoetProyecto> poetProyectoCollection) {
        this.poetProyectoCollection = poetProyectoCollection;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (poetId != null ? poetId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof CatPoet)) {
            return false;
        }
        CatPoet other = (CatPoet) object;
        if ((this.poetId == null && other.poetId != null) || (this.poetId != null && !this.poetId.equals(other.poetId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "mx.gob.semarnat.model.CatPoet[ poetId=" + poetId + " ]";
    }
    
}
