/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package mx.gob.semarnat.model;

import java.io.Serializable;
import java.math.BigInteger;
import java.util.Date;
import java.util.List;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author mauricio
 */
@Entity
@Table(name = "MANGLARES")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Manglares.findAll", query = "SELECT m FROM Manglares m"),
    @NamedQuery(name = "Manglares.findByNumFolio", query = "SELECT m FROM Manglares m WHERE m.manglaresPK.numFolio = :numFolio"),
    @NamedQuery(name = "Manglares.findByCveProy", query = "SELECT m FROM Manglares m WHERE m.manglaresPK.cveProy = :cveProy"),
    @NamedQuery(name = "Manglares.findByCveArea", query = "SELECT m FROM Manglares m WHERE m.manglaresPK.cveArea = :cveArea"),
    @NamedQuery(name = "Manglares.findByVersion", query = "SELECT m FROM Manglares m WHERE m.manglaresPK.version = :version"),
    @NamedQuery(name = "Manglares.findByClase", query = "SELECT m FROM Manglares m WHERE m.clase = :clase"),
    @NamedQuery(name = "Manglares.findBySupEa", query = "SELECT m FROM Manglares m WHERE m.supEa = :supEa"),
    @NamedQuery(name = "Manglares.findByProy", query = "SELECT m FROM Manglares m WHERE m.proy = :proy"),
    @NamedQuery(name = "Manglares.findByComp", query = "SELECT m FROM Manglares m WHERE m.comp = :comp"),
    @NamedQuery(name = "Manglares.findByDescrip", query = "SELECT m FROM Manglares m WHERE m.descrip = :descrip"),
    @NamedQuery(name = "Manglares.findByAreabuffer", query = "SELECT m FROM Manglares m WHERE m.areabuffer = :areabuffer"),
    @NamedQuery(name = "Manglares.findByArea", query = "SELECT m FROM Manglares m WHERE m.area = :area"),
    @NamedQuery(name = "Manglares.findByFechaHora", query = "SELECT m FROM Manglares m WHERE m.fechaHora = :fechaHora")})
public class Manglares implements Serializable {
    private static final long serialVersionUID = 1L;
    @EmbeddedId
    protected ManglaresPK manglaresPK;
    @Column(name = "CLASE")
    private String clase;
    @Column(name = "SUP_EA")
    private BigInteger supEa;
    @Column(name = "PROY")
    private String proy;
    @Column(name = "COMP")
    private String comp;
    @Column(name = "DESCRIP")
    private String descrip;
    @Column(name = "AREABUFFER")
    private BigInteger areabuffer;
    @Column(name = "AREA")
    private BigInteger area;
    @Column(name = "FECHA_HORA")
    @Temporal(TemporalType.TIMESTAMP)
    private Date fechaHora;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "manglares")
    private List<SigeiaAnalisis> sigeiaAnalisisList;

    public Manglares() {
    }

    public Manglares(ManglaresPK manglaresPK) {
        this.manglaresPK = manglaresPK;
    }

    public Manglares(String numFolio, String cveProy, String cveArea, short version) {
        this.manglaresPK = new ManglaresPK(numFolio, cveProy, cveArea, version);
    }

    public ManglaresPK getManglaresPK() {
        return manglaresPK;
    }

    public void setManglaresPK(ManglaresPK manglaresPK) {
        this.manglaresPK = manglaresPK;
    }

    public String getClase() {
        return clase;
    }

    public void setClase(String clase) {
        this.clase = clase;
    }

    public BigInteger getSupEa() {
        return supEa;
    }

    public void setSupEa(BigInteger supEa) {
        this.supEa = supEa;
    }

    public String getProy() {
        return proy;
    }

    public void setProy(String proy) {
        this.proy = proy;
    }

    public String getComp() {
        return comp;
    }

    public void setComp(String comp) {
        this.comp = comp;
    }

    public String getDescrip() {
        return descrip;
    }

    public void setDescrip(String descrip) {
        this.descrip = descrip;
    }

    public BigInteger getAreabuffer() {
        return areabuffer;
    }

    public void setAreabuffer(BigInteger areabuffer) {
        this.areabuffer = areabuffer;
    }

    public BigInteger getArea() {
        return area;
    }

    public void setArea(BigInteger area) {
        this.area = area;
    }

    public Date getFechaHora() {
        return fechaHora;
    }

    public void setFechaHora(Date fechaHora) {
        this.fechaHora = fechaHora;
    }

    @XmlTransient
    public List<SigeiaAnalisis> getSigeiaAnalisisList() {
        return sigeiaAnalisisList;
    }

    public void setSigeiaAnalisisList(List<SigeiaAnalisis> sigeiaAnalisisList) {
        this.sigeiaAnalisisList = sigeiaAnalisisList;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (manglaresPK != null ? manglaresPK.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Manglares)) {
            return false;
        }
        Manglares other = (Manglares) object;
        if ((this.manglaresPK == null && other.manglaresPK != null) || (this.manglaresPK != null && !this.manglaresPK.equals(other.manglaresPK))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "mx.gob.semarnat.model.Manglares[ manglaresPK=" + manglaresPK + " ]";
    }
    
}
