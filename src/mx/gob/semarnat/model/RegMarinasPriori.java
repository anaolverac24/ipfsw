/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package mx.gob.semarnat.model;

import java.io.Serializable;
import java.math.BigInteger;
import java.util.Date;
import java.util.List;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author mauricio
 */
@Entity
@Table(name = "REG_MARINAS_PRIORI")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "RegMarinasPriori.findAll", query = "SELECT r FROM RegMarinasPriori r"),
    @NamedQuery(name = "RegMarinasPriori.findByNumFolio", query = "SELECT r FROM RegMarinasPriori r WHERE r.regMarinasPrioriPK.numFolio = :numFolio"),
    @NamedQuery(name = "RegMarinasPriori.findByCveProy", query = "SELECT r FROM RegMarinasPriori r WHERE r.regMarinasPrioriPK.cveProy = :cveProy"),
    @NamedQuery(name = "RegMarinasPriori.findByCveArea", query = "SELECT r FROM RegMarinasPriori r WHERE r.regMarinasPrioriPK.cveArea = :cveArea"),
    @NamedQuery(name = "RegMarinasPriori.findByVersion", query = "SELECT r FROM RegMarinasPriori r WHERE r.regMarinasPrioriPK.version = :version"),
    @NamedQuery(name = "RegMarinasPriori.findByRegionId", query = "SELECT r FROM RegMarinasPriori r WHERE r.regionId = :regionId"),
    @NamedQuery(name = "RegMarinasPriori.findByRegion", query = "SELECT r FROM RegMarinasPriori r WHERE r.region = :region"),
    @NamedQuery(name = "RegMarinasPriori.findByProvincia", query = "SELECT r FROM RegMarinasPriori r WHERE r.provincia = :provincia"),
    @NamedQuery(name = "RegMarinasPriori.findByBiodiv", query = "SELECT r FROM RegMarinasPriori r WHERE r.biodiv = :biodiv"),
    @NamedQuery(name = "RegMarinasPriori.findByAmenaza", query = "SELECT r FROM RegMarinasPriori r WHERE r.amenaza = :amenaza"),
    @NamedQuery(name = "RegMarinasPriori.findByUso", query = "SELECT r FROM RegMarinasPriori r WHERE r.uso = :uso"),
    @NamedQuery(name = "RegMarinasPriori.findBySupEa", query = "SELECT r FROM RegMarinasPriori r WHERE r.supEa = :supEa"),
    @NamedQuery(name = "RegMarinasPriori.findByProy", query = "SELECT r FROM RegMarinasPriori r WHERE r.proy = :proy"),
    @NamedQuery(name = "RegMarinasPriori.findByComp", query = "SELECT r FROM RegMarinasPriori r WHERE r.comp = :comp"),
    @NamedQuery(name = "RegMarinasPriori.findByDescrip", query = "SELECT r FROM RegMarinasPriori r WHERE r.descrip = :descrip"),
    @NamedQuery(name = "RegMarinasPriori.findByAreabuffer", query = "SELECT r FROM RegMarinasPriori r WHERE r.areabuffer = :areabuffer"),
    @NamedQuery(name = "RegMarinasPriori.findByArea", query = "SELECT r FROM RegMarinasPriori r WHERE r.area = :area"),
    @NamedQuery(name = "RegMarinasPriori.findByFechaHora", query = "SELECT r FROM RegMarinasPriori r WHERE r.fechaHora = :fechaHora")})
public class RegMarinasPriori implements Serializable {
    private static final long serialVersionUID = 1L;
    @EmbeddedId
    protected RegMarinasPrioriPK regMarinasPrioriPK;
    @Column(name = "REGION_ID")
    private String regionId;
    @Column(name = "REGION")
    private String region;
    @Column(name = "PROVINCIA")
    private String provincia;
    @Column(name = "BIODIV")
    private String biodiv;
    @Column(name = "AMENAZA")
    private String amenaza;
    @Column(name = "USO")
    private String uso;
    @Column(name = "SUP_EA")
    private BigInteger supEa;
    @Column(name = "PROY")
    private String proy;
    @Column(name = "COMP")
    private String comp;
    @Column(name = "DESCRIP")
    private String descrip;
    @Column(name = "AREABUFFER")
    private BigInteger areabuffer;
    @Column(name = "AREA")
    private BigInteger area;
    @Column(name = "FECHA_HORA")
    @Temporal(TemporalType.TIMESTAMP)
    private Date fechaHora;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "regMarinasPriori")
    private List<SigeiaAnalisis> sigeiaAnalisisList;

    public RegMarinasPriori() {
    }

    public RegMarinasPriori(RegMarinasPrioriPK regMarinasPrioriPK) {
        this.regMarinasPrioriPK = regMarinasPrioriPK;
    }

    public RegMarinasPriori(String numFolio, String cveProy, String cveArea, short version) {
        this.regMarinasPrioriPK = new RegMarinasPrioriPK(numFolio, cveProy, cveArea, version);
    }

    public RegMarinasPrioriPK getRegMarinasPrioriPK() {
        return regMarinasPrioriPK;
    }

    public void setRegMarinasPrioriPK(RegMarinasPrioriPK regMarinasPrioriPK) {
        this.regMarinasPrioriPK = regMarinasPrioriPK;
    }

    public String getRegionId() {
        return regionId;
    }

    public void setRegionId(String regionId) {
        this.regionId = regionId;
    }

    public String getRegion() {
        return region;
    }

    public void setRegion(String region) {
        this.region = region;
    }

    public String getProvincia() {
        return provincia;
    }

    public void setProvincia(String provincia) {
        this.provincia = provincia;
    }

    public String getBiodiv() {
        return biodiv;
    }

    public void setBiodiv(String biodiv) {
        this.biodiv = biodiv;
    }

    public String getAmenaza() {
        return amenaza;
    }

    public void setAmenaza(String amenaza) {
        this.amenaza = amenaza;
    }

    public String getUso() {
        return uso;
    }

    public void setUso(String uso) {
        this.uso = uso;
    }

    public BigInteger getSupEa() {
        return supEa;
    }

    public void setSupEa(BigInteger supEa) {
        this.supEa = supEa;
    }

    public String getProy() {
        return proy;
    }

    public void setProy(String proy) {
        this.proy = proy;
    }

    public String getComp() {
        return comp;
    }

    public void setComp(String comp) {
        this.comp = comp;
    }

    public String getDescrip() {
        return descrip;
    }

    public void setDescrip(String descrip) {
        this.descrip = descrip;
    }

    public BigInteger getAreabuffer() {
        return areabuffer;
    }

    public void setAreabuffer(BigInteger areabuffer) {
        this.areabuffer = areabuffer;
    }

    public BigInteger getArea() {
        return area;
    }

    public void setArea(BigInteger area) {
        this.area = area;
    }

    public Date getFechaHora() {
        return fechaHora;
    }

    public void setFechaHora(Date fechaHora) {
        this.fechaHora = fechaHora;
    }

    @XmlTransient
    public List<SigeiaAnalisis> getSigeiaAnalisisList() {
        return sigeiaAnalisisList;
    }

    public void setSigeiaAnalisisList(List<SigeiaAnalisis> sigeiaAnalisisList) {
        this.sigeiaAnalisisList = sigeiaAnalisisList;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (regMarinasPrioriPK != null ? regMarinasPrioriPK.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof RegMarinasPriori)) {
            return false;
        }
        RegMarinasPriori other = (RegMarinasPriori) object;
        if ((this.regMarinasPrioriPK == null && other.regMarinasPrioriPK != null) || (this.regMarinasPrioriPK != null && !this.regMarinasPrioriPK.equals(other.regMarinasPrioriPK))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "mx.gob.semarnat.model.RegMarinasPriori[ regMarinasPrioriPK=" + regMarinasPrioriPK + " ]";
    }
    
}
