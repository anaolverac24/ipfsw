/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package mx.gob.semarnat.model;

import java.io.Serializable;
import java.math.BigDecimal;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.JoinColumns;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author mauricio
 */
@Entity
@Table(name = "CONTAMINANTE_PROYECTO")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "ContaminanteProyecto.findAll", query = "SELECT c FROM ContaminanteProyecto c"),
    @NamedQuery(name = "ContaminanteProyecto.findByFolioProyecto", query = "SELECT c FROM ContaminanteProyecto c WHERE c.contaminanteProyectoPK.folioProyecto = :folioProyecto"),
    @NamedQuery(name = "ContaminanteProyecto.findBySerialProyecto", query = "SELECT c FROM ContaminanteProyecto c WHERE c.contaminanteProyectoPK.serialProyecto = :serialProyecto"),
    @NamedQuery(name = "ContaminanteProyecto.findByEtapaId", query = "SELECT c FROM ContaminanteProyecto c WHERE c.contaminanteProyectoPK.etapaId = :etapaId"),
    @NamedQuery(name = "ContaminanteProyecto.findByContaminanteId", query = "SELECT c FROM ContaminanteProyecto c WHERE c.contaminanteProyectoPK.contaminanteId = :contaminanteId"),
    @NamedQuery(name = "ContaminanteProyecto.findByContaminanteCantidad", query = "SELECT c FROM ContaminanteProyecto c WHERE c.contaminanteCantidad = :contaminanteCantidad"),
    @NamedQuery(name = "ContaminanteProyecto.findByCtunClave", query = "SELECT c FROM ContaminanteProyecto c WHERE c.ctunClave = :ctunClave"),
    @NamedQuery(name = "ContaminanteProyecto.findByContaminanteMedidaControl", query = "SELECT c FROM ContaminanteProyecto c WHERE c.contaminanteMedidaControl = :contaminanteMedidaControl"),
    @NamedQuery(name = "ContaminanteProyecto.findByFuenteEmisora", query = "SELECT c FROM ContaminanteProyecto c WHERE c.fuenteEmisora = :fuenteEmisora"),
    @NamedQuery(name = "ContaminanteProyecto.findByObservaciones", query = "SELECT c FROM ContaminanteProyecto c WHERE c.observaciones = :observaciones")})
public class ContaminanteProyecto implements Serializable {
    @JoinColumn(name = "ETAPA_ID", referencedColumnName = "ETAPA_ID", insertable = false, updatable = false)
    @ManyToOne(optional = false)
    private CatEtapa catEtapa;
    private static final long serialVersionUID = 1L;
    @EmbeddedId
    protected ContaminanteProyectoPK contaminanteProyectoPK;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Column(name = "CONTAMINANTE_CANTIDAD")
    private BigDecimal contaminanteCantidad;
    @Column(name = "CTUN_CLAVE")
    private Short ctunClave;
    @Column(name = "CONTAMINANTE_MEDIDA_CONTROL")
    private String contaminanteMedidaControl;
    @Column(name = "FUENTE_EMISORA")
    private String fuenteEmisora;
    @Column(name = "OBSERVACIONES")
    private String observaciones;
    @JoinColumn(name = "CONTAMINANTE_ID", referencedColumnName = "CONTAMINANTE_ID", insertable = false, updatable = false)
    @ManyToOne(optional = false)
    private CatContaminante catContaminante;
    @JoinColumns({
        @JoinColumn(name = "SERIAL_PROYECTO", referencedColumnName = "SERIAL_PROYECTO", insertable = false, updatable = false),
        @JoinColumn(name = "FOLIO_PROYECTO", referencedColumnName = "FOLIO_PROYECTO", insertable = false, updatable = false),
        @JoinColumn(name = "ETAPA_ID", referencedColumnName = "ETAPA_ID", insertable = false, updatable = false)})
    @ManyToOne(optional = false)
    private EtapaProyecto etapaProyecto;

    public ContaminanteProyecto() {
    }

    public ContaminanteProyecto(ContaminanteProyectoPK contaminanteProyectoPK) {
        this.contaminanteProyectoPK = contaminanteProyectoPK;
    }

    public ContaminanteProyecto(String folioProyecto, short serialProyecto, short etapaId, short contaminanteId) {
        this.contaminanteProyectoPK = new ContaminanteProyectoPK(folioProyecto, serialProyecto, etapaId, contaminanteId);
    }

    public ContaminanteProyectoPK getContaminanteProyectoPK() {
        return contaminanteProyectoPK;
    }

    public void setContaminanteProyectoPK(ContaminanteProyectoPK contaminanteProyectoPK) {
        this.contaminanteProyectoPK = contaminanteProyectoPK;
    }

    public BigDecimal getContaminanteCantidad() {
        return contaminanteCantidad;
    }

    public void setContaminanteCantidad(BigDecimal contaminanteCantidad) {
        this.contaminanteCantidad = contaminanteCantidad;
    }

    public Short getCtunClave() {
        return ctunClave;
    }

    public void setCtunClave(Short ctunClave) {
        this.ctunClave = ctunClave;
    }

    public String getContaminanteMedidaControl() {
        return contaminanteMedidaControl;
    }

    public void setContaminanteMedidaControl(String contaminanteMedidaControl) {
        this.contaminanteMedidaControl = contaminanteMedidaControl;
    }

    public String getFuenteEmisora() {
        return fuenteEmisora;
    }

    public void setFuenteEmisora(String fuenteEmisora) {
        this.fuenteEmisora = fuenteEmisora;
    }

    public String getObservaciones() {
        return observaciones;
    }

    public void setObservaciones(String observaciones) {
        this.observaciones = observaciones;
    }

    public CatContaminante getCatContaminante() {
        return catContaminante;
    }

    public void setCatContaminante(CatContaminante catContaminante) {
        this.catContaminante = catContaminante;
    }

    public EtapaProyecto getEtapaProyecto() {
        return etapaProyecto;
    }

    public void setEtapaProyecto(EtapaProyecto etapaProyecto) {
        this.etapaProyecto = etapaProyecto;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (contaminanteProyectoPK != null ? contaminanteProyectoPK.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof ContaminanteProyecto)) {
            return false;
        }
        ContaminanteProyecto other = (ContaminanteProyecto) object;
        if ((this.contaminanteProyectoPK == null && other.contaminanteProyectoPK != null) || (this.contaminanteProyectoPK != null && !this.contaminanteProyectoPK.equals(other.contaminanteProyectoPK))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "mx.gob.semarnat.model.ContaminanteProyecto[ contaminanteProyectoPK=" + contaminanteProyectoPK + " ]";
    }

    public CatEtapa getCatEtapa() {
        return catEtapa;
    }

    public void setCatEtapa(CatEtapa catEtapa) {
        this.catEtapa = catEtapa;
    }
    
}
