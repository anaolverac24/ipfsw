/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package mx.gob.semarnat.model;

import java.io.Serializable;
import java.math.BigInteger;
import java.util.Date;
import java.util.List;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author mauricio
 */
@Entity
@Table(name = "ORD_ECOLOG_LOC")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "OrdEcologLoc.findAll", query = "SELECT o FROM OrdEcologLoc o"),
    @NamedQuery(name = "OrdEcologLoc.findByNumFolio", query = "SELECT o FROM OrdEcologLoc o WHERE o.ordEcologLocPK.numFolio = :numFolio"),
    @NamedQuery(name = "OrdEcologLoc.findByCveProy", query = "SELECT o FROM OrdEcologLoc o WHERE o.ordEcologLocPK.cveProy = :cveProy"),
    @NamedQuery(name = "OrdEcologLoc.findByCveArea", query = "SELECT o FROM OrdEcologLoc o WHERE o.ordEcologLocPK.cveArea = :cveArea"),
    @NamedQuery(name = "OrdEcologLoc.findByVersion", query = "SELECT o FROM OrdEcologLoc o WHERE o.ordEcologLocPK.version = :version"),
    @NamedQuery(name = "OrdEcologLoc.findByNomOe", query = "SELECT o FROM OrdEcologLoc o WHERE o.nomOe = :nomOe"),
    @NamedQuery(name = "OrdEcologLoc.findByTipoOe", query = "SELECT o FROM OrdEcologLoc o WHERE o.tipoOe = :tipoOe"),
    @NamedQuery(name = "OrdEcologLoc.findByUga", query = "SELECT o FROM OrdEcologLoc o WHERE o.uga = :uga"),
    @NamedQuery(name = "OrdEcologLoc.findByUgaComp", query = "SELECT o FROM OrdEcologLoc o WHERE o.ugaComp = :ugaComp"),
    @NamedQuery(name = "OrdEcologLoc.findByPolitica", query = "SELECT o FROM OrdEcologLoc o WHERE o.politica = :politica"),
    @NamedQuery(name = "OrdEcologLoc.findByPolMapear", query = "SELECT o FROM OrdEcologLoc o WHERE o.polMapear = :polMapear"),
    @NamedQuery(name = "OrdEcologLoc.findByUsoPred", query = "SELECT o FROM OrdEcologLoc o WHERE o.usoPred = :usoPred"),
    @NamedQuery(name = "OrdEcologLoc.findByCriterios", query = "SELECT o FROM OrdEcologLoc o WHERE o.criterios = :criterios"),
    @NamedQuery(name = "OrdEcologLoc.findBySupEa", query = "SELECT o FROM OrdEcologLoc o WHERE o.supEa = :supEa"),
    @NamedQuery(name = "OrdEcologLoc.findByProy", query = "SELECT o FROM OrdEcologLoc o WHERE o.proy = :proy"),
    @NamedQuery(name = "OrdEcologLoc.findByComp", query = "SELECT o FROM OrdEcologLoc o WHERE o.comp = :comp"),
    @NamedQuery(name = "OrdEcologLoc.findByDescrip", query = "SELECT o FROM OrdEcologLoc o WHERE o.descrip = :descrip"),
    @NamedQuery(name = "OrdEcologLoc.findByAreabuffer", query = "SELECT o FROM OrdEcologLoc o WHERE o.areabuffer = :areabuffer"),
    @NamedQuery(name = "OrdEcologLoc.findByArea", query = "SELECT o FROM OrdEcologLoc o WHERE o.area = :area"),
    @NamedQuery(name = "OrdEcologLoc.findByFechaHora", query = "SELECT o FROM OrdEcologLoc o WHERE o.fechaHora = :fechaHora")})
public class OrdEcologLoc implements Serializable {
    private static final long serialVersionUID = 1L;
    @EmbeddedId
    protected OrdEcologLocPK ordEcologLocPK;
    @Column(name = "NOM_OE")
    private String nomOe;
    @Column(name = "TIPO_OE")
    private String tipoOe;
    @Column(name = "UGA")
    private String uga;
    @Column(name = "UGA_COMP")
    private String ugaComp;
    @Column(name = "POLITICA")
    private String politica;
    @Column(name = "POL_MAPEAR")
    private String polMapear;
    @Column(name = "USO_PRED")
    private String usoPred;
    @Column(name = "CRITERIOS")
    private String criterios;
    @Column(name = "SUP_EA")
    private BigInteger supEa;
    @Column(name = "PROY")
    private String proy;
    @Column(name = "COMP")
    private String comp;
    @Column(name = "DESCRIP")
    private String descrip;
    @Column(name = "AREABUFFER")
    private BigInteger areabuffer;
    @Column(name = "AREA")
    private BigInteger area;
    @Column(name = "FECHA_HORA")
    @Temporal(TemporalType.TIMESTAMP)
    private Date fechaHora;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "ordEcologLoc")
    private List<SigeiaAnalisis> sigeiaAnalisisList;

    public OrdEcologLoc() {
    }

    public OrdEcologLoc(OrdEcologLocPK ordEcologLocPK) {
        this.ordEcologLocPK = ordEcologLocPK;
    }

    public OrdEcologLoc(String numFolio, String cveProy, String cveArea, short version) {
        this.ordEcologLocPK = new OrdEcologLocPK(numFolio, cveProy, cveArea, version);
    }

    public OrdEcologLocPK getOrdEcologLocPK() {
        return ordEcologLocPK;
    }

    public void setOrdEcologLocPK(OrdEcologLocPK ordEcologLocPK) {
        this.ordEcologLocPK = ordEcologLocPK;
    }

    public String getNomOe() {
        return nomOe;
    }

    public void setNomOe(String nomOe) {
        this.nomOe = nomOe;
    }

    public String getTipoOe() {
        return tipoOe;
    }

    public void setTipoOe(String tipoOe) {
        this.tipoOe = tipoOe;
    }

    public String getUga() {
        return uga;
    }

    public void setUga(String uga) {
        this.uga = uga;
    }

    public String getUgaComp() {
        return ugaComp;
    }

    public void setUgaComp(String ugaComp) {
        this.ugaComp = ugaComp;
    }

    public String getPolitica() {
        return politica;
    }

    public void setPolitica(String politica) {
        this.politica = politica;
    }

    public String getPolMapear() {
        return polMapear;
    }

    public void setPolMapear(String polMapear) {
        this.polMapear = polMapear;
    }

    public String getUsoPred() {
        return usoPred;
    }

    public void setUsoPred(String usoPred) {
        this.usoPred = usoPred;
    }

    public String getCriterios() {
        return criterios;
    }

    public void setCriterios(String criterios) {
        this.criterios = criterios;
    }

    public BigInteger getSupEa() {
        return supEa;
    }

    public void setSupEa(BigInteger supEa) {
        this.supEa = supEa;
    }

    public String getProy() {
        return proy;
    }

    public void setProy(String proy) {
        this.proy = proy;
    }

    public String getComp() {
        return comp;
    }

    public void setComp(String comp) {
        this.comp = comp;
    }

    public String getDescrip() {
        return descrip;
    }

    public void setDescrip(String descrip) {
        this.descrip = descrip;
    }

    public BigInteger getAreabuffer() {
        return areabuffer;
    }

    public void setAreabuffer(BigInteger areabuffer) {
        this.areabuffer = areabuffer;
    }

    public BigInteger getArea() {
        return area;
    }

    public void setArea(BigInteger area) {
        this.area = area;
    }

    public Date getFechaHora() {
        return fechaHora;
    }

    public void setFechaHora(Date fechaHora) {
        this.fechaHora = fechaHora;
    }

    @XmlTransient
    public List<SigeiaAnalisis> getSigeiaAnalisisList() {
        return sigeiaAnalisisList;
    }

    public void setSigeiaAnalisisList(List<SigeiaAnalisis> sigeiaAnalisisList) {
        this.sigeiaAnalisisList = sigeiaAnalisisList;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (ordEcologLocPK != null ? ordEcologLocPK.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof OrdEcologLoc)) {
            return false;
        }
        OrdEcologLoc other = (OrdEcologLoc) object;
        if ((this.ordEcologLocPK == null && other.ordEcologLocPK != null) || (this.ordEcologLocPK != null && !this.ordEcologLocPK.equals(other.ordEcologLocPK))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "mx.gob.semarnat.model.OrdEcologLoc[ ordEcologLocPK=" + ordEcologLocPK + " ]";
    }
    
}
