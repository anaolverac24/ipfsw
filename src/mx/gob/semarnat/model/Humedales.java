/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package mx.gob.semarnat.model;

import java.io.Serializable;
import java.math.BigInteger;
import java.util.Date;
import java.util.List;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author mauricio
 */
@Entity
@Table(name = "HUMEDALES")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Humedales.findAll", query = "SELECT h FROM Humedales h"),
    @NamedQuery(name = "Humedales.findByNumFolio", query = "SELECT h FROM Humedales h WHERE h.humedalesPK.numFolio = :numFolio"),
    @NamedQuery(name = "Humedales.findByCveProy", query = "SELECT h FROM Humedales h WHERE h.humedalesPK.cveProy = :cveProy"),
    @NamedQuery(name = "Humedales.findByCveArea", query = "SELECT h FROM Humedales h WHERE h.humedalesPK.cveArea = :cveArea"),
    @NamedQuery(name = "Humedales.findByVersion", query = "SELECT h FROM Humedales h WHERE h.humedalesPK.version = :version"),
    @NamedQuery(name = "Humedales.findByClase", query = "SELECT h FROM Humedales h WHERE h.clase = :clase"),
    @NamedQuery(name = "Humedales.findByCombEleme", query = "SELECT h FROM Humedales h WHERE h.combEleme = :combEleme"),
    @NamedQuery(name = "Humedales.findByClaseDes", query = "SELECT h FROM Humedales h WHERE h.claseDes = :claseDes"),
    @NamedQuery(name = "Humedales.findByIdSigeia", query = "SELECT h FROM Humedales h WHERE h.idSigeia = :idSigeia"),
    @NamedQuery(name = "Humedales.findBySupEa", query = "SELECT h FROM Humedales h WHERE h.supEa = :supEa"),
    @NamedQuery(name = "Humedales.findByProy", query = "SELECT h FROM Humedales h WHERE h.proy = :proy"),
    @NamedQuery(name = "Humedales.findByComp", query = "SELECT h FROM Humedales h WHERE h.comp = :comp"),
    @NamedQuery(name = "Humedales.findByDescrip", query = "SELECT h FROM Humedales h WHERE h.descrip = :descrip"),
    @NamedQuery(name = "Humedales.findByAreabuffer", query = "SELECT h FROM Humedales h WHERE h.areabuffer = :areabuffer"),
    @NamedQuery(name = "Humedales.findByArea", query = "SELECT h FROM Humedales h WHERE h.area = :area"),
    @NamedQuery(name = "Humedales.findByFechaHora", query = "SELECT h FROM Humedales h WHERE h.fechaHora = :fechaHora")})
public class Humedales implements Serializable {
    private static final long serialVersionUID = 1L;
    @EmbeddedId
    protected HumedalesPK humedalesPK;
    @Column(name = "CLASE")
    private String clase;
    @Column(name = "COMB_ELEME")
    private String combEleme;
    @Column(name = "CLASE_DES")
    private String claseDes;
    @Column(name = "ID_SIGEIA")
    private BigInteger idSigeia;
    @Column(name = "SUP_EA")
    private BigInteger supEa;
    @Column(name = "PROY")
    private String proy;
    @Column(name = "COMP")
    private String comp;
    @Column(name = "DESCRIP")
    private String descrip;
    @Column(name = "AREABUFFER")
    private BigInteger areabuffer;
    @Column(name = "AREA")
    private BigInteger area;
    @Column(name = "FECHA_HORA")
    @Temporal(TemporalType.TIMESTAMP)
    private Date fechaHora;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "humedales")
    private List<SigeiaAnalisis> sigeiaAnalisisList;

    public Humedales() {
    }

    public Humedales(HumedalesPK humedalesPK) {
        this.humedalesPK = humedalesPK;
    }

    public Humedales(String numFolio, String cveProy, String cveArea, short version) {
        this.humedalesPK = new HumedalesPK(numFolio, cveProy, cveArea, version);
    }

    public HumedalesPK getHumedalesPK() {
        return humedalesPK;
    }

    public void setHumedalesPK(HumedalesPK humedalesPK) {
        this.humedalesPK = humedalesPK;
    }

    public String getClase() {
        return clase;
    }

    public void setClase(String clase) {
        this.clase = clase;
    }

    public String getCombEleme() {
        return combEleme;
    }

    public void setCombEleme(String combEleme) {
        this.combEleme = combEleme;
    }

    public String getClaseDes() {
        return claseDes;
    }

    public void setClaseDes(String claseDes) {
        this.claseDes = claseDes;
    }

    public BigInteger getIdSigeia() {
        return idSigeia;
    }

    public void setIdSigeia(BigInteger idSigeia) {
        this.idSigeia = idSigeia;
    }

    public BigInteger getSupEa() {
        return supEa;
    }

    public void setSupEa(BigInteger supEa) {
        this.supEa = supEa;
    }

    public String getProy() {
        return proy;
    }

    public void setProy(String proy) {
        this.proy = proy;
    }

    public String getComp() {
        return comp;
    }

    public void setComp(String comp) {
        this.comp = comp;
    }

    public String getDescrip() {
        return descrip;
    }

    public void setDescrip(String descrip) {
        this.descrip = descrip;
    }

    public BigInteger getAreabuffer() {
        return areabuffer;
    }

    public void setAreabuffer(BigInteger areabuffer) {
        this.areabuffer = areabuffer;
    }

    public BigInteger getArea() {
        return area;
    }

    public void setArea(BigInteger area) {
        this.area = area;
    }

    public Date getFechaHora() {
        return fechaHora;
    }

    public void setFechaHora(Date fechaHora) {
        this.fechaHora = fechaHora;
    }

    @XmlTransient
    public List<SigeiaAnalisis> getSigeiaAnalisisList() {
        return sigeiaAnalisisList;
    }

    public void setSigeiaAnalisisList(List<SigeiaAnalisis> sigeiaAnalisisList) {
        this.sigeiaAnalisisList = sigeiaAnalisisList;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (humedalesPK != null ? humedalesPK.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Humedales)) {
            return false;
        }
        Humedales other = (Humedales) object;
        if ((this.humedalesPK == null && other.humedalesPK != null) || (this.humedalesPK != null && !this.humedalesPK.equals(other.humedalesPK))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "mx.gob.semarnat.model.Humedales[ humedalesPK=" + humedalesPK + " ]";
    }
    
}
