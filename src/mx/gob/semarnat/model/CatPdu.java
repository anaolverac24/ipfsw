/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package mx.gob.semarnat.model;

import java.io.Serializable;
import java.util.Collection;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author Rengerden
 */
@Entity
@Table(name = "CAT_PDU")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "CatPdu.findAll", query = "SELECT c FROM CatPdu c"),
    @NamedQuery(name = "CatPdu.findByPduId", query = "SELECT c FROM CatPdu c WHERE c.pduId = :pduId"),
    @NamedQuery(name = "CatPdu.findByPduDescripcion", query = "SELECT c FROM CatPdu c WHERE c.pduDescripcion = :pduDescripcion")})
public class CatPdu implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @Column(name = "PDU_ID")
    private Short pduId;
    @Basic(optional = false)
    @Column(name = "PDU_DESCRIPCION")
    private String pduDescripcion;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "catPdu")
    private Collection<PduProyecto> pduProyectoCollection;

    public CatPdu() {
    }

    public CatPdu(Short pduId) {
        this.pduId = pduId;
    }

    public CatPdu(Short pduId, String pduDescripcion) {
        this.pduId = pduId;
        this.pduDescripcion = pduDescripcion;
    }

    public Short getPduId() {
        return pduId;
    }

    public void setPduId(Short pduId) {
        this.pduId = pduId;
    }

    public String getPduDescripcion() {
        return pduDescripcion;
    }

    public void setPduDescripcion(String pduDescripcion) {
        this.pduDescripcion = pduDescripcion;
    }

    @XmlTransient
    public Collection<PduProyecto> getPduProyectoCollection() {
        return pduProyectoCollection;
    }

    public void setPduProyectoCollection(Collection<PduProyecto> pduProyectoCollection) {
        this.pduProyectoCollection = pduProyectoCollection;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (pduId != null ? pduId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof CatPdu)) {
            return false;
        }
        CatPdu other = (CatPdu) object;
        if ((this.pduId == null && other.pduId != null) || (this.pduId != null && !this.pduId.equals(other.pduId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "mx.gob.semarnat.model.CatPdu[ pduId=" + pduId + " ]";
    }
    
}
