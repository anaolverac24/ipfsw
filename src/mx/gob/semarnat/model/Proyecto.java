/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package mx.gob.semarnat.model;

import java.io.Serializable;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.SortedSet;
import java.util.TreeSet;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Lob;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;
import org.hibernate.annotations.Sort;
import org.hibernate.annotations.SortType;

/**
 *
 * @author mauricio
 */
@Entity
@Table(name = "PROYECTO")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Proyecto.findAll", query = "SELECT p FROM Proyecto p"),
    @NamedQuery(name = "Proyecto.findByFolioProyecto", query = "SELECT p FROM Proyecto p WHERE p.proyectoPK.folioProyecto = :folioProyecto"),
    @NamedQuery(name = "Proyecto.findBySerialProyecto", query = "SELECT p FROM Proyecto p WHERE p.proyectoPK.serialProyecto = :serialProyecto"),
    @NamedQuery(name = "Proyecto.findByProySupuesto", query = "SELECT p FROM Proyecto p WHERE p.proySupuesto = :proySupuesto"),
    @NamedQuery(name = "Proyecto.findByTipoAsentamientoId", query = "SELECT p FROM Proyecto p WHERE p.tipoAsentamientoId = :tipoAsentamientoId"),
    @NamedQuery(name = "Proyecto.findByCveTipoVial", query = "SELECT p FROM Proyecto p WHERE p.cveTipoVial = :cveTipoVial"),
    @NamedQuery(name = "Proyecto.findByNtipo", query = "SELECT p FROM Proyecto p WHERE p.ntipo = :ntipo"),
    @NamedQuery(name = "Proyecto.findByNrama", query = "SELECT p FROM Proyecto p WHERE p.nrama = :nrama"),
    @NamedQuery(name = "Proyecto.findByProyNombre", query = "SELECT p FROM Proyecto p WHERE p.proyNombre = :proyNombre"),
    @NamedQuery(name = "Proyecto.findByProyNombreVialidad", query = "SELECT p FROM Proyecto p WHERE p.proyNombreVialidad = :proyNombreVialidad"),
    @NamedQuery(name = "Proyecto.findByProyNumeroExterior", query = "SELECT p FROM Proyecto p WHERE p.proyNumeroExterior = :proyNumeroExterior"),
    @NamedQuery(name = "Proyecto.findByProyNumeroInterior", query = "SELECT p FROM Proyecto p WHERE p.proyNumeroInterior = :proyNumeroInterior"),
    @NamedQuery(name = "Proyecto.findByProyNombreAsentamiento", query = "SELECT p FROM Proyecto p WHERE p.proyNombreAsentamiento = :proyNombreAsentamiento"),
    @NamedQuery(name = "Proyecto.findByProyCodigoPostal", query = "SELECT p FROM Proyecto p WHERE p.proyCodigoPostal = :proyCodigoPostal"),
    @NamedQuery(name = "Proyecto.findByProyLocalidad", query = "SELECT p FROM Proyecto p WHERE p.proyLocalidad = :proyLocalidad"),
    @NamedQuery(name = "Proyecto.findByProyInversionRequerida", query = "SELECT p FROM Proyecto p WHERE p.proyInversionRequerida = :proyInversionRequerida"),
    @NamedQuery(name = "Proyecto.findByProyMedidasPrevencion", query = "SELECT p FROM Proyecto p WHERE p.proyMedidasPrevencion = :proyMedidasPrevencion"),
    @NamedQuery(name = "Proyecto.findByProyEmpleosPermanentes", query = "SELECT p FROM Proyecto p WHERE p.proyEmpleosPermanentes = :proyEmpleosPermanentes"),
    @NamedQuery(name = "Proyecto.findByProyEmpleosTemporales", query = "SELECT p FROM Proyecto p WHERE p.proyEmpleosTemporales = :proyEmpleosTemporales"),
    @NamedQuery(name = "Proyecto.findByProyTiempoVidaAnios", query = "SELECT p FROM Proyecto p WHERE p.proyTiempoVidaAnios = :proyTiempoVidaAnios"),
    @NamedQuery(name = "Proyecto.findByProyTiempoVidaMeses", query = "SELECT p FROM Proyecto p WHERE p.proyTiempoVidaMeses = :proyTiempoVidaMeses"),
    @NamedQuery(name = "Proyecto.findByProyTiempoVidaSemanas", query = "SELECT p FROM Proyecto p WHERE p.proyTiempoVidaSemanas = :proyTiempoVidaSemanas"),
    @NamedQuery(name = "Proyecto.findByProyDiagramaGantt", query = "SELECT p FROM Proyecto p WHERE p.proyDiagramaGantt = :proyDiagramaGantt"),
    @NamedQuery(name = "Proyecto.findByProyRespTecIgualLegal", query = "SELECT p FROM Proyecto p WHERE p.proyRespTecIgualLegal = :proyRespTecIgualLegal"),
    @NamedQuery(name = "Proyecto.findByRespTecId", query = "SELECT p FROM Proyecto p WHERE p.respTecId = :respTecId"),
    @NamedQuery(name = "Proyecto.findByProyInversionFederalOri", query = "SELECT p FROM Proyecto p WHERE p.proyInversionFederalOri = :proyInversionFederalOri"),
    @NamedQuery(name = "Proyecto.findByProyInversionEstatalOri", query = "SELECT p FROM Proyecto p WHERE p.proyInversionEstatalOri = :proyInversionEstatalOri"),
    @NamedQuery(name = "Proyecto.findByProyInversionMunicipalOri", query = "SELECT p FROM Proyecto p WHERE p.proyInversionMunicipalOri = :proyInversionMunicipalOri"),
    @NamedQuery(name = "Proyecto.findByProyInversionPrivadaOri", query = "SELECT p FROM Proyecto p WHERE p.proyInversionPrivadaOri = :proyInversionPrivadaOri"),
    @NamedQuery(name = "Proyecto.findByProyRepresentanteId", query = "SELECT p FROM Proyecto p WHERE p.proyRepresentanteId = :proyRepresentanteId"),
    @NamedQuery(name = "Proyecto.findByProyDescNat", query = "SELECT p FROM Proyecto p WHERE p.proyDescNat = :proyDescNat"),
    @NamedQuery(name = "Proyecto.findByProyDemandaServ", query = "SELECT p FROM Proyecto p WHERE p.proyDemandaServ = :proyDemandaServ"),
    @NamedQuery(name = "Proyecto.findByProyDescPaisaje", query = "SELECT p FROM Proyecto p WHERE p.proyDescPaisaje = :proyDescPaisaje"),
    @NamedQuery(name = "Proyecto.findByProyDiagAmbiental", query = "SELECT p FROM Proyecto p WHERE p.proyDiagAmbiental = :proyDiagAmbiental"),
    @NamedQuery(name = "Proyecto.findByProyJustMetodologia", query = "SELECT p FROM Proyecto p WHERE p.proyJustMetodologia = :proyJustMetodologia"),
    @NamedQuery(name = "Proyecto.findByProyRequiMontos", query = "SELECT p FROM Proyecto p WHERE p.proyRequiMontos = :proyRequiMontos"),
    @NamedQuery(name = "Proyecto.findByProyEsceSinP", query = "SELECT p FROM Proyecto p WHERE p.proyEsceSinP = :proyEsceSinP"),
    @NamedQuery(name = "Proyecto.findByProyEsceConP", query = "SELECT p FROM Proyecto p WHERE p.proyEsceConP = :proyEsceConP"),
    @NamedQuery(name = "Proyecto.findByProyEsceConPymed", query = "SELECT p FROM Proyecto p WHERE p.proyEsceConPymed = :proyEsceConPymed"),
    @NamedQuery(name = "Proyecto.findByProyDescEvaluAlter", query = "SELECT p FROM Proyecto p WHERE p.proyDescEvaluAlter = :proyDescEvaluAlter"),
    @NamedQuery(name = "Proyecto.findByClaveProyecto", query = "SELECT p FROM Proyecto p WHERE p.claveProyecto = :claveProyecto"),
    @NamedQuery(name = "Proyecto.findByBitacoraProyecto", query = "SELECT p FROM Proyecto p WHERE p.bitacoraProyecto = :bitacoraProyecto"),
    @NamedQuery(name = "Proyecto.findByNsub", query = "SELECT p FROM Proyecto p WHERE p.nsub = :nsub"),
    @NamedQuery(name = "Proyecto.findByNsec", query = "SELECT p FROM Proyecto p WHERE p.nsec = :nsec"),
    @NamedQuery(name = "Proyecto.findByEstatusProyecto", query = "SELECT p FROM Proyecto p WHERE p.estatusProyecto = :estatusProyecto")})
    
public class Proyecto implements Serializable {
    @Column(name = "PROY_VALOR_CRITERIO")
    private Short proyValorCriterio;
    @Column(name = "PROY_CRITERIO_MONTO")
    private BigDecimal proyCriterioMonto;
    @Column(name = "PROY_LOTE")
    private Long proyLote;
    @Column(name = "PROY_ENT_AFECTADO")
    private String proyEntAfectado;
    @Column(name = "PROY_MUN_AFECTADO")
    private String proyMunAfectado;
    @Column(name = "ESTUDIO_RIESGO_ID")
    private Short estudioRiesgoId;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "proyecto")
    private List<RepLegalProyecto> repLegalProyectoList;

    @Column(name = "PROY_NORMA_ID")
    private Short proyNormaId;

    @Column(name = "PROY_SUP_POET_PDU")
    private Character proySupPoetPdu;

    
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "proyecto")
    private Collection<ParqueIndustrialProyecto> parqueIndustrialProyectoCollection;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "proyecto")
    private Collection<PduProyecto> pduProyectoCollection;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "proyecto")
    private Collection<PoetProyecto> poetProyectoCollection;
    private static final long serialVersionUID = 1L;
    @EmbeddedId
    protected ProyectoPK proyectoPK;
    @Column(name = "PROY_SUPUESTO")
    private Character proySupuesto;
    @Column(name = "TIPO_ASENTAMIENTO_ID")
    private Short tipoAsentamientoId;
    @Column(name = "CVE_TIPO_VIAL")
    private Short cveTipoVial;
    @Column(name = "NTIPO")
    private Short ntipo;
    @Column(name = "NRAMA")
    private Short nrama;
    @Column(name = "PROY_NOMBRE")
    private String proyNombre;
    @Column(name = "PROY_NOMBRE_VIALIDAD")
    private String proyNombreVialidad;
    @Column(name = "PROY_NUMERO_EXTERIOR")
    private Integer proyNumeroExterior;
    @Column(name = "PROY_NUMERO_INTERIOR")
    private Integer proyNumeroInterior;
    @Column(name = "PROY_NOMBRE_ASENTAMIENTO")
    private String proyNombreAsentamiento;
    @Column(name = "PROY_CODIGO_POSTAL")
    private Integer proyCodigoPostal;
    @Column(name = "PROY_LOCALIDAD")
    private String proyLocalidad;
    @Lob
    @Column(name = "PROY_UBICACION_DESCRITA")
    private String proyUbicacionDescrita;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Column(name = "PROY_INVERSION_REQUERIDA")
    private BigDecimal proyInversionRequerida;
    @Column(name = "PROY_MEDIDAS_PREVENCION")
    private BigDecimal proyMedidasPrevencion;
    @Column(name = "PROY_EMPLEOS_PERMANENTES")
    private Integer proyEmpleosPermanentes;
    @Column(name = "PROY_EMPLEOS_TEMPORALES")
    private Integer proyEmpleosTemporales;
    @Column(name = "PROY_TIEMPO_VIDA_ANIOS")
    private Short proyTiempoVidaAnios;
    @Column(name = "PROY_TIEMPO_VIDA_MESES")
    private Short proyTiempoVidaMeses;
    @Column(name = "PROY_TIEMPO_VIDA_SEMANAS")
    private Short proyTiempoVidaSemanas;
    @Lob
    @Column(name = "PROY_DESC_MEDIDAS_COMPEN")
    private String proyDescMedidasCompen;
    @Lob
    @Column(name = "PROY_DESC_PROTEC_CONSERV")
    private String proyDescProtecConserv;
    @Lob
    @Column(name = "PROY_DESC_PARTICULAR")
    private String proyDescParticular;
    @Lob
    @Column(name = "PROY_DESC_AMBIENTE")
    private String proyDescAmbiente;
    @Column(name = "PROY_DIAGRAMA_GANTT")
    private String proyDiagramaGantt;
    @Column(name = "PROY_RESP_TEC_IGUAL_LEGAL")
    private Character proyRespTecIgualLegal;
    @Column(name = "RESP_TEC_ID")
    private Short respTecId;
    @Column(name = "PROY_INVERSION_FEDERAL_ORI")
    private BigDecimal proyInversionFederalOri;
    @Column(name = "PROY_INVERSION_ESTATAL_ORI")
    private BigDecimal proyInversionEstatalOri;
    @Column(name = "PROY_INVERSION_MUNICIPAL_ORI")
    private BigDecimal proyInversionMunicipalOri;
    @Column(name = "PROY_INVERSION_PRIVADA_ORI")
    private BigDecimal proyInversionPrivadaOri;
    @Column(name = "PROY_REPRESENTANTE_ID")
    private BigInteger proyRepresentanteId;
    @Column(name = "PROY_DESC_NAT")
    private String proyDescNat;
    @Column(name = "PROY_DEMANDA_SERV")
    private Character proyDemandaServ;
    @Column(name = "PROY_DESC_PAISAJE")
    private String proyDescPaisaje;
    @Column(name = "PROY_DIAG_AMBIENTAL")
    private String proyDiagAmbiental;
    @Column(name = "PROY_JUST_METODOLOGIA")
    private String proyJustMetodologia;
    @Column(name = "PROY_REQUI_MONTOS")
    private Character proyRequiMontos;
    @Column(name = "PROY_ESCE_SIN_P")
    private String proyEsceSinP;
    @Column(name = "PROY_ESCE_CON_P")
    private String proyEsceConP;
    @Column(name = "PROY_ESCE_CON_PYMED")
    private String proyEsceConPymed;
    @Column(name = "PROY_DESC_EVALU_ALTER")
    private String proyDescEvaluAlter;
    @Column(name = "CLAVE_PROYECTO")
    private String claveProyecto;
    @Column(name = "BITACORA_PROYECTO")
    private String bitacoraProyecto;
    @Column(name = "NSUB")
    private Short nsub;
    @Column(name = "NSEC")
    private Short nsec;
    @Column(name = "ESTATUS_PROYECTO")
    private String estatusProyecto;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "proyecto")
    private List<AnexosProyecto> anexosProyectoList;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "proyecto", fetch = FetchType.EAGER, orphanRemoval = true)
    //@OrderBy("ESPECIFICACION_ID")
    @Sort(type=SortType.NATURAL)
    private SortedSet<EspecificacionProyecto> especificacionProyectoList;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "proyecto")
    private List<ImpactoProyecto> impactoProyectoList;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "proyecto", orphanRemoval = true)
    private List<PreguntaProyecto> preguntaProyectoList;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "proyecto")
    private List<EtapaProyecto> etapaProyectoList;
//    @JoinColumn(name = "ESTUDIO_RIESGO_ID", referencedColumnName = "ESTUDIO_RIESGO_ID")
//    @ManyToOne
//    private EstudioRiesgoProyecto estudioRiesgoId;
    @Column(name = "PROY_DOM_ESTABLECIDO")
    private Character proyDomEstablecido;

    public Character getProyDomEstablecido() {
        return proyDomEstablecido;
    }

    public void setProyDomEstablecido(Character proyDomEstablecido) {
        this.proyDomEstablecido = proyDomEstablecido;
    }
    
    public Proyecto() {
    }

    public Proyecto(ProyectoPK proyectoPK) {
        this.proyectoPK = proyectoPK;
    }

    public Proyecto(String folioProyecto, short serialProyecto) {
        this.proyectoPK = new ProyectoPK(folioProyecto, serialProyecto);
    }
    
    public Proyecto(String folioProyecto, short serialProyecto, String clveProy) {
        this.proyectoPK = new ProyectoPK(folioProyecto, serialProyecto);
        this.claveProyecto = clveProy;
    }

    public ProyectoPK getProyectoPK() {
        return proyectoPK;
    }

    public void setProyectoPK(ProyectoPK proyectoPK) {
        this.proyectoPK = proyectoPK;
    }

    public Character getProySupuesto() {
        return proySupuesto;
    }

    public void setProySupuesto(Character proySupuesto) {
        this.proySupuesto = proySupuesto;
    }

    public Short getTipoAsentamientoId() {
        return tipoAsentamientoId;
    }

    public void setTipoAsentamientoId(Short tipoAsentamientoId) {
        this.tipoAsentamientoId = tipoAsentamientoId;
    }

    public Short getCveTipoVial() {
        return cveTipoVial;
    }

    public void setCveTipoVial(Short cveTipoVial) {
        this.cveTipoVial = cveTipoVial;
    }

    public Short getNtipo() {
        return ntipo;
    }

    public void setNtipo(Short ntipo) {
        this.ntipo = ntipo;
    }

    public Short getNrama() {
        return nrama;
    }

    public void setNrama(Short nrama) {
        this.nrama = nrama;
    }

    public String getProyNombre() {
        return proyNombre;
    }

    public void setProyNombre(String proyNombre) {
        this.proyNombre = proyNombre;
    }

    public String getProyNombreVialidad() {
        return proyNombreVialidad;
    }

    public void setProyNombreVialidad(String proyNombreVialidad) {
        this.proyNombreVialidad = proyNombreVialidad;
    }

    public Integer getProyNumeroExterior() {
        return proyNumeroExterior;
    }

    public void setProyNumeroExterior(Integer proyNumeroExterior) {
        this.proyNumeroExterior = proyNumeroExterior;
    }

    public Integer getProyNumeroInterior() {
        return proyNumeroInterior;
    }

    public void setProyNumeroInterior(Integer proyNumeroInterior) {
        this.proyNumeroInterior = proyNumeroInterior;
    }

    public String getProyNombreAsentamiento() {
        return proyNombreAsentamiento;
    }

    public void setProyNombreAsentamiento(String proyNombreAsentamiento) {
        this.proyNombreAsentamiento = proyNombreAsentamiento;
    }

    public Integer getProyCodigoPostal() {
        return proyCodigoPostal;
    }

    public void setProyCodigoPostal(Integer proyCodigoPostal) {
        this.proyCodigoPostal = proyCodigoPostal;
    }

    public String getProyLocalidad() {
        return proyLocalidad;
    }

    public void setProyLocalidad(String proyLocalidad) {
        this.proyLocalidad = proyLocalidad;
    }

    public String getProyUbicacionDescrita() {
        return proyUbicacionDescrita;
    }

    public void setProyUbicacionDescrita(String proyUbicacionDescrita) {
        this.proyUbicacionDescrita = proyUbicacionDescrita;
    }

    public BigDecimal getProyInversionRequerida() {
        return proyInversionRequerida;
    }

    public void setProyInversionRequerida(BigDecimal proyInversionRequerida) {
        this.proyInversionRequerida = proyInversionRequerida;
    }

    public BigDecimal getProyMedidasPrevencion() {
        return proyMedidasPrevencion;
    }

    public void setProyMedidasPrevencion(BigDecimal proyMedidasPrevencion) {
        this.proyMedidasPrevencion = proyMedidasPrevencion;
    }

    public Integer getProyEmpleosPermanentes() {
        return proyEmpleosPermanentes;
    }

    public void setProyEmpleosPermanentes(Integer proyEmpleosPermanentes) {
        this.proyEmpleosPermanentes = proyEmpleosPermanentes;
    }

    public Integer getProyEmpleosTemporales() {
        return proyEmpleosTemporales;
    }

    public void setProyEmpleosTemporales(Integer proyEmpleosTemporales) {
        this.proyEmpleosTemporales = proyEmpleosTemporales;
    }

    public Short getProyTiempoVidaAnios() {
        return proyTiempoVidaAnios;
    }

    public void setProyTiempoVidaAnios(Short proyTiempoVidaAnios) {
        this.proyTiempoVidaAnios = proyTiempoVidaAnios;
    }

    public Short getProyTiempoVidaMeses() {
        return proyTiempoVidaMeses;
    }

    public void setProyTiempoVidaMeses(Short proyTiempoVidaMeses) {
        this.proyTiempoVidaMeses = proyTiempoVidaMeses;
    }

    public Short getProyTiempoVidaSemanas() {
        return proyTiempoVidaSemanas;
    }

    public void setProyTiempoVidaSemanas(Short proyTiempoVidaSemanas) {
        this.proyTiempoVidaSemanas = proyTiempoVidaSemanas;
    }

    public String getProyDescMedidasCompen() {
        return proyDescMedidasCompen;
    }

    public void setProyDescMedidasCompen(String proyDescMedidasCompen) {
        this.proyDescMedidasCompen = proyDescMedidasCompen;
    }

    public String getProyDescProtecConserv() {
        return proyDescProtecConserv;
    }

    public void setProyDescProtecConserv(String proyDescProtecConserv) {
        this.proyDescProtecConserv = proyDescProtecConserv;
    }

    public String getProyDescParticular() {
        return proyDescParticular;
    }

    public void setProyDescParticular(String proyDescParticular) {
        this.proyDescParticular = proyDescParticular;
    }

    public String getProyDescAmbiente() {
        return proyDescAmbiente;
    }

    public void setProyDescAmbiente(String proyDescAmbiente) {
        this.proyDescAmbiente = proyDescAmbiente;
    }

    public String getProyDiagramaGantt() {
        return proyDiagramaGantt;
    }

    public void setProyDiagramaGantt(String proyDiagramaGantt) {
        this.proyDiagramaGantt = proyDiagramaGantt;
    }

    public Character getProyRespTecIgualLegal() {
        return proyRespTecIgualLegal;
    }

    public void setProyRespTecIgualLegal(Character proyRespTecIgualLegal) {
        this.proyRespTecIgualLegal = proyRespTecIgualLegal;
    }

    public Short getRespTecId() {
        return respTecId;
    }

    public void setRespTecId(Short respTecId) {
        this.respTecId = respTecId;
    }

    public BigDecimal getProyInversionFederalOri() {
        return proyInversionFederalOri;
    }

    public void setProyInversionFederalOri(BigDecimal proyInversionFederalOri) {
        this.proyInversionFederalOri = proyInversionFederalOri;
    }

    public BigDecimal getProyInversionEstatalOri() {
        return proyInversionEstatalOri;
    }

    public void setProyInversionEstatalOri(BigDecimal proyInversionEstatalOri) {
        this.proyInversionEstatalOri = proyInversionEstatalOri;
    }

    public BigDecimal getProyInversionMunicipalOri() {
        return proyInversionMunicipalOri;
    }

    public void setProyInversionMunicipalOri(BigDecimal proyInversionMunicipalOri) {
        this.proyInversionMunicipalOri = proyInversionMunicipalOri;
    }

    public BigDecimal getProyInversionPrivadaOri() {
        return proyInversionPrivadaOri;
    }

    public void setProyInversionPrivadaOri(BigDecimal proyInversionPrivadaOri) {
        this.proyInversionPrivadaOri = proyInversionPrivadaOri;
    }

    public BigInteger getProyRepresentanteId() {
        return proyRepresentanteId;
    }

    public void setProyRepresentanteId(BigInteger proyRepresentanteId) {
        this.proyRepresentanteId = proyRepresentanteId;
    }

    public String getProyDescNat() {
        return proyDescNat;
    }

    public void setProyDescNat(String proyDescNat) {
        this.proyDescNat = proyDescNat;
    }

    public Character getProyDemandaServ() {
        return proyDemandaServ;
    }

    public void setProyDemandaServ(Character proyDemandaServ) {
        this.proyDemandaServ = proyDemandaServ;
    }

    public String getProyDescPaisaje() {
        return proyDescPaisaje;
    }

    public void setProyDescPaisaje(String proyDescPaisaje) {
        this.proyDescPaisaje = proyDescPaisaje;
    }

    public String getProyDiagAmbiental() {
        return proyDiagAmbiental;
    }

    public void setProyDiagAmbiental(String proyDiagAmbiental) {
        this.proyDiagAmbiental = proyDiagAmbiental;
    }

    public String getProyJustMetodologia() {
        return proyJustMetodologia;
    }

    public void setProyJustMetodologia(String proyJustMetodologia) {
        this.proyJustMetodologia = proyJustMetodologia;
    }

    public Character getProyRequiMontos() {
        return proyRequiMontos;
    }

    public void setProyRequiMontos(Character proyRequiMontos) {
        this.proyRequiMontos = proyRequiMontos;
    }

    public String getProyEsceSinP() {
        return proyEsceSinP;
    }

    public void setProyEsceSinP(String proyEsceSinP) {
        this.proyEsceSinP = proyEsceSinP;
    }

    public String getProyEsceConP() {
        return proyEsceConP;
    }

    public void setProyEsceConP(String proyEsceConP) {
        this.proyEsceConP = proyEsceConP;
    }

    public String getProyEsceConPymed() {
        return proyEsceConPymed;
    }

    public void setProyEsceConPymed(String proyEsceConPymed) {
        this.proyEsceConPymed = proyEsceConPymed;
    }

    public String getProyDescEvaluAlter() {
        return proyDescEvaluAlter;
    }

    public void setProyDescEvaluAlter(String proyDescEvaluAlter) {
        this.proyDescEvaluAlter = proyDescEvaluAlter;
    }

    public String getClaveProyecto() {
        return claveProyecto;
    }

    public void setClaveProyecto(String claveProyecto) {
        this.claveProyecto = claveProyecto;
    }

    public String getBitacoraProyecto() {
        return bitacoraProyecto;
    }

    public void setBitacoraProyecto(String bitacoraProyecto) {
        this.bitacoraProyecto = bitacoraProyecto;
    }

    public Short getNsub() {
        return nsub;
    }

    public void setNsub(Short nsub) {
        this.nsub = nsub;
    }

    public Short getNsec() {
        return nsec;
    }

    public void setNsec(Short nsec) {
        this.nsec = nsec;
    }

    public String getEstatusProyecto() {
        return estatusProyecto;
    }

    public void setEstatusProyecto(String estatusProyecto) {
        this.estatusProyecto = estatusProyecto;
    }

    @XmlTransient
    public List<AnexosProyecto> getAnexosProyectoList() {
        return anexosProyectoList;
    }

    public void setAnexosProyectoList(List<AnexosProyecto> anexosProyectoList) {
        this.anexosProyectoList = anexosProyectoList;
    }

    @XmlTransient
    public SortedSet<EspecificacionProyecto> getEspecificacionProyectoList() {
        return especificacionProyectoList;
    }

    public void setEspecificacionProyectoList(SortedSet<EspecificacionProyecto> especificacionProyectoList) {
        this.especificacionProyectoList = especificacionProyectoList;
    }

    @XmlTransient
    public List<ImpactoProyecto> getImpactoProyectoList() {
        return impactoProyectoList;
    }

    public void setImpactoProyectoList(List<ImpactoProyecto> impactoProyectoList) {
        this.impactoProyectoList = impactoProyectoList;
    }

    @XmlTransient
    public List<PreguntaProyecto> getPreguntaProyectoList() {
        return preguntaProyectoList;
    }

    public void setPreguntaProyectoList(List<PreguntaProyecto> preguntaProyectoList) {
        this.preguntaProyectoList = preguntaProyectoList;
    }

    @XmlTransient
    public List<EtapaProyecto> getEtapaProyectoList() {
        return etapaProyectoList;
    }

    public void setEtapaProyectoList(List<EtapaProyecto> etapaProyectoList) {
        this.etapaProyectoList = etapaProyectoList;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (proyectoPK != null ? proyectoPK.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Proyecto)) {
            return false;
        }
        Proyecto other = (Proyecto) object;
        if ((this.proyectoPK == null && other.proyectoPK != null) || (this.proyectoPK != null && !this.proyectoPK.equals(other.proyectoPK))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "mx.gob.semarnat.model.Proyecto[ proyectoPK=" + proyectoPK + " ]";
    }

    @XmlTransient
    public Collection<ParqueIndustrialProyecto> getParqueIndustrialProyectoCollection() {
        return parqueIndustrialProyectoCollection;
    }

    public void setParqueIndustrialProyectoCollection(Collection<ParqueIndustrialProyecto> parqueIndustrialProyectoCollection) {
        this.parqueIndustrialProyectoCollection = parqueIndustrialProyectoCollection;
    }

    @XmlTransient
    public Collection<PduProyecto> getPduProyectoCollection() {
        return pduProyectoCollection;
    }

    public void setPduProyectoCollection(Collection<PduProyecto> pduProyectoCollection) {
        this.pduProyectoCollection = pduProyectoCollection;
    }

    @XmlTransient
    public Collection<PoetProyecto> getPoetProyectoCollection() {
        return poetProyectoCollection;
    }

    public void setPoetProyectoCollection(Collection<PoetProyecto> poetProyectoCollection) {
        this.poetProyectoCollection = poetProyectoCollection;
    }

//    public Short getEstudioRiesgoId() {
//        return estudioRiesgoId;
//    }
//
//    public void setEstudioRiesgoId(Short estudioRiesgoId) {
//        this.estudioRiesgoId = estudioRiesgoId;
//    }

    public Short getProyNormaId() {
        return proyNormaId;
    }

    public void setProyNormaId(Short proyNormaId) {
        this.proyNormaId = proyNormaId;
    }

//    public EstudioRiesgoProyecto getEstudioRiesgoProyecto() {
//        return estudioRiesgoProyecto;
//    }
//
//    public void setEstudioRiesgoProyecto(EstudioRiesgoProyecto estudioRiesgoProyecto) {
//        this.estudioRiesgoProyecto = estudioRiesgoProyecto;
//    }

//    /**
//     * @return the estudioRiesgoId
//     */
//    public EstudioRiesgoProyecto getEstudioRiesgoId() {
//        return estudioRiesgoId;
//    }
//
//    /**
//     * @param estudioRiesgoId the estudioRiesgoId to set
//     */
//    public void setEstudioRiesgoId(EstudioRiesgoProyecto estudioRiesgoId) {
//        this.estudioRiesgoId = estudioRiesgoId;
//    }

//    public Short getEstudioRiesgoId() {
//        return estudioRiesgoId;
//    }
//
//    public void setEstudioRiesgoId(Short estudioRiesgoId) {
//        this.estudioRiesgoId = estudioRiesgoId;
//    }

    /**
     * @return the proySupPoetPdu
     */
    public Character getProySupPoetPdu() {
        return proySupPoetPdu;
    }

    /**
     * @param proySupPoetPdu the proySupPoetPdu to set
     */
    public void setProySupPoetPdu(Character proySupPoetPdu) {
        this.proySupPoetPdu = proySupPoetPdu;
    }

//    public EstudioRiesgoProyecto getEstudioRiesgoProyecto() {
//        return estudioRiesgoProyecto;
//    }
//
//    public void setEstudioRiesgoProyecto(EstudioRiesgoProyecto estudioRiesgoProyecto) {
//        this.estudioRiesgoProyecto = estudioRiesgoProyecto;
//    }

    public Short getEstudioRiesgoId() {
        return estudioRiesgoId;
    }

    public void setEstudioRiesgoId(Short estudioRiesgoId) {
        this.estudioRiesgoId = estudioRiesgoId;
    }

    @XmlTransient
    public List<RepLegalProyecto> getRepLegalProyectoList() {
        return repLegalProyectoList;
    }

    public void setRepLegalProyectoList(List<RepLegalProyecto> repLegalProyectoList) {
        this.repLegalProyectoList = repLegalProyectoList;
    }

    public String getProyEntAfectado() {
        return proyEntAfectado;
    }

    public void setProyEntAfectado(String proyEntAfectado) {
        this.proyEntAfectado = proyEntAfectado;
    }

    public String getProyMunAfectado() {
        return proyMunAfectado;
    }

    public void setProyMunAfectado(String proyMunAfectado) {
        this.proyMunAfectado = proyMunAfectado;
    }

    public Long getProyLote() {
        return proyLote;
    }

    public void setProyLote(Long proyLote) {
        this.proyLote = proyLote;
    }

    public Short getProyValorCriterio() {
        return proyValorCriterio;
    }

    public void setProyValorCriterio(Short proyValorCriterio) {
        this.proyValorCriterio = proyValorCriterio;
    }

    public BigDecimal getProyCriterioMonto() {
        return proyCriterioMonto;
    }

    public void setProyCriterioMonto(BigDecimal proyCriterioMonto) {
        this.proyCriterioMonto = proyCriterioMonto;
    }
    
}
