/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package mx.gob.semarnat.model;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.JoinColumns;
import javax.persistence.Lob;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author mauricio
 */
@Entity
@Table(name = "ESTUDIO_RIESGO_PROYECTO")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "EstudioRiesgoProyecto.findAll", query = "SELECT e FROM EstudioRiesgoProyecto e"),
    @NamedQuery(name = "EstudioRiesgoProyecto.findByFolioProyecto", query = "SELECT e FROM EstudioRiesgoProyecto e WHERE e.estudioRiesgoProyectoPK.folioProyecto = :folioProyecto"),
    @NamedQuery(name = "EstudioRiesgoProyecto.findBySerialProyecto", query = "SELECT e FROM EstudioRiesgoProyecto e WHERE e.estudioRiesgoProyectoPK.serialProyecto = :serialProyecto"),
    @NamedQuery(name = "EstudioRiesgoProyecto.findByEstudioNombreRazonSocial", query = "SELECT e FROM EstudioRiesgoProyecto e WHERE e.estudioNombreRazonSocial = :estudioNombreRazonSocial"),
    @NamedQuery(name = "EstudioRiesgoProyecto.findByEstudioRfc", query = "SELECT e FROM EstudioRiesgoProyecto e WHERE e.estudioRfc = :estudioRfc"),
    @NamedQuery(name = "EstudioRiesgoProyecto.findByEstudioNomRpteLegal", query = "SELECT e FROM EstudioRiesgoProyecto e WHERE e.estudioNomRpteLegal = :estudioNomRpteLegal"),
    @NamedQuery(name = "EstudioRiesgoProyecto.findByEstudioRfcRpte", query = "SELECT e FROM EstudioRiesgoProyecto e WHERE e.estudioRfcRpte = :estudioRfcRpte"),
    @NamedQuery(name = "EstudioRiesgoProyecto.findByEstudioNomRespEla", query = "SELECT e FROM EstudioRiesgoProyecto e WHERE e.estudioNomRespEla = :estudioNomRespEla"),
    @NamedQuery(name = "EstudioRiesgoProyecto.findByEstudioRfcRespEla", query = "SELECT e FROM EstudioRiesgoProyecto e WHERE e.estudioRfcRespEla = :estudioRfcRespEla")})
public class EstudioRiesgoProyecto implements Serializable {
    private static final long serialVersionUID = 1L;
    @EmbeddedId
    protected EstudioRiesgoProyectoPK estudioRiesgoProyectoPK;
    @Column(name = "ESTUDIO_NOMBRE_RAZON_SOCIAL")
    private String estudioNombreRazonSocial;
    @Column(name = "ESTUDIO_RFC")
    private String estudioRfc;
    @Column(name = "ESTUDIO_NOM_RPTE_LEGAL")
    private String estudioNomRpteLegal;
    @Column(name = "ESTUDIO_RFC_RPTE")
    private String estudioRfcRpte;
    @Column(name = "ESTUDIO_NOM_RESP_ELA")
    private String estudioNomRespEla;
    @Column(name = "ESTUDIO_RFC_RESP_ELA")
    private String estudioRfcRespEla;
    @Lob
    @Column(name = "ESTUDIO_DESC_SIST_TANSP")
    private String estudioDescSistTansp;
    @Lob
    @Column(name = "ESTUDIO_DESC_BASES_DISE")
    private String estudioDescBasesDise;
    @Lob
    @Column(name = "ESTUDIO_OPERACION")
    private String estudioOperacion;
    @Lob
    @Column(name = "ESTUDIO_PRUEBAS_VERIFICACION")
    private String estudioPruebasVerificacion;
    @Lob
    @Column(name = "ESTUDIO_PROCEDIMIENTOS_MEDIDAS")
    private String estudioProcedimientosMedidas;
    @Lob
    @Column(name = "ESTUDIO_ANTECEDENTES_ACCI_INCI")
    private String estudioAntecedentesAcciInci;
    @Lob
    @Column(name = "ESTUDIO_METODOLOGIA_IDEN_JERAR")
    private String estudioMetodologiaIdenJerar;
    @Lob
    @Column(name = "ESTUDIO_RADIOS_POTENCIALES")
    private String estudioRadiosPotenciales;
    @Lob
    @Column(name = "ESTUDIO_INTERACCIONES_RIESGO")
    private String estudioInteraccionesRiesgo;
    @Lob
    @Column(name = "ESTUDIO_EFECTOS_AREA_INFLU")
    private String estudioEfectosAreaInflu;
    @Lob
    @Column(name = "ESTUDIO_RECOMENDACIONES_TEC_OP")
    private String estudioRecomendacionesTecOp;
    @Lob
    @Column(name = "ESTUDIO_SISTEMAS_SEGURIDAD")
    private String estudioSistemasSeguridad;
    @Lob
    @Column(name = "ESTUDIO_MEDIDAS_PREVENTIVAS")
    private String estudioMedidasPreventivas;

    @Lob
    @Column(name = "ESTUDIO_SENALA_CONCLUSION")
    private String estudioSenalaConclusion;
    @Lob
    @Column(name = "ESTUDIO_RESUMEN_SITUACION")
    private String estudioResumenSituacion;
    @Lob
    @Column(name = "ESTUDIO_INFORME_TECNICO")
    private String estudioInformeTecnico;
    
    
//    @JoinColumns({
//        @JoinColumn(name = "FOLIO_PROYECTO", referencedColumnName = "FOLIO_PROYECTO", insertable = false, updatable = false),
//        @JoinColumn(name = "SERIAL_PROYECTO", referencedColumnName = "SERIAL_PROYECTO", insertable = false, updatable = false)})
//    @OneToOne(optional = false)
//    private Proyecto proyecto;

    public EstudioRiesgoProyecto() {
    }

    public EstudioRiesgoProyecto(EstudioRiesgoProyectoPK estudioRiesgoProyectoPK) {
        this.estudioRiesgoProyectoPK = estudioRiesgoProyectoPK;
    }

    public EstudioRiesgoProyecto(String folioProyecto, short serialProyecto) {
        this.estudioRiesgoProyectoPK = new EstudioRiesgoProyectoPK(folioProyecto, serialProyecto);
    }

    public EstudioRiesgoProyectoPK getEstudioRiesgoProyectoPK() {
        return estudioRiesgoProyectoPK;
    }

    public void setEstudioRiesgoProyectoPK(EstudioRiesgoProyectoPK estudioRiesgoProyectoPK) {
        this.estudioRiesgoProyectoPK = estudioRiesgoProyectoPK;
    }

    public String getEstudioNombreRazonSocial() {
        return estudioNombreRazonSocial;
    }

    public void setEstudioNombreRazonSocial(String estudioNombreRazonSocial) {
        this.estudioNombreRazonSocial = estudioNombreRazonSocial;
    }

    public String getEstudioRfc() {
        return estudioRfc;
    }

    public void setEstudioRfc(String estudioRfc) {
        this.estudioRfc = estudioRfc;
    }

    public String getEstudioNomRpteLegal() {
        return estudioNomRpteLegal;
    }

    public void setEstudioNomRpteLegal(String estudioNomRpteLegal) {
        this.estudioNomRpteLegal = estudioNomRpteLegal;
    }

    public String getEstudioRfcRpte() {
        return estudioRfcRpte;
    }

    public void setEstudioRfcRpte(String estudioRfcRpte) {
        this.estudioRfcRpte = estudioRfcRpte;
    }

    public String getEstudioNomRespEla() {
        return estudioNomRespEla;
    }

    public void setEstudioNomRespEla(String estudioNomRespEla) {
        this.estudioNomRespEla = estudioNomRespEla;
    }

    public String getEstudioRfcRespEla() {
        return estudioRfcRespEla;
    }

    public void setEstudioRfcRespEla(String estudioRfcRespEla) {
        this.estudioRfcRespEla = estudioRfcRespEla;
    }

    public String getEstudioDescSistTansp() {
        return estudioDescSistTansp;
    }

    public void setEstudioDescSistTansp(String estudioDescSistTansp) {
        this.estudioDescSistTansp = estudioDescSistTansp;
    }

    public String getEstudioDescBasesDise() {
        return estudioDescBasesDise;
    }

    public void setEstudioDescBasesDise(String estudioDescBasesDise) {
        this.estudioDescBasesDise = estudioDescBasesDise;
    }

    public String getEstudioOperacion() {
        return estudioOperacion;
    }

    public void setEstudioOperacion(String estudioOperacion) {
        this.estudioOperacion = estudioOperacion;
    }

    public String getEstudioPruebasVerificacion() {
        return estudioPruebasVerificacion;
    }

    public void setEstudioPruebasVerificacion(String estudioPruebasVerificacion) {
        this.estudioPruebasVerificacion = estudioPruebasVerificacion;
    }

    public String getEstudioProcedimientosMedidas() {
        return estudioProcedimientosMedidas;
    }

    public void setEstudioProcedimientosMedidas(String estudioProcedimientosMedidas) {
        this.estudioProcedimientosMedidas = estudioProcedimientosMedidas;
    }

    public String getEstudioAntecedentesAcciInci() {
        return estudioAntecedentesAcciInci;
    }

    public void setEstudioAntecedentesAcciInci(String estudioAntecedentesAcciInci) {
        this.estudioAntecedentesAcciInci = estudioAntecedentesAcciInci;
    }

    public String getEstudioMetodologiaIdenJerar() {
        return estudioMetodologiaIdenJerar;
    }

    public void setEstudioMetodologiaIdenJerar(String estudioMetodologiaIdenJerar) {
        this.estudioMetodologiaIdenJerar = estudioMetodologiaIdenJerar;
    }

    public String getEstudioRadiosPotenciales() {
        return estudioRadiosPotenciales;
    }

    public void setEstudioRadiosPotenciales(String estudioRadiosPotenciales) {
        this.estudioRadiosPotenciales = estudioRadiosPotenciales;
    }

    public String getEstudioInteraccionesRiesgo() {
        return estudioInteraccionesRiesgo;
    }

    public void setEstudioInteraccionesRiesgo(String estudioInteraccionesRiesgo) {
        this.estudioInteraccionesRiesgo = estudioInteraccionesRiesgo;
    }

    public String getEstudioEfectosAreaInflu() {
        return estudioEfectosAreaInflu;
    }

    public void setEstudioEfectosAreaInflu(String estudioEfectosAreaInflu) {
        this.estudioEfectosAreaInflu = estudioEfectosAreaInflu;
    }

    public String getEstudioRecomendacionesTecOp() {
        return estudioRecomendacionesTecOp;
    }

    public void setEstudioRecomendacionesTecOp(String estudioRecomendacionesTecOp) {
        this.estudioRecomendacionesTecOp = estudioRecomendacionesTecOp;
    }

    public String getEstudioSistemasSeguridad() {
        return estudioSistemasSeguridad;
    }

    public void setEstudioSistemasSeguridad(String estudioSistemasSeguridad) {
        this.estudioSistemasSeguridad = estudioSistemasSeguridad;
    }

    public String getEstudioMedidasPreventivas() {
        return estudioMedidasPreventivas;
    }

    public void setEstudioMedidasPreventivas(String estudioMedidasPreventivas) {
        this.estudioMedidasPreventivas = estudioMedidasPreventivas;
    }

//    public Proyecto getProyecto() {
//        return proyecto;
//    }
//
//    public void setProyecto(Proyecto proyecto) {
//        this.proyecto = proyecto;
//    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (estudioRiesgoProyectoPK != null ? estudioRiesgoProyectoPK.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof EstudioRiesgoProyecto)) {
            return false;
        }
        EstudioRiesgoProyecto other = (EstudioRiesgoProyecto) object;
        if ((this.estudioRiesgoProyectoPK == null && other.estudioRiesgoProyectoPK != null) || (this.estudioRiesgoProyectoPK != null && !this.estudioRiesgoProyectoPK.equals(other.estudioRiesgoProyectoPK))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "mx.gob.semarnat.model.EstudioRiesgoProyecto[ estudioRiesgoProyectoPK=" + estudioRiesgoProyectoPK + " ]";
    }

    /**
     * @return the estudioSenalaConclusion
     */
    public String getEstudioSenalaConclusion() {
        return estudioSenalaConclusion;
    }

    /**
     * @param estudioSenalaConclusion the estudioSenalaConclusion to set
     */
    public void setEstudioSenalaConclusion(String estudioSenalaConclusion) {
        this.estudioSenalaConclusion = estudioSenalaConclusion;
    }

    /**
     * @return the estudioResumenSituacion
     */
    public String getEstudioResumenSituacion() {
        return estudioResumenSituacion;
    }

    /**
     * @param estudioResumenSituacion the estudioResumenSituacion to set
     */
    public void setEstudioResumenSituacion(String estudioResumenSituacion) {
        this.estudioResumenSituacion = estudioResumenSituacion;
    }

    /**
     * @return the estudioInformeTecnico
     */
    public String getEstudioInformeTecnico() {
        return estudioInformeTecnico;
    }

    /**
     * @param estudioInformeTecnico the estudioInformeTecnico to set
     */
    public void setEstudioInformeTecnico(String estudioInformeTecnico) {
        this.estudioInformeTecnico = estudioInformeTecnico;
    }
    
}
