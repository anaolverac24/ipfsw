package mx.gob.semarnat.model;


import java.io.Serializable;
import javax.persistence.*;


/**
 * The persistent class for the PROY_SUBSECTOR_GUIAS database table.
 * 
 */
@Entity
@Table(name="PROY_SUBSECTOR_GUIAS")
@NamedQuery(name="ProySubsectorGuias.findAll", query="SELECT p FROM ProySubsectorGuias p")
public class ProySubsectorGuias implements Serializable {
	private static final long serialVersionUID = 1L;

	@EmbeddedId
	private ProySubsectorGuiasPK id;

	public ProySubsectorGuias() {
	}

	public ProySubsectorGuiasPK getId() {
		return this.id;
	}

	public void setId(ProySubsectorGuiasPK id) {
		this.id = id;
	}

}
