/**
 * 
 */
package mx.gob.semarnat.model;

import java.io.Serializable;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * @author dpaniagua.
 *
 */
@Entity
@Table(name = "CAT_NORMA_ASOCIACION")
@XmlRootElement
public class CatNormaAsociacion implements Serializable {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "ASOCIACION_ID", unique = true, nullable = false)
	private Short asociacionId;
	
    @Column(name = "NORMA_ID")
	private Short normaId;
	
    @Column(name = "NSEC")
	private Short nsec;
	
    @Column(name = "NSUB")
	private Short nsub;

    @Column(name = "NRAMA")
	private Short nrama;
	
    @Column(name = "NTIPO")
	private Short ntipo;

	/**
	 * @return the asociacionId
	 */
	public Short getAsociacionId() {
		return asociacionId;
	}

	/**
	 * @param asociacionId the asociacionId to set
	 */
	public void setAsociacionId(Short asociacionId) {
		this.asociacionId = asociacionId;
	}

	/**
	 * @return the normaId
	 */
	public Short getNormaId() {
		return normaId;
	}

	/**
	 * @param normaId the normaId to set
	 */
	public void setNormaId(Short normaId) {
		this.normaId = normaId;
	}

	/**
	 * @return the nsec
	 */
	public Short getNsec() {
		return nsec;
	}

	/**
	 * @param nsec the nsec to set
	 */
	public void setNsec(Short nsec) {
		this.nsec = nsec;
	}

	/**
	 * @return the nsub
	 */
	public Short getNsub() {
		return nsub;
	}

	/**
	 * @param nsub the nsub to set
	 */
	public void setNsub(Short nsub) {
		this.nsub = nsub;
	}

	/**
	 * @return the nrama
	 */
	public Short getNrama() {
		return nrama;
	}

	/**
	 * @param nrama the nrama to set
	 */
	public void setNrama(Short nrama) {
		this.nrama = nrama;
	}

	/**
	 * @return the ntipo
	 */
	public Short getNtipo() {
		return ntipo;
	}

	/**
	 * @param ntipo the ntipo to set
	 */
	public void setNtipo(Short ntipo) {
		this.ntipo = ntipo;
	}
}
