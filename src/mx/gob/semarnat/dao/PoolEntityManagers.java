/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.gob.semarnat.dao;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

/**
 *
 * @author mauricio
 */
public class PoolEntityManagers {

    private static EntityManagerFactory emf = Persistence.createEntityManagerFactory("DGIRA_MIAE");
    private static EntityManagerFactory emfCat = Persistence.createEntityManagerFactory("CatalogosPU");
    private static EntityManagerFactory emfSinatec = Persistence.createEntityManagerFactory("SINATEC");

    /**
     * @return the emf
     */
    public static EntityManagerFactory getEmf() {
        return emf;
    }

    /**
     * @param aEmf the emf to set
     */
    public static void setEmf(EntityManagerFactory aEmf) {
        emf = aEmf;
    }

    /**
     * @return the emfCat
     */
    public static EntityManagerFactory getEmfCat() {
        return emfCat;
    }

    /**
     * @param aEmfCat the emfCat to set
     */
    public static void setEmfCat(EntityManagerFactory aEmfCat) {
        emfCat = aEmfCat;
    }

   

    public static EntityManagerFactory getEmfSinatec() {
        return emfSinatec;
    }

    public static void setEmfSinatec(EntityManagerFactory emfSinatec) {
        PoolEntityManagers.emfSinatec = emfSinatec;
    }

    
    
}
