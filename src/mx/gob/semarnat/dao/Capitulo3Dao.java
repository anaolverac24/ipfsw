package mx.gob.semarnat.dao;

import java.io.File;
import java.io.Serializable;
import java.nio.file.Files;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.EntityTransaction;
import javax.persistence.Query;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import mx.gob.semarnat.model.AvanceProyecto;
import mx.gob.semarnat.model.AvanceProyectoPK;
import mx.gob.semarnat.model.CatEtapa;
import mx.gob.semarnat.model.Proyecto;
import mx.gob.semarnat.model.SustanciaProyecto;
import mx.gob.semarnat.view.Cap32_Sustan_bean.SustanciaHelper;

/**
 *
 * @author Admin
 */
public class Capitulo3Dao implements Serializable {

    private final EntityManagerFactory emf = PoolEntityManagers.getEmf();
    private EntityManager em = emf.createEntityManager();

    public List<?> listado(Class<?> tipo) {
        CriteriaBuilder cBuilder = em.getCriteriaBuilder();
        CriteriaQuery<Object> cq = cBuilder.createQuery();
        cq.select(cq.from(tipo));
        return em.createQuery(cq).getResultList();
    }

    public Object busca(Class<?> tipo, Object id) {
        return em.find(tipo, id);
    }

    public void agrega(Object o) {
        em.clear();
        EntityTransaction tx = em.getTransaction();
        tx.begin();
        try {
            if (o != null && tx.isActive()) {
                em.persist(o);
                tx.commit();
            }
        } catch (Exception e) {
            e.printStackTrace();
            if (tx.isActive()) {
                tx.rollback();
            }
        }
    }

    public void modifica(Object o) {
        em.clear();
        EntityTransaction tx = em.getTransaction();
        try {
            tx.begin();
            em.merge(o);
            tx.commit();
//        em.close();
        } catch (Exception e) {
            if (tx.isActive()) {
                tx.rollback();
            }
        }
    }

    public void elimina(Class<?> tipo, Object id) {
        em.clear();
        EntityTransaction tx = em.getTransaction();
        Object o = em.find(tipo, id);
        try {
            if (o != null) {
                tx.begin();
                em.remove(o);
                tx.commit();
            }
        } catch (Exception e) {
            if (tx.isActive()) {
                tx.rollback();
            }
        }
//        em.close();
    }

    public List<?> lista_namedQuery(String qry, Object[] args, String[] paramName) {
        Query q = em.createNamedQuery(qry);
        for (int i = 0; i < args.length; i++) {
            q.setParameter(paramName[i], args[i]);
        }
        return q.getResultList();
    }

    public List<?> listado_where(Class<?> tipo, String atributeName, Object o) {
        if (!em.isOpen()) {
            em = PoolEntityManagers.getEmf().createEntityManager();
        }
        CriteriaBuilder cBuilder = em.getCriteriaBuilder();
        CriteriaQuery<Object> cq = cBuilder.createQuery();
        Root<?> r = cq.from(tipo);
        cq.select(r);
        cq.where(cBuilder.equal(r.get(atributeName), o));
        return em.createQuery(cq).getResultList();
    }

    public List<?> listado_where_comp(Class<?> tipo, String atributeName, String nestedAttrib, Object o) {
        if (!em.isOpen()) {
            em = PoolEntityManagers.getEmf().createEntityManager();
        }
        CriteriaBuilder cBuilder = em.getCriteriaBuilder();
        CriteriaQuery<Object> cq = cBuilder.createQuery();
        Root<?> r = cq.from(tipo);
        cq.select(r);
        cq.where(cBuilder.equal(r.get(atributeName).get(nestedAttrib), o));
        return em.createQuery(cq).getResultList();
    }

    public List<?> listado_where_comp(Class<?> tipo, String atributeName, String nestedAttrib, Object o, String orderBy) {
        if (!em.isOpen()) {
            em = PoolEntityManagers.getEmf().createEntityManager();
        }
        CriteriaBuilder cBuilder = em.getCriteriaBuilder();
        CriteriaQuery<Object> cq = cBuilder.createQuery();
        Root<?> r = cq.from(tipo);
        cq.select(r);
        cq.where(cBuilder.equal(r.get(atributeName).get(nestedAttrib), o));
        cq.orderBy(cBuilder.asc(r.get(orderBy)));
        return em.createQuery(cq).getResultList();
    }

    public List<?> listado_where_comp(Class<?> tipo, String[] atributeName, String[] nestedAttrib, Object[] o) {
        if (!em.isOpen()) {
            em = PoolEntityManagers.getEmf().createEntityManager();
        }
        CriteriaBuilder cBuilder = em.getCriteriaBuilder();
        CriteriaQuery<Object> cq = cBuilder.createQuery();
        Root<?> r = cq.from(tipo);
        cq.select(r);
        cq.where(cBuilder.equal(r.get(atributeName[0]).get(nestedAttrib[0]), o[0]),
                cBuilder.equal(r.get(atributeName[1]).get(nestedAttrib[1]), o[1]));
        return em.createQuery(cq).getResultList();
    }

    public List<?> listado_like(Class<?> tipo, String atributeName, Object o) {

        System.out.println("tipo: " + tipo.getName());
        System.out.println("att: " + atributeName);
        System.out.println("obj: " + o.getClass().getName() + " " + o.toString());

        Query q = em.createQuery("SELECT o FROM " + tipo.getName() + " o WHERE UPPER(o." + atributeName + ") like '" + o + "' ");

        return q.getResultList();
//        CriteriaBuilder cBuilder = em.getCriteriaBuilder();
//        CriteriaQuery<Object> cq = cBuilder.createQuery();
//        Root<?> r = cq.from(tipo);
//        cq.select(r);
//        Expression<String> path = r.get(atributeName);
//        cq.where(cBuilder.like(path, o.toString()));
//        return em.createQuery(cq).getResultList();
    }

    public Object objeto_where(Class<?> tipo, String atributeName, Object o) {
        CriteriaBuilder cBuilder = em.getCriteriaBuilder();
        CriteriaQuery<Object> cq = cBuilder.createQuery();
        Root<?> r = cq.from(tipo);
        cq.select(r);
        cq.where(cBuilder.equal(r.get(atributeName), o));
        return em.createQuery(cq).getSingleResult();
    }

    public List<?> listado_where_AND(Class<?> tipo, String[] atributeName, Object[] o) {
        CriteriaBuilder cBuilder = em.getCriteriaBuilder();
        CriteriaQuery<Object> cq = cBuilder.createQuery();
        Root<?> r = cq.from(tipo);
        cq.select(r);
        cq.where(cBuilder.equal(r.get(atributeName[0]), o[0]),
                cBuilder.equal(r.get(atributeName[1]), o[1]));
        return em.createQuery(cq).getResultList();
    }

    public List<?> listado_BETWEEN(Class<?> tipo, String atributeName, Date f1, Date f2) {
        CriteriaBuilder cBuilder = em.getCriteriaBuilder();
        CriteriaQuery<Object> cq = cBuilder.createQuery();
        Root<?> r = cq.from(tipo);
        cq.select(r);
        cq.where(cBuilder.between(r.<Date>get(atributeName), f1, f2));
        return em.createQuery(cq).getResultList();
    }

    public void guardarAvance(Proyecto proyecto, Short avance, String capitulo) {
        em.getTransaction().begin();
//        AvanceProyecto ap = new AvanceProyecto(proyecto.getProyectoPK().getFolioProyecto(), proyecto.getProyectoPK().getSerialProyecto());
        AvanceProyecto ap = (AvanceProyecto) busca(AvanceProyecto.class, new AvanceProyectoPK(proyecto.getProyectoPK().getFolioProyecto(), proyecto.getProyectoPK().getSerialProyecto(), new Short(capitulo)));
        if (ap == null) {
            ap = new AvanceProyecto(new AvanceProyectoPK(proyecto.getProyectoPK().getFolioProyecto(), proyecto.getProyectoPK().getSerialProyecto(), new Short(capitulo)));
            ap.setAvance(avance);
            em.persist(ap);
        } else {
//            ap.setAvance(new Short("" + (avance + ap.getAvance())));
            ap.setAvance(avance);
            em.merge(ap);
        }
        em.getTransaction().commit();
    }

    public void eliminaSustancias(Short serial, String folio, CatEtapa etapa) {
        em.clear();
        em.getTransaction().begin();
        Query q = em.createQuery("DELETE FROM SustanciaProyecto s WHERE s.sustanciaProyectoPK.folioProyecto = :folioProyecto AND "
                + "s.sustanciaProyectoPK.serialProyecto = :serialProyecto AND "
                + "s.etapaId = :etapaId");
        q.setParameter("folioProyecto", folio);
        q.setParameter("serialProyecto", serial);
        q.setParameter("etapaId", etapa);
        q.executeUpdate();
        em.getTransaction().commit();
    }

    public void eliminaImpacto(Short serial, String folio, CatEtapa etapa) {
        em.clear();
        em.getTransaction().begin();
        Query q = em.createQuery("DELETE FROM ImpactoProyecto i WHERE i.impactoProyectoPK.folioProyecto = :folioProyecto AND "
                + "i.impactoProyectoPK.serialProyecto = :serialProyecto AND "
                + "i.etapaId = :etapaId");
        q.setParameter("folioProyecto", folio);
        q.setParameter("serialProyecto", serial);
        q.setParameter("etapaId", etapa);
        q.executeUpdate();
        em.getTransaction().commit();
    }
    
    public void eliminarAnexo(SustanciaProyecto sustancia){
        em.clear();
        em.getTransaction().begin();
        
            Query q = em.createNativeQuery("SELECT anexo_url FROM sustancia_anexo where folio_proyecto=:folio and serial_proyecto=:serial and sustancia_proy_id=:idt");
            q.setParameter("folio", sustancia.getSustanciaProyectoPK().getFolioProyecto());
            q.setParameter("serial", sustancia.getSustanciaProyectoPK().getSerialProyecto());
            q.setParameter("idt", sustancia.getSustanciaProyectoPK().getSustProyId());
            ArrayList<Object> lst = (ArrayList<Object>)q.getResultList();
            if (lst != null && !lst.isEmpty()){
                String anexoUrl = (String)lst.get(0);
                
                eliminaArchivoRemoto(anexoUrl);
            }        
        em.close();
    }
    
    private void eliminaArchivoRemoto(String rutaArch) {
        try {
            File file = new File(rutaArch);
            Files.deleteIfExists(file.toPath());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }    
}
