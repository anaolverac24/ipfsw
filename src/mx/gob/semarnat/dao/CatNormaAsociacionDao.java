/**
 * 
 */
package mx.gob.semarnat.dao;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.Query;

import mx.gob.semarnat.model.CatNormaAsociacion;
import mx.gob.semarnat.model.catalogos.CatUnidadMedida;

/**
 * @author Diego
 *
 */
public class CatNormaAsociacionDao {
    private EntityManager enMgr = PoolEntityManagers.getEmf().createEntityManager();
    
    /**
     * Permite consultar la lista de normas asociacion. 
     * @return la lista de normas asociacion.
     */
    public List<CatNormaAsociacion> consultaNormasAsociacion() throws Exception {
        Query query = enMgr.createQuery("FROM CatNormaAsociacion");
        List<CatNormaAsociacion> catNormasAsociacion = (List<CatNormaAsociacion>)query.getResultList();
        return catNormasAsociacion;
    }
    
    /**
     * Permite consultar el detalle de la norma asociacion por id de la norma seleccionada
     * @param normaSeleccionada de la asociacion.
     * @return la norma asociacion.
     */
    public CatNormaAsociacion consultarNormaAsociacionPorID(Short normaSeleccionada) throws Exception {
        Query query = enMgr.createQuery("FROM CatNormaAsociacion where normaId = " + normaSeleccionada);
        CatNormaAsociacion catNormasAsociacion = (CatNormaAsociacion)query.getSingleResult();
        return catNormasAsociacion;		
	}
}
