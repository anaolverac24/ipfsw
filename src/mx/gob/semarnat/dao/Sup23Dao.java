/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.gob.semarnat.dao;

import java.io.Serializable;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.EntityTransaction;
import javax.persistence.Persistence;
import javax.persistence.Query;
import mx.gob.semarnat.model.CatParqueIndustrial;
import mx.gob.semarnat.model.CatPdu;
import mx.gob.semarnat.model.CatPoet;

/**
 *
 * @author Rengerden
 */
public class Sup23Dao implements Serializable {

//    public static EntityManagerFactory emf = PoolEntityManagers.getEmf();
    public EntityManager em = PoolEntityManagers.getEmf().createEntityManager();

    public List<CatPoet> poets(short poetId) {

//        EntityManager em = emf.createEntityManager();
        Query q = em.createQuery("SELECT c FROM CatPoet c WHERE c.poetId = " + poetId);
        List l = q.getResultList();
//        em.close();
        return l;
    }

    public List<CatPdu> pdus(short pduId) {
//        EntityManager em = emf.createEntityManager();
        Query q = em.createQuery("SELECT c FROM CatPdu c WHERE c.pduId = " + pduId);
        List l = q.getResultList();
//        em.close();
        return l;

    }

    public List<CatParqueIndustrial> parques(short parqueId) {
//        EntityManager em = emf.createEntityManager();
        Query q = em.createQuery("SELECT c FROM CatParqueIndustrial c WHERE c.parqueId = " + parqueId);
        List l = q.getResultList();
//        em.close();
        return l;
    }

    public <T> List<T> getListObject(Object o) {
//        EntityManager em = emf.createEntityManager();
        List l = em.createQuery("SELECT o FROM " + o.getClass().getName() + " o ").getResultList();
//        em.close();

        return l;
    }

    public Object getObject(Object o, String id) {
//        EntityManager em = emf.createEntityManager();
        Object ob = em.find(o.getClass(), id);
//        em.close();

        return ob;
    }

    public Object getObject(Object o, Integer id) {
//        EntityManager em = emf.createEntityManager();
        Object ob = em.find(o.getClass(), id);
//        em.close();

        return ob;
    }
//
//    public void persist(Object object) {
//        EntityManager em = emf.createEntityManager();
//        try {
//            em.getTransaction().begin();
//            em.persist(object);
//            em.getTransaction().commit();
//        } catch (Exception e) {
//            Logger.getLogger(getClass().getName()).log(Level.SEVERE, "exception caught", e);
//            em.getTransaction().rollback();
//        } finally {
//            em.close();
//        }
//    }

    public List<CatPdu> pdus() {
//        EntityManager em = emf.createEntityManager();
        Query q = em.createQuery("SELECT c FROM CatPdu c WHERE c.pduId =  pduId");
        List l = q.getResultList();
//        em.close();
        return l;

    }

    public List<CatParqueIndustrial> parques() {
//        EntityManager em = emf.createEntityManager();
        Query q = em.createQuery("SELECT c FROM CatParqueIndustrial c WHERE c.parqueId =  parqueId");
        List l = q.getResultList();
//        em.close();
        return l;
    }

    public void agrega(Object o) {
        EntityTransaction tx = em.getTransaction();
        tx.begin();
        try {
            if (o != null && tx.isActive()) {
                em.persist(o);
                tx.commit();
                System.out.println("Guardado");
            }
        } catch (Exception e) {
            if (tx.isActive()) {
                tx.rollback();
                e.printStackTrace();
            }
        }
    }

}
