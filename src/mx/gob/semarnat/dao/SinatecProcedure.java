/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.gob.semarnat.dao;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Rodrigo
 */
public class SinatecProcedure {

    private final String PSTFOLIOAVANCE_RG = "{ call  SINATEC.PSTFOLIOAVANCE_RG(?, ?, ?, ?) }";
    private final String PSTFOLIODOCSATLOTEFIN_RG = "{ call SINATEC.PSTFOLIODOCSATLOTEFIN_RG(?, ?, ?, ?) }";
    private final String PSTFOLIODOCSATLOTE_RG = "{ call SINATEC.PSTFOLIODOCSATLOTE_RG(?, ?, ?, ?, ?) }";
    private final String PSTFOLIODOCSATELITAL_RG = "{ call SINATEC.PSTFOLIODOCSATELITAL_RG(?, ?, ?, ?, ?, ?) }";

    private Connection connection = null;
    final String driverDB = "oracle.jdbc.OracleDriver";
    //================= POOL DE DESARROLLO ==========================
    final String urlDB = "jdbc:oracle:thin:@scan-db.semarnat.gob.mx:1525/SINDESA";
    final String userDB = "DGIRA_MIAE2";
    final String userDB_dg = "DGIRA_MIAE2";
    final String passDB = "DGIRA_MIAE2";
    final String passDB_dg = "DGIRA_MIAE2";
    //================= POOL DE PRODUCCION ==========================
    /*final String urlDB = "jdbc:oracle:thin:@rac1-scan.semarnat.gob.mx:1525/SINAT";
    final String userDB = "DGIRA_MIAE2";
    final String userDB_dg = "DGIRA_MIAE2";
    final String passDB = "Dg1r4MiaE2";
    final String passDB_dg = "Dg1r4MiaE2";*/
    
    private Connection getConexion() throws ClassNotFoundException, SQLException {
        Class.forName(driverDB);
        connection = DriverManager.getConnection(urlDB, userDB, passDB);
        return connection;
    }
    
    private Connection getConexionDgira() throws ClassNotFoundException, SQLException{
        Class.forName(driverDB);
        connection = DriverManager.getConnection(urlDB, userDB_dg, passDB_dg);
        return connection;
    }

    private void closeConnection() throws SQLException {
        connection.close();
    }

    //SP de carga de avance del proyecto
    public String ejecutaSP(Integer folio, Integer avance) {
        String msj = "err";
        try {
            //guarda avance en SINATEC
            CallableStatement cs = getConexion().prepareCall(PSTFOLIOAVANCE_RG);
            cs.setInt(1, folio);
            cs.setInt(2, avance);
            cs.registerOutParameter(3, java.sql.Types.NUMERIC);
            cs.registerOutParameter(4, java.sql.Types.VARCHAR);
            cs.execute();
            msj = cs.getString(4);
            closeConnection();
            return msj;
        } catch (ClassNotFoundException ex) {
            Logger.getLogger(SinatecProcedure.class.getName()).log(Level.SEVERE, null, ex);
            return msj;
        } catch (SQLException ex) {
            Logger.getLogger(SinatecProcedure.class.getName()).log(Level.SEVERE, null, ex);
            return msj;
        }
    }

    //SP final del trámite
    public int sp_fin(Long folio, Integer noArchivos) {
        CallableStatement cs;
        String msj = "err";
        int err=1;
        try {
            cs = getConexion().prepareCall(PSTFOLIODOCSATLOTEFIN_RG);
            cs.setLong(1, folio);
            cs.setInt(2, noArchivos);
            cs.registerOutParameter(3, java.sql.Types.NUMERIC);//==0?registro correcto : error
            cs.registerOutParameter(4, java.sql.Types.VARCHAR);//motivo de error
            cs.execute();
            msj = cs.getString(4);
            err = cs.getInt(3);
            closeConnection();
            if (err != 0) {
                System.out.println("Error en " + this.getClass().getName() + " descripción:{" + msj + "}");
            }
            return err;
        } catch (ClassNotFoundException ex) {
            Logger.getLogger(SinatecProcedure.class.getName()).log(Level.SEVERE, null, ex);
            return err;
        } catch (SQLException ex) {
            Logger.getLogger(SinatecProcedure.class.getName()).log(Level.SEVERE, null, ex);
            return err;
        }
    }

    /*El objetivo de este procedimiento es solicitar un número de Lote para 
     registrar todos los documentos y se ejecutará cuando el Sistema 
     Satelital está listo para  cargar los documentos electrónicos temáticos en el 
     Sistema Gestor de Trámites, a través de éste procedimiento solicita un lote, 
     el procedimiento responderá un número de lote que con el cual se relacionará con 
     los archivos tanto en su primer envío o cuando existan actualización posteriores.
     */
    public String solicitudCarga(Integer folio, String descripcion, String archivo, Integer loteOut) {
        CallableStatement cs;

        Integer lote, resultado;
        String msj = "err";
        if (loteOut == null) {
            try {
                cs = getConexion().prepareCall(PSTFOLIODOCSATLOTE_RG);
                cs.setInt(1, folio);
                cs.setString(2, descripcion);
                cs.registerOutParameter(3, java.sql.Types.NUMERIC);//lote
                cs.registerOutParameter(4, java.sql.Types.NUMERIC);//==0?registro correcto : error
                cs.registerOutParameter(5, java.sql.Types.VARCHAR);//motivo de error
                cs.execute();
                lote = cs.getInt(3);
                resultado = cs.getInt(4);
                msj = cs.getString(5);
                closeConnection();
                if (resultado == 0 && (descripcion.compareTo("FINALIZA_TRAMITE") != 0)) {//si no falla el SP carga los archivos
                    Statement statement = getConexionDgira().createStatement();
                    String sql = String.format("UPDATE Proyecto SET PROY_LOTE=%d WHERE FOLIO_PROYECTO='%s'", new Object[]{lote, folio});
                    System.out.println("query update lote tabla Proyecto: " + sql);
                    statement.executeUpdate(sql);
                    System.out.println("-------Procedimeinto SINATEC PSTFOLIODOCSATELITAL_RG--------");
                    msj = cargaArchivo(lote, folio, descripcion, archivo);
                    closeConnection();
                }else if (resultado == 0 && (descripcion.compareTo("FINALIZA_TRAMITE") == 0)) {
                    Statement statement = getConexionDgira().createStatement();
                    String sql = String.format("UPDATE EVALUACION_PROYECTO SET EVA_LOTE_INFO_ADICIONAL=%d WHERE BITACORA_PROYECTO='%s'", new Object[]{lote, archivo});
                    System.out.println("query update tabla evaluacion_proyecto: " + sql);
                    statement.executeUpdate(sql);
                    System.out.println("-------Procedimeinto SINATEC PSTFOLIODOCSATELITAL_RG--------");
                    msj = cargaArchivo(lote, folio, descripcion, archivo);
                    closeConnection();                    
                }else {

                    System.out.println("Mensaje ERROR!!!!!!!!!!: " + msj);
                }

                return msj;
            } catch (ClassNotFoundException ex) {
                Logger.getLogger(SinatecProcedure.class.getName()).log(Level.SEVERE, null, ex);
                return msj;
            } catch (SQLException ex) {
                Logger.getLogger(SinatecProcedure.class.getName()).log(Level.SEVERE, null, ex);
                return msj;
            }
        } else {
            System.out.println("-------Procedimeinto SINATEC PSTFOLIODOCSATELITAL_RG--------");
            msj = cargaArchivo(loteOut, folio, descripcion, archivo);
        }
        return msj;
    }
    /*
     El objetivo de éste procedimiento es permitir registrar en la base de datos SINATEC, 
     los documentos electrónicos temáticos, recibidos en el Sistema Satelital, 
     El procedimiento se debe de ejecutar por cada archivo que se tenga.
     */

    private String cargaArchivo(Integer lote, Integer folio, String descripcion, String archivo) {
        CallableStatement cs;
        String msj = "err";
        int err;

        try {
            cs = getConexion().prepareCall(PSTFOLIODOCSATELITAL_RG);
            cs.setInt(1, lote);
            cs.setInt(2, folio);
            cs.setString(3, descripcion);
            cs.setString(4, archivo);
            cs.registerOutParameter(5, java.sql.Types.NUMERIC);//==0?registro correcto : error
            cs.registerOutParameter(6, java.sql.Types.VARCHAR);//motivo de error
            cs.execute();

            msj = cs.getString(6);
            err = cs.getInt(5);
            closeConnection();
            if (err != 0) {
                System.out.println("Error!!!!!!! AQUI: " + msj);
            }
            System.out.println("Carga de archivo: " + archivo + "  " + msj);
            return msj;
        } catch (ClassNotFoundException ex) {
            Logger.getLogger(SinatecProcedure.class.getName()).log(Level.SEVERE, null, ex);
            return msj;
        } catch (SQLException ex) {
            Logger.getLogger(SinatecProcedure.class.getName()).log(Level.SEVERE, null, ex);
            return msj;
        }
    }
}
