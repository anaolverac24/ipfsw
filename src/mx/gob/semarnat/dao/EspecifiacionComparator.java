/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.gob.semarnat.dao;

import java.util.Comparator;
import mx.gob.semarnat.model.EspecificacionProyecto;

/**
 *
 * @author mauricio
 */
public class EspecifiacionComparator implements Comparator<EspecificacionProyecto> {

    @Override
    public int compare(EspecificacionProyecto o1, EspecificacionProyecto o2) {
        if ( o1 != null ){
            return 0;
        }
        if ( o2 != null ){
            return 0;
        }
        return o1.getCatEspecificacion().getEspecificacionOrden() - o2.getCatEspecificacion().getEspecificacionOrden();
    }

}
