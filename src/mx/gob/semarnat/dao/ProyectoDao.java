/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.gob.semarnat.dao;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.ArrayList;
import javax.persistence.EntityManager;
import javax.persistence.EntityTransaction;
import javax.persistence.NoResultException;
import javax.persistence.Query;
import mx.gob.semarnat.model.CatCapitulo;
import mx.gob.semarnat.model.CatSeccion;
import mx.gob.semarnat.model.CatSubcapitulo;
import mx.gob.semarnat.model.EstudioRiesgoProyecto;
import mx.gob.semarnat.model.EvaluacionProyecto;
import mx.gob.semarnat.model.Proyecto;
import mx.gob.semarnat.model.RespTecProyecto;

/**
 *
 * @author mauricio
 */
public class ProyectoDao implements Serializable {

    private final EntityManager em = PoolEntityManagers.getEmf().createEntityManager();
 
    public String avanceProyecto(Proyecto p) {
        Query q = em.createQuery("SELECT sum(a.avance) FROM AvanceProyecto a where a.avanceProyectoPK.folioProyecto = :folio and a.avanceProyectoPK.serialProyeco = :serial");
        q.setParameter("folio", p.getProyectoPK().getFolioProyecto());
        q.setParameter("serial", p.getProyectoPK().getSerialProyecto());
        Object a = q.getSingleResult();

        if (a == null) {
            return "0";
        }
        System.out.println("a " + a.toString());
        
        return a.toString();
    }
    
    public Proyecto cargaProyecto(String folio) {
        EntityManager em = PoolEntityManagers.getEmf().createEntityManager();

        Query q = em.createQuery("SELECT p FROM Proyecto p WHERE p.proyectoPK.folioProyecto = :folioProyecto");
        q.setParameter("folioProyecto", folio);
        Object o = null;

        try {
            o = q.getSingleResult();
        } catch (NoResultException e) {
            System.out.println("EROR: Folio no encontrado");
        } catch (Exception e) {
            System.out.println("" + e.getLocalizedMessage());
            e.printStackTrace();
        }

//        em.close();
        em.clear();

        return (Proyecto) o;
    }

    public void persistProyecto(Proyecto p) {
        EntityManager em = PoolEntityManagers.getEmf().createEntityManager();
        EntityTransaction tx = em.getTransaction();

        tx.begin();
        try {
            em.persist(p);
            tx.commit();
            System.out.println("Proyecto guardado .. ok ");
        } catch (Exception e) {
            e.printStackTrace();
            tx.rollback();
        }
//        em.close();
        em.clear();
    }

    public EstudioRiesgoProyecto cargaEstudioRiesgo(Proyecto proyecto) {
        EstudioRiesgoProyecto e = null;

        System.out.println("Folio " + proyecto.getProyectoPK().getFolioProyecto());
        System.out.println("Serial " + proyecto.getProyectoPK().getFolioProyecto());

        try {
            Query q = em.createQuery("SELECT e FROM EstudioRiesgoProyecto e WHERE e.estudioRiesgoProyectoPK.folioProyecto = :folio and e.estudioRiesgoProyectoPK.serialProyecto = :serial");
            q.setParameter("folio", proyecto.getProyectoPK().getFolioProyecto());
            q.setParameter("serial", proyecto.getProyectoPK().getSerialProyecto());
            e = (EstudioRiesgoProyecto) q.getSingleResult();
//            e = em.find(EstudioRiesgoProyecto.class, new EstudioRiesgoProyectoPK(proyecto.getProyectoPK().getFolioProyecto(), proyecto.getProyectoPK().getSerialProyecto()));
        } catch (NoResultException err) {
            e = new EstudioRiesgoProyecto(proyecto.getProyectoPK().getFolioProyecto(), proyecto.getProyectoPK().getSerialProyecto());
        }

        if (e == null) {
            e = new EstudioRiesgoProyecto(proyecto.getProyectoPK().getFolioProyecto(), proyecto.getProyectoPK().getSerialProyecto());

        }

        return e;
    }

    public Proyecto proyecto(String folio) {

        Query q = em.createQuery("SELECT p FROM Proyecto p WHERE p.proyectoPK.folioProyecto = :folioProyecto");
        q.setParameter("folioProyecto", folio);
        Object o = null;

        try {
            o = q.getSingleResult();
        } catch (NoResultException e) {
            System.out.println("EROR: Folio no encontrado");
        } catch (Exception e) {
            System.out.println("" + e.getLocalizedMessage());
            e.printStackTrace();
        }
        return (Proyecto) o;
    }

    public void persistObject(Object p) {
        EntityTransaction tx = em.getTransaction();

        tx.begin();
        try {
            em.persist(p);
            tx.commit();
            System.out.println("Objeto guardada: ");
        } catch (Exception e) {
            e.printStackTrace();
            tx.rollback();
        }

    }

    public String getAyuda(short nsub, short capitulo, short subCapitulo, short seccion) {
        String ayuda = "";

        if (subCapitulo == -1 && seccion == -1) {
            System.out.println("capitulo");
            System.out.println("capitulo " + capitulo);
            System.out.println("nsub " + nsub);
            Query q = em.createQuery("SELECT c from CatCapitulo c where c.catCapituloPK.capituloId = :cap and c.catCapituloPK.nsub = :nsub ");
            q.setParameter("cap", capitulo);
            q.setParameter("nsub", nsub);
            CatCapitulo c = (CatCapitulo) q.getSingleResult();
            ayuda = c.getCapituloAyuda();
        }

        if (subCapitulo > -1 && seccion == -1) {
            System.out.println("subcapitulo");
            Query q = em.createQuery("SELECT c from CatSubcapitulo c where c.catSubcapituloPK.capituloId = :cap and c.catSubcapituloPK.nsub = :nsub and c.catSubcapituloPK.subcapituloId = :sub");
            q.setParameter("cap", capitulo);
            q.setParameter("nsub", nsub);
            q.setParameter("sub", subCapitulo);
            CatSubcapitulo c = (CatSubcapitulo) q.getSingleResult();
            ayuda = c.getSupcapituloAyuda();
        }

        if (subCapitulo > -1 && seccion > -1) {
            System.out.println("todos");
            Query q = em.createQuery("SELECT c from CatSeccion c where c.catSeccionPK.capituloId = :cap and c.catSeccionPK.nsub = :nsub and c.catSeccionPK.subcapituloId = :sub and c.catSeccionPK.seccionId = :sec");
            q.setParameter("cap", capitulo);
            q.setParameter("nsub", nsub);
            q.setParameter("sub", subCapitulo);
            q.setParameter("sec", seccion);
            CatSeccion c = (CatSeccion) q.getSingleResult();
            ayuda = c.getSeccionAyuda();
        }

        em.clear();
        return ayuda;
    }
   
    public RespTecProyecto cargaRespTec(Proyecto proyecto) {
        RespTecProyecto e = null;
        //EntityManager em = emf1.createEntityManager();
        
        System.out.println("Folio " + proyecto.getProyectoPK().getFolioProyecto());
        System.out.println("Serial " + proyecto.getProyectoPK().getFolioProyecto());
               
        try {
            Query q = em.createQuery("SELECT e FROM RespTecProyecto e WHERE e.respTecProyectoPK.folioProyecto = :folioProyecto and e.respTecProyectoPK.serialProyecto = :serialProyecto");
            q.setParameter("folioProyecto", proyecto.getProyectoPK().getFolioProyecto());
            q.setParameter("serialProyecto", proyecto.getProyectoPK().getSerialProyecto());
            e = (RespTecProyecto) q.getSingleResult();

        } catch (NoResultException err) {
            e = new RespTecProyecto(proyecto.getProyectoPK().getFolioProyecto(), proyecto.getProyectoPK().getSerialProyecto());
            e.setRespTecApellido1("");
            e.setRespTecApellido2("");
            e.setRespTecCartaVerdad("");
            e.setRespTecCurp("");
            e.setRespTecNombre("");
            e.setRespTecRfc("");
        }   
          

        if (e == null) {
            e = new RespTecProyecto(proyecto.getProyectoPK().getFolioProyecto(), proyecto.getProyectoPK().getSerialProyecto());
            e.setRespTecApellido1("");
            e.setRespTecApellido2("");
            e.setRespTecCartaVerdad("");
            e.setRespTecCurp("");
            e.setRespTecNombre("");
            e.setRespTecRfc("");
        }

        return e;
    }
    
    public Long conteoAdjuntos(String folio, short serial, String nombre) {
        BigDecimal dRes = BigDecimal.ZERO;
        String strNombre = null;
        boolean bNombre = nombre != null;
        if (bNombre) {
            strNombre = "%" + nombre;
        }
        try {
            EntityManager em = PoolEntityManagers.getEmf().createEntityManager();
            Query q = em.createNativeQuery(
			"select sum(conteo) from " + 
			"( " + 
			"    select count (*) as conteo  " + 
			"        from anexos_proyecto AP  " + 
			"        where AP.folio_proyecto = :folio  " + 
			"          and AP.serial_proyecto = :serial  " + 
			(bNombre ? "          and AP.anexo_nombre like :nombre " : "") + 
			"    union all " + 
			"    select count (*) as conteo  " + 
			"        from especificacion_anexo EA  " + 
			"        where EA.folio_proyecto = :folio  " + 
			"          and EA.serial_proyecto = :serial  " + 
                        (bNombre ? "          and EA.espec_anexo_nombre like :nombre " : "") + 
			"    union all " + 
			"    select count (*) as conteo  " + 
			"        from sustancia_anexo SA  " + 
			"        where SA.folio_proyecto = :folio  " + 
			"          and SA.serial_proyecto = :serial  " + 
                        (bNombre ? "          and SA.anexo_nombre like :nombre " : "") +
//			"    union all " + 
//			"    select count (*) as conteo  " + 
//			"        from estudios_esp_proy EE  " + 
//			"        where EE.folio_serial = :folio  " + 
//			"          and EE.serial_proyecto = :serial  " +
//                        "          and EE.anexo_nombre is not null " + 
//                        (bNombre ? "          and EE.anexo_nombre like :nombre " : "") +                                
			") "            
            );
            
            q.setParameter("serial", serial);
            q.setParameter("folio", folio);
            if (bNombre) {
                q.setParameter("nombre", strNombre);
            }
            dRes = (BigDecimal) q.getSingleResult();
            em.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return dRes.longValue();
    }    
    
    public ArrayList<Object[]> obtenerAdjuntos(String folio, short serial) {
        ArrayList<Object[]> lstAdjuntos = null;

        try {
            EntityManager em = PoolEntityManagers.getEmf().createEntityManager();
            Query q = em.createNativeQuery(
			"select * from " + 
			"( " + 
			"    select coalesce(to_char(AP.anexo_descripcion), '') as descripcion, AP.anexo_nombre as nombre " + 
			"        from anexos_proyecto AP  " + 
			"        where AP.folio_proyecto = :folio  " + 
			"          and AP.serial_proyecto = :serial  " + 
			"    union all " + 
			"    select coalesce(to_char(EA.espec_anexo_descripcion), '') as descripcion, EA.espec_anexo_nombre as nombre " + 
			"        from especificacion_anexo EA  " + 
			"        where EA.folio_proyecto = :folio  " + 
			"          and EA.serial_proyecto = :serial  " + 
			"    union all " + 
			"    select coalesce(to_char(SA.anexo_desc), '') as descripcion, SA.anexo_nombre as nombre " + 
			"        from sustancia_anexo SA  " + 
			"        where SA.folio_proyecto = :folio  " + 
			"          and SA.serial_proyecto = :serial  " + 
			") "            
            );
            
            q.setParameter("serial", serial);
            q.setParameter("folio", folio);
            
            lstAdjuntos = (ArrayList<Object[]>)q.getResultList();
            em.close();
            
        } catch (Exception e) {
            e.printStackTrace();
        }
        
        return lstAdjuntos;
    }
    
    public short numRegAnexo() {
        int r = 0;
        try {
            EntityManager em = PoolEntityManagers.getEmf().createEntityManager();
            Query q = em.createQuery("SELECT max(a.anexosProyectoPK.anexoId) FROM AnexosProyecto a");// a WHERE a.anexosProyectoPK.apartadoId = :apartado and a.anexosProyectoPK.capituloId = :capitulo and a.anexosProyectoPK.folioProyecto = :folio and a.anexosProyectoPK.serialProyecto = :serial and a.anexosProyectoPK.seccionId = :seccion and a.anexosProyectoPK.subcapituloId = :subcapitulo");
            r = (Short) q.getSingleResult();
            r++;
            em.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return (short) r;
    }    
    
    public Long obtenerLoteProyecto(String folio, short serial) {
        Long lote = null;
        try {
            EntityManager em = PoolEntityManagers.getEmf().createEntityManager();
            Query q = em.createNativeQuery("select P.PROY_LOTE from proyecto P where P.folio_proyecto =:folio and p.serial_proyecto =:serial ");
            q.setParameter("folio", folio);
            q.setParameter("serial", serial);
            
            BigDecimal bdLote = (BigDecimal)q.getSingleResult();
            lote = bdLote.longValue();
            em.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        
        return lote;
    }  
    
    public EvaluacionProyecto datosEvaProy(String numBita) {
        EntityManager em = PoolEntityManagers.getEmf().createEntityManager();
        Query q = em.createQuery("SELECT a FROM EvaluacionProyecto a WHERE a.bitacoraProyecto=:bitacora");
        q.setParameter("bitacora", numBita);

        Object o = null;
        try {
            o = q.getSingleResult();
            em.close();
        } catch (NoResultException e) {
            System.out.println("EROR: Folio no encontrado");
        } catch (Exception e) {
            System.out.println("" + e.getLocalizedMessage());
            e.printStackTrace();
        }
        return (EvaluacionProyecto) o;
    }
    
    public static void merge(Object object) {
        EntityManager em = PoolEntityManagers.getEmf().createEntityManager();
        try {
            em.getTransaction().begin();
            em.persist(object);
            em.getTransaction().commit();
            em.close();
        } catch (Exception e) {
            e.printStackTrace();
            em.getTransaction().rollback();
        } finally {
        }
    }    
    
}
