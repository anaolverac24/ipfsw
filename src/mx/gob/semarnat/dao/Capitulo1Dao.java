/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.gob.semarnat.dao;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.LinkedList;
import mx.gob.semarnat.model.CatEspecificacion;
import mx.gob.semarnat.model.CatPregunta;
import java.util.List;
import java.util.TreeSet;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.faces.model.SelectItem;
import javax.persistence.EntityManager;
import javax.persistence.Query;
import mx.gob.semarnat.model.AvanceProyecto;
import mx.gob.semarnat.model.CatMineralesReservados;
import mx.gob.semarnat.model.CatNorma;
import mx.gob.semarnat.model.CatObra;
import mx.gob.semarnat.model.CatTipoClima;
import mx.gob.semarnat.model.CatTipoVegetacion;
import mx.gob.semarnat.model.EspecificacionAnexo;
import mx.gob.semarnat.model.EspecificacionProyecto;
import mx.gob.semarnat.model.PreguntaClima;
import mx.gob.semarnat.model.PreguntaMineral;
import mx.gob.semarnat.model.PreguntaObra;
import mx.gob.semarnat.model.PreguntaProyecto;
import mx.gob.semarnat.model.PreguntaVegetacion;
import mx.gob.semarnat.model.Proyecto;
import mx.gob.semarnat.model.ProyectoPK;
import mx.gob.semarnat.model.catalogos.CatUnidadMedida;

/**
 *
 * @author Case Solutions 10
 */
public class Capitulo1Dao implements Serializable {

    private EntityManager enMgr = PoolEntityManagers.getEmf().createEntityManager();
    private ProyectoDao proyectoDao = new ProyectoDao();

    /**
     * Recuperar proyecto
     *
     * @param folioProyecto
     * @param serialProyecto
     * @return
     */
    public Proyecto cargaProyecto(String folioProyecto, short serialProyecto) {
        if (!enMgr.isOpen()) {
            enMgr = PoolEntityManagers.getEmf().createEntityManager();
        }
        return (Proyecto) enMgr.find(Proyecto.class, new ProyectoPK(folioProyecto, serialProyecto));
    }

    public List<PreguntaObra> getObras(String folioProyecto, short serialProyecto) {
        if (!enMgr.isOpen()) {
            enMgr = PoolEntityManagers.getEmf().createEntityManager();
        }
        Query q = enMgr.createQuery("SELECT o from PreguntaObra o where o.preguntaObraPK.folioProyecto = :folio and o.preguntaObraPK.serialProyecto = :serial");
        q.setParameter("folio", folioProyecto);
        q.setParameter("serial", serialProyecto);
        return q.getResultList();
    }

    public CatMineralesReservados getMineral(Short idMineral) {
        if (!enMgr.isOpen()) {
            enMgr = PoolEntityManagers.getEmf().createEntityManager();
        }
        return (CatMineralesReservados) enMgr.find(CatMineralesReservados.class, idMineral);
    }

    public List<PreguntaMineral> getMinerales(String folioProyecto, short serialProyecto) {
        if (!enMgr.isOpen()) {
            enMgr = PoolEntityManagers.getEmf().createEntityManager();
        }
        Query q = enMgr.createQuery("SELECT m FROM PreguntaMineral m WHERE m.preguntaMineralPK.folioProyecto = :folio and m.preguntaMineralPK.serialProyecto = :serial");
        q.setParameter("folio", folioProyecto);
        q.setParameter("serial", serialProyecto);

        return q.getResultList();
    }

    public CatTipoVegetacion getCatTipoVegetacion(Short idV) {
        if (!enMgr.isOpen()) {
            enMgr = PoolEntityManagers.getEmf().createEntityManager();
        }
        return (CatTipoVegetacion) enMgr.find(CatTipoVegetacion.class, idV);
    }

    public CatTipoClima getCatTpoclima(Short idClima) {
        if (!enMgr.isOpen()) {
            enMgr = PoolEntityManagers.getEmf().createEntityManager();
        }
        return (CatTipoClima) enMgr.find(CatTipoClima.class, idClima);
    }

    public PreguntaVegetacion getPreguntaVegetacion(String folioProyecto, short serialProyecto) {
        if (!enMgr.isOpen()) {
            enMgr = PoolEntityManagers.getEmf().createEntityManager();
        }
        Query q = enMgr.createQuery("SELECT p FROM PreguntaVegetacion p WHERE p.preguntaVegetacionPK.folioProyecto = :folio and p.preguntaVegetacionPK.serialProyecto = :serial");
        q.setParameter("folio", folioProyecto);
        q.setParameter("serial", serialProyecto);
        try {
            return (PreguntaVegetacion) q.getSingleResult();
        } catch (Exception e) {

        }
        return new PreguntaVegetacion();

    }

    public PreguntaClima getPreguntaClima(String folioProyecto, short serialProyecto) {
        if (!enMgr.isOpen()) {
            enMgr = PoolEntityManagers.getEmf().createEntityManager();
        }
        Query q = enMgr.createQuery("SELECT p FROM PreguntaClima p WHERE p.preguntaClimaPK.folioProyecto = :folio and p.preguntaClimaPK.serialProyecto = :serial");
        q.setParameter("folio", folioProyecto);
        q.setParameter("serial", serialProyecto);
        try {
            return (PreguntaClima) q.getSingleResult();
        } catch (Exception e) {

        }
        return new PreguntaClima();
//        return (PreguntaClima)enMgr.find(PreguntaProyecto.class, new PreguntaProyectoPK(folioProyecto, serialProyecto, serialProyecto))
    }

    public void limpiarPreguntasProyecto(Proyecto proyecto) {
        if (!enMgr.isOpen()) {
            enMgr = PoolEntityManagers.getEmf().createEntityManager();
        }
        enMgr.getTransaction().begin();

        Query query = enMgr.createQuery("DELETE FROM PreguntaClima c WHERE c.preguntaClimaPK.folioProyecto = :folio AND c.preguntaClimaPK.serialProyecto = :serial");
        query.setParameter("folio", proyecto.getProyectoPK().getFolioProyecto());
        query.setParameter("serial", proyecto.getProyectoPK().getSerialProyecto());
        int deletedCount = query.executeUpdate();

        query = enMgr.createQuery("DELETE FROM EspecificacionAnexo a WHERE a.especificacionAnexoPK.folioProyecto = :folio and a.especificacionAnexoPK.serialProyecto = :serial");
        query.setParameter("folio", proyecto.getProyectoPK().getFolioProyecto());
        query.setParameter("serial", proyecto.getProyectoPK().getSerialProyecto());
        deletedCount = query.executeUpdate();

        query = enMgr.createQuery("DELETE FROM PreguntaClima c WHERE c.preguntaClimaPK.folioProyecto = :folio AND c.preguntaClimaPK.serialProyecto = :serial");
        query.setParameter("folio", proyecto.getProyectoPK().getFolioProyecto());
        query.setParameter("serial", proyecto.getProyectoPK().getSerialProyecto());
        deletedCount = query.executeUpdate();

        query = enMgr.createQuery("DELETE FROM PreguntaVegetacion c WHERE c.preguntaVegetacionPK.folioProyecto = :folio AND c.preguntaVegetacionPK.serialProyecto = :serial");
        query.setParameter("folio", proyecto.getProyectoPK().getFolioProyecto());
        query.setParameter("serial", proyecto.getProyectoPK().getSerialProyecto());
        deletedCount = query.executeUpdate();

        query = enMgr.createQuery("DELETE FROM PreguntaMineral c WHERE c.preguntaMineralPK.folioProyecto = :folio AND c.preguntaMineralPK.serialProyecto = :serial");
        query.setParameter("folio", proyecto.getProyectoPK().getFolioProyecto());
        query.setParameter("serial", proyecto.getProyectoPK().getSerialProyecto());
        deletedCount = query.executeUpdate();

        query = enMgr.createQuery("DELETE FROM PreguntaObra c WHERE c.preguntaObraPK.folioProyecto = :folio AND c.preguntaObraPK.serialProyecto = :serial");
        query.setParameter("folio", proyecto.getProyectoPK().getFolioProyecto());
        query.setParameter("serial", proyecto.getProyectoPK().getSerialProyecto());
        deletedCount = query.executeUpdate();

        proyecto.getPreguntaProyectoList().clear();
        proyecto.getEspecificacionProyectoList().clear();

        enMgr.merge(proyecto);

        enMgr.getTransaction().commit();
    }

    public void guardarCap1(Proyecto proyecto, List<PreguntaProyecto> preguntas, List<EspecificacionProyecto> especificaciones) {
        if (!enMgr.isOpen()) {
            enMgr = PoolEntityManagers.getEmf().createEntityManager();
        }
        enMgr.getTransaction().begin();

        enMgr.merge(proyecto);

        AvanceProyecto ap;
        try {
            Query q = enMgr.createQuery("SELECT a FROM AvanceProyecto a WHERE a.avanceProyectoPK.folioProyecto = :folio and a.avanceProyectoPK.serialProyeco = :serial and a.capitulo = :capitulo");
            q.setParameter("folio", proyecto.getProyectoPK().getFolioProyecto());
            q.setParameter("serial", proyecto.getProyectoPK().getSerialProyecto());
            q.setParameter("capitulo", (short) 1);
            ap = (AvanceProyecto) q.getSingleResult();
        } catch (Exception exc) {
            ap = new AvanceProyecto(proyecto.getProyectoPK().getFolioProyecto(), proyecto.getProyectoPK().getSerialProyecto(), new Short("1"));
        }

        if (proyecto.getProyNormaId() != null && proyecto.getProyNormaId() == (short) 129) {
            ap.setAvance((short) 24);
        } else {
            ap.setAvance((short) 32);
        }
        enMgr.merge(ap);

        enMgr.getTransaction().commit();
        //método para guardar avance en sinatec y verificar
        SinatecProcedure sp = new SinatecProcedure();
        int avance = Integer.parseInt(proyectoDao.avanceProyecto(proyecto));
        sp.ejecutaSP(Integer.parseInt(proyecto.getProyectoPK().getFolioProyecto()), avance);
//        if (avance == 99) {
//            reqcontEnv.execute("alert('Ha completado el registro de datos del Informe Preventivo.')");
//        }
        //terminan procedimientos con SINATEC
        System.out.println("Guardado .. ok");
    }

    public CatNorma getNorma(short id) {
        if (!enMgr.isOpen()) {
            enMgr = PoolEntityManagers.getEmf().createEntityManager();
        }
        Query q = enMgr.createQuery("SELECT c FROM CatNorma c WHERE c.normaId = :nid");
        q.setParameter("nid", id);
        CatNorma r = (CatNorma) q.getSingleResult();
        System.out.println("r" + r);
        return r;
    }

    public void guardaObjeto(Object object) {
        if (!enMgr.isOpen()) {
            enMgr = PoolEntityManagers.getEmf().createEntityManager();
        }
        try {
            enMgr.getTransaction().begin();
            enMgr.persist(object);
            enMgr.getTransaction().commit();
        } catch (Exception e) {
            Logger.getLogger(getClass().getName()).log(Level.SEVERE, "exception caught", e);
            enMgr.getTransaction().rollback();
        } finally {
        }
    }

    public void guardarCap12(Proyecto proyecto, List<PreguntaProyecto> preguntas, List<EspecificacionProyecto> especificaciones) {
        if (!enMgr.isOpen()) {
            enMgr = PoolEntityManagers.getEmf().createEntityManager();
        }
        try {
            enMgr.getTransaction().begin();

            if (proyecto.getProySupuesto() == '1') {

                Query qa = enMgr.createQuery("SELECT a FROM EspecificacionAnexo a WHERE a.especificacionAnexoPK.folioProyecto = :folio and a.especificacionAnexoPK.serialProyecto = :serial and a.especificacionAnexoPK.normaId = :norma");
                qa.setParameter("folio", proyecto.getProyectoPK().getFolioProyecto());
                qa.setParameter("norma", proyecto.getProyNormaId());
                qa.setParameter("serial", proyecto.getProyectoPK().getSerialProyecto());
                List<EspecificacionAnexo> anexos = qa.getResultList();

                // REspaldar anexos de la norma
                List<EspecificacionAnexo> anexosr = new ArrayList<EspecificacionAnexo>();
                for (EspecificacionAnexo ea : anexos) {
                    EspecificacionAnexo tmp = new EspecificacionAnexo(
                            ea.getEspecificacionAnexoPK().getFolioProyecto(),
                            ea.getEspecificacionAnexoPK().getSerialProyecto(),
                            ea.getEspecificacionAnexoPK().getEspecificacionId(),
                            ea.getEspecificacionAnexoPK().getNormaId(),
                            ea.getEspecificacionAnexoPK().getEspecificacionAnexoId()
                    );
                    tmp.setEspecAnexoDescripcion(ea.getEspecAnexoDescripcion());
                    tmp.setEspecAnexoExtension(ea.getEspecAnexoExtension());
                    tmp.setEspecAnexoNombre(ea.getEspecAnexoNombre());
                    tmp.setEspecAnexoTamanio(ea.getEspecAnexoTamanio());
                    tmp.setEspecAnexoUrl(ea.getEspecAnexoUrl());
                    anexosr.add(tmp);
                }

                Query query = enMgr.createQuery("DELETE FROM EspecificacionAnexo a WHERE a.especificacionAnexoPK.folioProyecto = :folio and a.especificacionAnexoPK.serialProyecto = :serial");
                query.setParameter("folio", proyecto.getProyectoPK().getFolioProyecto());
                query.setParameter("serial", proyecto.getProyectoPK().getSerialProyecto());
                int deletedCount = query.executeUpdate();

                query = enMgr.createQuery("DELETE FROM PreguntaClima c WHERE c.preguntaClimaPK.folioProyecto = :folio AND c.preguntaClimaPK.serialProyecto = :serial");
                query.setParameter("folio", proyecto.getProyectoPK().getFolioProyecto());
                query.setParameter("serial", proyecto.getProyectoPK().getSerialProyecto());
                deletedCount = query.executeUpdate();

                query = enMgr.createQuery("DELETE FROM PreguntaVegetacion c WHERE c.preguntaVegetacionPK.folioProyecto = :folio AND c.preguntaVegetacionPK.serialProyecto = :serial");
                query.setParameter("folio", proyecto.getProyectoPK().getFolioProyecto());
                query.setParameter("serial", proyecto.getProyectoPK().getSerialProyecto());
                deletedCount = query.executeUpdate();

                query = enMgr.createQuery("DELETE FROM PreguntaMineral c WHERE c.preguntaMineralPK.folioProyecto = :folio AND c.preguntaMineralPK.serialProyecto = :serial");
                query.setParameter("folio", proyecto.getProyectoPK().getFolioProyecto());
                query.setParameter("serial", proyecto.getProyectoPK().getSerialProyecto());
                deletedCount = query.executeUpdate();

                enMgr.getTransaction().commit();
                enMgr.getTransaction().begin();

                if (proyecto.getPreguntaProyectoList() != null) {
                    for (PreguntaProyecto p : proyecto.getPreguntaProyectoList()) {
                        if (p.getPreguntaObraList() != null) {
                            p.getPreguntaObraList().clear();
                        }
                    }
                }

                query = enMgr.createQuery("DELETE FROM PreguntaObra c WHERE c.preguntaObraPK.folioProyecto = :folio AND c.preguntaObraPK.serialProyecto = :serial");
                query.setParameter("folio", proyecto.getProyectoPK().getFolioProyecto());
                query.setParameter("serial", proyecto.getProyectoPK().getSerialProyecto());
                deletedCount = query.executeUpdate();

                if (proyecto.getEspecificacionProyectoList() != null) {
                    proyecto.getEspecificacionProyectoList().clear();
                }
                if (proyecto.getPreguntaProyectoList() != null) {
                    proyecto.getPreguntaProyectoList().clear();
                }

//            em.merge(proyecto);
                Query q2 = enMgr.createQuery("delete from EspecificacionProyecto e WHERE e.especificacionProyectoPK.folioProyecto = :folio and e.especificacionProyectoPK.serialProyecto = :serial");
                q2.setParameter("folio", proyecto.getProyectoPK().getFolioProyecto());
                q2.setParameter("serial", proyecto.getProyectoPK().getSerialProyecto());
                q2.executeUpdate();

                Query q3 = enMgr.createQuery("delete from PreguntaProyecto e WHERE e.preguntaProyectoPK.folioProyecto =  :folio and e.preguntaProyectoPK.serialProyecto = :serial");
                q2.setParameter("folio", proyecto.getProyectoPK().getFolioProyecto());
                q2.setParameter("serial", proyecto.getProyectoPK().getSerialProyecto());
                q2.executeUpdate();

                proyecto = enMgr.merge(proyecto);

//                enMgr.getTransaction().commit();
//                enMgr.getTransaction().begin();
                if (proyecto.getPreguntaProyectoList() == null) {
                    proyecto.setPreguntaProyectoList(new ArrayList<PreguntaProyecto>());
                }
                if (proyecto.getEspecificacionProyectoList() == null) {
                    proyecto.setEspecificacionProyectoList(new TreeSet<EspecificacionProyecto>());
                }
//                if (proyecto.getAnexosProyectoList()==null){
//                    proyecto.setAnexosProyectoList(new ArrayList<AnexosProyecto>());
//                }

                for (PreguntaProyecto p1 : preguntas) {
                    proyecto.getPreguntaProyectoList().add(p1);
                }

                for (EspecificacionProyecto e1 : especificaciones) {
                    proyecto.getEspecificacionProyectoList().add(e1);
                }

                for (EspecificacionAnexo ea : anexosr) {
                    enMgr.merge(ea);
                }

            }

//            proyecto = enMgr.merge(proyecto);
            enMgr.getTransaction().commit();

            System.out.println("Guardado Proyecto, preguntas, especifiaciones, anexos ... ok");
        } catch (Exception e) {
            e.printStackTrace();
            enMgr.getTransaction().rollback();
        }
    }

    public EntityManager emc = PoolEntityManagers.getEmfCat().createEntityManager();
    public EntityManager em = PoolEntityManagers.getEmf().createEntityManager();

    public List<CatEspecificacion> especificaciones(Short idNom) {
        Query q = em.createQuery("SELECT a FROM CatEspecificacion a WHERE a.catNorma.normaId = :norma ORDER BY a.especificacionOrden ASC");
        q.setParameter("norma", idNom);
        List l = q.getResultList();

        return l;

    }

    public List<CatPregunta> preguntas(String idNom) {
        Query q = em.createQuery("SELECT a FROM CatPregunta a WHERE a.normaId = " + idNom);
        List l = q.getResultList();
        return l;
    }

    public List<CatNorma> listaNormas() {
        Query q = em.createQuery("SELECT c FROM CatNorma c WHERE c.tipoTramite='I' ORDER BY c.normaId ASC");
        return q.getResultList();
    }

    public <T> List<T> getListObject(Object o) {
        List l = em.createQuery("SELECT o FROM " + o.getClass().getName() + " o ").getResultList();
        return l;
    }

    public Object getObject(Object o, Object id) {
        Object ob = em.find(o.getClass(), id);

        return ob;
    }

    public Object getObject(Object o, String id) {
        Object ob = em.find(o.getClass(), id);

        return ob;
    }

    public Object getObject(Object o, Integer id) {
        Object ob = em.find(o.getClass(), id);

        return ob;
    }

    public void persist(Object object) {
        try {
            em.getTransaction().begin();
            em.persist(object);
            em.getTransaction().commit();
        } catch (Exception e) {
            Logger.getLogger(getClass().getName()).log(Level.SEVERE, "exception caught", e);
            em.getTransaction().rollback();
        } finally {
        }
    }

    public void merge(Object object) {
        em.getTransaction().begin();
        em.merge(object);
        em.getTransaction().commit();
    }

    //nuevos métodos
    public List<CatTipoClima> tipoClima() {
        Query q = em.createQuery("SELECT a FROM CatTipoClima a");
        List l = q.getResultList();
        return l;
    }

    public List<CatObra> tipoObra() {
        Query q = em.createQuery("SELECT a FROM CatObra a");
        List l = q.getResultList();
        return l;
    }

    public List<CatTipoVegetacion> tipoVegetacion() {
        Query q = em.createQuery("SELECT a FROM CatTipoVegetacion a");
        List l = q.getResultList();
        return l;
    }

    public List<CatUnidadMedida> unidadMedida(String tipo) {
        Query q = emc.createQuery("SELECT a FROM CatUnidadMedida a");
        List l = q.getResultList();
        return l;
    }

    public List<SelectItem> unidadMedidaSelectItem() {
        List<SelectItem> lista = new LinkedList<SelectItem>();
        Query q = em.createQuery("SELECT a FROM CatUnidadMedida a");
        List<CatUnidadMedida> l = q.getResultList();
        for (CatUnidadMedida c : l) {
            SelectItem temp = new SelectItem(c, c.getCtunDesc());
            lista.add(temp);
        }
        return lista;
    }

    public List<SelectItem> mineralSelectItem() {
        List<SelectItem> lista = new LinkedList<SelectItem>();
        Query q = em.createNamedQuery("CatMineralesReservados.findAll");
        List<CatMineralesReservados> l = q.getResultList();
        for (CatMineralesReservados m : l) {
            SelectItem temp = new SelectItem(m, m.getDescripcionMinerales());
            lista.add(temp);
        }
        return lista;
    }

    public void eliminarEspecificaciones(String folio, Short serial) {
        try {
            em.getTransaction().begin();

            Query query = em.createQuery("DELETE FROM EspecificacionProyecto c WHERE c.especificacionProyectoPK.folioProyecto = :folio AND c.especificacionProyectoPK.serialProyecto = :serial");
            query.setParameter("folio", folio);
            query.setParameter("serial", serial);

            int deletedCount = query.executeUpdate();
            System.out.println("Eliminados " + deletedCount);

            em.getTransaction().commit();
        } catch (Exception e) {
            e.printStackTrace();
            em.getTransaction().rollback();
        } finally {
        }

    }

    public void eliminarPreguntas(String folio, Short serial) {
        try {
            em.getTransaction().begin();

            Query query = em.createQuery("DELETE FROM PreguntaVegetacion c WHERE c.preguntaVegetacionPK.folioProyecto = :folio AND c.preguntaVegetacionPK.serialProyecto = :serial");
            query.setParameter("folio", folio);
            query.setParameter("serial", serial);
            int deletedCount = query.executeUpdate();
            System.out.println("Vegetacion Eliminados " + deletedCount);

            query = em.createQuery("DELETE FROM PreguntaClima c WHERE c.preguntaClimaPK.folioProyecto = :folio AND c.preguntaClimaPK.serialProyecto = :serial");
            query.setParameter("folio", folio);
            query.setParameter("serial", serial);
            deletedCount = query.executeUpdate();
            System.out.println("Clima Eliminados " + deletedCount);

            query = em.createQuery("DELETE FROM PreguntaMineral c WHERE c.preguntaMineralPK.folioProyecto = :folio AND c.preguntaMineralPK.serialProyecto = :serial");
            query.setParameter("folio", folio);
            query.setParameter("serial", serial);
            deletedCount = query.executeUpdate();
            System.out.println("Mineral Eliminados " + deletedCount);

            query = em.createQuery("DELETE FROM PreguntaObra c WHERE c.preguntaObraPK.folioProyecto = :folio AND c.preguntaObraPK.serialProyecto = :serial");
            query.setParameter("folio", folio);
            query.setParameter("serial", serial);
            deletedCount = query.executeUpdate();
            System.out.println("Obra Eliminados " + deletedCount);
            em.getTransaction().commit();
        } catch (Exception e) {
            e.printStackTrace();
            em.getTransaction().rollback();
        } finally {
        }

    }

    public void upsert(Object o) {
//        if (em.getTransaction().isActive()) {
        em.getTransaction().begin();
        try {
            em.merge(o);
            em.getTransaction().commit();
        } catch (Exception e) {
            e.printStackTrace();
            em.getTransaction().rollback();
        }
//        }
    }

}
