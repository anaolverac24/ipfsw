/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.gob.semarnat.dao;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.NoResultException;
import javax.persistence.Query;
import javax.swing.JOptionPane;
import mx.gob.semarnat.model.AnexosProyecto;
import mx.gob.semarnat.model.CatCapitulo;
import mx.gob.semarnat.model.CatSeccion;
import mx.gob.semarnat.model.CatSubcapitulo;
import mx.gob.semarnat.model.ComentarioProyecto;
import mx.gob.semarnat.model.EspecificacionAnexo;
import mx.gob.semarnat.model.Proyecto;
import mx.gob.semarnat.model.ProyectoPK;
import mx.gob.semarnat.model.SustanciaAnexo;

/**
 *
 * @author mauricio
 */
public class VisorDao implements Serializable {

    public static EntityManagerFactory emf = PoolEntityManagers.getEmf();
    
    public static int tamanioLista(List<?> l){
        return l.size();
    }

    public static void merge(Object object) {
        EntityManager em = emf.createEntityManager();
        try {
            em.getTransaction().begin();
            em.persist(object);
            em.getTransaction().commit();
        } catch (Exception e) {
            e.printStackTrace();
//            Logger.getLogger(getClass().getName()).log(Level.SEVERE, "exception caught", e);
            em.getTransaction().rollback();
        } finally {
        }
    }

    public static List<SustanciaAnexo> getSustanciaAnexos(String folioProyecto, Short serialProyecto, Short sustanciaProyId) {
        EntityManager em = emf.createEntityManager();
        List<SustanciaAnexo> hs;
            if(sustanciaProyId!=null){
                Query q = em.createQuery("SELECT s FROM SustanciaAnexo s WHERE s.sustanciaAnexoPK.folioProyecto = :folio AND s.sustanciaAnexoPK.serialProyecto = :serial AND s.sustanciaAnexoPK.sustanciaProyId= :id");
                q.setParameter("folio", folioProyecto);
                q.setParameter("serial", serialProyecto);
                q.setParameter("id", sustanciaProyId);
                hs = q.getResultList();
            }else{
                Query q = em.createQuery("SELECT s FROM SustanciaAnexo s WHERE s.sustanciaAnexoPK.folioProyecto = :folio AND s.sustanciaAnexoPK.serialProyecto= :serial");
                q.setParameter("folio", folioProyecto);
                q.setParameter("serial", serialProyecto);
                hs = q.getResultList();
            }
            return hs;
    }

    public short numRegAnexoEspe(String folioProyecto, short serialProyecto, String especifiacionId) {
        short r = 0;
        try {
            EntityManager em = emf.createEntityManager();
            Query q = em.createQuery("SELECT max(a.especificacionAnexoPK.especificacionAnexoId) FROM EspecificacionAnexo a");
//            Query q = em.createQuery("SELECT max(a.especificacionAnexoPK.especificacionAnexoId) FROM EspecificacionAnexo a Where a.especificacionAnexoPK.folioProyecto = :folio and a.especificacionAnexoPK.serialProyecto = :serial and a.especificacionAnexoPK.especificacionId = :especificacion");
//            q.setParameter("folio", folioProyecto);
//            q.setParameter("serial", serialProyecto);
//            q.setParameter("especificacion", especifiacionId);
            r = (Short) q.getSingleResult();
            em.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return (short) r;
    }

    public short numRegAnexo(short capituloId, short subcapituloId, short seccionId, short apartadoId, String folioProyecto, short serialProyecto) {
        int r = 0;
        try {
            EntityManager em = emf.createEntityManager();
            Query q = em.createQuery("SELECT max(a.anexosProyectoPK.anexoId) FROM AnexosProyecto a");
            r = (Short) q.getSingleResult();
            em.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return (short) r;
    }

    public static List<AnexosProyecto> getAnexos(short capituloId, short subcapituloId, short seccionId, short apartadoId, String folioProyecto, short serialProyecto) {
        List<AnexosProyecto> r = new ArrayList<AnexosProyecto>();
        try {
            if (capituloId != 0) {
                EntityManager em = emf.createEntityManager();
                Query q = em.createQuery("SELECT a FROM AnexosProyecto a WHERE a.anexosProyectoPK.apartadoId = :apartado and a.anexosProyectoPK.capituloId = :capitulo and a.anexosProyectoPK.folioProyecto = :folio and a.anexosProyectoPK.serialProyecto = :serial and a.anexosProyectoPK.seccionId = :seccion and a.anexosProyectoPK.subcapituloId = :subcapitulo");
                q.setParameter("apartado", apartadoId);
                q.setParameter("capitulo", capituloId);
                q.setParameter("folio", folioProyecto);
                q.setParameter("serial", serialProyecto);
                q.setParameter("seccion", seccionId);
                q.setParameter("subcapitulo", subcapituloId);
                r = q.getResultList();
                em.close();
            } else {
                //obtiene todos los adjuntos del folio
                EntityManager em = emf.createEntityManager();
                Query q = em.createQuery("SELECT a FROM AnexosProyecto a WHERE a.anexosProyectoPK.folioProyecto = :folio and a.anexosProyectoPK.serialProyecto = :serial");
                q.setParameter("folio", folioProyecto);
                q.setParameter("serial", serialProyecto);
                r = q.getResultList();
                em.close();
            }
        } catch (Exception e) {

        }

        return r;

    }

    public static List<EspecificacionAnexo> getEspecifiacionesAnexos(String folio, String especifiacionId, Short serialProeycto, Short normaId) {
        EntityManager em = emf.createEntityManager();
        List l;
        if (normaId != null) {
            Query q = em.createQuery("SELECT a FROM EspecificacionAnexo a WHERE a.especificacionAnexoPK.folioProyecto = :folio and a.especificacionAnexoPK.especificacionId = :especificacion and a.especificacionAnexoPK.serialProyecto = :serial and a.especificacionAnexoPK.normaId = :norma");
            q.setParameter("folio", folio);
            q.setParameter("especificacion", especifiacionId);
            q.setParameter("serial", serialProeycto);
            q.setParameter("norma", normaId);
            l = q.getResultList();

            em.close();
        } else {
            Query q = em.createQuery("SELECT a FROM EspecificacionAnexo a WHERE a.especificacionAnexoPK.folioProyecto = :folio and a.especificacionAnexoPK.serialProyecto = :serial");
            q.setParameter("folio", folio);
            q.setParameter("serial", serialProeycto);
            l = q.getResultList();
        }
        return l;
    }

    public List<CatCapitulo> getCapitulos() {
        EntityManager em = emf.createEntityManager();
        Query q = em.createQuery("SELECT a FROM CatCapitulo a ");
        List l = q.getResultList();

        em.close();
        return l;
    }

    public List<AnexosProyecto> getAnexos(CatCapitulo cap, CatSubcapitulo sub, CatSeccion sec, Object ap) {
        String hql = "";

        if (sub == null) {
            sub = new CatSubcapitulo();
//            sub.setId(-1);
        }
        if (sec == null) {
            sec = new CatSeccion();
        }
//        if (ap == null) {
//            ap = new CatApartado();
//            ap.setId(-1);
//        }

//        if (ap == null || ap.getId() < 1) {
//            hql = "SELECT a FROM AnexoProyecto a where a.apartado.apartado.seccion.id = " + sec.getCatSeccionPK().getSeccionId();
//        }
        if (sec == null || sec.getCatSeccionPK().getSeccionId() < 1) {
//            hql = "SELECT a FROM AnexoProyecto a where a.apartado.apartado.seccion.subCapitulo.id = " + sub.getId();
        }

//        if (sub == null || sub.getId() < 1) {
//            hql = "SELECT a FROM AnexoProyecto a where a.apartado.apartado.seccion.subCapitulo.capitulo = " + cap.getCatCapituloPK().getCapituloId();
//        }
        if (cap == null || cap.getCatCapituloPK().getCapituloId() < 1) {
            hql = "SELECT a FROM AnexoProyecto a where a.apartado.apartado.seccion.subCapitulo.capitulo.id > 0 ";
        }

        if (hql.isEmpty()) {
//            hql = "SELECT a FROM AnexoProyecto a where a.apartado.apartado.id = " + ap.getId();
        }

        EntityManager em = emf.createEntityManager();
        Query q = em.createQuery(hql);
        List l = q.getResultList();
        em.close();

        return l;
    }

    public List<AnexosProyecto> getAnexos(int capitulo, int subCapitulo, int seccion, int apartado) {
        EntityManager em = emf.createEntityManager();
        Query q = em.createQuery("SELECT a FROM AnexoProyecto a where a.apartado.apartado.seccion.subCapitulo.id ");
        List l = q.getResultList();
        em.close();

        return l;
    }

    public List<CatSubcapitulo> getSubCapitulos(String idCapitulo) {
        EntityManager em = emf.createEntityManager();
        Query q = em.createQuery("SELECT c FROM CatSubCapitulo c WHERE c.capitulo.id = " + idCapitulo);
        System.out.println("SELECT c FROM CatSubCapitulo c WHERE c.capitulo.id = " + idCapitulo);

        List l = q.getResultList();
        System.out.println("total " + l.size());
        em.close();

        return l;
    }

    public List<CatSeccion> getSecciones(String idSubCapitulo) {
        EntityManager em = emf.createEntityManager();
        Query q = em.createQuery("SELECT c FROM CatSeccion c WHERE c.subCapitulo.id = " + idSubCapitulo);
        List l = q.getResultList();
        em.close();

        return l;
    }

//    public List<CatApartado> getApartados(String idSeccion) {
//        EntityManager em = emf.createEntityManager();
//        Query q = em.createQuery("SELECT c FROM CatApartado c WHERE c.seccion.id = " + idSeccion);
//        List l = q.getResultList();
//        em.close();
//
//        return l;
//    }
    public List<AnexosProyecto> getApartado(CatCapitulo cap, CatSubcapitulo sub, CatSeccion sec, Object ap) {

        EntityManager em = emf.createEntityManager();
        //       Query q = em.createQuery("SELECT ap FROM  ApartadoProyecto ap  WHERE ap.apartado.id in ( SELECT catAp.id FROM  CatApartado catAp where catAp.seccion )  " );

        Query q = em.createQuery("SELECT anexo FROM AnexoProyecto anexo");
        List l = q.getResultList();
        em.close();

        return l;
    }

    /**
     * Busca un registro por su id
     *
     * @param o Objeto a buscar
     * @param id id del registro
     * @return Object<t>
     */
    public Object getObject(Object o, String id) {
        EntityManager em = emf.createEntityManager();
        Object ob = em.find(o.getClass(), id);
        em.close();

        return ob;
    }
    public Object getObject(Class<?> tipo, Object id) {
        EntityManager em = emf.createEntityManager();
        Object ob = em.find(tipo, id);
        em.close();

        return ob;
    }

    public Object getObject(Object o, Integer id) {
        EntityManager em = emf.createEntityManager();
        Object ob = em.find(o.getClass(), id);
        em.close();

        return ob;
    }

    public static void persist(Object object) {
        EntityManager em = emf.createEntityManager();
        try {
            em.getTransaction().begin();
            em.persist(object);
            em.getTransaction().commit();
        } catch (Exception e) {
            e.printStackTrace();
//            Logger.getLogger(getClass().getName()).log(Level.SEVERE, "exception caught", e);
            em.getTransaction().rollback();
        } finally {
        }
    }

    public short numRegAnexoSust() {
        int id = 0;
        try{
            EntityManager em = emf.createEntityManager();
            Query q = em.createQuery("SELECT max(s.sustanciaAnexoPK.sustanciaAnexoId) FROM SustanciaAnexo s");
            id = (Short)q.getSingleResult();
            em.close();
        }catch(Exception e){
            e.printStackTrace();
        }
        return (short) id;
    }
    
    public Proyecto verificaERA(String numBita) { 
        EntityManager em = emf.createEntityManager();
        Query q = em.createNamedQuery("Proyecto.findByBitacoraProyecto");
        q.setParameter("bitacoraProyecto", numBita);
        Proyecto p = new Proyecto();
        try {
            p = (Proyecto) q.getSingleResult();
        } catch (NoResultException e) {
            System.out.println("EROR: Folio no encontrado");
        } catch (Exception e) {
//            System.out.println("" + e.getLocalizedMessage());
            e.printStackTrace();
           JOptionPane.showMessageDialog(null, "folio:  " + e.getMessage(), "Error", JOptionPane.INFORMATION_MESSAGE);
        }
        return p;
    }
    
    public Proyecto cargaProyecto(String folioProyecto, short serialProyecto) {
        EntityManager em = emf.createEntityManager();
        return (Proyecto) em.find(Proyecto.class, new ProyectoPK(folioProyecto, serialProyecto));
    }
    
    public ComentarioProyecto buscaInfoAdic(String bitacoraProy, Short capituloId, Short subcapituloId, Short seccionId, Short apartadoId ) { //, Integer idusuario
        EntityManager em = emf.createEntityManager();
        Query q = em.createQuery("SELECT p FROM ComentarioProyecto p WHERE p.comentarioProyectoPK.bitacoraProyecto=:bitacora and p.comentarioProyectoPK.capituloId=:capitulo and p.comentarioProyectoPK.subcapituloId=:subcapitulo and p.comentarioProyectoPK.seccionId=:seccion and p.comentarioProyectoPK.apartadoId=:apartado"); // and p.comentarioProyectoPK.comentarioIdusuario=:Idusuario
        q.setParameter("bitacora", bitacoraProy); //capitulo
        q.setParameter("capitulo", capituloId);
        q.setParameter("subcapitulo", subcapituloId);
        q.setParameter("seccion", seccionId);
        q.setParameter("apartado", apartadoId);
       // q.setParameter("Idusuario", idusuario);
        Object o = null;
        try {
            o = q.getSingleResult();
        } catch (NoResultException e) {
            System.out.println("EROR: Folio no encontrado");
        } catch (Exception e) {
            System.out.println("" + e.getLocalizedMessage());
            e.printStackTrace();
        }
        return (ComentarioProyecto) o;
    }

}
