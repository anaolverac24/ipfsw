/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.gob.semarnat.dao;

import java.util.Date;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.EntityTransaction;
import javax.persistence.Persistence;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Expression;
import javax.persistence.criteria.Root;

/**
 *
 * @author Admin
 */
public class Capitulo3DaoCatalogos {
    
    public EntityManagerFactory emf = PoolEntityManagers.getEmf();
    public EntityManagerFactory emfCat = PoolEntityManagers.getEmfCat();
    public EntityManager em = emfCat.createEntityManager();
    
    /*
     * @param Clase de la Tabla a buscar
     */

    public List<?> listado(Class<?> tipo) {
        CriteriaBuilder cBuilder = em.getCriteriaBuilder();
        CriteriaQuery<Object> cq = cBuilder.createQuery();
        cq.select(cq.from(tipo));
        return em.createQuery(cq).getResultList();
    }

    public Object busca(Class<?> tipo, Object id) {
        return em.find(tipo, id);
    }

    public void agrega(Object o) {
        EntityTransaction tx = em.getTransaction();
        tx.begin();
        try {
            if (o != null && tx.isActive()) {
                em.persist(o);
                tx.commit();
            }
        } catch (Exception e) {
            if (tx.isActive()) {
                tx.rollback();
            }
        }
    }

    public void modifica(Object o) {
        EntityTransaction tx = em.getTransaction();
        try {
            tx.begin();
            em.merge(o);
            tx.commit();
//        em.close();
        } catch (Exception e) {
            if (tx.isActive()) {
                tx.rollback();
            }
        }
    }

    public void elimina(Class<?> tipo, Object id) {
        EntityTransaction tx = em.getTransaction();
        Object o = em.find(tipo, id);
        try {
            if (o != null) {
                tx.begin();
                em.remove(o);
                tx.commit();
            }
        } catch (Exception e) {
            if (tx.isActive()) {
                tx.rollback();
            }
        }
//        em.close();
    }

    public List<?> listado_where(Class<?> tipo, String atributeName, Object o) {
        CriteriaBuilder cBuilder = em.getCriteriaBuilder();
        CriteriaQuery<Object> cq = cBuilder.createQuery();
        Root<?> r = cq.from(tipo);
        cq.select(r);
        cq.where(cBuilder.equal(r.get(atributeName), o));
        return em.createQuery(cq).getResultList();
    }

    public List<?> listado_where_comp(Class<?> tipo, String atributeName, String nestedAttrib, Object o) {
        CriteriaBuilder cBuilder = em.getCriteriaBuilder();
        CriteriaQuery<Object> cq = cBuilder.createQuery();
        Root<?> r = cq.from(tipo);
        cq.select(r);
        cq.where(cBuilder.equal(r.get(atributeName).get(nestedAttrib), o));
        return em.createQuery(cq).getResultList();
    }

    public List<?> listado_like(Class<?> tipo, String atributeName, Object o) {
        CriteriaBuilder cBuilder = em.getCriteriaBuilder();
        CriteriaQuery<Object> cq = cBuilder.createQuery();
        Root<?> r = cq.from(tipo);
        cq.select(r);
        Expression<String> path = r.get(atributeName);
        cq.where(cBuilder.like(path, o.toString()));
        return em.createQuery(cq).getResultList();
    }

    public Object objeto_where(Class<?> tipo, String atributeName, Object o) {
        CriteriaBuilder cBuilder = em.getCriteriaBuilder();
        CriteriaQuery<Object> cq = cBuilder.createQuery();
        Root<?> r = cq.from(tipo);
        cq.select(r);
        cq.where(cBuilder.equal(r.get(atributeName), o));
        return em.createQuery(cq).getSingleResult();
    }

    public List<?> listado_where_AND(Class<?> tipo, String[] atributeName, Object[] o) {
        CriteriaBuilder cBuilder = em.getCriteriaBuilder();
        CriteriaQuery<Object> cq = cBuilder.createQuery();
        Root<?> r = cq.from(tipo);
        cq.select(r);
        cq.where(cBuilder.equal(r.get(atributeName[0]), o[0]),
                cBuilder.equal(r.get(atributeName[1]), o[1]));
        return em.createQuery(cq).getResultList();
    }

    public List<?> listado_BETWEEN(Class<?> tipo, String atributeName, Date f1, Date f2) {
        CriteriaBuilder cBuilder = em.getCriteriaBuilder();
        CriteriaQuery<Object> cq = cBuilder.createQuery();
        Root<?> r = cq.from(tipo);
        cq.select(r);
        cq.where(cBuilder.between(r.<Date>get(atributeName), f1, f2));
        return em.createQuery(cq).getResultList();
    }
}
