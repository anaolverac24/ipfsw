/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.gob.semarnat.dao;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.EntityTransaction;
import javax.persistence.Query;
import mx.gob.semarnat.model.AnexosProyecto;
import mx.gob.semarnat.model.AvanceProyecto;
import mx.gob.semarnat.model.AvanceProyectoPK;
import mx.gob.semarnat.model.ProySubsectorGuias;
import mx.gob.semarnat.model.Proyecto;
import mx.gob.semarnat.model.catalogos.CatTipoAsen;
import mx.gob.semarnat.model.catalogos.CatVialidad;
import mx.gob.semarnat.model.catalogos.RamaProyecto;
import mx.gob.semarnat.model.catalogos.SectorProyecto;
import mx.gob.semarnat.model.catalogos.SubsectorProyecto;
import mx.gob.semarnat.model.catalogos.TipoProyecto;
import mx.gob.semarnat.model.sinatec.Vexdatosusuario;
import mx.gob.semarnat.model.sinatec.Vexdatosusuariorep;

/**
 * @author Case Solutions 10
 */
public class Capitulo2Dao implements Serializable {

//    private final static EntityManagerFactory emfSinatec = PoolEntityManagers.getEmfSinatec();
//    private final static EntityManagerFactory emfCat = PoolEntityManagers.getEmfCat();
//    private final static EntityManagerFactory emf = PoolEntityManagers.getEmf();
    private EntityManager em = PoolEntityManagers.getEmf().createEntityManager();
    private EntityManager emSinatec = PoolEntityManagers.getEmfSinatec().createEntityManager();
    private EntityManager emCat = PoolEntityManagers.getEmfCat().createEntityManager();

    /**
     * @param Folio
     * @param ClveProy
     * @param version
     * @return
     */
    public List<Object[]> mpiosSumObra(String Folio, String ClveProy, short version) {
        if (!em.isOpen()) {
            em = PoolEntityManagers.getEmf().createEntityManager();
        }

        Query q, q2;
        Object o1, o2;
        List<Object[]> lista = new ArrayList<Object[]>();
        q = em.createQuery("SELECT DISTINCT c.nomEstado FROM MpiosCruzadavshambre c WHERE c.mpiosCruzadavshambrePK.numFolio=:folio AND c.mpiosCruzadavshambrePK.version=:version");
        q.setParameter("folio", Folio);        
        q.setParameter("version", version);
        List<String> edos = q.getResultList();
        for (String string : edos) {
            q = em.createNativeQuery("SELECT SUM(mayor) FROM (SELECT SUM(a.area) as mayor from mpios_cruzadavshambre a where a.num_folio = :folio and a.version = :version AND a.nom_estado=:edomun AND a.comp='OBRA')");
            q.setParameter("folio", Folio);            
            q.setParameter("version", version);
            q.setParameter("edomun", string);
            q2 = em.createNativeQuery("select m.idr from mpios_cruzadavshambre m where m.area = ("
                    + "select distinct max(m.area) from mpios_cruzadavshambre where num_folio=:folio and version = :version) and num_folio = :folio and nom_estado=:estado and version = :version AND comp='OBRA' order by m.area desc");
            q2.setParameter("folio", Folio);
            q2.setParameter("version", version);
            q2.setParameter("estado", string);
            o1 = q.getSingleResult();
            if (o1 != null) {
                o2 = q2.getResultList().get(0);
                lista.add(new Object[]{o2, o1});
            }
        }
        Collections.sort(lista, new MpiosComparator());
        return lista;
    }

    public List<Object[]> mpiosSumPred(String Folio, String ClveProy, short version) {
        if (!em.isOpen()) {
            em = PoolEntityManagers.getEmf().createEntityManager();
        }
        Query q, q2;
        Object o1, o2;
        List<Object[]> lista = new ArrayList<Object[]>();
        q = em.createQuery("SELECT DISTINCT c.nomEstado FROM MpiosCruzadavshambre c WHERE c.mpiosCruzadavshambrePK.numFolio=:folio AND c.mpiosCruzadavshambrePK.version=:version");
        q.setParameter("folio", Folio);
        //q.setParameter("cve", ClveProy);
        q.setParameter("version", version);
        List<String> edos = q.getResultList();
        for (String string : edos) {
            q = em.createNativeQuery("SELECT SUM(mayor) FROM (SELECT SUM(a.area) as mayor from mpios_cruzadavshambre a where a.num_folio = :folio and a.version = :version AND a.nom_estado=:edomun AND a.comp='PREDIO')");
            q.setParameter("folio", Folio);
            //q.setParameter("cve", ClveProy);
            q.setParameter("version", version);
            q.setParameter("edomun", string);
            q2 = em.createNativeQuery("select m.idr from mpios_cruzadavshambre m where m.area = ("
                    + "select distinct max(m.area) from mpios_cruzadavshambre where num_folio=:folio and version = :version) and num_folio = :folio and version = :version and nom_estado=:estado AND comp='PREDIO' order by m.area desc");
            q2.setParameter("folio", Folio);
            q2.setParameter("version", version);
            q2.setParameter("estado", string);
            o1 = q.getSingleResult();
            if (o1 != null) {
                o2 = q2.getResultList().get(0);
                lista.add(new Object[]{o2, o1});
            }
        }
        Collections.sort(lista, new MpiosComparator());
        return lista;
    }

    public List<Object[]> proySigSum(String Folio, String ClveProy, short version) {
        if (!em.isOpen()) {
            em = PoolEntityManagers.getEmf().createEntityManager();
        }
        Query q = em.createQuery("SELECT a.proysigPK.cveArea, a.comp, sum(a.shapeArea) FROM Proysig a  WHERE a.proysigPK.numFolio=:fol and a.proysigPK.version=:versions GROUP BY a.comp, a.proysigPK.cveArea  ");
        q.setParameter("fol", Folio);
        //q.setParameter("clveProy", ClveProy);
        q.setParameter("versions", version);
        List<Object[]> l = q.getResultList();
        return l;
    }

    public List<Object[]> edoMasAfectado(String Folio, String ClveProy, Short version, Integer id) {
        if (!em.isOpen()) {
            em = PoolEntityManagers.getEmf().createEntityManager();
        }
        Query q = em.createQuery("SELECT a.cEdomun , a.clvMunici , a.nomEstado, a.nomMunici "
                + "FROM MpiosCruzadavshambre a "
                + " WHERE a.mpiosCruzadavshambrePK.numFolio='" + Folio
                //+ "' and a.mpiosCruzadavshambrePK.cveProy='" + ClveProy
                + "' and a.mpiosCruzadavshambrePK.version =" + version
                + " and a.mpiosCruzadavshambrePK.idr =" + id);

        List l = q.getResultList();
        return l;
    }

    public List<Object[]> edoTodosAfectado(String Folio, String ClveProy, short version) {
        if (!em.isOpen()) {
            em = PoolEntityManagers.getEmf().createEntityManager();
        }
        System.out.println("folioNum  " + Folio + "   proyClve " + ClveProy + "   vers  " + version);                
        
        //Primero se verifica si existen obras
        Query q = em.createQuery("SELECT count(a.area) FROM MpiosCruzadavshambre a WHERE a.comp = 'OBRA' and a.mpiosCruzadavshambrePK.numFolio=:folioNum and a.mpiosCruzadavshambrePK.version =:vers")
                .setParameter("folioNum", Folio)
                //.setParameter("proyClve", ClveProy)
                .setParameter("vers", version);

        String vObra = q.getSingleResult().toString();

        //Se verifica si existen predios.
        q = em.createQuery("SELECT count(a.area) FROM MpiosCruzadavshambre a WHERE a.comp = 'PREDIO' and a.mpiosCruzadavshambrePK.numFolio=:folioNum and a.mpiosCruzadavshambrePK.version =:vers")
                .setParameter("folioNum", Folio)
                //.setParameter("proyClve", ClveProy)
                .setParameter("vers", version);

        String vPredio = q.getSingleResult().toString();

        String vFiltroPredioObra = "";

        if (Integer.valueOf(vObra) > 0) {
            vFiltroPredioObra = "OBRA";
        } else if (Integer.valueOf(vPredio) > 0) {
            vFiltroPredioObra = "PREDIO";
        };

        System.out.println("---- Obras " + vObra + "---  Predios  " + vPredio);
        
               q = em.createQuery("SELECT a.cEdomun, a.clvMunici, a.nomEstado, a.nomMunici, sum(a.area) FROM MpiosCruzadavshambre a "
                       + "WHERE a.comp = '"+ vFiltroPredioObra + "' and a.mpiosCruzadavshambrePK.numFolio=:folioNum and a.mpiosCruzadavshambrePK.version =:vers"
                       + " GROUP BY a.cEdomun, a.clvMunici, a.nomEstado, a.nomMunici")
                .setParameter("folioNum", Folio)
                //.setParameter("proyClve", ClveProy)
                .setParameter("vers", version);
               
        return q.getResultList();
    }

    public List<Object[]> proySigSupProyecto(String Folio, String ClveProy, short version) {
        if (!em.isOpen()) {
            em = PoolEntityManagers.getEmf().createEntityManager();
        }
        Query q = em.createQuery("SELECT a.comp, a.descrip, a.shapeArea FROM Proysig a  WHERE a.proysigPK.numFolio=:fol and a.proysigPK.version=:versions ORDER BY a.comp "); 
        q.setParameter("fol", Folio);
        //q.setParameter("clveProy", ClveProy);
        q.setParameter("versions", version);
        List<Object[]> l = q.getResultList();
        return l;
    }

    public List<Object[]> proySigSumObras(String Folio, String ClveProy, short version) {
        if (!em.isOpen()) {
            em = PoolEntityManagers.getEmf().createEntityManager();
        }
        Query q = em.createQuery("SELECT sum(a.shapeArea), sum(a.shapeArea) FROM Proysig a  WHERE a.proysigPK.numFolio=:fol and a.proysigPK.version=:versions and a.comp='OBRA'");
        q.setParameter("fol", Folio);
        //q.setParameter("clveProy", ClveProy);
        q.setParameter("versions", version);
        List<Object[]> l = q.getResultList();
        em.clear();
        return l;
    }

    public List<Object[]> proySigSumPredios(String Folio, String ClveProy, short version) {
        if (!em.isOpen()) {
            em = PoolEntityManagers.getEmf().createEntityManager();
        }
        Query q = em.createQuery("SELECT sum(a.shapeArea), sum(a.shapeArea) FROM Proysig a  WHERE a.proysigPK.numFolio=:fol and a.proysigPK.version=:versions and a.comp='PREDIO'");
        q.setParameter("fol", Folio);
        //q.setParameter("clveProy", ClveProy);
        q.setParameter("versions", version);
        List<Object[]> l = q.getResultList();
        em.clear();
        return l;
    }

    //------------catalogos de esquema CATALOGOS----------------------   
    public List<Object[]> sector(Short idSubsec) {
        Query q = emCat.createQuery("SELECT  A.nsec, B.sector FROM SubsectorProyecto A,  SectorProyecto B WHERE  A.nsec = B.nsec AND A.nsub = " + idSubsec);
        List<Object[]> l = q.getResultList();
        return l;
    }

    public List<SubsectorProyecto> todosCatSubSec() {
        Query q = emCat.createQuery("SELECT a FROM SubsectorProyecto a ORDER BY a.nsub");
        List l = q.getResultList();
        return l;
    }

    public List<RamaProyecto> rama(Short idSubsec) {
        Query q = emCat.createQuery("SELECT a FROM RamaProyecto a WHERE a.nsub = " + idSubsec);
        List l = q.getResultList();
        return l;
    }

    public List<TipoProyecto> todosTipProySubSec(Short nrama) {
        Query q = emCat.createQuery("SELECT a FROM TipoProyecto a WHERE a.nrama = " + nrama + " ORDER BY a.tipoProyecto");
        List l = q.getResultList();
        return l;
    }

    public List<CatVialidad> todosCatVialidad() {
        Query q = emCat.createQuery("SELECT a FROM CatVialidad a ORDER BY a.cveTipoVial");
        List l = q.getResultList();
        return l;
    }
    
    /**
     * Permite consultar el subsector del proyecto.
     * @param nSub del proyecto.
     * @return el subsector del proyecto.
     * @throws Exception en caso de error al consultar el subsector.
     */
    public SubsectorProyecto consultarSubSectorProyecto(Short nSub) throws Exception {
        Query query = emCat.createQuery("FROM SubsectorProyecto where nsub = " + nSub);
        SubsectorProyecto subsectorProyecto = (SubsectorProyecto) query.getSingleResult();
        return subsectorProyecto;		
	}
    
    /**
     * Permite consultar el sector del proyecto.
     * @param nSec del proyecto.
     * @return el sector del proyecto
     * @throws Exception en caso de error al consultar el sector.
     */
    public SectorProyecto consultarSectorProyecto(Short nSec) throws Exception {
        Query query = emCat.createQuery("FROM SectorProyecto where nsec = " + nSec);
        SectorProyecto sectorProyecto = (SectorProyecto) query.getSingleResult();
        return sectorProyecto;		
	}   
    
    /**
     * Permite consultar la rama del proyecto.
     * @param nSub del proyecto consultado.
     * @return la rama del proyecto consulatado por el id del SubSector.
     */
    public RamaProyecto consultarRamaProyectoPorIdSubSector(Short nSub, Short nRama) throws Exception {
        Query q = emCat.createQuery("FROM RamaProyecto where nsub = " + nSub +  " and nrama = " + nRama);
        RamaProyecto ramaProyecto = (RamaProyecto) q.getSingleResult();
        return ramaProyecto;
    }
    
    /**
     * Permite consultar el tipo del proyecto por rama.
     * @param nrama del proyecto.
     * @return el tipo de proyecto segun su rama.
     */
    public TipoProyecto consultarTipoProyectoPorRama(Short tipo, Short nrama) throws Exception {
        Query q = emCat.createQuery("FROM TipoProyecto WHERE ntipo = " +tipo+ " and nrama = " + nrama);
        TipoProyecto tipoProyecto = (TipoProyecto) q.getSingleResult();
        return tipoProyecto;
    }    

    public List<CatTipoAsen> todosCatAsent() {
        Query q = emCat.createQuery("SELECT a FROM CatTipoAsen a ORDER BY a.cveTipoAsen");
        List l = q.getResultList();
        return l;
    }

//------------------SINATEC---------------------- 
    public List<Vexdatosusuariorep> datosPrepLeg(Integer idTramite) {
        Query q = emSinatec.createQuery("SELECT DISTINCT a FROM Vexdatosusuariorep a WHERE a.bgtramiteid=:idTramite");
        q.setParameter("idTramite", idTramite);
        List l = q.getResultList();
        return l;
    }

    public void borraRepLegal(String id) {//metodo para borrar todos los representantes legales ligados al proyecto
        em.clear();
        em.getTransaction().begin();
        Query q = em.createQuery("DELETE FROM RepLegalProyecto r WHERE "
                + "r.repLegalProyectoPK.folioProyecto = :folioProyecto");
        q.setParameter("folioProyecto", id);
        q.executeUpdate();
        em.getTransaction().commit();
    }

    public Vexdatosusuariorep repLegPorCurp(String curp, Integer id) {
        Query q = emSinatec.createQuery("SELECT v FROM Vexdatosusuariorep v WHERE v.vccurp = :vccurp AND v.bgtramiteid=:id");
        q.setParameter("vccurp", curp);
        q.setParameter("id", id);
        return (Vexdatosusuariorep) q.getSingleResult();
    }

    public List<Vexdatosusuario> datosPromovente(Integer idTramite) {
        Query q = emSinatec.createQuery("SELECT DISTINCT a  FROM Vexdatosusuario a WHERE a.bgtramiteid=:idTramite");
        q.setParameter("idTramite", idTramite);
        List l = q.getResultList();
        return l;
    }

//--------------------------funciones generales----------------    
    public void agrega(Object o) {
        em.clear();
        EntityTransaction tx = em.getTransaction();
        tx.begin();
        try {
            if (o != null && tx.isActive()) {
                em.persist(o);
                tx.commit();
            }
        } catch (Exception e) {
            if (tx.isActive()) {
                tx.rollback();
            }
        }
    }

    public void modifica(Object o) {
        em.clear();
        EntityTransaction tx = em.getTransaction();
        try {
            tx.begin();
            em.merge(o);
            tx.commit();
        } catch (Exception e) {
            if (tx.isActive()) {
                tx.rollback();
            }
        }
    }

    public List<AnexosProyecto> anexos(String folio, Short version, Short capitulo, Short subcapitulo, Short seccion) {//        EntityManager em = emf.createEntityManager();
        Query q = em.createQuery("SELECT p FROM AnexosProyecto p WHERE p.anexosProyectoPK.folioProyecto = :folioProyecto and p.anexosProyectoPK.serialProyecto = :serialProyecto and p.anexosProyectoPK.capituloId = :capitulo and p.anexosProyectoPK.subcapituloId = :subcapitulo and p.anexosProyectoPK.seccionId = :seccion");
        q.setParameter("folioProyecto", folio);
        q.setParameter("serialProyecto", version);
        q.setParameter("capitulo", capitulo);
        q.setParameter("subcapitulo", subcapitulo);
        q.setParameter("folioProyecto", seccion);
        List l = q.getResultList();
        return l;
    }

    public Object busca(Class<?> tipo, Object id) {
        return em.find(tipo, id);
    }

    public void guardarAvance(Proyecto proyecto, Short avance) {
        em.clear();
        em.getTransaction().begin();
        AvanceProyecto ap = (AvanceProyecto) busca(AvanceProyecto.class, new AvanceProyectoPK(proyecto.getProyectoPK().getFolioProyecto(), proyecto.getProyectoPK().getSerialProyecto(), new Short("2")));
        if (ap == null) {
            ap = new AvanceProyecto(new AvanceProyectoPK(proyecto.getProyectoPK().getFolioProyecto(), proyecto.getProyectoPK().getSerialProyecto(), new Short("2")));
            ap.setAvance(avance);
            em.persist(ap);
        } else {
            if (ap.getAvance() < 25) {
                ap.setAvance(new Short("" + (avance + ap.getAvance())));
                em.merge(ap);
            }
        }
        em.getTransaction().commit();
    }
    
    public List<ProySubsectorGuias> getUrlGuiaPdf(int subsec, int claveTramite) {
		Query q = em.createQuery("SELECT c FROM ProySubsectorGuias c WHERE c.id.subsectorId=:subsec "
				                   + "and c.id.tramiteCve=:claveTramite");
		q.setParameter("subsec",(long)subsec);
		q.setParameter("claveTramite", String.valueOf(claveTramite) );
		return q.getResultList();
	}

}
