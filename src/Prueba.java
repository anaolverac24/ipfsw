
import java.util.List;
import javax.persistence.Entity;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.Query;
import javax.transaction.Transaction;
import mx.gob.semarnat.dao.PoolEntityManagers;
import mx.gob.semarnat.model.CatEspecificacion;
import mx.gob.semarnat.model.EspecificacionAnexo;
import mx.gob.semarnat.model.PreguntaProyecto;
import mx.gob.semarnat.model.Proyecto;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author mauricio
 */
public class Prueba {

    public static void main(String[] a) {
//        EntityManager em = PoolEntityManagers.getEm();
//
//        Query q = em.createQuery("SELECT e FROM CatEspecificacion e");
//        List<CatEspecificacion> l = q.getResultList();
//        
//        for (CatEspecificacion c : l) {
//            System.out.println("-----");
//            System.out.println("|" + c.getCatEspecificacionPK().getEspecificacionId()+ "|");
//            System.out.println("-");
//            System.out.println("|" + c.getCatEspecificacionPK().getEspecificacionId().replaceAll("\\n", " ").replace("\\t", "") +"|");
//        }
//        
//        
        
//        em.getTransaction().begin();
//
//        Query q = em.createQuery("SELECT p FROM Proyecto p WHERE p.proyectoPK.folioProyecto = '01'");
//        Proyecto proyecto = (Proyecto) q.getSingleResult();
//
//        Query query = em.createQuery("DELETE FROM PreguntaClima c WHERE c.preguntaClimaPK.folioProyecto = :folio AND c.preguntaClimaPK.serialProyecto = :serial");
//        query.setParameter("folio", proyecto.getProyectoPK().getFolioProyecto());
//        query.setParameter("serial", proyecto.getProyectoPK().getSerialProyecto());
//        int deletedCount = query.executeUpdate();
//        System.out.println("Clima Eliminados " + deletedCount);
//
//                    query = em.createQuery("DELETE FROM PreguntaVegetacion c WHERE c.preguntaVegetacionPK.folioProyecto = :folio AND c.preguntaVegetacionPK.serialProyecto = :serial");
//            query.setParameter("folio", proyecto.getProyectoPK().getFolioProyecto());
//            query.setParameter("serial", proyecto.getProyectoPK().getSerialProyecto());
//            deletedCount = query.executeUpdate();
//            System.out.println("Vegetacion Eliminados " + deletedCount);
//
//            query = em.createQuery("DELETE FROM PreguntaMineral c WHERE c.preguntaMineralPK.folioProyecto = :folio AND c.preguntaMineralPK.serialProyecto = :serial");
//            query.setParameter("folio", proyecto.getProyectoPK().getFolioProyecto());
//            query.setParameter("serial", proyecto.getProyectoPK().getSerialProyecto());
//            deletedCount = query.executeUpdate();
//            System.out.println("Mineral Eliminados " + deletedCount);
//
//            query = em.createQuery("DELETE FROM PreguntaObra c WHERE c.preguntaObraPK.folioProyecto = :folio AND c.preguntaObraPK.serialProyecto = :serial");
//            query.setParameter("folio", proyecto.getProyectoPK().getFolioProyecto());
//            query.setParameter("serial", proyecto.getProyectoPK().getSerialProyecto());
//            deletedCount = query.executeUpdate();
//            System.out.println("Obra Eliminados " + deletedCount);
//
//        
//        System.out.println("P " + proyecto.getProyectoPK().getFolioProyecto());
//
//        for (PreguntaProyecto p : proyecto.getPreguntaProyectoList()) {
//            p.getPreguntaObraList().clear();
//        }
//
//        proyecto.getEspecificacionProyectoList().clear();
//        proyecto.getPreguntaProyectoList().clear();
//        
//
//        
//        em.persist(proyecto);
//        
//        em.getTransaction().commit();

//            System.out.println("Folio " + proyecto.getProyectoPK().getFolioProyecto());
//            System.out.println("Serial " + proyecto.getProyectoPK().getSerialProyecto());
//
//            Query query = em.createQuery("DELETE FROM PreguntaClima c WHERE c.preguntaClimaPK.folioProyecto = :folio AND c.preguntaClimaPK.serialProyecto = :serial");
//            query.setParameter("folio", proyecto.getProyectoPK().getFolioProyecto());
//            query.setParameter("serial", proyecto.getProyectoPK().getSerialProyecto());
//            int deletedCount = query.executeUpdate();
//            System.out.println("Clima Eliminados " + deletedCount);
//
//            query = em.createQuery("DELETE FROM PreguntaVegetacion c WHERE c.preguntaVegetacionPK.folioProyecto = :folio AND c.preguntaVegetacionPK.serialProyecto = :serial");
//            query.setParameter("folio", proyecto.getProyectoPK().getFolioProyecto());
//            query.setParameter("serial", proyecto.getProyectoPK().getSerialProyecto());
//            deletedCount = query.executeUpdate();
//            System.out.println("Vegetacion Eliminados " + deletedCount);
//
//            query = em.createQuery("DELETE FROM PreguntaMineral c WHERE c.preguntaMineralPK.folioProyecto = :folio AND c.preguntaMineralPK.serialProyecto = :serial");
//            query.setParameter("folio", proyecto.getProyectoPK().getFolioProyecto());
//            query.setParameter("serial", proyecto.getProyectoPK().getSerialProyecto());
//            deletedCount = query.executeUpdate();
//            System.out.println("Mineral Eliminados " + deletedCount);
//
//            query = em.createQuery("DELETE FROM PreguntaObra c WHERE c.preguntaObraPK.folioProyecto = :folio AND c.preguntaObraPK.serialProyecto = :serial");
//            query.setParameter("folio", proyecto.getProyectoPK().getFolioProyecto());
//            query.setParameter("serial", proyecto.getProyectoPK().getSerialProyecto());
//            deletedCount = query.executeUpdate();
//            System.out.println("Obra Eliminados " + deletedCount);
//
////            em.getTransaction().commit();
////            dao.eliminarPreguntas(pv.getProyecto().getProyectoPK().getFolioProyecto(), pv.getProyecto().getProyectoPK().getSerialProyecto());
//            Query q2 = em.createQuery("delete from EspecificacionProyecto e WHERE e.especificacionProyectoPK.folioProyecto = :folio and e.especificacionProyectoPK.serialProyecto = :serial");
//            q2.setParameter("folio", pv.getProyecto().getProyectoPK().getFolioProyecto());
//            q2.setParameter("serial", pv.getProyecto().getProyectoPK().getSerialProyecto());
//            q2.executeUpdate();
//
//            Query q3 = em.createQuery("delete from PreguntaProyecto e WHERE e.preguntaProyectoPK.folioProyecto =  :folio and e.preguntaProyectoPK.serialProyecto = :serial");
//            q2.setParameter("folio", pv.getProyecto().getProyectoPK().getFolioProyecto());
//            q2.setParameter("serial", pv.getProyecto().getProyectoPK().getSerialProyecto());
//            q2.executeUpdate();
//
//            em.persist(pv.getProyecto());
//
//            em.getTransaction().commit();
//
//            em.getTransaction().begin();
//        EntityManagerFactory emf = Persistence.createEntityManagerFactory("DGIRA_MIAE");
//        EntityManager em = emf.createEntityManager();
//        
//        EspecificacionAnexo ea = new EspecificacionAnexo("01", (short)0, "5.2.7", (short)149, (short)2);
//        
//        em.getTransaction().begin();
//        em.persist(ea);
//        em.getTransaction().commit();
////
//        Query q = em.createQuery("SELECT p FROM Proyecto p WHERE p.proyectoPK.folioProyecto = '01' and p.proyectoPK.serialProyecto = 0");
//        Proyecto p = (Proyecto) q.getSingleResult();
//
//        System.out.println(p);
//        System.out.println(p.getEspecificacionProyectoList().size());
//
//        p.getEspecificacionProyectoList().clear();
//        System.out.println(p.getEspecificacionProyectoList().size());
//
//        em.getTransaction().begin();
//        em.persist(p);
//
//        Query q2 = em.createQuery("delete from EspecificacionProyecto e WHERE e.especificacionProyectoPK.folioProyecto = '01' and e.especificacionProyectoPK.serialProyecto = 0");
//        q2.executeUpdate();
//
//        em.getTransaction().commit();
//        em.close();
//        emf.close();
    }

}
